package com.hihexa.cms.request;

import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.util.JacksonUtils;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.env.Environment;

import com.hihexa.cms.model.GatewayResponse;
import com.hihexa.cms.model.wallet.GatewayModel;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.stereotype.Service;

@Service
public class RestRequest {
	private String GATEWAY_SERVER;
	private String GATEWAY_ENDPOINT_BALANCE;
	private String CREATE_WD_GW;

	public RestRequest(Environment loadPath) {
		this.GATEWAY_ENDPOINT_BALANCE = loadPath.getProperty("gateway.endpoint.balance");
		this.GATEWAY_SERVER = loadPath.getProperty("gateway.server");
		this.CREATE_WD_GW = loadPath.getProperty("gateway.endpoint.withdraw.create");
	}



	 private GatewayResponse getResponse(String responseData) throws Exception {
	        JSONObject json;
	        try {
	            json = new JSONObject(responseData);
	        } catch (Exception e) {
	            throw new Exception("API Gateway response json not define format or server not connect.");
	        }
	        return new GatewayResponse(json.getInt("error"), json.getString("message"));
	    }
	
	public GatewayResponse accessGateway(GatewayModel model) throws Exception {
		OkHttpClient client = new OkHttpClient();
		String url = String.format("%s%s", GATEWAY_SERVER, GATEWAY_ENDPOINT_BALANCE);
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, model.toString());
		Request request = new Request.Builder()
		  .url(url)
		  .put(body)
		  .addHeader("Content-Type", "application/json")
		  .build();

		Response response = client.newCall(request).execute();
		return getResponse(response.body().string());
	}

	public GatewayResponse createWithdraw(WithdrawEntity requestData) throws Exception{
        OkHttpClient client = new OkHttpClient();
        String url = String.format("%s%s", GATEWAY_SERVER, CREATE_WD_GW);
        MediaType mediaType = MediaType.parse("application/json");
        String dataJson = JacksonUtils.toJson(requestData);
        if (dataJson == null) return null;

        RequestBody body = RequestBody.create(mediaType, dataJson);
        Request request = new Request.Builder()
            .url(url)
            .post(body)
            .addHeader("Content-Type", "application/json")
            .build();

        Response response = client.newCall(request).execute();
        return getResponse(response.body().string());
    }
	
}
