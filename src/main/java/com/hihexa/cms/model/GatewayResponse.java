package com.hihexa.cms.model;

public class GatewayResponse {
	private Integer error;
	private String message;

	public Integer getError() {
		return error;
	}

	public void setError(Integer error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public GatewayResponse(Integer error, String message) {
		this.error = error;
		this.message = message;
	}

    @Override
    public String toString() {
        return "GatewayResponse{" +
            "error=" + error +
            ", message='" + message + '\'' +
            '}';
    }
}
