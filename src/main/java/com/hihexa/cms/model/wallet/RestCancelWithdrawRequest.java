package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RestCancelWithdrawRequest {
	private String userId;
	private Long withdrawId;

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getWithdrawId() {
		return withdrawId;
	}
	public void setWithdrawId(Long withdrawId) {
		this.withdrawId = withdrawId;
	}
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
}
