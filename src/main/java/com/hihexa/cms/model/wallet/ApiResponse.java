package com.hihexa.cms.model.wallet;

import java.io.Serializable;

public class ApiResponse<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1822828153179967688L;
	private final static String DefaultSuccess = "SUCCESS";
	private int errorCode;
	private String errorMessage;
	private T data;

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public ApiResponse(T data) {
		//success
		this.errorCode = 1;
		this.errorMessage = DefaultSuccess;
		this.data = data;
	}
	public ApiResponse(){
        this.errorCode = 1;
        this.errorMessage = DefaultSuccess;
    }

	
	public ApiResponse(String errorMessage) {
		this.errorCode = 0;
		this.errorMessage = errorMessage;
	}

	public ApiResponse(int errorCode, String errorMessage, T data) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.data = data;
	}

	public ApiResponse(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
}
