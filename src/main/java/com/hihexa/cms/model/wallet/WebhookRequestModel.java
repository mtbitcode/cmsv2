package com.hihexa.cms.model.wallet;

public class WebhookRequestModel {

	private String wallet_id;
	private Long num_confirmations;
	private String url;

	public String getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(String wallet_id) {
		this.wallet_id = wallet_id;
	}

	public Long getNum_confirmations() {
		return num_confirmations;
	}

	public void setNum_confirmations(Long num_confirmations) {
		this.num_confirmations = num_confirmations;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WebhookRequestModel(String wallet_id, Long num_confirmations, String url) {
		this.wallet_id = wallet_id;
		this.num_confirmations = num_confirmations;
		this.url = url;
	}

	public WebhookRequestModel() {
	}

	@Override
	public String toString() {
		return "WebhookModel [wallet_id=" + wallet_id + ", num_confirmations=" + num_confirmations + ", url=" + url
				+ "]";
	}

}
