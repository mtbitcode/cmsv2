package com.hihexa.cms.model.wallet;

public class WithdrawModel {
	private String address;
	private String amount;
	private String account_name;
	private String memo;
	private String destination_tag;
	private String source_tag;

	public String getDestination_tag() {
		return destination_tag;
	}

	public void setDestination_tag(String destination_tag) {
		this.destination_tag = destination_tag;
	}

	public String getSource_tag() {
		return source_tag;
	}

	public void setSource_tag(String source_tag) {
		this.source_tag = source_tag;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public WithdrawModel(String address, String amount) {
		this.address = address;
		this.amount = amount;
	}

	public WithdrawModel(String address, String amount, String account_name, String memo, String destination_tag,
			String source_tag) {
		this.address = address;
		this.amount = amount;
		this.account_name = account_name;
		this.memo = memo;
		this.destination_tag = destination_tag;
		this.source_tag = source_tag;
	}

	@Override
	public String toString() {
		return "WithdrawModel [address=" + address + ", amount=" + amount + ", account_name=" + account_name + ", memo="
				+ memo + ", destination_tag=" + destination_tag + ", source_tag=" + source_tag + "]";
	}

}
