package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.util.EnumConstants;


public class WithdrawCreationResponse {
	
	private Long withdrawId;
	private Long userId;
	private String reqId;
	private String address;
	private String currency;
	
	private BigDecimal amount;
	
	private BigDecimal feeValue = BigDecimal.ZERO;
	
	private BigDecimal balance;
	
	private BigDecimal reserveBalance;
	private Integer status;
	private Integer errorCode;
	private String errorMessage;
	
	public WithdrawCreationResponse() {
		this.setErrorCode(EnumConstants.ErrorDatabase.SUCCESS.getKey());
		this.setErrorMessage(EnumConstants.ErrorDatabase.SUCCESS.getValue());
	}
	public WithdrawCreationResponse(WithdrawEntity entity) {
		this();
		this.setWithdrawId(entity.getId());
		this.setUserId(entity.getUserId());
		this.setCurrency(entity.getCurrency());
		this.setAddress(entity.getAddress());
		this.setAmount(entity.getAmount());
		this.setFeeValue(entity.getFeeValue());
		this.setStatus(entity.getStatus());
	}
	
	public WithdrawCreationResponse(RestConfirmWithdrawRequest withdrawRequest, Integer status, Integer errorCode) {
		this();
		this.setWithdrawId(withdrawRequest.getWithdrawId());
		this.setUserId(withdrawRequest.getUserId());
//    	this.setCurrency(withdrawRequest.getCurrency());
//    	this.setAddress(withdrawRequest.getAddress());
//    	this.setAmount(withdrawRequest.getAmount());
//    	this.setFeeValue(withdrawRequest.getFeeValue());
    	this.setErrorCode(errorCode);
    	this.setErrorMessage(EnumConstants.ErrorDatabase.getValue(errorCode));
    	this.setStatus(status);
	}
	public WithdrawCreationResponse(WithdrawCreationRequest withdrawRequest, Integer status, Integer errorCode) {
		this();
		this.setUserId(withdrawRequest.getUserId());
    	this.setCurrency(withdrawRequest.getCurrency());
    	this.setAddress(withdrawRequest.getAddress());
    	this.setAmount(withdrawRequest.getAmount());
    	this.setFeeValue(withdrawRequest.getFeeValue());
    	this.setErrorCode(errorCode);
    	this.setErrorMessage(EnumConstants.ErrorDatabase.getValue(errorCode));
    	this.setStatus(status);
	}
	public WithdrawCreationResponse(Long withdrawId, Long userId, String reqId, String address, String currency,
			BigDecimal amount, BigDecimal feeValue, BigDecimal balance, BigDecimal reserveBalance, Integer status,
			Integer errorCode, String errorMessage) {
		this();
		this.withdrawId = withdrawId;
		this.userId = userId;
		this.reqId = reqId;
		this.address = address;
		this.currency = currency;
		this.amount = amount;
		this.feeValue = feeValue;
		this.balance = balance;
		this.reserveBalance = reserveBalance;
		this.status = status;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	public Long getWithdrawId() {
		return withdrawId;
	}
	public void setWithdrawId(Long withdrawId) {
		this.withdrawId = withdrawId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getFeeValue() {
		return feeValue;
	}
	public void setFeeValue(BigDecimal feeValue) {
		this.feeValue = feeValue;
	}
	public BigDecimal getBalance() {
		return balance;
	}
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	public BigDecimal getReserveBalance() {
		return reserveBalance;
	}
	public void setReserveBalance(BigDecimal reserveBalance) {
		this.reserveBalance = reserveBalance;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
}
