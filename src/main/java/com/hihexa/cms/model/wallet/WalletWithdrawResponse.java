package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class WalletWithdrawResponse {

	private TransferModel transfer;

	public TransferModel getTransfer() {
		return transfer;
	}

	public void setTransfer(TransferModel transfer) {
		this.transfer = transfer;
	}

	public WalletWithdrawResponse(TransferModel transfer) {
		super();
		this.transfer = transfer;
	}

	public WalletWithdrawResponse() {
		super();
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

}
