package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class DepositRequestModel {

	private String type;

	@NotBlank(message = "hash/txId may not be blank")
	private String tx_id;// hash

	@NotBlank(message = "coin/currency may not be blank")
	private String currency;// coin
	
	@NotNull
	private Long included_in_block;
	
	@NotNull
	private Long confirmations;

	@NotEmpty(message = "Address may not be empty")
	private String address;

	// @DecimalMin("0")
	@NotNull(message = "Amount may not be null")
	private BigDecimal amount;

	private Long target_confirm;
	
	private Long destination_tag;
	
	private String memo;
	
	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getDestination_tag() {
		return destination_tag;
	}

	public void setDestination_tag(Long destination_tag) {
		this.destination_tag = destination_tag;
	}

	public Long getTarget_confirm() {
		return target_confirm;
	}

	public void setTarget_confirm(Long target_confirm) {
		this.target_confirm = target_confirm;
	}

	public String getTx_id() {
		return tx_id;
	}

	public void setTx_id(String tx_id) {
		this.tx_id = tx_id;
	}

	public Long getIncluded_in_block() {
		return included_in_block;
	}

	public void setIncluded_in_block(Long included_in_block) {
		this.included_in_block = included_in_block;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Long getConfirmations() {
		return confirmations;
	}

	public void setConfirmations(Long confirmations) {
		this.confirmations = confirmations;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DepositRequestModel(String type, String tx_id, String currency, Long included_in_block, Long confirmations,
			String address, BigDecimal amount, Long target_confirm) {
		this.type = type;
		this.tx_id = tx_id;
		this.currency = currency;
		this.included_in_block = included_in_block;
		this.confirmations = confirmations;
		this.address = address;
		this.amount = amount;
		this.target_confirm = target_confirm;
	}
	
	public DepositRequestModel() {
	}

	public DepositRequestModel(String type, String tx_id, String currency, Long included_in_block, Long confirmations,
			String address, BigDecimal amount, Long target_confirm, Long destination_tag, String memo) {
		this.type = type;
		this.tx_id = tx_id;
		this.currency = currency;
		this.included_in_block = included_in_block;
		this.confirmations = confirmations;
		this.address = address;
		this.amount = amount;
		this.target_confirm = target_confirm;
		this.destination_tag = destination_tag;
		this.memo = memo;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

}
