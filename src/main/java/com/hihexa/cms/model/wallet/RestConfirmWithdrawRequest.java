package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RestConfirmWithdrawRequest {

	@NotNull
	private Long userId;

	@NotNull
	private Long withdrawId;

	@NotNull
	private Integer brokerId;

	@NotNull
	private BigDecimal amountToBTC;

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public BigDecimal getAmountToBTC() {
		return amountToBTC;
	}

	public void setAmountToBTC(BigDecimal amountToBTC) {
		this.amountToBTC = amountToBTC;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getWithdrawId() {
		return withdrawId;
	}

	public void setWithdrawId(Long withdrawId) {
		this.withdrawId = withdrawId;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
}
