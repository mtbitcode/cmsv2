package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hihexa.cms.domain.Address;

public class AddressModel {
	private String currency;
	private String address;
	private String path;
	private String extraUuid;
	private Integer currencyId;
	private Long userId;
	private Integer brokerId;

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String getExtraUuid() {
		return extraUuid;
	}

	public void setExtraUuid(String extraUuid) {
		this.extraUuid = extraUuid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

//	public AddressModel(String currency, String address, String path, String extraUuid) {
//		this.currency = currency;
//		this.address = address;
//		this.path = path;
//		this.extraUuid = extraUuid;
//	}

	public AddressModel(Address entity) {
		this.address=entity.getAddress();
		this.brokerId=entity.getBrokerId();
		this.currency=entity.getCurrency();
		this.currencyId=entity.getCurrencyId();
		this.extraUuid=entity.getExtraUuid();
		this.path=entity.getPath();
		this.userId=entity.getUserId();
	}

	public AddressModel() {
	}

	public AddressModel(String currency, String address, String path, String extraUuid, Integer currencyId, Long userId,
                        Integer brokerId) {
		this.currency = currency;
		this.address = address;
		this.path = path;
		this.extraUuid = extraUuid;
		this.currencyId = currencyId;
		this.userId = userId;
		this.brokerId = brokerId;
	}

	public AddressModel(Long userId, Integer currencyId, Integer brokerId ,String currency) {
		this.currency = currency;
		this.currencyId = currencyId;
		this.userId = userId;
		this.brokerId = brokerId;
	}

}
