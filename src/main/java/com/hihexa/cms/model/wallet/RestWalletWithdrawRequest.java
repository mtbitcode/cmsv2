package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RestWalletWithdrawRequest {
	
	private String currency;
	private String address;
	private String amount;
	private String userId;
	
	public RestWalletWithdrawRequest(String currency, String address, String amount, String userId) {
		super();
		this.currency = currency == null ? "" : currency.toUpperCase();
		this.address = address;
		this.amount = amount;
		this.userId = userId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
	public String getCurrency() {
		return currency == null ? "" : currency.toUpperCase();
	}
	public void setCurrency(String currency) {
		this.currency = currency == null ? "" : currency.toUpperCase();
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
