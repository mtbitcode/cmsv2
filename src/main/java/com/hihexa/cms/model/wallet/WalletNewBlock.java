package com.hihexa.cms.model.wallet;

public class WalletNewBlock {
	private String currency_id;
	private String url;

	public String getCurrency_id() {
		return currency_id;
	}

	public void setCurrency_id(String currency_id) {
		this.currency_id = currency_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WalletNewBlock(String currency_id, String url) {
		this.currency_id = currency_id;
		this.url = url;
	}

	public WalletNewBlock() {
		super();
	}

}
