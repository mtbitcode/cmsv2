package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class RestWalletWithdrawResponse {
	
	private Integer errorCode;
	private String errorMessage;
	private String reqid;
	private Integer state;

	public RestWalletWithdrawResponse() {
		
	}

	public RestWalletWithdrawResponse(Integer errorCode, String errorMessage, String reqid, Integer state) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.reqid = reqid;
		this.state = state;
	}
	public Integer getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getReqid() {
		return reqid;
	}
	public void setReqid(String reqid) {
		this.reqid = reqid;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}
}
