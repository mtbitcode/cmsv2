package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hihexa.cms.util.DConstants;


public class DepositConfirmModel {

	private String userId;
	private String currency;
	
	private BigDecimal amount;
	
	private BigDecimal reserveAmount;
	
	private BigDecimal depositAmount;
	private String email;
	
	public DepositConfirmModel() {
		
	}
	

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public BigDecimal getAmount() {
		if (amount  != null) {
			amount.setScale(DConstants.MAX_ROUND_NUMBER, BigDecimal.ROUND_FLOOR);
    	}
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getReserveAmount() {
		if (reserveAmount  != null) {
			reserveAmount.setScale(DConstants.MAX_ROUND_NUMBER, BigDecimal.ROUND_FLOOR);
    	}
		return reserveAmount;
	}
	public void setReserveAmount(BigDecimal reserveAmount) {
		this.reserveAmount = reserveAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
