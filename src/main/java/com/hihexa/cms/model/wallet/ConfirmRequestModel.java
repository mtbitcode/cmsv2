package com.hihexa.cms.model.wallet;

public class ConfirmRequestModel {
	private String txId;
	private Integer height;
	private Integer statusIn;
	private Long withdrawId;
	private String lstWithdrawId;
	
	public Integer getStatusIn() {
		return statusIn;
	}

	public void setStatusIn(Integer statusIn) {
		this.statusIn = statusIn;
	}

	public String getLstWithdrawId() {
		return lstWithdrawId;
	}

	public void setLstWithdrawId(String lstWithdrawId) {
		this.lstWithdrawId = lstWithdrawId;
	}

	public String getTxId() {
		return txId;
	}

	public void setTxId(String txId) {
		this.txId = txId;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Long getWithdrawId() {
		return withdrawId;
	}

	public void setWithdrawId(Long withdrawId) {
		this.withdrawId = withdrawId;
	}

	public ConfirmRequestModel(String txId, Integer height, Integer statusIn, Long withdrawId) {
		this.txId = txId;
		this.height = height;
		this.statusIn = statusIn;
		this.withdrawId = withdrawId;
	}
	
	public ConfirmRequestModel(String txId, Integer height, Integer statusIn, String lstWithdrawId) {
		this.txId = txId;
		this.height = height;
		this.statusIn = statusIn;
		this.lstWithdrawId = lstWithdrawId;
	}
	public ConfirmRequestModel(String txId, Integer height, Integer statusIn) {
		this.txId = txId;
		this.height = height;
		this.statusIn = statusIn;
	}

	public ConfirmRequestModel() {
	}

}
