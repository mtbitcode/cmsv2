package com.hihexa.cms.model.wallet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class WalletInfoResponse {

	private String currency_id;
	private String multisig;
	private String storage;
	private String hd_scope;
	private String created_at;
	private String updated_at;
	private String id;

	public String getCurrency_id() {
		return currency_id;
	}

	public void setCurrency_id(String currency_id) {
		this.currency_id = currency_id;
	}

	public String getMultisig() {
		return multisig;
	}

	public void setMultisig(String multisig) {
		this.multisig = multisig;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getHd_scope() {
		return hd_scope;
	}

	public void setHd_scope(String hd_scope) {
		this.hd_scope = hd_scope;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public WalletInfoResponse(String currency_id, String multisig, String storage, String hd_scope,
			String created_at, String updated_at, String id) {
		this.currency_id = currency_id;
		this.multisig = multisig;
		this.storage = storage;
		this.hd_scope = hd_scope;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this.id = id;
	}
	
	public WalletInfoResponse() {
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}	

}
