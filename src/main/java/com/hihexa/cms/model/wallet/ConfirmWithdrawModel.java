package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;


public class ConfirmWithdrawModel {
	private String address;
	private String amount;
	private String currency;
	private Long userId;
	private Long withdrawId;
	private String extraUuid;
	private String destinationExtraUuid;
	private BigDecimal fee;
	private BigDecimal amountValue;
	private Integer brokerId;
	private Integer currencyId;

	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public BigDecimal getAmountValue() {
		return amountValue;
	}

	public void setAmountValue(BigDecimal amountValue) {
		this.amountValue = amountValue;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getExtraUuid() {
		return extraUuid;
	}

	public void setExtraUuid(String extraUuid) {
		this.extraUuid = extraUuid;
	}

	public String getDestinationExtraUuid() {
		return destinationExtraUuid;
	}

	public void setDestinationExtraUuid(String destinationExtraUuid) {
		this.destinationExtraUuid = destinationExtraUuid;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getWithdrawId() {
		return withdrawId;
	}

	public void setWithdrawId(Long withdrawId) {
		this.withdrawId = withdrawId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public ConfirmWithdrawModel(String address,
			BigDecimal amount,
			String currency,
			Long userId,
			Long withdrawId,
			BigDecimal feeValue,
			String extraUuid,
			String destinationExtraUuid,
                                Integer currencyId ,
                                Integer brokerId) {
		this.address = address;
		this.amountValue = amount;
		this.currency = currency;
		this.userId = userId;
		this.withdrawId = withdrawId;
		this.extraUuid = extraUuid;
		this.destinationExtraUuid = destinationExtraUuid;
		this.fee = feeValue;
		this.currencyId = currencyId;
		this.brokerId = brokerId;
	}
/*
	public ConfirmWithdrawModel(String address, BigDecimal amount, String currency, String userId, Long withdrawId,
			BigDecimal feeValue, String extraUuid, String destinationExtraUuid) {
		this.address = address;
		this.currency = currency;
		this.userId = userId;
		this.withdrawId = withdrawId;
		BigDecimal fee = feeValue == null ? BigDecimal.ZERO : feeValue;
		BigDecimal amountReal = amount.subtract(fee);
		BigInteger amoutWallet = BigInteger.ZERO;
		// x10^8 before call wallet btc ,ltc , usdt
		// x10^18 before call wallet eth , etc
		// x10^6 before call wallet usdc , XRP ,XTZ
		// x10^0 before call wallet eos
		if (DConstants.ACCOUNT_BAT.BTC.equalsIgnoreCase(currency)
				|| DConstants.ACCOUNT_BAT.LTC.equalsIgnoreCase(currency)
				|| DConstants.ACCOUNT_BASE.USDT.equalsIgnoreCase(currency)) {

			amoutWallet = amountReal.multiply(BigDecimal.valueOf(100000000)).toBigInteger();
		} else if (DConstants.ACCOUNT_BASE.ETH.equalsIgnoreCase(currency) || DConstants.ACCOUNT_BASE.ETC.equalsIgnoreCase(currency)) {

			amoutWallet = amountReal.multiply(BigDecimal.valueOf(1000000000000000000d)).toBigInteger();
		} else if (DConstants.ACCOUNT_BASE.EOS.equalsIgnoreCase(currency)) {

			amountReal = amountReal.setScale(DConstants.CURRENCY_UNIT.EOS, BigDecimal.ROUND_DOWN);
		}else {
			amoutWallet = amountReal.multiply(BigDecimal.valueOf(1000000)).toBigInteger();
		}
		this.amount = DConstants.ACCOUNT_BASE.EOS.equalsIgnoreCase(currency) ? amountReal.toString()
				: amoutWallet.toString();
		this.extraUuid = extraUuid;
		this.destinationExtraUuid = destinationExtraUuid;
	}
*/
	public ConfirmWithdrawModel() {
	}

	@Override
	public String toString() {
		return "ConfirmWithdrawModel [address=" + address + ", amount=" + amount + ", currency=" + currency
				+ ", userId=" + userId + ", withdrawId=" + withdrawId + ", extraUuid=" + extraUuid
				+ ", destinationExtraUuid=" + destinationExtraUuid + ", fee=" + fee + ", amountValue=" + amountValue
				+ ", brokerId=" + brokerId + ", currencyId=" + currencyId + "]";
	}


}
