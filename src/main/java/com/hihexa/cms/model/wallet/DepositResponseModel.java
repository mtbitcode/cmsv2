package com.hihexa.cms.model.wallet;

import java.math.BigDecimal;

public class DepositResponseModel {
	private Integer responseStatus;
	private Long userId;
	private String email;
	private String currency;
	private BigDecimal amount;

	public Integer getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public DepositResponseModel(Integer responseStatus, Long userId, String email, String currency, BigDecimal amount) {
		this.responseStatus = responseStatus;
		this.userId = userId;
		this.email = email;
		this.currency = currency;
		this.amount = amount;
	}

	public DepositResponseModel(Integer responseStatus, Long userId, String email, String currency) {
		this.responseStatus = responseStatus;
		this.userId = userId;
		this.email = email;
		this.currency = currency;
	}

	public DepositResponseModel() {
	}

}
