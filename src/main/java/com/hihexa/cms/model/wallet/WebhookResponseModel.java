package com.hihexa.cms.model.wallet;

public class WebhookResponseModel {
	private String wallet_id;
	private String num_confirmations;
	private String url;
	private String created_at;
	private String updated_at;
	private String _id;
	private String status;

	public String getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(String wallet_id) {
		this.wallet_id = wallet_id;
	}

	public String getNum_confirmations() {
		return num_confirmations;
	}

	public void setNum_confirmations(String num_confirmations) {
		this.num_confirmations = num_confirmations;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public WebhookResponseModel() {
	}

	public WebhookResponseModel(String wallet_id, String num_confirmations, String url, String created_at,
			String updated_at, String _id, String status) {
		this.wallet_id = wallet_id;
		this.num_confirmations = num_confirmations;
		this.url = url;
		this.created_at = created_at;
		this.updated_at = updated_at;
		this._id = _id;
		this.status = status;
	}

	
}
