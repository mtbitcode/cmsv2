package com.hihexa.cms.model.wallet;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class WalletWithdrawRequest {
	private String wallet_id;
	private String token_name;
	List<WithdrawModel> recipients;

	public String getToken_name() {
		return token_name;
	}

	public void setToken_name(String token_name) {
		this.token_name = token_name;
	}

	public String getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(String wallet_id) {
		this.wallet_id = wallet_id;
	}

	public List<WithdrawModel> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<WithdrawModel> recipients) {
		this.recipients = recipients;
	}

	public WalletWithdrawRequest(String wallet_id, String token_name, List<WithdrawModel> recipients) {
		this.wallet_id = wallet_id;
		this.token_name = token_name;
		this.recipients = recipients;
	}
	
	public WalletWithdrawRequest(String wallet_id, List<WithdrawModel> recipients) {
		this.wallet_id = wallet_id;
		this.recipients = recipients;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

}
