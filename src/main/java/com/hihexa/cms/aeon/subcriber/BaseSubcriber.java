package com.hihexa.cms.aeon.subcriber;
import io.aeron.Aeron;
import io.aeron.Aeron.Context;
import org.agrona.concurrent.BusySpinIdleStrategy;
import org.agrona.concurrent.IdleStrategy;

import java.util.concurrent.atomic.AtomicBoolean;

public class BaseSubcriber {
    public static final AtomicBoolean BASE_RUNNING = new AtomicBoolean(true);

    public static final IdleStrategy BASE_IDLESTRATEGY = new BusySpinIdleStrategy();

    public static final Aeron BASE_AERON = Aeron.connect(new Context());

    public void stopListening() {BASE_RUNNING.set(false);}
}
