package com.hihexa.cms.aeon.model;

public class BaseEvent {
    public byte eventType;
    public long orderId;
    public long clientOrderId;
    public long timestamp;

    public int symbol;
    public long price;
    public long amount;
    public long stopPrice;
    public int orderSide;
    public byte orderType;

    public byte tradeType;
    public byte tradeOption;

    public long userId;

    @Override
    public String toString() {
        return "BaseEvent{" +
            "eventType=" + eventType +
            ", orderId=" + orderId +
            ", clientOrderId=" + clientOrderId +
            ", timestamp=" + timestamp +
            ", symbol=" + symbol +
            ", price=" + price +
            ", amount=" + amount +
            ", stopPrice=" + stopPrice +
            ", orderSide=" + orderSide +
            ", orderType=" + orderType +
            ", tradeType=" + tradeType +
            ", tradeOption=" + tradeOption +
            ", userId=" + userId +
            '}';
    }
}
