package com.hihexa.cms.aeon.model;

public class BalanceModel {
    public long userId;
    public int currencyId;
    public long amount;
    public long reserveAmount;
    public BalanceModel() {

    }

    public BalanceModel(long userId, int currencyId, long amount, long reserveAmount) {
        super();
        this.userId = userId;
        this.currencyId = currencyId;
        this.amount = amount;
        this.reserveAmount = reserveAmount;
    }

}
