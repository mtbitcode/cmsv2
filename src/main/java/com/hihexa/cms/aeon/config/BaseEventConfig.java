package com.hihexa.cms.aeon.config;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:application.properties")
public class BaseEventConfig implements EnvironmentAware {
    public static final String AERON_MODE_MDC = "MDC";
    public static final String AERON_MODE_MULTICAST = "MULTICAST";
    public static final String AERON_CHANNEL_START = "aeron:udp?";
    public static final String AERON_CHANNEL_ENPOINT = "endpoint=";
    public static final String AERON_CHANNEL_CONTROL = "control=";
    public static final String AERON_CHANNEL_CONTROL_MODE = "control-mode=";
    public static final String AERON_CHANNEL_INTERFACE = "interface=";

    static Environment environment;


    @Override
    public void setEnvironment(Environment environment) {
        BaseEventConfig.environment = environment;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }


    public static int getFragmentLimitSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.fragmentLimit"));
//        return 256;
    }

    public static String getChannelEnpointIpPub() {
        return environment.getProperty("server.stream.aeron.pub.channel.enpoint.ip");
//        return "192.168.2.19";
    }

    public static String getChannelEnpointPortPub() {
//        return environment.getProperty("server.stream.aeron.pub.channel.enpoint.port");
        return "40123";
    }


    public static String getChannelPub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getChannelEnpointIpPub());
        strBuffer.append(":");
        strBuffer.append(getChannelEnpointPortPub());
        return strBuffer.toString();
    }

    public static int getStreamIdPub() {
//        return Integer.parseInt(environment.getProperty("server.stream.aeron.pub.streamid"));
        return 10;
    }

    public static int getMessageLengthPub() {
//        return Integer.parseInt(environment.getProperty("server.stream.aeron.pub.message.limit"));
        return 10000;
    }

    public static String getServerModeSub() {
//        return environment.getProperty("server.stream.aeron.sub.mode");
        return "MDC";
    }

    public static String getMDCControlIpSub() {
//        return environment.getProperty("server.stream.aeron.sub.mdc.control.ip");
        return "192.168.2.19";
    }

    public static String getMDCEnpointIpSub() {
//        return environment.getProperty("server.stream.aeron.sub.mdc.endpoint.ip");
        return "192.168.2.169";
    }

    public static String getMDCControlMode() {
//        return environment.getProperty("server.stream.aeron.sub.mdc.control.mode");
        return "dynamic";
    }

    public static String getMDCChannelBalanceModelEnpointPort() {
        return environment.getProperty("server.stream.aeron.sub.mdc.balance.model.channel.endpoint.port");
    }

    public static String getMDCChannelControlBalanceModelPortSub() {
        return environment.getProperty("server.stream.aeron.sub.mdc.balance.model.channel.control.port");
    }

    public static String getMDCChannelBalanceModelSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.setLength(0);
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMDCEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelBalanceModelEnpointPort());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMDCControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelControlBalanceModelPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMDCControlMode());
        return strBuffer.toString();
    }

    public static int getMDCStreamIdBalanceModelSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.mdc.balance.model.streamid"));
    }

    public static String getMDCChannelObjectImageEnpointPort() {
        return environment.getProperty("server.stream.aeron.sub.mdc.object.image.channel.endpoint.port");
    }

    public static String getMDCChannelControlObjectImagePortSub() {
        return environment.getProperty("server.stream.aeron.sub.mdc.object.image.channel.control.port");
    }

    public static String getMDCChannelObjectImageSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMDCEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelObjectImageEnpointPort());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMDCControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelControlObjectImagePortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMDCControlMode());
        return strBuffer.toString();
    }

    public static int getMDCStreamIdObjectImageSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.mdc.object.image.streamid"));
    }

    public static String getMDCChannelMarketDataEventEnpointPort() {
        return environment.getProperty("server.stream.aeron.sub.mdc.market.data.event.channel.endpoint.port");
    }

    public static String getMDCChannelControlMarketDataEventPortSub() {
        return environment.getProperty("server.stream.aeron.sub.mdc.market.data.event.channel.control.port");
    }

    public static String getMDCChannelMarketDataEventSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMDCEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelMarketDataEventEnpointPort());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMDCControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelControlMarketDataEventPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMDCControlMode());
        return strBuffer.toString();
    }

    public static int getMDCStreamIdMarketDataEventSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.mdc.market.data.event.streamid"));
    }

    public static String getMDCChannelOrderHistoryModelEnpointPort() {
//        return environment.getProperty("server.stream.aeron.sub.mdc.order.history.model.channel.endpoint.port");
        return "40731";
    }

    public static String getMDCChannelControlOrderHistoryModelPortSub() {
//        return environment.getProperty("server.stream.aeron.sub.mdc.order.history.model.channel.control.port");
        return "40261";
    }

    public static String getMDCChannelOrderHistoryModelSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMDCEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelOrderHistoryModelEnpointPort());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMDCControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMDCChannelControlOrderHistoryModelPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMDCControlMode());
        return strBuffer.toString();
    }


    public static int getMDCStreamIdOrderHistoryModelSub() {
//        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.mdc.order.history.model.streamid"));
        return 14;
    }

    public static String getMULTICASTEnpointIpSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.enpoint.ip");
    }

    public static String getMULTICASTEnpointPortSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.enpoint.port");
    }

    public static String getMULTICASTInterfaceSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.interface");
    }

    public static String getMULTICASTControlIpSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.control.ip");
    }

    public static String getMULTICASTControlModeSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.control.mode");
    }

    public static String getMULTICASTChannelBalanceModelPortSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.balance.model.channel.port");
    }


    public static String getMULTICASTChannelBalanceModelSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMULTICASTEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTEnpointPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_INTERFACE);
        strBuffer.append(getMULTICASTInterfaceSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMULTICASTControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTChannelBalanceModelPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMULTICASTControlModeSub());
        return strBuffer.toString();
    }

    public static int getMULTICASTStreamIdBalanceModelSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.multi.balance.model.streamid"));
    }

    public static String getMULTICASTChannelObjectImagePortSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.object.image.channel.port");
    }


    public static String getMULTICASTChannelObjectImageSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMULTICASTEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTEnpointPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_INTERFACE);
        strBuffer.append(getMULTICASTInterfaceSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMULTICASTControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTChannelObjectImagePortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMULTICASTControlModeSub());
        return strBuffer.toString();
    }

    public static int getMULTICASTStreamIdObjectImageSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.multi.object.image.streamid"));
    }

    public static String getMULTICASTChannelMarketDataEventPortSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.market.data.event.channel.port");
    }


    public static String getMULTICASTChannelMarketDataEventSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMULTICASTEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTEnpointPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_INTERFACE);
        strBuffer.append(getMULTICASTInterfaceSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMULTICASTControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTChannelMarketDataEventPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMULTICASTControlModeSub());
        return strBuffer.toString();
    }

    public static int getMULTICASTStreamIdMarketDataEventSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.multi.market.data.event.streamid"));
    }


    public static String getMULTICASTChannelOrderHistoryModelPortSub() {
        return environment.getProperty("server.stream.aeron.sub.multi.order.history.model.channel.port");
    }


    public static String getMULTICASTChannelOrderHistoryModelSub() {
        StringBuffer strBuffer = new StringBuffer();
        strBuffer.append(AERON_CHANNEL_START);
        strBuffer.append(AERON_CHANNEL_ENPOINT);
        strBuffer.append(getMULTICASTEnpointIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTEnpointPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_INTERFACE);
        strBuffer.append(getMULTICASTInterfaceSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL);
        strBuffer.append(getMULTICASTControlIpSub());
        strBuffer.append(":");
        strBuffer.append(getMULTICASTChannelOrderHistoryModelPortSub());
        strBuffer.append("|");
        strBuffer.append(AERON_CHANNEL_CONTROL_MODE);
        strBuffer.append(getMULTICASTControlModeSub());
        return strBuffer.toString();
    }

    public static int getMULTICASTStreamIdOrderHistoryModelSub() {
        return Integer.parseInt(environment.getProperty("server.stream.aeron.sub.multi.order.history.model.streamid"));
    }
}
