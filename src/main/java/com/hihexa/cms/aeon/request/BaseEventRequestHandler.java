package com.hihexa.cms.aeon.request;

import com.hihexa.cms.aeon.model.BaseEvent;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class BaseEventRequestHandler implements RequestHandler<BaseEvent>{
    private BaseEventRequestPublisher baseEventRequestPublisher;

    @PostConstruct
    private void setting(){
        this.baseEventRequestPublisher = new BaseEventRequestPublisher();
        this.baseEventRequestPublisher.loadConfig();
        this.baseEventRequestPublisher.retryConnectionPublisher();
    }
    @Override
    public void handlerEvent(BaseEvent event) throws Exception {
        this.baseEventRequestPublisher.addBaseEventPublisher(event);
    }
}
