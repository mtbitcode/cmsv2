package com.hihexa.cms.aeon.request;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.agrona.BitUtil;
import org.agrona.BufferUtil;
import org.agrona.concurrent.BusySpinIdleStrategy;
import org.agrona.concurrent.IdleStrategy;
import org.agrona.concurrent.UnsafeBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hihexa.cms.aeon.config.BaseEventConfig;
import com.hihexa.cms.aeon.model.BaseEvent;
import com.hihexa.cms.aeon.sbe.BaseEventEnDeCoder;

import io.aeron.Aeron;
import io.aeron.Aeron.Context;
import io.aeron.Publication;
@Service
public class BaseEventRequestPublisher {

    private static final Logger log = LoggerFactory.getLogger(BaseEventRequestPublisher.class);
    private static final int RETRY_TIMES = 20;

    private Aeron BASE_EVENT_AERON;
    private Publication BASE_EVENT_PUBLICATION;
    private UnsafeBuffer BASE_EVENT_OFFER_BUFFER;

    private static BaseEventEnDeCoder BASE_EVENT_ENDE_CODER ;
    private String CHANNEL_PUBLISHER;
    private int STREAM_ID_PUBLISHER;
    private int MESSAGE_LENGTH_PUBLISHER;
    private IdleStrategy strategy = new BusySpinIdleStrategy();

    @PostConstruct
    public void loadConfig(){
        CHANNEL_PUBLISHER = BaseEventConfig.getChannelPub();
        STREAM_ID_PUBLISHER = BaseEventConfig.getStreamIdPub();
        MESSAGE_LENGTH_PUBLISHER = BaseEventConfig.getMessageLengthPub();
        BASE_EVENT_ENDE_CODER =  new BaseEventEnDeCoder();
        this.retryConnectionPublisher();
    }

    public void retryConnectionPublisher(){
        BASE_EVENT_AERON = Aeron.connect(new Context().keepAliveIntervalNs(TimeUnit.MILLISECONDS.toNanos(4000)));
        BASE_EVENT_PUBLICATION = BASE_EVENT_AERON.addPublication(CHANNEL_PUBLISHER, STREAM_ID_PUBLISHER);
        BASE_EVENT_OFFER_BUFFER = new UnsafeBuffer(BufferUtil.allocateDirectAligned(MESSAGE_LENGTH_PUBLISHER, BitUtil.CACHE_LINE_LENGTH));
    }

    public void addBaseEventPublisher(BaseEvent baseEvent){
        final byte[] messageBytes = BASE_EVENT_ENDE_CODER.encodeByte(baseEvent);
        final int length = messageBytes.length;
        BASE_EVENT_OFFER_BUFFER.putBytes(0, messageBytes);
        log.info("baseEvent " + baseEvent.toString());
        int counter = 0;
        while (counter++ < RETRY_TIMES){
            long result = BASE_EVENT_PUBLICATION.offer(BASE_EVENT_OFFER_BUFFER, 0, length);
            log.info("Offer result: {}", result);
            if(result < 0){
                if (result == Publication.NOT_CONNECTED || result == Publication.CLOSED){
                    this.retryConnectionPublisher();
                }
            }else {
                break;
            }
            strategy.idle();
        }
    }
}
