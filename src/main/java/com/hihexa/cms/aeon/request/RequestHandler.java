package com.hihexa.cms.aeon.request;

public interface RequestHandler<T> {
    public void handlerEvent(T event) throws Exception;
}
