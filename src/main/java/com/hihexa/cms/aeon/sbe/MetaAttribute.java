package com.hihexa.cms.aeon.sbe;

public enum MetaAttribute {
    EPOCH,
    TIME_UNIT,
    SEMANTIC_TYPE,
    PRESENCE
}
