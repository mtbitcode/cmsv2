package com.hihexa.cms.aeon.balanceService;

import com.hihexa.cms.aeon.model.BaseEvent;
import com.hihexa.cms.aeon.model.BaseEventType;
import com.hihexa.cms.aeon.request.BaseEventRequestHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CoreBalanceService {
    private static final Logger LOG = LogManager.getLogger(CoreBalanceService.class);
    @Autowired
    public BaseEventRequestHandler BASE_EVENT_REQUEST_HANDLER;
    public void updateBalance(long userId, int currencyId, long amount, int orderSide, long wdId){
        try {
            BaseEvent baseEvent = new BaseEvent();
            baseEvent.eventType = BaseEventType.CHANGE_BALANCE;
            baseEvent.userId = userId;
            baseEvent.symbol = currencyId;
            baseEvent.amount = amount;
            baseEvent.clientOrderId = wdId;
            baseEvent.orderSide = orderSide;
            BASE_EVENT_REQUEST_HANDLER.handlerEvent(baseEvent);
        } catch (Exception e){
            LOG.error("send msg balance to core exception" , e);
        }

    }
}
