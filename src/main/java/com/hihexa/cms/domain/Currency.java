package com.hihexa.cms.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A Currency.
 */
@Entity
@Table(name = "currency")
public class Currency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "target_confirms")
    private Long targetConfirms;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "is_demo")
    private Integer isDemo;

    @Column(name = "max_size")
    private Integer maxSize;

    @Column(name = "min_size")
    private Integer minSize;

    @Column(name = "max_time")
    private Integer maxTime;

    @Column(name = "init_amount_demo", precision = 21, scale = 2)
    private BigDecimal initAmountDemo;

    @Column(name = "decimal_amount")
    private Integer decimalAmount;

    @Column(name = "base_unit")
    private Integer baseUnit;

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Currency name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public Currency code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getTargetConfirms() {
        return targetConfirms;
    }

    public Currency targetConfirms(Long targetConfirms) {
        this.targetConfirms = targetConfirms;
        return this;
    }

    public void setTargetConfirms(Long targetConfirms) {
        this.targetConfirms = targetConfirms;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public Currency activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public Timestamp getCreated() {
        return created;
    }

    public Currency created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public Currency updated(Timestamp updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Integer getIsDemo() {
        return isDemo;
    }

    public Currency isDemo(Integer isDemo) {
        this.isDemo = isDemo;
        return this;
    }

    public void setIsDemo(Integer isDemo) {
        this.isDemo = isDemo;
    }

    public Integer getMaxSize() {
        return maxSize;
    }

    public Currency maxSize(Integer maxSize) {
        this.maxSize = maxSize;
        return this;
    }

    public void setMaxSize(Integer maxSize) {
        this.maxSize = maxSize;
    }

    public Integer getMinSize() {
        return minSize;
    }

    public Currency minSize(Integer minSize) {
        this.minSize = minSize;
        return this;
    }

    public void setMinSize(Integer minSize) {
        this.minSize = minSize;
    }

    public Integer getMaxTime() {
        return maxTime;
    }

    public Currency maxTime(Integer maxTime) {
        this.maxTime = maxTime;
        return this;
    }

    public void setMaxTime(Integer maxTime) {
        this.maxTime = maxTime;
    }

    public BigDecimal getInitAmountDemo() {
        return initAmountDemo;
    }

    public Currency initAmountDemo(BigDecimal initAmountDemo) {
        this.initAmountDemo = initAmountDemo;
        return this;
    }

    public void setInitAmountDemo(BigDecimal initAmountDemo) {
        this.initAmountDemo = initAmountDemo;
    }

    public Integer getDecimalAmount() {
        return decimalAmount;
    }

    public Currency decimalAmount(Integer decimalAmount) {
        this.decimalAmount = decimalAmount;
        return this;
    }

    public void setDecimalAmount(Integer decimalAmount) {
        this.decimalAmount = decimalAmount;
    }

    public Integer getBaseUnit() {
        return baseUnit;
    }

    public Currency baseUnit(Integer baseUnit) {
        this.baseUnit = baseUnit;
        return this;
    }

    public void setBaseUnit(Integer baseUnit) {
        this.baseUnit = baseUnit;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Currency)) {
            return false;
        }
        return id != null && id.equals(((Currency) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Currency{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", targetConfirms=" + getTargetConfirms() +
            ", activeFlg=" + getActiveFlg() +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", isDemo=" + getIsDemo() +
            ", maxSize=" + getMaxSize() +
            ", minSize=" + getMinSize() +
            ", maxTime=" + getMaxTime() +
            ", initAmountDemo=" + getInitAmountDemo() +
            ", decimalAmount=" + getDecimalAmount() +
            ", baseUnit=" + getBaseUnit() +
            "}";
    }
}
