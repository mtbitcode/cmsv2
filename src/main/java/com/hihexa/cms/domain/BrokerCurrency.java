package com.hihexa.cms.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * A BrokerCurrency.
 */
@Entity
@Table(name = "broker_currency")
public class BrokerCurrency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "broker_id")
    @NotNull
    private Integer brokerId;

    @Column(name = "currency_id")
    @NotNull
    private Integer currencyId;

    @Column(name = "enable_withdraw")
    private Integer enableWithdraw;

    @Column(name = "enable_deposit")
    private Integer enableDeposit;

    @Column(name = "min_deposit")
    private Long minDeposit;

    @Column(name = "min_withdraw")
    private Long minWithdraw;

    @Column(name = "fee_withdraw")
    private Long feeWithdraw;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "max_allow_withdraw", precision = 21, scale = 2)
    private BigDecimal maxAllowWithdraw;

    @Column(name = "wallet_id")
    private String walletId;

    @Transient
	private String accountName;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBrokerId() {
        return brokerId;
    }

    public BrokerCurrency brokerId(Integer brokerId) {
        this.brokerId = brokerId;
        return this;
    }

    public void setBrokerId(Integer brokerId) {
        this.brokerId = brokerId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public BrokerCurrency currencyId(Integer currencyId) {
        this.currencyId = currencyId;
        return this;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getEnableWithdraw() {
        return enableWithdraw;
    }

    public BrokerCurrency enableWithdraw(Integer enableWithdraw) {
        this.enableWithdraw = enableWithdraw;
        return this;
    }

    public void setEnableWithdraw(Integer enableWithdraw) {
        this.enableWithdraw = enableWithdraw;
    }

    public Integer getEnableDeposit() {
        return enableDeposit;
    }

    public BrokerCurrency enableDeposit(Integer enableDeposit) {
        this.enableDeposit = enableDeposit;
        return this;
    }

    public void setEnableDeposit(Integer enableDeposit) {
        this.enableDeposit = enableDeposit;
    }

    public Long getMinDeposit() {
        return minDeposit;
    }

    public BrokerCurrency minDeposit(Long minDeposit) {
        this.minDeposit = minDeposit;
        return this;
    }

    public void setMinDeposit(Long minDeposit) {
        this.minDeposit = minDeposit;
    }

    public Long getMinWithdraw() {
        return minWithdraw;
    }

    public BrokerCurrency minWithdraw(Long minWithdraw) {
        this.minWithdraw = minWithdraw;
        return this;
    }

    public void setMinWithdraw(Long minWithdraw) {
        this.minWithdraw = minWithdraw;
    }

    public Long getFeeWithdraw() {
        return feeWithdraw;
    }

    public BrokerCurrency feeWithdraw(Long feeWithdraw) {
        this.feeWithdraw = feeWithdraw;
        return this;
    }

    public void setFeeWithdraw(Long feeWithdraw) {
        this.feeWithdraw = feeWithdraw;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public BrokerCurrency activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public Timestamp getCreated() {
        return created;
    }

    public BrokerCurrency created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public BrokerCurrency updated(Timestamp updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public BigDecimal getMaxAllowWithdraw() {
        return maxAllowWithdraw;
    }

    public BrokerCurrency maxAllowWithdraw(BigDecimal maxAllowWithdraw) {
        this.maxAllowWithdraw = maxAllowWithdraw;
        return this;
    }

    public void setMaxAllowWithdraw(BigDecimal maxAllowWithdraw) {
        this.maxAllowWithdraw = maxAllowWithdraw;
    }

    public String getWalletId() {
        return walletId;
    }

    public BrokerCurrency walletId(String walletId) {
        this.walletId = walletId;
        return this;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BrokerCurrency)) {
            return false;
        }
        return id != null && id.equals(((BrokerCurrency) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BrokerCurrency{" +
            "id=" + getId() +
            ", brokerId=" + getBrokerId() +
            ", currencyId=" + getCurrencyId() +
            ", enableWithdraw=" + getEnableWithdraw() +
            ", enableDeposit=" + getEnableDeposit() +
            ", minDeposit=" + getMinDeposit() +
            ", minWithdraw=" + getMinWithdraw() +
            ", feeWithdraw=" + getFeeWithdraw() +
            ", activeFlg=" + getActiveFlg() +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", maxAllowWithdraw=" + getMaxAllowWithdraw() +
            ", walletId='" + getWalletId() + "'" +
            "}";
    }
}
