package com.hihexa.cms.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "extra_address")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtraAddressEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1976979461587520276L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "address")
	private String address;

	@Column(name = "currency")
	private String currency;

	@Column(name = "created")
	private Timestamp created;

	@Column(name = "updated")
	private Timestamp updated;

	@Column(name = "status")
	private Integer status;

	@Column(name = "path")
	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Timestamp getUpdated() {
		return updated;
	}

	public void setUpdated(Timestamp updated) {
		this.updated = updated;
	}

	public ExtraAddressEntity(Long id, String address, String currency, Integer status, Timestamp created,
			Timestamp updated) {
		this.id = id;
		this.address = address;
		this.currency = currency;
		this.created = created;
		this.status = status;
		this.updated = updated;
	}

	public ExtraAddressEntity(String address, String currency, Integer status, String path, Timestamp created,
			Timestamp updated) {
		this.address = address;
		this.currency = currency;
		this.created = created;
		this.status = status;
		this.updated = updated;
		this.path = path;
	}

	public ExtraAddressEntity() {
	}

}
