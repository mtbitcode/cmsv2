package com.hihexa.cms.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A BrokerSymbolConfig.
 */
@Entity
@Table(name = "broker_symbol_config")
public class BrokerSymbolConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "broker_id")
    private Long brokerId;

    @Column(name = "symbol_id")
    private Long symbolId;

    @Column(name = "tradable")
    private Integer tradable;

    @Column(name = "min_trade")
    private Long minTrade;

    @Column(name = "margin_enable")
    private Integer marginEnable;

    @Column(name = "max_trade")
    private Long maxTrade;

    @Column(name = "max_price")
    private Long maxPrice;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "min_price")
    private Long minPrice;

    @Column(name = "active_flg")
    private Integer activeFlg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrokerId() {
        return brokerId;
    }

    public BrokerSymbolConfig brokerId(Long brokerId) {
        this.brokerId = brokerId;
        return this;
    }

    public void setBrokerId(Long brokerId) {
        this.brokerId = brokerId;
    }

    public Long getSymbolId() {
        return symbolId;
    }

    public BrokerSymbolConfig symbolId(Long symbolId) {
        this.symbolId = symbolId;
        return this;
    }

    public void setSymbolId(Long symbolId) {
        this.symbolId = symbolId;
    }

    public Integer getTradable() {
        return tradable;
    }

    public BrokerSymbolConfig tradable(Integer tradable) {
        this.tradable = tradable;
        return this;
    }

    public void setTradable(Integer tradable) {
        this.tradable = tradable;
    }

    public Long getMinTrade() {
        return minTrade;
    }

    public BrokerSymbolConfig minTrade(Long minTrade) {
        this.minTrade = minTrade;
        return this;
    }

    public void setMinTrade(Long minTrade) {
        this.minTrade = minTrade;
    }

    public Integer getMarginEnable() {
        return marginEnable;
    }

    public BrokerSymbolConfig marginEnable(Integer marginEnable) {
        this.marginEnable = marginEnable;
        return this;
    }

    public void setMarginEnable(Integer marginEnable) {
        this.marginEnable = marginEnable;
    }

    public Long getMaxTrade() {
        return maxTrade;
    }

    public BrokerSymbolConfig maxTrade(Long maxTrade) {
        this.maxTrade = maxTrade;
        return this;
    }

    public void setMaxTrade(Long maxTrade) {
        this.maxTrade = maxTrade;
    }

    public Long getMaxPrice() {
        return maxPrice;
    }

    public BrokerSymbolConfig maxPrice(Long maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public void setMaxPrice(Long maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Timestamp getCreated() {
        return created;
    }

    public BrokerSymbolConfig created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public BrokerSymbolConfig updated(Timestamp updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Long getMinPrice() {
        return minPrice;
    }

    public BrokerSymbolConfig minPrice(Long minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public void setMinPrice(Long minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public BrokerSymbolConfig activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BrokerSymbolConfig)) {
            return false;
        }
        return id != null && id.equals(((BrokerSymbolConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BrokerSymbolConfig{" +
            "id=" + getId() +
            ", brokerId=" + getBrokerId() +
            ", symbolId=" + getSymbolId() +
            ", tradable=" + getTradable() +
            ", minTrade=" + getMinTrade() +
            ", marginEnable=" + getMarginEnable() +
            ", maxTrade=" + getMaxTrade() +
            ", maxPrice=" + getMaxPrice() +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", minPrice=" + getMinPrice() +
            ", activeFlg=" + getActiveFlg() +
            "}";
    }
}
