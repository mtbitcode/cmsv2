package com.hihexa.cms.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A UserSetting.
 */
@Entity
@Table(name = "user_setting")
public class UserSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login_alert")
    private Integer loginAlert;

    @Column(name = "keep_alive")
    private Integer keepAlive;

    @Column(name = "detect_ip_change")
    private Integer detectIpChange;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "dob")
    private Timestamp dob;

    @Column(name = "avator")
    private String avator;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "sfa")
    private Integer sfa;

    @Column(name = "fa_seed")
    private String faSeed;

    @Column(name = "ref_id")
    private String refId;

    @Column(name = "verify_level")
    private Integer verifyLevel;

    @Column(name = "wd_daily_limit", precision = 21, scale = 2)
    private BigDecimal wdDailyLimit;

    @Column(name = "daily_wd_amount", precision = 21, scale = 2)
    private BigDecimal dailyWdAmount;

    @Column(name = "address")
    private String address;

    @Column(name = "post_code")
    private String postCode;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "taker_fee")
    private Long takerFee;

    @Column(name = "maker_fee")
    private Long makerFee;

    @Column(name = "fee_type")
    private Integer feeType;

    @Column(name = "identity_id")
    private String identityId;

    @Column(name = "withdraw_blocktime")
    private Long withdrawBlocktime;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLoginAlert() {
        return loginAlert;
    }

    public UserSetting loginAlert(Integer loginAlert) {
        this.loginAlert = loginAlert;
        return this;
    }

    public void setLoginAlert(Integer loginAlert) {
        this.loginAlert = loginAlert;
    }

    public Integer getKeepAlive() {
        return keepAlive;
    }

    public UserSetting keepAlive(Integer keepAlive) {
        this.keepAlive = keepAlive;
        return this;
    }

    public void setKeepAlive(Integer keepAlive) {
        this.keepAlive = keepAlive;
    }

    public Integer getDetectIpChange() {
        return detectIpChange;
    }

    public UserSetting detectIpChange(Integer detectIpChange) {
        this.detectIpChange = detectIpChange;
        return this;
    }

    public void setDetectIpChange(Integer detectIpChange) {
        this.detectIpChange = detectIpChange;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public UserSetting activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public Timestamp getCreated() {
        return created;
    }

    public UserSetting created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public UserSetting updated(Timestamp updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    public Timestamp getDob() {
        return dob;
    }

    public UserSetting dob(Timestamp dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public String getAvator() {
        return avator;
    }

    public UserSetting avator(String avator) {
        this.avator = avator;
        return this;
    }

    public void setAvator(String avator) {
        this.avator = avator;
    }

    public String getUsername() {
        return username;
    }

    public UserSetting username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserSetting firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public UserSetting middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserSetting lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSfa() {
        return sfa;
    }

    public UserSetting sfa(Integer sfa) {
        this.sfa = sfa;
        return this;
    }

    public void setSfa(Integer sfa) {
        this.sfa = sfa;
    }

    public String getFaSeed() {
        return faSeed;
    }

    public UserSetting faSeed(String faSeed) {
        this.faSeed = faSeed;
        return this;
    }

    public void setFaSeed(String faSeed) {
        this.faSeed = faSeed;
    }

    public String getRefId() {
        return refId;
    }

    public UserSetting refId(String refId) {
        this.refId = refId;
        return this;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Integer getVerifyLevel() {
        return verifyLevel;
    }

    public UserSetting verifyLevel(Integer verifyLevel) {
        this.verifyLevel = verifyLevel;
        return this;
    }

    public void setVerifyLevel(Integer verifyLevel) {
        this.verifyLevel = verifyLevel;
    }

    public BigDecimal getWdDailyLimit() {
        return wdDailyLimit;
    }

    public UserSetting wdDailyLimit(BigDecimal wdDailyLimit) {
        this.wdDailyLimit = wdDailyLimit;
        return this;
    }

    public void setWdDailyLimit(BigDecimal wdDailyLimit) {
        this.wdDailyLimit = wdDailyLimit;
    }

    public BigDecimal getDailyWdAmount() {
        return dailyWdAmount;
    }

    public UserSetting dailyWdAmount(BigDecimal dailyWdAmount) {
        this.dailyWdAmount = dailyWdAmount;
        return this;
    }

    public void setDailyWdAmount(BigDecimal dailyWdAmount) {
        this.dailyWdAmount = dailyWdAmount;
    }

    public String getAddress() {
        return address;
    }

    public UserSetting address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostCode() {
        return postCode;
    }

    public UserSetting postCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public UserSetting city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public UserSetting country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getTakerFee() {
        return takerFee;
    }

    public UserSetting takerFee(Long takerFee) {
        this.takerFee = takerFee;
        return this;
    }

    public void setTakerFee(Long takerFee) {
        this.takerFee = takerFee;
    }

    public Long getMakerFee() {
        return makerFee;
    }

    public UserSetting makerFee(Long makerFee) {
        this.makerFee = makerFee;
        return this;
    }

    public void setMakerFee(Long makerFee) {
        this.makerFee = makerFee;
    }

    public Integer getFeeType() {
        return feeType;
    }

    public UserSetting feeType(Integer feeType) {
        this.feeType = feeType;
        return this;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public String getIdentityId() {
        return identityId;
    }

    public UserSetting identityId(String identityId) {
        this.identityId = identityId;
        return this;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public Long getWithdrawBlocktime() {
        return withdrawBlocktime;
    }

    public UserSetting withdrawBlocktime(Long withdrawBlocktime) {
        this.withdrawBlocktime = withdrawBlocktime;
        return this;
    }

    public void setWithdrawBlocktime(Long withdrawBlocktime) {
        this.withdrawBlocktime = withdrawBlocktime;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserSetting)) {
            return false;
        }
        return id != null && id.equals(((UserSetting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserSetting{" +
            "id=" + getId() +
            ", loginAlert=" + getLoginAlert() +
            ", keepAlive=" + getKeepAlive() +
            ", detectIpChange=" + getDetectIpChange() +
            ", activeFlg=" + getActiveFlg() +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", dob='" + getDob() + "'" +
            ", avator='" + getAvator() + "'" +
            ", username='" + getUsername() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", middleName='" + getMiddleName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", sfa=" + getSfa() +
            ", faSeed='" + getFaSeed() + "'" +
            ", refId='" + getRefId() + "'" +
            ", verifyLevel=" + getVerifyLevel() +
            ", wdDailyLimit=" + getWdDailyLimit() +
            ", dailyWdAmount=" + getDailyWdAmount() +
            ", address='" + getAddress() + "'" +
            ", postCode='" + getPostCode() + "'" +
            ", city='" + getCity() + "'" +
            ", country='" + getCountry() + "'" +
            ", takerFee=" + getTakerFee() +
            ", makerFee=" + getMakerFee() +
            ", feeType=" + getFeeType() +
            ", identityId='" + getIdentityId() + "'" +
            ", withdrawBlocktime=" + getWithdrawBlocktime() +
            "}";
    }
}
