package com.hihexa.cms.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A SymbolConfig.
 */
@Entity
@Table(name = "symbol_config")
public class SymbolConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "base_cc_id")
    private Integer baseCcId;

    @Column(name = "base_cc")
    private String baseCc;

    @Column(name = "counter_cc_id")
    private Integer counterCcId;

    @Column(name = "counter_cc")
    private String counterCc;

    @Column(name = "type")
    private Integer type;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "description")
    private String description;

    @Column(name = "decimal_place_amount")
    private Integer decimalPlaceAmount;

    @Column(name = "decimal_place_price")
    private Integer decimalPlacePrice;

    @Column(name = "is_demo")
    private Integer isDemo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public SymbolConfig symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getBaseCcId() {
        return baseCcId;
    }

    public SymbolConfig baseCcId(Integer baseCcId) {
        this.baseCcId = baseCcId;
        return this;
    }

    public void setBaseCcId(Integer baseCcId) {
        this.baseCcId = baseCcId;
    }

    public String getBaseCc() {
        return baseCc;
    }

    public SymbolConfig baseCc(String baseCc) {
        this.baseCc = baseCc;
        return this;
    }

    public void setBaseCc(String baseCc) {
        this.baseCc = baseCc;
    }

    public Integer getCounterCcId() {
        return counterCcId;
    }

    public SymbolConfig counterCcId(Integer counterCcId) {
        this.counterCcId = counterCcId;
        return this;
    }

    public void setCounterCcId(Integer counterCcId) {
        this.counterCcId = counterCcId;
    }

    public String getCounterCc() {
        return counterCc;
    }

    public SymbolConfig counterCc(String counterCc) {
        this.counterCc = counterCc;
        return this;
    }

    public void setCounterCc(String counterCc) {
        this.counterCc = counterCc;
    }

    public Integer getType() {
        return type;
    }

    public SymbolConfig type(Integer type) {
        this.type = type;
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGroupName() {
        return groupName;
    }

    public SymbolConfig groupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public SymbolConfig description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDecimalPlaceAmount() {
        return decimalPlaceAmount;
    }

    public SymbolConfig decimalPlaceAmount(Integer decimalPlaceAmount) {
        this.decimalPlaceAmount = decimalPlaceAmount;
        return this;
    }

    public void setDecimalPlaceAmount(Integer decimalPlaceAmount) {
        this.decimalPlaceAmount = decimalPlaceAmount;
    }

    public Integer getDecimalPlacePrice() {
        return decimalPlacePrice;
    }

    public SymbolConfig decimalPlacePrice(Integer decimalPlacePrice) {
        this.decimalPlacePrice = decimalPlacePrice;
        return this;
    }

    public void setDecimalPlacePrice(Integer decimalPlacePrice) {
        this.decimalPlacePrice = decimalPlacePrice;
    }

    public Integer getIsDemo() {
        return isDemo;
    }

    public SymbolConfig isDemo(Integer isDemo) {
        this.isDemo = isDemo;
        return this;
    }

    public void setIsDemo(Integer isDemo) {
        this.isDemo = isDemo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SymbolConfig)) {
            return false;
        }
        return id != null && id.equals(((SymbolConfig) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "SymbolConfig{" +
            "id=" + getId() +
            ", symbol='" + getSymbol() + "'" +
            ", baseCcId=" + getBaseCcId() +
            ", baseCc='" + getBaseCc() + "'" +
            ", counterCcId=" + getCounterCcId() +
            ", counterCc='" + getCounterCc() + "'" +
            ", type=" + getType() +
            ", groupName='" + getGroupName() + "'" +
            ", description='" + getDescription() + "'" +
            ", decimalPlaceAmount=" + getDecimalPlaceAmount() +
            ", decimalPlacePrice=" + getDecimalPlacePrice() +
            ", isDemo=" + getIsDemo() +
            "}";
    }
}
