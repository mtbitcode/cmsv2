package com.hihexa.cms.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * A UserLog.
 */
@Entity
@Table(name = "user_log")
public class UserLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ip")
    private String ip;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "action")
    private String action;

    @Column(name = "detail")
    private String detail;

    @Column(name = "created")
    private Timestamp created;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public UserLog ip(String ip) {
        this.ip = ip;
        return this;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public UserLog userAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getAction() {
        return action;
    }

    public UserLog action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDetail() {
        return detail;
    }

    public UserLog detail(String detail) {
        this.detail = detail;
        return this;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Timestamp getCreated() {
        return created;
    }

    public UserLog created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserLog)) {
            return false;
        }
        return id != null && id.equals(((UserLog) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserLog{" +
            "id=" + getId() +
            ", ip='" + getIp() + "'" +
            ", userAgent='" + getUserAgent() + "'" +
            ", action='" + getAction() + "'" +
            ", detail='" + getDetail() + "'" +
            ", created='" + getCreated() + "'" +
            "}";
    }
    public UserLog() {
	}
	public UserLog(String userAgent, String action, String detail, Timestamp created) {
		this.userAgent = userAgent;
		this.action = action;
		this.detail = detail;
		this.created = created;
	}
    
}
