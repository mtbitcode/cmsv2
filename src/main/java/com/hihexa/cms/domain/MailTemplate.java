package com.hihexa.cms.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A MailTemplate.
 */
@Entity
@Table(name = "mail_template")
public class MailTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mail_code")
    private String mailCode;

    @Lob
    @Column(name = "mail_content")
    private String mailContent;

    @Column(name = "mail_type")
    private String mailType;

    @Column(name = "cc")
    private String cc;

    @Column(name = "bcc")
    private String bcc;

    @Column(name = "created")
    private Instant created;

    @Column(name = "updated")
    private Instant updated;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Lob
    @Column(name = "mail_subject")
    private String mailSubject;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMailCode() {
        return mailCode;
    }

    public MailTemplate mailCode(String mailCode) {
        this.mailCode = mailCode;
        return this;
    }

    public void setMailCode(String mailCode) {
        this.mailCode = mailCode;
    }

    public String getMailContent() {
        return mailContent;
    }

    public MailTemplate mailContent(String mailContent) {
        this.mailContent = mailContent;
        return this;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    public String getMailType() {
        return mailType;
    }

    public MailTemplate mailType(String mailType) {
        this.mailType = mailType;
        return this;
    }

    public void setMailType(String mailType) {
        this.mailType = mailType;
    }

    public String getCc() {
        return cc;
    }

    public MailTemplate cc(String cc) {
        this.cc = cc;
        return this;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public MailTemplate bcc(String bcc) {
        this.bcc = bcc;
        return this;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public Instant getCreated() {
        return created;
    }

    public MailTemplate created(Instant created) {
        this.created = created;
        return this;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Instant getUpdated() {
        return updated;
    }

    public MailTemplate updated(Instant updated) {
        this.updated = updated;
        return this;
    }

    public void setUpdated(Instant updated) {
        this.updated = updated;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public MailTemplate activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public MailTemplate mailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
        return this;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MailTemplate)) {
            return false;
        }
        return id != null && id.equals(((MailTemplate) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MailTemplate{" +
            "id=" + getId() +
            ", mailCode='" + getMailCode() + "'" +
            ", mailContent='" + getMailContent() + "'" +
            ", mailType='" + getMailType() + "'" +
            ", cc='" + getCc() + "'" +
            ", bcc='" + getBcc() + "'" +
            ", created='" + getCreated() + "'" +
            ", updated='" + getUpdated() + "'" +
            ", activeFlg=" + getActiveFlg() +
            ", mailSubject='" + getMailSubject() + "'" +
            "}";
    }
}
