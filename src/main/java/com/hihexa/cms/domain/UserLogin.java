package com.hihexa.cms.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * A UserLogin.
 */
@Entity
@Table(name = "user_login")
public class UserLogin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "broker_id")
    private Integer brokerId;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private Integer status;

    @Column(name = "last_login")
    private Timestamp lastLogin;

    @Column(name = "agents")
    private String agents;

    @Column(name = "ips")
    private String ips;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Column(name = "created")
    private Timestamp created;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getBrokerId() {
        return brokerId;
    }

    public UserLogin brokerId(Integer brokerId) {
        this.brokerId = brokerId;
        return this;
    }

    public void setBrokerId(Integer brokerId) {
        this.brokerId = brokerId;
    }

    public String getEmail() {
        return email;
    }

    public UserLogin email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public UserLogin password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getStatus() {
        return status;
    }

    public UserLogin status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getLastLogin() {
        return lastLogin;
    }

    public UserLogin lastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getAgents() {
        return agents;
    }

    public UserLogin agents(String agents) {
        this.agents = agents;
        return this;
    }

    public void setAgents(String agents) {
        this.agents = agents;
    }

    public String getIps() {
        return ips;
    }

    public UserLogin ips(String ips) {
        this.ips = ips;
        return this;
    }

    public void setIps(String ips) {
        this.ips = ips;
    }

    public Integer getActiveFlg() {
        return activeFlg;
    }

    public UserLogin activeFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public Timestamp getCreated() {
        return created;
    }

    public UserLogin created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserLogin)) {
            return false;
        }
        return id != null && id.equals(((UserLogin) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserLogin{" +
            "id=" + getId() +
            ", brokerId=" + getBrokerId() +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            ", status=" + getStatus() +
            ", lastLogin='" + getLastLogin() + "'" +
            ", agents='" + getAgents() + "'" +
            ", ips='" + getIps() + "'" +
            ", activeFlg=" + getActiveFlg() +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
