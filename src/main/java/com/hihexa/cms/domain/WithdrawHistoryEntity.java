package com.hihexa.cms.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@Entity
@Table(name = "withdraw_history")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WithdrawHistoryEntity implements Serializable {
	private static final long serialVersionUID = 6808647214427336920L;
	@Id
	@Column(name = "id")
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "currency_id")
	private Integer currencyId;

	@Column(name = "address")
	private String address;

	@Column(name = "destination_address")
	private String destinationAddress;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "fee_value")
	private BigDecimal feeValue;

	@Column(name = "tx")
	private String tx;

	@Column(name = "currency")
	private String currency;

	@Column(name = "included_block")
	private Long includedBlock;


	@Column(name = "status")
	private Integer status;

	@Column(name = "created")
	private Timestamp created;

	@Column(name = "updated")
	private Timestamp updated;

	@Column(name = "active_flg")
	private Integer activeFlg;

	@Column(name = "extra_uuid")
	private String extraUuid;

	@Column(name = "destination_extra_uuid")
	private String destinationExtraUuid;

	@Column(name = "broker_id")
	private Long brokerId;
    @Column(name = "email")
	private String email;

    @Column(name = "account_admin")
    private String accountAdmin;

    @Column(name = "balance_before")
    private BigDecimal balanceBefore;
    @Column(name = "balance_after")
    private BigDecimal balanceAfter;

    @Column(name = "request_id")
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getAccountAdmin() {
        return accountAdmin;
    }

    public void setAccountAdmin(String accountAdmin) {
        this.accountAdmin = accountAdmin;
    }

	public Long getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Long brokerId) {
		this.brokerId = brokerId;
	}

	public String getExtraUuid() {
		return extraUuid;
	}

	public void setExtraUuid(String extraUuid) {
		this.extraUuid = extraUuid;
	}

	public String getDestinationExtraUuid() {
		return destinationExtraUuid;
	}

	public void setDestinationExtraUuid(String destinationExtraUuid) {
		this.destinationExtraUuid = destinationExtraUuid;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the destinationAddress
	 */
	public String getDestinationAddress() {
		return destinationAddress;
	}

	/**
	 * @param destinationAddress the destinationAddress to set
	 */
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the tx
	 */
	public String getTx() {
		return tx;
	}

	/**
	 * @param tx the tx to set
	 */
	public void setTx(String tx) {
		this.tx = tx;
	}

	/**
	 * @return the includedBlock
	 */
	public Long getIncludedBlock() {
		return includedBlock;
	}

	/**
	 * @param includedBlock the includedBlock to set
	 */
	public void setIncludedBlock(Long includedBlock) {
		this.includedBlock = includedBlock;
	}

	/**
	 * @return the withdrawTime
	 */

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the created
	 */
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }
    public void updated(){
        if (this.updated == null){
            this.updated = new Timestamp(System.currentTimeMillis());
        } else {
            this.updated.setTime(System.currentTimeMillis());
        }
    }
    /**
	 * @return the activeFlg
	 */
	public Integer getActiveFlg() {
		return activeFlg;
	}

	/**
	 * @param activeFlg the activeFlg to set
	 */
	public void setActiveFlg(Integer activeFlg) {
		this.activeFlg = activeFlg;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getFeeValue() {
		return feeValue;
	}

	public void setFeeValue(BigDecimal feeValue) {
		this.feeValue = feeValue;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }
}
