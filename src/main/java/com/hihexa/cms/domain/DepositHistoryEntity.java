package com.hihexa.cms.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@Entity
@Table(name = "deposit_history")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DepositHistoryEntity implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 6808647214427336920L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "address")
    private String address;

    @Column(name = "amount")

    private BigDecimal amount;

    @Column(name = "tx")
    private String tx;

    @Column(name = "currency")
    private String currency;

    @Column(name = "included_block")
    private Long includedBlock;



    @Column(name = "confirmed")
    private Long confirmed;

    @Column(name = "target_confirm")
    private Long targetConfirm;

    @Column(name = "status")
    private Integer status;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "updated")
    private Timestamp updated;

    @Column(name = "active_flg")
    private Integer activeFlg;

    @Column(name = "extra_uuid")
    private String extraUuid;

    @Column(name = "broker_id")
    private Integer brokerId;

    @Column(name = "currency_id")
    private Integer currencyId;

    @Column(name = "email")
    private String email;

    public Integer getBrokerId() {
        return brokerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBrokerId(Integer brokerId) {
        this.brokerId = brokerId;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getExtraUuid() {
        return extraUuid;
    }

    public void setExtraUuid(String extraUuid) {
        this.extraUuid = extraUuid;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the tx
     */
    public String getTx() {
        return tx;
    }

    /**
     * @param tx the tx to set
     */
    public void setTx(String tx) {
        this.tx = tx;
    }

    /**
     * @return the includedBlock
     */
    public Long getIncludedBlock() {
        return includedBlock;
    }

    /**
     * @param includedBlock the includedBlock to set
     */
    public void setIncludedBlock(Long includedBlock) {
        this.includedBlock = includedBlock;
    }



    /**
     * @return the confirmed
     */
    public Long getConfirmed() {
        return confirmed;
    }

    /**
     * @param confirmed the confirmed to set
     */
    public void setConfirmed(Long confirmed) {
        this.confirmed = confirmed;
    }

    /**
     * @return the targetConfirm
     */
    public Long getTargetConfirm() {
        return targetConfirm;
    }

    /**
     * @param targetConfirm the targetConfirm to set
     */
    public void setTargetConfirm(Long targetConfirm) {
        this.targetConfirm = targetConfirm;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the created
     */
    public Timestamp getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(Timestamp created) {
        this.created = created;
    }

    /**
     * @return the updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    /**
     * @return the activeFlg
     */
    public Integer getActiveFlg() {
        return activeFlg;
    }

    /**
     * @param activeFlg the activeFlg to set
     */
    public void setActiveFlg(Integer activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

    public void updated(){
        if (this.updated == null){
            this.updated = new Timestamp(System.currentTimeMillis());
        } else {
            this.updated.setTime(System.currentTimeMillis());
        }
    }
}
