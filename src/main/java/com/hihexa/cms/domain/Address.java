package com.hihexa.cms.domain;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * A Address.
 */
@Entity
@Table(name = "address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "balance_id")
    private Long balanceId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "address")
    private String address;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "currency")
    private String currency;

    @Column(name = "extra_uuid")
    private String extraUuid;

    @Column(name = "currency_id")
    private Integer currencyId;

    @Column(name = "path")
    private String path;

    @Column(name = "created")
    private Timestamp created;

    @Column(name = "broker_id")
    private Integer brokerId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBalanceId() {
        return balanceId;
    }

    public Address balanceId(Long balanceId) {
        this.balanceId = balanceId;
        return this;
    }

    public void setBalanceId(Long balanceId) {
        this.balanceId = balanceId;
    }

    public Integer getStatus() {
        return status;
    }

    public Address status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public Address address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getUserId() {
        return userId;
    }

    public Address userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCurrency() {
        return currency;
    }

    public Address currency(String currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExtraUuid() {
        return extraUuid;
    }

    public Address extraUuid(String extraUuid) {
        this.extraUuid = extraUuid;
        return this;
    }

    public void setExtraUuid(String extraUuid) {
        this.extraUuid = extraUuid;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Address currencyId(Integer currencyId) {
        this.currencyId = currencyId;
        return this;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getPath() {
        return path;
    }

    public Address path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Timestamp getCreated() {
        return created;
    }

    public Address created(Timestamp created) {
        this.created = created;
        return this;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Integer getBrokerId() {
        return brokerId;
    }

    public Address brokerId(Integer brokerId) {
        this.brokerId = brokerId;
        return this;
    }

    public void setBrokerId(Integer brokerId) {
        this.brokerId = brokerId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Address)) {
            return false;
        }
        return id != null && id.equals(((Address) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Address{" +
            "id=" + getId() +
            ", balanceId=" + getBalanceId() +
            ", status=" + getStatus() +
            ", address='" + getAddress() + "'" +
            ", userId=" + getUserId() +
            ", currency='" + getCurrency() + "'" +
            ", extraUuid='" + getExtraUuid() + "'" +
            ", currencyId=" + getCurrencyId() +
            ", path='" + getPath() + "'" +
            ", created='" + getCreated() + "'" +
            ", brokerId=" + getBrokerId() +
            "}";
    }
}
