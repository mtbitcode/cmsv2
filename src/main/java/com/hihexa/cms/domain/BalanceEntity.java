package com.hihexa.cms.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * A BalanceEntity.
 */
@Entity
@Table(name = "balance")
public class BalanceEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "user_id", nullable = false)
	private Long userId;

	@NotNull
	@Column(name = "currency_id", nullable = false)
	private Integer currencyId;

	@NotNull
	@Column(name = "type", nullable = false)
	private Integer type;

	@Column(name = "amount", precision = 21, scale = 2)
	private BigInteger amount;

	@Column(name = "reserve_amount", precision = 21, scale = 2)
	private BigInteger reserveAmount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public BalanceEntity userId(Long userId) {
		this.userId = userId;
		return this;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public BalanceEntity currencyId(Integer currencyId) {
		this.currencyId = currencyId;
		return this;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	public Integer getType() {
		return type;
	}

	public BalanceEntity type(Integer type) {
		this.type = type;
		return this;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public BalanceEntity amount(BigInteger amount) {
		this.amount = amount;
		return this;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public BigInteger getReserveAmount() {
		return reserveAmount;
	}

	public BalanceEntity reserveAmount(BigInteger reserveAmount) {
		this.reserveAmount = reserveAmount;
		return this;
	}

	public void setReserveAmount(BigInteger reserveAmount) {
		this.reserveAmount = reserveAmount;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "BalanceEntity [id=" + id + ", userId=" + userId + ", currencyId=" + currencyId + ", type=" + type
				+ ", amount=" + amount + ", reserveAmount=" + reserveAmount + "]";
	}

}
