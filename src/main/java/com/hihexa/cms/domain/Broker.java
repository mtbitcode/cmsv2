package com.hihexa.cms.domain;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Broker.
 */
@Entity
@Table(name = "broker")
public class Broker implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "secret_id")
    private String secretId;

    @Column(name = "name")
    private String name;

    @Column(name = "active_flg")
    private String activeFlg;

    @Column(name = "uri")
    private String uri;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSecretId() {
        return secretId;
    }

    public Broker secretId(String secretId) {
        this.secretId = secretId;
        return this;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getName() {
        return name;
    }

    public Broker name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActiveFlg() {
        return activeFlg;
    }

    public Broker activeFlg(String activeFlg) {
        this.activeFlg = activeFlg;
        return this;
    }

    public void setActiveFlg(String activeFlg) {
        this.activeFlg = activeFlg;
    }

    public String getUri() {
        return uri;
    }

    public Broker uri(String uri) {
        this.uri = uri;
        return this;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Broker)) {
            return false;
        }
        return id != null && id.equals(((Broker) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Broker{" +
            "id=" + getId() +
            ", secretId='" + getSecretId() + "'" +
            ", name='" + getName() + "'" +
            ", activeFlg='" + getActiveFlg() + "'" +
            ", uri='" + getUri() + "'" +
            "}";
    }
}
