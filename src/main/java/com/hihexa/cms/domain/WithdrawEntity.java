package com.hihexa.cms.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hihexa.cms.model.wallet.WithdrawCreationRequest;
import com.hihexa.cms.util.DConstants;

@Entity
@Table(name = "withdraw")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WithdrawEntity implements Serializable {
	private static final long serialVersionUID = 6808647214427336920L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "currency_id")
	private Integer currencyId;

	@Column(name = "address")
	private String address;

	@Column(name = "destination_address")
	private String destinationAddress;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "fee_value")
	private BigDecimal feeValue;

	@Column(name = "tx")
	private String tx;

	@Column(name = "currency")
	private String currency;

	@Column(name = "included_block")
	private Long includedBlock;

	@Column(name = "status")
	private Integer status;

	@Column(name = "created")
	private Timestamp created;

	@Column(name = "updated")
	private Timestamp updated;

	@Column(name = "active_flg")
	private Integer activeFlg;

	@Column(name = "extra_uuid")
	private String extraUuid;

	@Column(name = "destination_extra_uuid")
	private String destinationExtraUuid;

	@Column(name = "broker_id")
	private Integer brokerId;

    @Column(name = "email")
	private String email;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "account_admin")
    private String accountAdmin;

    @Column(name = "balance_before")
    private BigDecimal balanceBefore;
    @Column(name = "balance_after")
    private BigDecimal balanceAfter;

    public String getAccountAdmin() {
        return accountAdmin;
    }

    public void setAccountAdmin(String accountAdmin) {
        this.accountAdmin = accountAdmin;
    }

    public BigDecimal getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(BigDecimal balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public BigDecimal getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(BigDecimal balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}

	public String getExtraUuid() {
		return extraUuid;
	}

	public void setExtraUuid(String extraUuid) {
		this.extraUuid = extraUuid;
	}

	public String getDestinationExtraUuid() {
		return destinationExtraUuid;
	}

	public void setDestinationExtraUuid(String destinationExtraUuid) {
		this.destinationExtraUuid = destinationExtraUuid;
	}

	public Integer getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the destinationAddress
	 */
	public String getDestinationAddress() {
		return destinationAddress;
	}

	/**
	 * @param destinationAddress the destinationAddress to set
	 */
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the tx
	 */
	public String getTx() {
		return tx;
	}

	/**
	 * @param tx the tx to set
	 */
	public void setTx(String tx) {
		this.tx = tx;
	}

	/**
	 * @return the includedBlock
	 */
	public Long getIncludedBlock() {
		return includedBlock;
	}

	/**
	 * @param includedBlock the includedBlock to set
	 */
	public void setIncludedBlock(Long includedBlock) {
		this.includedBlock = includedBlock;
	}

	/**
	 * @return the withdrawTime
	 */

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}



	/**
	 * @return the activeFlg
	 */
	public Integer getActiveFlg() {
		return activeFlg;
	}

	/**
	 * @param activeFlg the activeFlg to set
	 */
	public void setActiveFlg(Integer activeFlg) {
		this.activeFlg = activeFlg;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getFeeValue() {
		return feeValue;
	}

	public void setFeeValue(BigDecimal feeValue) {
		this.feeValue = feeValue;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public WithdrawEntity(Long userId, String address, String destinationAddress,
                          BigDecimal amount, BigDecimal feeValue, String tx, Integer currencyId, String currency, Long includedBlock,
                          Integer status, Timestamp created, Timestamp updated, Integer activeFlg,
                          String extraUuid, String destinationExtraUuid , Integer brokerId, String requestId) {
		this.userId = userId;
		this.address = address;
		this.destinationAddress = destinationAddress;
		this.amount = amount;
		this.feeValue = feeValue;
		this.tx = tx;
		this.currency = currency;
		this.includedBlock = includedBlock;
		this.status = status;
		this.created = created;
		this.updated = updated;
		this.activeFlg = activeFlg;
		this.extraUuid = extraUuid;
		this.destinationExtraUuid = destinationExtraUuid;
		this.currencyId = currencyId;
		this.brokerId = brokerId;
        this.setRequestId(requestId);
	}

	public WithdrawEntity(WithdrawCreationRequest request, Currency currency, Address addressEntity) {
		BigDecimal amount = request.getAmount();
        Timestamp timex = new Timestamp(System.currentTimeMillis());
		this.userId = request.getUserId();
		this.address = addressEntity.getAddress();
		this.destinationAddress = request.getAddress();
		//unit amount
		this.amount = amount.setScale(currency.getDecimalAmount(), BigDecimal.ROUND_DOWN);
		this.feeValue = request.getFeeValue();
		this.currency = currency.getCode();
		this.includedBlock = 0l;
		this.status = DConstants.WITHDRAW_STATUS.REQUEST_CREATE;
		this.created = timex;
		this.updated = timex;
		this.activeFlg = 1;
		this.extraUuid = addressEntity.getExtraUuid();
		this.destinationExtraUuid = request.getExtraUuid();
		this.currencyId = currency.getId();
		this.brokerId = request.getBrokerId();
		this.setEmail(request.getEmail());
		this.setRequestId(request.getRequestId());
		this.balanceBefore = request.getBalance();

	}

	public String toString() {
		ObjectWriter ow = new ObjectMapper().writer();
		ow.with(SerializationFeature.INDENT_OUTPUT);
		try {
			String json = ow.writeValueAsString(this);
			return json;
		} catch (Exception e) {
			return null;
		}
	}

	public WithdrawEntity() {
	}

    public void updated(){
        if (this.updated == null){
            this.updated = new Timestamp(System.currentTimeMillis());
        } else {
            this.updated.setTime(System.currentTimeMillis());
        }
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }
}
