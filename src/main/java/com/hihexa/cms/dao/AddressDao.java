package com.hihexa.cms.dao;

import com.hihexa.cms.model.wallet.AddressModel;

public interface AddressDao {
	String addAddress(AddressModel model) throws Exception;
}
