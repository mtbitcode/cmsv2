package com.hihexa.cms.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.procedure.ProcedureOutputs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hihexa.cms.dao.AddressDao;
import com.hihexa.cms.model.wallet.AddressModel;
import com.hihexa.cms.util.SQLServerConstant;

public class AddressDaoImpl  implements AddressDao {
	
	private static final Logger log = LoggerFactory.getLogger(AddressDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public String addAddress(AddressModel model) throws Exception{
		long t1= System.currentTimeMillis();
		//init
		String extraUUID = "";
		StoredProcedureQuery storedProcedure = 
		entityManager.createStoredProcedureQuery(SQLServerConstant.PROC_ADD_ADDRESS)
		            .registerStoredProcedureParameter(1, Long.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(3, Long.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(5, String.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(6, Long.class, ParameterMode.IN)
		            .registerStoredProcedureParameter(7, String.class, ParameterMode.OUT)
		            .registerStoredProcedureParameter(8, Integer.class, ParameterMode.OUT)
		            .setParameter(1, model.getUserId())
		            .setParameter(2, model.getCurrency())
		            .setParameter(3, model.getCurrencyId())
		            .setParameter(4, model.getAddress())
		            .setParameter(5, model.getPath())
		            .setParameter(6, model.getBrokerId());
		try {
			storedProcedure.execute();
			// get values
			extraUUID = (String) storedProcedure.getOutputParameterValue(7);
			Integer responseStatus = (Integer) storedProcedure.getOutputParameterValue(8);

			long t2 = System.currentTimeMillis();
			log.info("time = {}", (t2 - t1));
			if (responseStatus != 1) {
				log.info("[DB] Create Address error , responseStatus = {}", responseStatus);
				throw new Exception("[DB] Create Address error");
			}
		} catch (Exception e) {
			log.error("[DB] Create Address error , message = {}", e.getMessage());
		} finally {
			storedProcedure.unwrap(ProcedureOutputs.class).release();
		}
		return extraUUID;
	}


}
