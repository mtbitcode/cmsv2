package com.hihexa.cms.service;

import com.hihexa.cms.domain.BrokerSymbolConfig;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link BrokerSymbolConfig}.
 */
public interface BrokerSymbolConfigService {

    /**
     * Save a brokerSymbolConfig.
     *
     * @param brokerSymbolConfig the entity to save.
     * @return the persisted entity.
     */
    BrokerSymbolConfig save(BrokerSymbolConfig brokerSymbolConfig);

    /**
     * Get all the brokerSymbolConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BrokerSymbolConfig> findAll(Pageable pageable);


    /**
     * Get the "id" brokerSymbolConfig.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BrokerSymbolConfig> findOne(Long id);

    /**
     * Delete the "id" brokerSymbolConfig.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
