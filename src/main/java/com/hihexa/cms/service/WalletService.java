package com.hihexa.cms.service;

import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.domain.WithdrawHistoryEntity;
import com.hihexa.cms.model.wallet.*;

import java.util.Map;

public interface WalletService {

	AddressModel generationAddress(Long userId, Integer currencyId ,Integer brokerId) throws Exception;

	WithdrawEntity withdrawConfirmed(RestConfirmWithdrawRequest withdrawRequest) throws Exception;

    WithdrawEntity userCancelWithdraw(UpdateWithdrawRequest request) throws Exception;

    WithdrawHistoryEntity adminUpdateWithdraw(UpdateWithdrawRequest request) throws Exception;

	Integer createNewDeposit(DepositRequestModel depositRequest) throws Exception;

	WebhookResponseModel webhook(WebhookRequestModel depositRequest) throws Exception;

	WithdrawEntity createWithdraw(WithdrawCreationRequest withdrawRequest) throws Exception;

	WalletInfoResponse findWallet(String currency, String accountName) throws Exception;

	Map<String, String> newBlockRequest(WalletNewBlock block) throws Exception;

	void withdrawListeners() throws Exception;

	AddressModel getAddressByUserIdAndCurrency(Long userId, String currency) throws Exception;
}
