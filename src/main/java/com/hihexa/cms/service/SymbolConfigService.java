package com.hihexa.cms.service;

import com.hihexa.cms.domain.SymbolConfig;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link SymbolConfig}.
 */
public interface SymbolConfigService {

    /**
     * Save a symbolConfig.
     *
     * @param symbolConfig the entity to save.
     * @return the persisted entity.
     */
    SymbolConfig save(SymbolConfig symbolConfig);

    /**
     * Get all the symbolConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<SymbolConfig> findAll(Pageable pageable);

    Page<SymbolConfig> findSymbolByBaseCcorCounterCc(String BaseCc, String CounterCc, Pageable pageable);
    /**
     * Get the "id" symbolConfig.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<SymbolConfig> findOne(Long id);

    /**
     * Delete the "id" symbolConfig.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
