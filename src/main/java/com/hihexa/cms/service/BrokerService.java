package com.hihexa.cms.service;

import com.hihexa.cms.domain.Broker;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Broker}.
 */
public interface BrokerService {

    /**
     * Save a broker.
     *
     * @param broker the entity to save.
     * @return the persisted entity.
     */
    Broker save(Broker broker);

    /**
     * Get all the brokers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Broker> findAll(Pageable pageable);


    /**
     * Get the "id" broker.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Broker> findOne(Integer id);

    /**
     * Delete the "id" broker.
     *
     * @param id the id of the entity.
     */
    void delete(Integer id);
}
