package com.hihexa.cms.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.domain.WithdrawHistoryEntity;
import com.hihexa.cms.model.GatewayResponse;
import com.hihexa.cms.repository.WithdrawHistoryRepository;
import com.hihexa.cms.repository.WithdrawRepository;
import com.hihexa.cms.request.RestRequest;
import com.hihexa.cms.util.DConstants;

@Service
public class WithdrawHandleService {

    private static final Logger log = LoggerFactory.getLogger(WithdrawHandleService.class);
    @Autowired
    WithdrawRepository withdrawRepository;

    @Autowired
    WithdrawHistoryRepository withdrawHistoryRepository;

    @Autowired private Environment env;
    @Transactional
    public void handleCreateWithdraw(long wdId, boolean isSuccess){

        try {
            Optional<WithdrawEntity> wdEntityOp = withdrawRepository.findById(wdId);
            if (wdEntityOp.isPresent()){
                WithdrawEntity wdEntity = wdEntityOp.get();
                if (wdEntity.getStatus() != DConstants.WITHDRAW_STATUS.REQUEST_CREATE) return;

                if (isSuccess){
                    //case ok
                    //update status wd : PENDING
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.PENDING);
                } else {
                    // case fail
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.REJECT);
                }
                withdrawRepository.save(wdEntity);

                // call gw

                RestRequest restRequest = new RestRequest(env);

                GatewayResponse response = restRequest.createWithdraw(wdEntity);
                if (response == null){
                    log.info("gw response null");
                    return;
                }else {
                    log.info("gw response {}" , response.toString());
                }
            }

        }catch (Exception e){
            log.error("create wd exception " , e);
        }

    }

    public void handleConfirmWithdraw(long wdId, boolean isSuccess){
        try {
            Optional<WithdrawEntity> wdEntityOp = withdrawRepository.findById(wdId);
            if (wdEntityOp.isPresent()){
                WithdrawEntity wdEntity = wdEntityOp.get();
                if (wdEntity.getStatus() != DConstants.WITHDRAW_STATUS.CONFIRM) return;

                if (isSuccess){
                    //case ok
                    //update status wd : PENDING
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.CANCELLED);
                } else {
                    // case fail
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.REJECT);
                }
                withdrawRepository.deleteById(wdId);
                WithdrawHistoryEntity updateWdHistory = new WithdrawHistoryEntity();
                BeanUtils.copyProperties(wdEntity , updateWdHistory);
                withdrawHistoryRepository.save(updateWdHistory);

                // call gw update via wd

            }

        }catch (Exception e){
            log.error("create wd exception " , e);
        }
    }

    public void handleCancelWithdraw(long wdId, boolean isSuccess){

        try {
            Optional<WithdrawEntity> wdEntityOp = withdrawRepository.findById(wdId);
            if (wdEntityOp.isPresent()){
                WithdrawEntity wdEntity = wdEntityOp.get();
                if (wdEntity.getStatus() != DConstants.WITHDRAW_STATUS.REQUEST_CANCELLED) return;

                if (isSuccess){
                    //case ok
                    //update status wd : PENDING
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.CANCELLED);
                } else {
                    // case fail
                    wdEntity.setStatus(DConstants.WITHDRAW_STATUS.REJECT);
                }
                WithdrawHistoryEntity updateWdHistory = new WithdrawHistoryEntity();
                BeanUtils.copyProperties(wdEntity , updateWdHistory);
                withdrawRepository.deleteById(wdId);
                withdrawHistoryRepository.save(updateWdHistory);
                // call gw update via wd
            }

        }catch (Exception e){
            log.error("create wd exception " , e);
        }

    }
}
