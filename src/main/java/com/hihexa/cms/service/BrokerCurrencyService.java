package com.hihexa.cms.service;

import com.hihexa.cms.domain.BrokerCurrency;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link BrokerCurrency}.
 */
public interface BrokerCurrencyService {

    /**
     * Save a brokerCurrency.
     *
     * @param brokerCurrency the entity to save.
     * @return the persisted entity.
     */
    BrokerCurrency save(BrokerCurrency brokerCurrency) throws Exception;

    /**
     * Get all the brokerCurrencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BrokerCurrency> findAll(Pageable pageable);


    /**
     * Get the "id" brokerCurrency.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BrokerCurrency> findOne(Long id);

    /**
     * Delete the "id" brokerCurrency.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

	Optional<BrokerCurrency> findOneByBrokerIdAndCurrencyId(Integer brokerId, Integer currencyId);
}
