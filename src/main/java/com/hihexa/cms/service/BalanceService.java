package com.hihexa.cms.service;

import com.hihexa.cms.domain.BalanceEntity;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link BalanceEntity}.
 */
public interface BalanceService {

    /**
     * Save a balanceEntity.
     *
     * @param balanceEntity the entity to save.
     * @return the persisted entity.
     */
    BalanceEntity save(BalanceEntity balanceEntity);

    /**
     * Get all the balanceEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BalanceEntity> findAll(Pageable pageable);

    Page<BalanceEntity> findBalanceByUserIdOrCurrencyId(Long userId, Integer currencyId ,Pageable pageable);
    /**
     * Get the "id" balanceEntity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BalanceEntity> findOne(Long id);

    /**
     * Delete the "id" balanceEntity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
