package com.hihexa.cms.service;

import com.hihexa.cms.domain.UserLogin;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link UserLogin}.
 */
public interface UserLoginService {

    /**
     * Save a userLogin.
     *
     * @param userLogin the entity to save.
     * @return the persisted entity.
     */
    UserLogin save(UserLogin userLogin);

    /**
     * Get all the userLogins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserLogin> findAll(Pageable pageable);

    Page<UserLogin> findUserByEmail(String Email , Pageable pageable);
    /**
     * Get the "id" userLogin.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserLogin> findOne(Long id);

    /**
     * Delete the "id" userLogin.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
