package com.hihexa.cms.service;

import com.hihexa.cms.domain.Currency;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Currency}.
 */
public interface CurrencyService {

    /**
     * Save a currency.
     *
     * @param currency the entity to save.
     * @return the persisted entity.
     */
    Currency save(Currency currency);

    /**
     * Get all the currencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Currency> findAll(Pageable pageable);


    /**
     * Get all the currencies.
     *
     * @param list the information.
     * @return the list of entities.
     */
    List<Currency> getAll();


    /**
     * Get the "id" currency.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Currency> findOne(Integer id);

    String findCode(Integer id);

    Currency findOne(String code);

    /**
     * Delete the "id" currency.
     *
     * @param id the id of the entity.
     */
    void delete(Integer id);
}
