package com.hihexa.cms.service;

import com.hihexa.cms.domain.MailTemplate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MailTemplate}.
 */
public interface MailTemplateService {

    /**
     * Save a mailTemplate.
     *
     * @param mailTemplate the entity to save.
     * @return the persisted entity.
     */
    MailTemplate save(MailTemplate mailTemplate);

    /**
     * Get all the mailTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MailTemplate> findAll(Pageable pageable);


    /**
     * Get the "id" mailTemplate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MailTemplate> findOne(Long id);

    /**
     * Delete the "id" mailTemplate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
