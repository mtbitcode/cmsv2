package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.BrokerCurrencyService;
import com.hihexa.cms.service.BrokerService;
import com.hihexa.cms.service.CurrencyService;
import com.hihexa.cms.util.DConstants;
import com.hihexa.cms.domain.BrokerCurrency;
import com.hihexa.cms.domain.Currency;
import com.hihexa.cms.model.wallet.WalletInfoResponse;
import com.hihexa.cms.model.wallet.WebhookRequestModel;
import com.hihexa.cms.model.wallet.WebhookResponseModel;
import com.hihexa.cms.repository.BrokerCurrencyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BrokerCurrency}.
 * @author trungnv
 */
@Service
public class BrokerCurrencyServiceImpl implements BrokerCurrencyService {

    private static final Logger log = LoggerFactory.getLogger(BrokerCurrencyServiceImpl.class);

	@Autowired BrokerCurrencyRepository brokerCurrencyRepository;
	@Autowired CurrencyService currencyService;
	@Autowired BrokerService brokerService;
	@Autowired Environment env;
	private String walletServer, webhook, wallet, protocol, hostAddress ,serverPort ,depositUri;

	public BrokerCurrencyServiceImpl(Environment env) {
		this.walletServer = env.getProperty("wallet.server");
		this.webhook = env.getProperty("wallet.endpoint.webhook");
		this.wallet = env.getProperty("wallet.endpoint.id");
		this.depositUri = env.getProperty("wallet.endpoint.deposit");
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		this.protocol = protocol;
		this.serverPort = env.getProperty("server.port");
		String hostAddress = "localhost";
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.warn("The host name could not be determined, using `localhost` as fallback");
		}
		this.hostAddress = hostAddress;
	}
	@SuppressWarnings("unchecked")
	private WalletInfoResponse findWallet(String currency, String accountName) throws Exception {
		if (StringUtils.isEmpty(currency))
			throw new Exception("currency is null or empty");
		currency = currency.toLowerCase();
		String url = String.format("%s%s", walletServer, wallet);
		Map<String, String> mapIn = new HashMap<>();
		mapIn.put("currency_id", currency);
		if ("eos".equalsIgnoreCase(currency)) {
			mapIn.put("new_account_name", accountName);
		}
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> mapOut = restTemplate.postForObject(url, mapIn, Map.class);
		WalletInfoResponse response = new WalletInfoResponse(mapOut.get("currency_id"), mapOut.get("multisig"),
				mapOut.get("storage"), mapOut.get("hd_scope"), mapOut.get("created_at"), mapOut.get("updated_at"),
				mapOut.get("id"));
		return response;
	}

	private String getWalletId(Integer brokerId, Integer currencyId, String currency, String accountName) {
		try {
			//check base ETH
			String walletId = "";
			boolean isBase = false;
			for (String base : DConstants.BASE_ETH) {
				if(base.equals(currency)) {
					isBase = true;
					break;
				}
			}
			if(isBase) {
				Optional<BrokerCurrency> cObj = brokerCurrencyRepository.findOneByBrokerIdAndCurrencyId(brokerId, currencyId);
				if (!cObj.isPresent()) {
					WalletInfoResponse response = findWallet(currency ,accountName);
					walletId = response.getId();
				}else {
					walletId = cObj.get().getWalletId();
				}
			}else {
				WalletInfoResponse response = findWallet(currency ,accountName);
				walletId = response.getId();
			}

			return walletId;
		} catch (Exception e) {
			log.error("Cannot create Wallet ID {}", e);
		}
		return "";
	}

    /**
     * Save a brokerCurrency.
     *
     * @param brokerCurrency the entity to save.
     * @return the persisted entity.
     * @throws Exception
     */
    @Override
    public BrokerCurrency save(BrokerCurrency brokerCurrency) throws Exception {
        log.debug("Request to save BrokerCurrency : {}", brokerCurrency);
        //init
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Integer currencyId = brokerCurrency.getCurrencyId();
		Optional<Currency> currencyOp = currencyService.findOne(currencyId);
        if(!currencyOp.isPresent()) throw new Exception("Currency is not exist");
        Currency currencyEntity = currencyOp.get();
        //reality currency
        if(currencyEntity.getIsDemo() == 0) {
        	String code = currencyEntity.getCode();
            Long targetConfirm = currencyEntity.getTargetConfirms();
    		String walletId = "";
            Integer brokerId = brokerCurrency.getBrokerId();
    		if (!brokerService.findOne(brokerId).isPresent()) throw new Exception("Broker is not exist");
            //get wallet_id from wallet
    		if (brokerCurrency.getId() != null) {
            	//update
				walletId = brokerCurrency.getWalletId();
            }else {
            	//create
				walletId = getWalletId(brokerCurrency.getBrokerId(), currencyId, code, brokerCurrency.getAccountName());
				brokerCurrency.setWalletId(walletId);
				brokerCurrency.setCreated(now);
    		}

    		//push target confirmation
			String depositUrl = String.format("%s://%s:%s%s", protocol, hostAddress, serverPort, depositUri);
    		try {
				webhook(new WebhookRequestModel(walletId, targetConfirm, depositUrl));
    		} catch (Exception e) {
    			log.error("Call webhook error {}", e.getMessage());
    		}
        }

        brokerCurrency.setUpdated(now);
        return brokerCurrencyRepository.save(brokerCurrency);
    }

    private WebhookResponseModel webhook(WebhookRequestModel webhookRequest) throws Exception {
		String url = String.format("%s%s", walletServer, webhook);
		log.info("webhookRequest Call URL {}, PARAMS {} ", url, webhookRequest);
		RestTemplate restTemplate = new RestTemplate();
		WebhookResponseModel walletResponse = restTemplate.postForObject(url, webhookRequest,
				WebhookResponseModel.class);
		log.info("withdrawConfirm CALL URL RESPONSE {}", walletResponse);
		return walletResponse;
	}

    /**
     * Get all the brokerCurrencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BrokerCurrency> findAll(Pageable pageable) {
        log.debug("Request to get all BrokerCurrencies");
        return brokerCurrencyRepository.findAll(pageable);
    }


    /**
     * Get one brokerCurrency by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BrokerCurrency> findOne(Long id) {
        log.debug("Request to get BrokerCurrency : {}", id);
        return brokerCurrencyRepository.findById(id);
    }

    /**
     * Delete the brokerCurrency by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BrokerCurrency : {}", id);
        brokerCurrencyRepository.deleteById(id);
    }

	@Override
	public Optional<BrokerCurrency> findOneByBrokerIdAndCurrencyId(Integer brokerId, Integer currencyId) {
		return brokerCurrencyRepository.findOneByBrokerIdAndCurrencyId(brokerId, currencyId);
	}
}
