package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.CurrencyService;
import com.hihexa.cms.domain.Currency;
import com.hihexa.cms.repository.CurrencyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

/**
 * Service Implementation for managing {@link Currency}.
 */
@Service
@CacheConfig(cacheNames={"currency"})
public class CurrencyServiceImpl implements CurrencyService {

	public final static String ALL_CURRENCY = "allCURRENCY";

    private final Logger log = LoggerFactory.getLogger(CurrencyServiceImpl.class);

    private final CurrencyRepository currencyRepository;

    private List<Currency> lstCurrency = new ArrayList<Currency>();

    @PostConstruct
    private void initData() {
    	//lstCurrency = new ArrayList<Currency>();
    	lstCurrency = currencyRepository.findAll();
    }

    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    /**
     * Save a currency.
     *
     * @param currency the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public Currency save(Currency currency) {
        log.debug("Request to save Currency : {}", currency);
        return currencyRepository.save(currency);
    }

    /**
     * Get all the currencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Currency> findAll(Pageable pageable) {
        log.debug("Request to get all Currencies");
        return currencyRepository.findAll(pageable);
    }


    /**
     * Get one currency by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Currency> findOne(Integer id) {
        log.debug("Request to get Currency : {}", id);
        return currencyRepository.findById(id);
    }


    /**
     * Get one currency by code.
     *
     * @param code the code of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Currency findOne(String code) {
        log.debug("Request to get Currency : {}", code);
        for (Currency currency : lstCurrency) {
			if(code.equals(currency.getCode())) {
				return currency;
			}
		}
        return currencyRepository.findOneByCode(code).get();
    }

    /**
     * Delete the currency by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Integer id) {
        log.debug("Request to delete Currency : {}", id);
        currencyRepository.deleteById(id);
    }


    /**
     * Get all the currencies.
     *
     * @return the list of caches.
     */
	@Override
	@Cacheable(value = ALL_CURRENCY ,unless = "#result != null")
	public List<Currency> getAll() {
		return this.lstCurrency;
	}

//	@CachePut
	public Currency updateCurrency(Currency currency) {
	    this.lstCurrency.set(0, currency);
	    return this.lstCurrency.get(0);
	}

	@CacheEvict(ALL_CURRENCY)
	public void clearCache() {
		log.debug("Request to clear Currency cache : {}", ALL_CURRENCY);
	}

	@Override
	public String findCode(Integer id) {
		return lstCurrency.stream().filter(i->i.getId() == id).findAny().get().getCode();
//		for (Currency currency : lstCurrency) {
//			if (id == currency.getId()) {
//				return currency.getCode();
//			}
//		}
	}
}
