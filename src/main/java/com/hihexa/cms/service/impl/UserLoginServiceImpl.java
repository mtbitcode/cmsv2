package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.UserLoginService;
import com.hihexa.cms.domain.UserLogin;
import com.hihexa.cms.repository.UserLoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserLogin}.
 */
@Service
@Transactional
public class UserLoginServiceImpl implements UserLoginService {

    private final Logger log = LoggerFactory.getLogger(UserLoginServiceImpl.class);

    private final UserLoginRepository userLoginRepository;

    public UserLoginServiceImpl(UserLoginRepository userLoginRepository) {
        this.userLoginRepository = userLoginRepository;
    }

    /**
     * Save a userLogin.
     *
     * @param userLogin the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserLogin save(UserLogin userLogin) {
        log.debug("Request to save UserLogin : {}", userLogin);
        return userLoginRepository.save(userLogin);
    }

    /**
     * Get all the userLogins.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserLogin> findAll(Pageable pageable) {
        log.debug("Request to get all UserLogins");
        return userLoginRepository.findAll(pageable);
    }

    @Override
    public Page<UserLogin> findUserByEmail(String Email, Pageable pageable) {
        log.debug("Request to search UserLogin : {}", Email);
        return userLoginRepository.findUserLoginResourceByEmail(Email,pageable);
    }


    /**
     * Get one userLogin by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserLogin> findOne(Long id) {
        log.debug("Request to get UserLogin : {}", id);
        return userLoginRepository.findById(id);
    }

    /**
     * Delete the userLogin by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserLogin : {}", id);
        userLoginRepository.deleteById(id);
    }
}
