package com.hihexa.cms.service.impl;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import javax.transaction.Transactional;

import com.hihexa.cms.aeon.balanceService.CoreBalanceService;
import com.hihexa.cms.domain.*;
import com.hihexa.cms.domain.Currency;
import com.hihexa.cms.repository.*;
import com.hihexa.cms.util.PowerOfTens;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.hihexa.cms.model.wallet.*;
import com.hihexa.cms.model.CurrencyDto;
import com.hihexa.cms.model.GatewayResponse;
import com.hihexa.cms.request.RestRequest;
import com.hihexa.cms.service.BrokerCurrencyService;
import com.hihexa.cms.service.CurrencyService;
import com.hihexa.cms.service.UserSettingService;
import com.hihexa.cms.service.WalletService;
import com.hihexa.cms.util.DConstants;
import com.hihexa.cms.util.ErrorConstant;
import com.hihexa.cms.web.rest.errors.CMSException;

@Service
public class WalletServiceImpl implements WalletService {

	private static final Logger log = LoggerFactory.getLogger(WalletServiceImpl.class);

	@Autowired private AddressRepository addressRepository;
	@Autowired private CurrencyService currencyService;
	@Autowired private RestTemplate restTemplate;
	@Autowired private BalanceRepository balanceRepository;
	@Autowired private WithdrawRepository withdrawRepository;
	@Autowired private WithdrawHistoryRepository withdrawHistoryRepository;
	@Autowired private DepositRepository depositRepository;
	@Autowired private DepositHistoryRepository depositHistoryRepository;
	@Autowired private UserSettingService userSettingService;
	@Autowired private BrokerCurrencyService brokerCurrencyService;
	@Autowired private UserLogRepository userLogRepository;
	@Autowired private Environment env;

    @Autowired private UserLoginRepository userLoginRepository;

   @Autowired
    private CoreBalanceService coreBalanceService;

	private String walletServer, addressUrl, withdrawUrl, webhookUrl, walletUrl, walletBlock, token, chatId;
	public static Map<String, Integer> mapData = new ConcurrentHashMap<String, Integer>();
	private static Integer countTime = 0;

	public WalletServiceImpl(Environment env) {
		this.walletServer = env.getProperty("wallet.server");
		this.addressUrl = env.getProperty("wallet.endpoint.address");
		this.withdrawUrl = env.getProperty("wallet.endpoint.withdraw");
		this.webhookUrl = env.getProperty("wallet.endpoint.webhook");
		this.walletUrl = env.getProperty("wallet.endpoint.id");
		this.walletBlock = env.getProperty("wallet.endpoint.block");
		this.token = env.getProperty("telegram.token");
		this.chatId = env.getProperty("telegram.chatId");


	}

	private boolean isBaseEth(String currency) {
		for (String i : DConstants.BASE_ETH) {
			if (i.equals(currency)) {
				return true;
			}
		}
		return false;
	}

	private String getWalletId(Integer currencyId, Integer brokerId) throws Exception {
		Optional<BrokerCurrency> opt = brokerCurrencyService.findOneByBrokerIdAndCurrencyId(brokerId, currencyId);
		if (opt.isPresent() && !StringUtils.isEmpty(opt.get().getWalletId()))
			return opt.get().getWalletId();
		else {
			log.error("BrokerCurrency is null or Wallet_Id is null  , brokerId, currencyId {} {}", brokerId, currencyId);
			throw new Exception("Broker_Currency is null or Wallet_id is null , Please check again !");
		}
	}

	@Override
	public AddressModel generationAddress(Long userId, Integer currencyId, Integer brokerId) throws Exception {
		String url = String.format("%s%s", walletServer, addressUrl);
		log.info("generateNewAddress v2 Call URL {}, PARAMS {} ", url);
		//get currency
		Optional<Currency> currencyOpt = currencyService.findOne(currencyId);
		if(!currencyOpt.isPresent()) throw new Exception("currencyId is not exist");
		Currency currencyEntity = currencyOpt.get();
		String code = currencyEntity.getCode().toUpperCase();
		boolean isBaseEth = isBaseEth(code);

		//create model
		AddressModel model = new AddressModel(userId, currencyId, brokerId, code);
		//3 types of currency : baseExtra(XRP,EOS) , baseEth("HOT", "ZRX", "OMG", "MANA", "BAT", "WAX", "GNT", "PAX", "ENJ", "TUSD",...) , others
		// generation tags in db
		if (isBaseExtra(code)) {
			model = getAddressBaseExtra(model, url);
		} else if (isBaseEth) {
			model = getAddressCurrencyBaseEth(model, url);
		} else {
			model = getAddressWallet(model, url);
		}

		log.info("generateNewAddress v2 response from wallet {}", model);

		String result = addressRepository.addAddress(model);
		model.setExtraUuid(result);
		//create address for base currency
		if (isBaseEth) {
			List<Currency> lstCurrencies = currencyService.getAll();
			for (String i : DConstants.BASE_ETH) {
				if (!i.equals(code)) {
					for (Currency cEntity : lstCurrencies) {
						if (i.equals(cEntity.getCode())) {
							model.setCurrency(i);
							model.setCurrencyId(cEntity.getId());
							addressRepository.addAddress(model);
						}
					}

				}
			}
		}

		return model;
	}

	private boolean isBaseExtra(String currency) {
		if (DConstants.ACCOUNT_BASE.XRP.equals(currency) || DConstants.ACCOUNT_BASE.EOS.equals(currency)) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private AddressModel getAddressWallet(AddressModel model, String url) throws Exception {
		// call API wallet
		Map<String, String> obj = new HashMap<>();
		obj.put("wallet_id", getWalletId(model.getCurrencyId() , model.getBrokerId()));
		Map<String, String> res = restTemplate.postForObject(url, obj, Map.class);
		if (res != null && !res.isEmpty()) {
			model.setAddress(res.get("address"));
			model.setPath(res.get("path"));
		} else {
			throw new Exception("walletResponse is null for generating address");
		}
		return model;
	}

	private AddressModel getAddressCurrencyBaseEth(AddressModel model ,String url) throws Exception {
		String currency = model.getCurrency();
		Long userId = model.getUserId();
		Integer currencyId = model.getCurrencyId();
        Integer brokerId = model.getBrokerId();

		AddressModel walletResponse = null;
		Optional<Address> addressEntity = addressRepository.findByUserIdAndCurrencyAndStatus(userId, currency,
				DConstants.ADDRESS.ACTIVE);
		if (!addressEntity.isPresent()) {
			boolean isGen = false;
			Optional<Address> address = null;
			for (String base : DConstants.BASE_ETH) {
				address = addressRepository.findByUserIdAndCurrencyAndStatus(userId, base, DConstants.ADDRESS.ACTIVE);
				if (address.isPresent()) {
					isGen = false;
					break;
				}
				isGen = true;
			}
			if(isGen) {
				walletResponse = getAddressWallet(model, url);
			}else {
				walletResponse = new AddressModel(currency, address.get().getAddress(), address.get().getPath(),
						address.get().getExtraUuid(), currencyId, userId, brokerId);
			}
		}else {
			walletResponse = getAddressWallet(model, url);
		}
		return walletResponse;
	}

	private AddressModel getAddressBaseExtra(AddressModel model, String url) throws Exception {
		//AddressModel walletResponse = new AddressModel();
		// check exist balance
		List<Address> addressObj = addressRepository.findByCurrencyIdAndBrokerId(model.getCurrencyId(), model.getBrokerId());
		if (addressObj == null || addressObj.isEmpty()) {
			model = getAddressWallet(model, url);
		} else {
			String address = addressObj.get(0).getAddress();
			model.setAddress(address);
			model.setPath(addressObj.get(0).getPath());
		}
		return model;
	}



	private Address isValidWithdraw(WithdrawCreationRequest withdrawRequest,Currency currency) throws CMSException ,Exception{
		long userId = withdrawRequest.getUserId();
		Integer currencyId = withdrawRequest.getCurrency_id();
		String code =currency.getCode().toUpperCase();
		Integer decimalAmount = currency.getDecimalAmount();
		BigDecimal amount = withdrawRequest.getAmount();
		//
		Optional<BrokerCurrency> bCurrencyOpt = brokerCurrencyService.findOneByBrokerIdAndCurrencyId(withdrawRequest.getBrokerId(), currencyId);
		if(!bCurrencyOpt.isPresent()) throw new Exception("Broker Currency is null ");
		BrokerCurrency brokerCurrency = bCurrencyOpt.get();
		//check amount request
		if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
			log.error("Invalid withdraw {} amount {}", withdrawRequest.getCurrency(), withdrawRequest.getAmount());
			throw new CMSException(ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getCode(), ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getMessage());
		}
		BigDecimal unit = new BigDecimal(Math.pow(10, decimalAmount));
		BigDecimal minWd = new BigDecimal(brokerCurrency.getMinWithdraw());
		BigDecimal minFeeWithdraw =new BigDecimal(brokerCurrency.getFeeWithdraw());
		BigDecimal minValue = minWd.divide(unit);
		BigDecimal minFreeValue = minFeeWithdraw.divide(unit);
		//check minimize amount withdraw
		if (amount.compareTo(minValue) < 0) {
			log.error("Invalid withdraw {} amount {} minWithdraw {}", withdrawRequest.getCurrency(),
					withdrawRequest.getAmount(), brokerCurrency.getMinWithdraw());
			throw new CMSException(ErrorConstant.Withdraw.ERR_MIN_AMOUNT.getCode(), ErrorConstant.Withdraw.ERR_MIN_AMOUNT.getMessage());
		}
		//check min Fee Withdraw
		if (amount.compareTo(minFreeValue) <= 0) {
			log.error("Invalid minFeeWithdraw withdraw {} amount {} minFeeWithdraw {}", withdrawRequest.getCurrency(),
					withdrawRequest.getAmount(), brokerCurrency.getFeeWithdraw());
			throw new CMSException(ErrorConstant.Withdraw.ERR_MIN_FEE.getCode(), ErrorConstant.Withdraw.ERR_MIN_FEE.getMessage());
		}
		//check available balance
		BigInteger availableBalance = BigInteger.ZERO;
		List<BalanceEntity> lstBalances = balanceRepository.findAllByUserIdAndCurrencyId(userId, currencyId);
		for (BalanceEntity balance : lstBalances) {
			availableBalance = availableBalance.add(balance.getAmount());
		}
		if(availableBalance.compareTo(BigInteger.ZERO) <=0) {
			log.error("not enough money to withdraw {} amount {}", withdrawRequest.getCurrency(), withdrawRequest.getAmount());
			throw new CMSException(ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getCode(), ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getMessage());
		}
		if(new BigDecimal(availableBalance).divide(unit).compareTo(amount) <=0) {
			log.error("not enough money to withdraw {} amount {}", withdrawRequest.getCurrency(), withdrawRequest.getAmount());
			throw new CMSException(ErrorConstant.Withdraw.ERR_NOT_ENOUGHT_MONEY.getCode(), ErrorConstant.Withdraw.ERR_NOT_ENOUGHT_MONEY.getMessage());
		}
		//check address
		Optional<Address> addressOption = addressRepository.findByUserIdAndCurrencyAndStatus(userId, code,DConstants.ADDRESS.ACTIVE);
		if(addressOption.isPresent()) {
			Address entity = addressOption.get();
			if(StringUtils.isEmpty(addressOption.get().getAddress())) {
				log.error("address is null or empty {}", addressOption.toString());
				throw new CMSException(ErrorConstant.Withdraw.ERR_ADDRESS_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_ADDRESS_NULL_EMPTY.getMessage());
			}else {
				if(!isBaseExtra(code)) {
					//check address
					if(entity.getAddress().equals(withdrawRequest.getAddress())) {
						log.error("Error withdraw in the same address {}", addressOption.toString());
						throw new CMSException(ErrorConstant.Withdraw.ERR_WITHDRAW_SAME_ADDRESS.getCode(), ErrorConstant.Withdraw.ERR_WITHDRAW_SAME_ADDRESS.getMessage());
					}
				}else {
					//check uuid
					if(entity.getExtraUuid().equals(withdrawRequest.getExtraUuid())) {
						log.error("Error withdraw in the same uuid {}", addressOption.toString());
						throw new CMSException(ErrorConstant.Withdraw.ERR_WITHDRAW_SAME_UUID.getCode(), ErrorConstant.Withdraw.ERR_WITHDRAW_SAME_UUID.getMessage());
					}
				}

			}

		}else {
			log.error("address is null or empty {}", addressOption.toString());
			throw new CMSException(ErrorConstant.Withdraw.ERR_ADDRESS_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_ADDRESS_NULL_EMPTY.getMessage());
		}
		return addressOption.get();
	}



	@Override
	public WithdrawEntity createWithdraw(WithdrawCreationRequest withdrawRequest) throws CMSException ,Exception {
		Optional<Currency> currencyOp= currencyService.findOne(withdrawRequest.getCurrency_id());
		if(!currencyOp.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getMessage());
		WithdrawEntity entity = null;
		Currency currencyEntity = currencyOp.get();
		//validation
		Address addressEntity = isValidWithdraw(withdrawRequest ,currencyEntity);
		// balance update
        long amount = Math.round(withdrawRequest.getAmount().doubleValue() * PowerOfTens.getPowerDouble(currencyEntity.getDecimalAmount()) * -1);
			//save withdraw
        entity = new WithdrawEntity(withdrawRequest, currencyEntity, addressEntity);


        saveWithdraw(entity , withdrawRequest.getBalance());
        coreBalanceService.updateBalance(withdrawRequest.getUserId(), withdrawRequest.getCurrency_id().intValue(),
            amount,  DConstants.BALANCE_EVENT.REQUEST_WITHDRAW, entity.getId());
		return entity;
	}

	@Transactional

	public WithdrawEntity saveWithdraw(WithdrawEntity entity , BigDecimal balance) {
		WithdrawHistoryEntity wh = new WithdrawHistoryEntity();
		WithdrawEntity response = withdrawRepository.save(entity);
		BeanUtils.copyProperties(response, wh);

		if (balance != null){
		 wh.setBalanceBefore(balance);
        }
		withdrawHistoryRepository.save(wh);
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public WalletInfoResponse findWallet(String currency, String accountName) throws Exception {
		if (currency == null) {
			log.error("ERROR currency null");
			return null;
		}
		currency = currency.toUpperCase();
		String url = String.format("%s:%s", walletServer, walletUrl);
		log.info("find Wallet ID Call URL {}, PARAMS {} ", url, currency);
		Map<String, String> mapIn = new HashMap<>();
		mapIn.put("currency_id", currency);
		if (DConstants.ACCOUNT_BASE.EOS.equalsIgnoreCase(currency)) {
			mapIn.put("new_account_name", accountName);
		}
		Map<String,String> mapOut = restTemplate.postForObject(url, mapIn, Map.class);
		log.info("find Wallet ID CALL URL RESPONSE {}", mapOut.toString());
		WalletInfoResponse response= new WalletInfoResponse(mapOut.get("currency_id"),
				mapOut.get("multisig"),
				mapOut.get("storage"),
				mapOut.get("hd_scope"),
				mapOut.get("created_at"),
				mapOut.get("updated_at"),
				mapOut.get("id"));
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> newBlockRequest(WalletNewBlock block) throws Exception {
		String url = walletServer + walletBlock;
		log.info("confirm New One Block Call URL {}, PARAMS {} ", url, block);
		Map<String,String> mapOut = restTemplate.postForObject(url, block, Map.class);
		log.info("find Wallet ID CALL URL RESPONSE {}", mapOut.toString());
		return mapOut;
	}

	private String exchangeUnit(BigDecimal amountValue, BigDecimal fee, Integer baseUnit, Integer decimalAmount) {
		BigInteger amoutWallet = BigInteger.ZERO;
		baseUnit = baseUnit != null ? baseUnit : 0;
		// sub fee
		amountValue = amountValue.subtract(fee);
		// change unit
		if (baseUnit != 0) {
			amoutWallet = amountValue.multiply(BigDecimal.valueOf(Math.pow(10, baseUnit))).toBigInteger();
			return amoutWallet.toString();
		} else {
			amountValue = amountValue.setScale(decimalAmount, BigDecimal.ROUND_DOWN);
			return amountValue.toString();
		}
	};

	private WalletWithdrawResponse accessWithdrawWallet(WalletWithdrawRequest restWalletRequest) throws Exception {
		String url = String.format("%s%s", walletServer, withdrawUrl);
		log.info("withdrawConfirm Call URL {}, PARAMS {} ", url, restWalletRequest.toString());
		WalletWithdrawResponse walletResponse = restTemplate.postForObject(url, restWalletRequest,
				WalletWithdrawResponse.class);
		log.info("withdrawConfirm CALL URL RESPONSE {}", walletResponse.toString());
		return walletResponse;
	}

	private int getStatusCode(String status) {
		switch (status) {
		case "FAILED":
			return DConstants.WALLET_STATUS.FAILED;
		case "PENDING":
			return DConstants.WALLET_STATUS.PENDING;
		default:
			log.info("status response from wallet is not in (PENDING ,SUCCESS )");
			return DConstants.WALLET_STATUS.FAILED;
		}
	}

	@Async
	public String pushMessageTelegram(String msg) throws Exception {
		String urlString = "https://api.telegram.org/bot%s/sendMessage?chat_id=%s&text=%s";
		String text = "[WARNING] " + msg;

		urlString = String.format(urlString, token, chatId, text);

		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		StringBuilder sb = new StringBuilder();
		InputStream is = new BufferedInputStream(conn.getInputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String inputLine = "";
		while ((inputLine = br.readLine()) != null) {
			sb.append(inputLine);
		}
		return sb.toString();
	}

	private void telegramService(String msg, String curency) throws Exception {
		Integer value = mapData.get(curency);
		if (value == null || value == 6) {
			mapData.put(curency, 0);
			// push message
			pushMessageTelegram(msg);
		} else {
			mapData.put(curency, value + 1);
		}
	}

	private void handleAccountBases(ConfirmWithdrawModel obj) throws Exception{
	String currency = obj.getCurrency();
		WalletWithdrawResponse walletResponse = null;
		List<WithdrawModel> recipients = new ArrayList<>();
		//WithdrawModel model = new WithdrawModel(obj.getAddress(), obj.getAmount());
		WithdrawModel model = new WithdrawModel(obj.getAddress(), obj.getAmount(), obj.getAddress(), obj.getDestinationExtraUuid(),
			obj.getDestinationExtraUuid(), "");
		recipients.add(model);
		try {
			WalletWithdrawRequest restWalletRequest = new WalletWithdrawRequest(getWalletId(obj.getCurrencyId(),obj.getBrokerId()),
					recipients);
			restWalletRequest.setToken_name(currency.toLowerCase());
			walletResponse = accessWithdrawWallet(restWalletRequest);
		} catch (Exception e) {
			log.error("call a service wallet fail '{}'", e);
			log.info(e.getMessage());
		}
		if (walletResponse != null) {
			//withdraw success
			String txId = walletResponse.getTransfer().getTx_id();
			int height = walletResponse.getTransfer().getHeight();
			String statusNm = walletResponse.getTransfer().getStatus();
			Integer code = walletResponse.getTransfer().getCode();
			int statusIn = 0;
			if (statusNm != null) {
				statusIn = getStatusCode(statusNm);
			}
			if (statusIn == 0 && code != null && code == -1) {
				try {
					String msg = walletResponse.getTransfer().getMessage();
					telegramService(msg, currency);
				} catch (Exception e) {
					log.error("telegram push mesage error {}", e);
				}

			}
			// note : if status = fail ,won't retry
			if (statusIn != 0) {
				Long withdrawId = obj.getWithdrawId();
				if (withdrawId != null) {
					ConfirmRequestModel requests = new ConfirmRequestModel(txId, height, statusIn);
					withdrawComplete(requests, Arrays.asList(withdrawId));
					log.debug("Withdraw complete  withdrawId  '{}' '{}' ", withdrawId);
				}else {
					log.error("Proc WithdrawComplete fail one withdrawId ,responseCode  '{}' '{}' ", withdrawId);
				}
			}
		}
	}

	@Transactional
	//TODO sau Duong sua lai sau
	public void withdrawComplete(ConfirmRequestModel requests ,List<Long> lstWithdrawId) throws Exception{
		// transaction create
		List<WithdrawHistoryEntity> lstWithdrawHis = new ArrayList<WithdrawHistoryEntity>();
        List<WithdrawEntity> lstWithdraw = new ArrayList<WithdrawEntity>();

		for (Long id : lstWithdrawId) {
			Optional<WithdrawHistoryEntity> wdho = withdrawHistoryRepository.findById(id);
			if (wdho.isPresent()) {
				WithdrawHistoryEntity wdh = wdho.get();
				if (wdh.getAmount() != null && wdh.getAmount().compareTo(BigDecimal.ZERO) > 0) {
					wdh.setTx(requests.getTxId());
                    wdh.updated();

					// chua tru tien
					wdh.setStatus(DConstants.WITHDRAW_STATUS.CONFIRM);
					lstWithdrawHis.add(wdh);
                    Optional<Currency> currencyOp= currencyService.findOne(wdh.getCurrencyId());
                    if(!currencyOp.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getMessage());
                    WithdrawEntity entity = new WithdrawEntity();
                    Currency currencyEntity = currencyOp.get();
                    long amount = Math.round(wdh.getAmount().doubleValue() * PowerOfTens.getPowerDouble(currencyEntity.getDecimalAmount()) * -1);
                    // CALL SANG CORE TRU TIEN
                    coreBalanceService.updateBalance(wdh.getUserId(), wdh.getCurrencyId().intValue(), amount,  DConstants.BALANCE_EVENT.CONFIRM_WITHDRAW,wdh.getId());
						//delete wd

                    BeanUtils.copyProperties(wdh ,entity);
                    lstWithdraw.add(entity);


				}

			}
		}
		//update batch withdraw his
		withdrawHistoryRepository.saveAll(lstWithdrawHis);
		withdrawRepository.saveAll(lstWithdraw);
	}

	@Override
	@Scheduled(fixedRate = 120000)
	public void withdrawListeners() throws Exception {
		log.info("Scan data wallet  {}", countTime);
		countTime++;
		Set<CurrencyDto> lstBatchCurrencies = new HashSet<CurrencyDto>();//lst currency to run batch
		Set<Integer> lstBrokers = new HashSet<Integer>();
		List<ConfirmWithdrawModel> lstConfirm = withdrawRepository.findByStatus(DConstants.WITHDRAW_STATUS.PROCESSING);
		if (lstConfirm != null && !lstConfirm.isEmpty()) {
			lstConfirm.forEach(obj -> {
				String currency = obj.getCurrency();
				BigDecimal amountValue = obj.getAmountValue();
				BigDecimal fee = obj.getFee() == null ? BigDecimal.ZERO : obj.getFee();
				//change currency unit and add fee
				List<Currency> lstCurrencies = currencyService.getAll();
				lstCurrencies.forEach(i -> {
					if (i.getCode().equals(currency)) {
						String amount = exchangeUnit(amountValue, fee, i.getBaseUnit(),i.getDecimalAmount());
						obj.setAmount(amount);
					}
				});
				//fill currency type
				if (!DConstants.ACCOUNT_BAT.BTC.equalsIgnoreCase(currency) && !DConstants.ACCOUNT_BAT.LTC.equalsIgnoreCase(currency)) {
					try {
						handleAccountBases(obj);
					} catch (Exception e) {
						log.error("Confirm withdraw for Account_base error {}", e);
					}
				} else {

					lstBrokers.add(obj.getBrokerId());

					//handle account UXO TODO
					//btc || btcx ||btcy..
					Currency currencyEntity = currencyService.findOne(currency);
					Integer maxTime = currencyEntity.getMaxTime() / 2;
					Integer maxSize = currencyEntity.getMaxSize();
					Integer minSize = currencyEntity.getMinSize();
					//count
					long countCurrency = lstConfirm
							  .stream()
							  .filter(c -> currency.equals(c.getCurrency()))
							  .count();
					//TODO Should setting min ,max in broker_currency table--> Duong will change
					if (countTime < maxTime) {
						if (countCurrency >= maxSize) {
							CurrencyDto dto =new CurrencyDto(currencyEntity.getId(), currency);
							lstBatchCurrencies.add(dto);
						}
					} else {
						//start when it's time
						if (countCurrency >= minSize) {
							CurrencyDto dto =new CurrencyDto(currencyEntity.getId(), currency);
							lstBatchCurrencies.add(dto);
						}
						countTime = 0;
					}
				}
			});

			//handle account normal
			handleAccount(lstBatchCurrencies, lstBrokers, lstConfirm);

		}
	}

	private void handleAccount(Set<CurrencyDto> lstBatchCurrencies ,Set<Integer> lstBrokers ,List<ConfirmWithdrawModel> lstConfirm) throws Exception {
		if (lstBatchCurrencies.isEmpty() || lstBrokers.isEmpty() || lstConfirm.isEmpty()) return;

		List<Long> lstWithdrawId = new ArrayList<>();
		for (Integer brokerId : lstBrokers) {

			WalletWithdrawResponse walletResponse = null;

			for (CurrencyDto dto : lstBatchCurrencies) {
				List<WithdrawModel> recipients = new ArrayList<>();
				String walletId	= "";
				try {
					walletId = getWalletId(dto.getId() ,brokerId);
				} catch (Exception e) {
				}
				for (ConfirmWithdrawModel wd : lstConfirm) {

					Integer idBroker = wd.getBrokerId();
					String code = wd.getCurrency();

					if(dto.getCode().equals(code) && brokerId == idBroker) {
						WithdrawModel model = new WithdrawModel(wd.getAddress(), wd.getAmount());
						recipients.add(model);
						lstWithdrawId.add(wd.getWithdrawId());
					}
				}
				WalletWithdrawRequest restWalletRequest = new WalletWithdrawRequest(walletId, recipients);
				try {
					walletResponse = accessWithdrawWallet(restWalletRequest);
				} catch (Exception e) {
					log.error("call service wallet fail {}", e.getMessage());
				}
				if (walletResponse != null) {
					// withdraw success
					String txId = walletResponse.getTransfer().getTx_id();
					int height = walletResponse.getTransfer().getHeight();
					String statusNm = walletResponse.getTransfer().getStatus();
					Integer code = walletResponse.getTransfer().getCode();
					int statusIn = 0;
					if (statusNm != null) {
						statusIn = getStatusCode(statusNm);
					}
					if (statusIn == 0 && code != null && code == -1) {
						try {
							String msg = walletResponse.getTransfer().getMessage();
							telegramService(msg, dto.getCode());
						} catch (Exception e) {
							log.error("telegram push mesage error {}", e);
						}
					}
					// note : if status = fail will not retry
					if (!lstWithdrawId.isEmpty() && statusIn != 0) {
						ConfirmRequestModel requests = new ConfirmRequestModel(txId, height, statusIn);
						withdrawComplete(requests, lstWithdrawId);
						log.debug("Withdraw complete  lstWithdrawId  '{}' ", lstWithdrawId);
					} else {
						log.error("WithdrawComplete fail list withdrawId ,responseCode  '{}' '{}' ", lstWithdrawId);
					}

				} else {
					log.error("Call Service withdraw response fail !");
				}
			}

		}
	}

	@Override
	@Transactional
	public WithdrawEntity withdrawConfirmed(RestConfirmWithdrawRequest withdrawRequest) throws Exception ,CMSException {
		long id = withdrawRequest.getWithdrawId();
		long userId = withdrawRequest.getUserId();
        WithdrawEntity wd = withdrawRepository.getOne(id);
		BigDecimal amountToBTC = withdrawRequest.getAmountToBTC();
		//validation
		if(wd == null) throw new CMSException(ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getCode(), ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getMessage());
		if (amountToBTC == null) throw new Exception("amountToBTC is null");
		if(!wd.getUserId().equals(userId)) throw new CMSException(ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getCode(), ErrorConstant.Withdraw.ERR_INVALID_AMOUNT.getMessage());

		if(wd.getStatus() != DConstants.WITHDRAW_STATUS.PENDING ) throw new CMSException(ErrorConstant.Withdraw.ERR_STATUS_INVALID.getCode(), ErrorConstant.Withdraw.ERR_STATUS_INVALID.getMessage());
		Optional<UserSetting> userSetting = userSettingService.findOne(id);
		if(!userSetting.isPresent()) throw new Exception("UserSetting is null");
		UserSetting userEntity = userSetting.get();
		BigDecimal dailyWdAmount = userEntity.getDailyWdAmount() != null ? userEntity.getDailyWdAmount() : BigDecimal.ZERO;
		BigDecimal wdDailyLimit = userEntity.getWdDailyLimit() != null ? userEntity.getWdDailyLimit() : BigDecimal.ZERO;
		if(amountToBTC.add(dailyWdAmount).compareTo(wdDailyLimit) > 0) throw new CMSException(ErrorConstant.Withdraw.ERR_OVER_AMOUNT_WITHDRAW.getCode(), ErrorConstant.Withdraw.ERR_OVER_AMOUNT_WITHDRAW.getMessage());
		else {
			userEntity.setDailyWdAmount(dailyWdAmount.add(amountToBTC));
		}
		userSettingService.save(userEntity);
		//check max allow withdraw
		Optional<BrokerCurrency> brokerCu = brokerCurrencyService.findOneByBrokerIdAndCurrencyId(withdrawRequest.getBrokerId(), wd.getCurrencyId());

		BigDecimal maxAllowWd = brokerCu.isPresent() ? brokerCu.get().getMaxAllowWithdraw() : BigDecimal.ZERO;
		int status = DConstants.WITHDRAW_STATUS.PROCESSING;
		if (wd.getAmount().compareTo(maxAllowWd) > 0) {
			status = DConstants.WITHDRAW_STATUS.WAITING;
		}
		wd.setStatus(status);
		wd.updated();
		//save wd
		withdrawRepository.save(wd);
		//copy properties
		WithdrawHistoryEntity wdh = new WithdrawHistoryEntity();
		BeanUtils.copyProperties(wd, wdh);
		//save wdh
		withdrawHistoryRepository.save(wdh);
		return wd;
	}

    @Override
    public WithdrawEntity userCancelWithdraw(UpdateWithdrawRequest request) throws Exception {
        Optional<WithdrawEntity> wdOptional = withdrawRepository.findById(request.getWithdrawId());

        if(!wdOptional.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getCode(),
            ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getMessage());

        WithdrawEntity wd = wdOptional.get();
        if(wd.getStatus() != DConstants.WITHDRAW_STATUS.PENDING )
            throw new CMSException(ErrorConstant.Withdraw.ERR_STATUS_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_STATUS_INVALID.getMessage());
        if (wd.getBrokerId().intValue() != request.getBrokerId())
            throw new CMSException(ErrorConstant.Withdraw.ERR_BROKERID_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_BROKERID_INVALID.getMessage());

        if (wd.getUserId() != request.getUserId())
            throw new CMSException(ErrorConstant.Withdraw.ERR_USERID_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_USERID_INVALID.getMessage());

        wd.setStatus(DConstants.WITHDRAW_STATUS.REQUEST_CANCELLED);
        WithdrawHistoryEntity wdh = new WithdrawHistoryEntity();
        BeanUtils.copyProperties(wd, wdh);

        Optional<Currency> currencyOp = currencyService.findOne(wd.getCurrencyId());
        if(!currencyOp.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getMessage());
        Currency currencyEntity = currencyOp.get();
        long amount = Math.round(wd.getAmount().doubleValue() * PowerOfTens.getPowerDouble(currencyEntity.getDecimalAmount()) * -1);
        coreBalanceService.updateBalance(request.getUserId(), wd.getCurrencyId().intValue(), amount,  DConstants.BALANCE_EVENT.CANCEL_WITHDRAW, wd.getId());
        wdh.updated();
        withdrawHistoryRepository.save(wdh);
        withdrawRepository.delete(wd);

        // update balance
        return wd;
    }

    @Override
    public WithdrawHistoryEntity adminUpdateWithdraw(UpdateWithdrawRequest request) throws Exception {

        Optional<WithdrawEntity> wdOptional = withdrawRepository.findById(request.getWithdrawId());

        if(!wdOptional.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getCode(),
            ErrorConstant.Withdraw.ERR_RECORD_NOT_EXIST.getMessage());

        WithdrawEntity wd = wdOptional.get();

        if(wd.getStatus() != DConstants.WITHDRAW_STATUS.PENDING &&
            wd.getStatus() != DConstants.WITHDRAW_STATUS.WAITING &&
            wd.getStatus() != DConstants.WITHDRAW_STATUS.PROCESSING)
            throw new CMSException(ErrorConstant.Withdraw.ERR_STATUS_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_STATUS_INVALID.getMessage());

        if (wd.getBrokerId().intValue() != request.getBrokerId())
            throw new CMSException(ErrorConstant.Withdraw.ERR_BROKERID_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_BROKERID_INVALID.getMessage());

        if (wd.getUserId() != request.getUserId())
            throw new CMSException(ErrorConstant.Withdraw.ERR_USERID_INVALID.getCode(),
                ErrorConstant.Withdraw.ERR_USERID_INVALID.getMessage());
        int statusUpdate = request.getAction() == 1 ?  DConstants.WITHDRAW_STATUS.PROCESSING : DConstants.WITHDRAW_STATUS.REJECT;

        wd.setStatus(statusUpdate);
        WithdrawHistoryEntity wdh = new WithdrawHistoryEntity();
        BeanUtils.copyProperties(wd, wdh);
        wdh.setAccountAdmin(request.getAdminAccount());
        if (statusUpdate == DConstants.WITHDRAW_STATUS.CANCELLED){
            Optional<Currency> currencyOp = currencyService.findOne(wd.getCurrencyId());
            if(!currencyOp.isPresent()) throw new CMSException(ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getCode(), ErrorConstant.Withdraw.ERR_CURRENCY_NULL_EMPTY.getMessage());
            Currency currencyEntity = currencyOp.get();
            long amount = Math.round(wd.getAmount().doubleValue() * PowerOfTens.getPowerDouble(currencyEntity.getDecimalAmount()) * -1);
            coreBalanceService.updateBalance(request.getUserId(), wd.getCurrencyId().intValue(), amount,  DConstants.BALANCE_EVENT.CANCEL_WITHDRAW, wd.getId());
            withdrawRepository.delete(wd);
        } else {
            withdrawRepository.save(wd);
        }

        wdh.setUpdated(new Timestamp(System.currentTimeMillis()));
        withdrawHistoryRepository.save(wdh);

        return wdh;
    }

    private boolean isValidDeposit(DepositRequestModel depositRequest) {
		Currency currency = currencyService.findOne(depositRequest.getCurrency().toUpperCase());
		if (currency == null) {
			log.error("isValidDeposit Not exist currency {} ", depositRequest.getCurrency());
			return false;
		}
		if (DConstants.ACCOUNT_BASE.XRP.equals(depositRequest.getCurrency())) {
			if (StringUtils.isEmpty(depositRequest.getDestination_tag())) {
				log.error("Invalid deposit Destination_tag is null or empty {} {}", depositRequest.getCurrency(),
						depositRequest.getAmount());
				return false;
			}
		}
		// || DConstants.ACCOUNT_BASE_UPPER.EOS.equals(depositRequest.getCurrency())
		if (DConstants.ACCOUNT_BASE.EOS.equals(depositRequest.getCurrency())) {
			if (StringUtils.isEmpty(depositRequest.getMemo())) {
				log.error("Invalid deposit memo is null or empty {} {}", depositRequest.getCurrency(),
						depositRequest.getAmount());
				return false;
			}
		}
		//TODO duong check
//		BigDecimal amount = depositRequest.getAmount();
//		BigDecimal mindeposit =new BigDecimal(currency.getMinDeposit());
//		if (amount.compareTo(mindeposit) < 0) {
//			log.error("isValidDeposit {} amount {} < {}", depositRequest.getCurrency(), depositRequest.getAmount(),
//					mindeposit);
//			return false;
//		}
		return true;
	}

	@Override
	public Integer createNewDeposit(DepositRequestModel depositRequest) throws Exception {
		log.info("createNewDeposit wallet request {}" ,depositRequest);
		String currency = depositRequest.getCurrency().toUpperCase();
		depositRequest.setCurrency(currency);
		BigDecimal amount = depositRequest.getAmount();
		String extraUuid;
		if (DConstants.ACCOUNT_BASE.XRP.equals(currency)) {

			extraUuid = StringUtils.isEmpty(depositRequest.getDestination_tag()) ? "" : depositRequest.getDestination_tag().toString();
		} else if (DConstants.ACCOUNT_BASE.EOS.equals(currency)) {

			extraUuid = StringUtils.isEmpty(depositRequest.getMemo()) ? "" : depositRequest.getMemo().toString();
		} else {
			extraUuid = "";
		}
		if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
			log.error("isValidDeposit {} amount {} < 0", depositRequest.getCurrency(), depositRequest.getAmount());
			return 1;
		}
		boolean isValidDeposit = isValidDeposit(depositRequest);
		Integer depositStatus = DConstants.DEPOSIT_STATUS.PENDING;
		if (!isValidDeposit) {
			log.error("newdeposit request invalid {}", depositRequest.toString());
			depositStatus = DConstants.DEPOSIT_STATUS.ERROR;
		}
		Optional<Address> addressEntity = addressRepository.findOneByAddressAndStatusAndCurrency(depositRequest.getAddress(),
				DConstants.ADDRESS.ACTIVE, currency);
		// excute
		return createDeposit(depositRequest, depositStatus ,extraUuid ,addressEntity.get());
	}

	@Transactional
	public String saveDeposit(DepositEntity depositEntity) {
		 DepositEntity source = depositRepository.save(depositEntity);
		 DepositHistoryEntity depoHis = new DepositHistoryEntity();
		 BeanUtils.copyProperties(source, depoHis);
		 depositHistoryRepository.save(depoHis);
		 return source.getId();
	}

	@Transactional

	public Integer createDeposit(DepositRequestModel depositRequest , Integer depositStatus , String extraUuid, Address addressEntity) throws Exception, CMSException {
		//init
		long depositTime = System.currentTimeMillis();
		Timestamp now = new Timestamp(depositTime);
		long userId = addressEntity.getUserId();
		BigDecimal amount  = depositRequest.getAmount();
		String tx = depositRequest.getTx_id();
		String address = depositRequest.getAddress();
		Long includedBlock = depositRequest.getIncluded_in_block();
		Long confirmed = depositRequest.getConfirmations();
		Long targetConfirm = depositRequest.getTarget_confirm();
		Integer currencyId = addressEntity.getCurrencyId();
        Optional<UserLogin> userLogin = userLoginRepository.findById(userId);
        String email = userLogin.isPresent() ? userLogin.get().getEmail() : "user not found";
		if (depositStatus == DConstants.DEPOSIT_STATUS.ERROR) {
			DepositEntity depo = depositRepository.checkExist(tx, address, depositStatus, extraUuid);

			if (depo == null) {

                depo = new DepositEntity(userId, currencyId, depositRequest, DConstants.DEPOSIT_STATUS.ERROR, extraUuid, addressEntity.getBrokerId(), email);
				depositRepository.save(depo);
			}
			return DConstants.DEPOSIT_STATUS.ERROR_VALIDATION;
		} else {
			DepositEntity depo = depositRepository.checkExist(tx, address, DConstants.DEPOSIT_STATUS.PENDING, extraUuid);

			if (confirmed >= targetConfirm) {
				// access GG TODO
				GatewayModel model = new GatewayModel(userId, currencyId, amount.doubleValue());
				RestRequest restRequest = new RestRequest(env);
				Optional<DepositHistoryEntity> deHis = depositHistoryRepository.findOneByTxAndAddress(tx, address, DConstants.DEPOSIT_STATUS.CONFIRMED, extraUuid);
				if (deHis.isPresent()) {
					return DConstants.DEPOSIT_STATUS.ERROR_OVER;
				} else {
					if (depo == null) {
						// insert deposit
                        DepositEntity dep  = new DepositEntity(userId, currencyId, depositRequest, DConstants.DEPOSIT_STATUS.CONFIRMED, extraUuid, addressEntity.getBrokerId(), email);
						String id = saveDeposit(dep);
						// delete deposit
						depositRepository.deleteById(id);
						// insert user_log
						UserLog uLog = new UserLog(String.valueOf(userId), "DEPOSIT_COMPLETED", "DEPOSIT", now);
						userLogRepository.save(uLog);
						// update balance
						GatewayResponse responseGateway = restRequest.accessGateway(model);
						if (responseGateway.getError() != 0) {
							throw new CMSException(responseGateway.getError(), "Gateway " + responseGateway.getMessage());
						}

					} else {
						// delete deposit
						depositRepository.deleteById(depo.getId());
						// update history
						Optional<DepositHistoryEntity> deHisOp = depositHistoryRepository.findOneByTxAndAddress(tx, address, DConstants.DEPOSIT_STATUS.PENDING, extraUuid);
						DepositHistoryEntity dsHis = deHisOp.get();
						dsHis.setStatus(DConstants.DEPOSIT_STATUS.CONFIRMED);
						dsHis.setConfirmed(confirmed);
						dsHis.setUpdated(now);
						depositHistoryRepository.save(dsHis);
						// insert user_log
						UserLog uLog = new UserLog(String.valueOf(userId), "DEPOSIT_COMPLETED", "DEPOSIT", now);
						userLogRepository.save(uLog);
						// update balance
						GatewayResponse responseGateway = restRequest.accessGateway(model);
						if (responseGateway.getError() != 0) {
							throw new CMSException(responseGateway.getError(),
									"Gateway " + responseGateway.getMessage());
						}
					}
				}
			} else {
				if (depo == null) {

					// insert to DEPOSIT ,history
                    DepositEntity dep  = new DepositEntity(userId, currencyId, depositRequest, DConstants.DEPOSIT_STATUS.PENDING, extraUuid, addressEntity.getBrokerId(), email);
                    dep.setCurrencyId(currencyId);
                    if (userLogin.isPresent()){
                        dep.setEmail(userLogin.get().getEmail());
                    }
					saveDeposit(dep);
				} else {
					// UPDATE [deposit]
					depo.setConfirmed(confirmed);
					depo.setIncludedBlock(includedBlock);
					depo.setUpdated(now);
					depositRepository.saveAndFlush(depo);
				}
			}

		}
		log.info("new depositRequest deposit response '{}' '{}' '{}'", DConstants.DEPOSIT_STATUS.CONFIRMED, depositRequest.getTx_id(),
				depositRequest.getAddress());
		return DConstants.DEPOSIT_STATUS.CONFIRMED;
	}

	@Override
	public WebhookResponseModel webhook(WebhookRequestModel webhookRequest) throws Exception {
		String url = String.format("%s%s", walletServer, webhookUrl);
		log.info("webhookRequest Call URL {}, PARAMS {} ", url, webhookRequest.toString());
		WebhookResponseModel walletResponse = restTemplate.postForObject(url, webhookRequest,
				WebhookResponseModel.class);
		log.info("withdrawConfirm CALL URL RESPONSE {}", walletResponse.toString());
		return walletResponse;
	}

	@Override
	public AddressModel getAddressByUserIdAndCurrency(Long userId, String currency) throws Exception {
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(currency))
			throw new Exception("UserId or currency is Null or Empty");
		currency = currency.toUpperCase();
		Optional<Address> entity = addressRepository.findByUserIdAndCurrencyAndStatus(userId, currency, DConstants.ADDRESS.ACTIVE);
		if (entity.isPresent() && !StringUtils.isEmpty(entity.get().getAddress())) {
			return new AddressModel(entity.get());
		} else {
			AddressModel model = generationAddress(userId, entity.get().getCurrencyId(), entity.get().getBrokerId());
			if (model != null) {
				return model;
			} else {
				log.error("Generate address error {} ,{}", userId, currency);
				throw new Exception("[WALLET_ADDRESS]Generation address error");
			}
		}
	}


}
