package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.AuthorityService;
import com.hihexa.cms.domain.Authority;
import com.hihexa.cms.repository.AuthorityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Authority}.
 */
@Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

    private final Logger log = LoggerFactory.getLogger(AuthorityServiceImpl.class);

    private final AuthorityRepository authorityRepository;

    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    /**
     * Save a authority.
     *
     * @param authority the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Authority save(Authority authority) {
        log.debug("Request to save Authority : {}", authority);
        return authorityRepository.save(authority);
    }

    /**
     * Get all the authorities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Authority> findAll(Pageable pageable) {
        log.debug("Request to get all Authorities");
        return authorityRepository.findAll(pageable);
    }


    /**
     * Get one authority by name.
     *
     * @param name the name of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Authority> findOne(String name) {
        log.debug("Request to get Authority : {}", name);
        return authorityRepository.findById(name);
    }

    /**
     * Delete the authority by name.
     *
     * @param name the name of the entity.
     */
    @Override
    public void delete(String name) {
        log.debug("Request to delete Authority : {}", name);
        authorityRepository.deleteById(name);
    }
}
