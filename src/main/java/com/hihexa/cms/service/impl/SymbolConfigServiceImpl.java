package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.SymbolConfigService;
import com.hihexa.cms.domain.SymbolConfig;
import com.hihexa.cms.repository.SymbolConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SymbolConfig}.
 */
@Service
@Transactional
public class SymbolConfigServiceImpl implements SymbolConfigService {

    private final Logger log = LoggerFactory.getLogger(SymbolConfigServiceImpl.class);

    private final SymbolConfigRepository symbolConfigRepository;

    public SymbolConfigServiceImpl(SymbolConfigRepository symbolConfigRepository) {
        this.symbolConfigRepository = symbolConfigRepository;
    }

    /**
     * Save a symbolConfig.
     *
     * @param symbolConfig the entity to save.
     * @return the persisted entity.
     */
    @Override
    public SymbolConfig save(SymbolConfig symbolConfig) {
        log.debug("Request to save SymbolConfig : {}", symbolConfig);
        return symbolConfigRepository.save(symbolConfig);
    }

    /**
     * Get all the symbolConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SymbolConfig> findAll(Pageable pageable) {
        log.debug("Request to get all SymbolConfigs");
        return symbolConfigRepository.findAll(pageable);
    }

    @Override
    public Page<SymbolConfig> findSymbolByBaseCcorCounterCc(String BaseCc, String CounterCc, Pageable pageable) {
        log.debug("Request to get all SymbolConfigs by BaseCc or CounterCc");
        return symbolConfigRepository.findSymbolConfigByBaseCcOrCounterCc(BaseCc,CounterCc,pageable);
    }


    /**
     * Get one symbolConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<SymbolConfig> findOne(Long id) {
        log.debug("Request to get SymbolConfig : {}", id);
        return symbolConfigRepository.findById(id);
    }

    /**
     * Delete the symbolConfig by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SymbolConfig : {}", id);
        symbolConfigRepository.deleteById(id);
    }
}
