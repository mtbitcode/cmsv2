package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.UserLogService;
import com.hihexa.cms.domain.UserLog;
import com.hihexa.cms.repository.UserLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserLog}.
 */
@Service
@Transactional
public class UserLogServiceImpl implements UserLogService {

    private final Logger log = LoggerFactory.getLogger(UserLogServiceImpl.class);

    private final UserLogRepository userLogRepository;

    public UserLogServiceImpl(UserLogRepository userLogRepository) {
        this.userLogRepository = userLogRepository;
    }

    /**
     * Save a userLog.
     *
     * @param userLog the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserLog save(UserLog userLog) {
        log.debug("Request to save UserLog : {}", userLog);
        return userLogRepository.save(userLog);
    }

    /**
     * Get all the userLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserLog> findAll(Pageable pageable) {
        log.debug("Request to get all UserLogs");
        return userLogRepository.findAll(pageable);
    }


    /**
     * Get one userLog by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserLog> findOne(Long id) {
        log.debug("Request to get UserLog : {}", id);
        return userLogRepository.findById(id);
    }

    /**
     * Delete the userLog by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserLog : {}", id);
        userLogRepository.deleteById(id);
    }
}
