package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.BrokerService;
import com.hihexa.cms.domain.Broker;
import com.hihexa.cms.repository.BrokerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Broker}.
 */
@Service
@Transactional
public class BrokerServiceImpl implements BrokerService {

    private final Logger log = LoggerFactory.getLogger(BrokerServiceImpl.class);

    private final BrokerRepository brokerRepository;

    public BrokerServiceImpl(BrokerRepository brokerRepository) {
        this.brokerRepository = brokerRepository;
    }

    /**
     * Save a broker.
     *
     * @param broker the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Broker save(Broker broker) {
        log.debug("Request to save Broker : {}", broker);
        return brokerRepository.save(broker);
    }

    /**
     * Get all the brokers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Broker> findAll(Pageable pageable) {
        log.debug("Request to get all Brokers");
        return brokerRepository.findAll(pageable);
    }


    /**
     * Get one broker by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Broker> findOne(Integer id) {
        log.debug("Request to get Broker : {}", id);
        return brokerRepository.findById(id);
    }

    /**
     * Delete the broker by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Integer id) {
        log.debug("Request to delete Broker : {}", id);
        brokerRepository.deleteById(id);
    }
}
