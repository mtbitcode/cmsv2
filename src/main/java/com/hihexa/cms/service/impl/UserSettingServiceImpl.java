package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.UserSettingService;
import com.hihexa.cms.domain.UserSetting;
import com.hihexa.cms.repository.UserSettingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link UserSetting}.
 */
@Service
@Transactional
public class UserSettingServiceImpl implements UserSettingService {

    private final Logger log = LoggerFactory.getLogger(UserSettingServiceImpl.class);

    private final UserSettingRepository userSettingRepository;

    public UserSettingServiceImpl(UserSettingRepository userSettingRepository) {
        this.userSettingRepository = userSettingRepository;
    }

    /**
     * Save a userSetting.
     *
     * @param userSetting the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserSetting save(UserSetting userSetting) {
        log.debug("Request to save UserSetting : {}", userSetting);
        return userSettingRepository.save(userSetting);
    }

    /**
     * Get all the userSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserSetting> findAll(Pageable pageable) {
        log.debug("Request to get all UserSettings");
        return userSettingRepository.findAll(pageable);
    }


    /**
     * Get one userSetting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserSetting> findOne(Long id) {
        log.debug("Request to get UserSetting : {}", id);
        return userSettingRepository.findById(id);
    }

    /**
     * Delete the userSetting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserSetting : {}", id);
        userSettingRepository.deleteById(id);
    }
}
