package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.BalanceService;
import com.hihexa.cms.domain.BalanceEntity;
import com.hihexa.cms.repository.BalanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BalanceEntity}.
 */
@Service
@Transactional
public class BalanceServiceImpl implements BalanceService {

    private final Logger log = LoggerFactory.getLogger(BalanceServiceImpl.class);

    private final BalanceRepository balanceRepository;

    public BalanceServiceImpl(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    /**
     * Save a balanceEntity.findAllByUserIdAndCurrencyId
     *
     * @param balanceEntity the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BalanceEntity save(BalanceEntity balanceEntity) {
        log.debug("Request to save BalanceEntity : {}", balanceEntity);
        return balanceRepository.save(balanceEntity);
    }

    /**
     * Get all the balanceEntities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BalanceEntity> findAll(Pageable pageable) {
        log.debug("Request to get all BalanceEntities");
        return balanceRepository.findAll(pageable);
    }

    @Override
    public Page<BalanceEntity> findBalanceByUserIdOrCurrencyId(Long userId, Integer currencyId , Pageable pageable) {
        log.debug("Request to get all BalanceEntities filter by userId Or CurrencyId");
        return balanceRepository.findAllByUserIdAndCurrencyId(userId,currencyId,pageable);
    }


    /**
     * Get one balanceEntity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BalanceEntity> findOne(Long id) {
        log.debug("Request to get BalanceEntity : {}", id);
        return balanceRepository.findById(id);
    }

    /**
     * Delete the balanceEntity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BalanceEntity : {}", id);
        balanceRepository.deleteById(id);
    }
}
