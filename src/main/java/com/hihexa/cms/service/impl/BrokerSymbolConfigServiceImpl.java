package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.BrokerSymbolConfigService;
import com.hihexa.cms.domain.BrokerSymbolConfig;
import com.hihexa.cms.repository.BrokerSymbolConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BrokerSymbolConfig}.
 */
@Service
@Transactional
public class BrokerSymbolConfigServiceImpl implements BrokerSymbolConfigService {

    private final Logger log = LoggerFactory.getLogger(BrokerSymbolConfigServiceImpl.class);

    private final BrokerSymbolConfigRepository brokerSymbolConfigRepository;

    public BrokerSymbolConfigServiceImpl(BrokerSymbolConfigRepository brokerSymbolConfigRepository) {
        this.brokerSymbolConfigRepository = brokerSymbolConfigRepository;
    }

    /**
     * Save a brokerSymbolConfig.
     *
     * @param brokerSymbolConfig the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BrokerSymbolConfig save(BrokerSymbolConfig brokerSymbolConfig) {
        log.debug("Request to save BrokerSymbolConfig : {}", brokerSymbolConfig);
        return brokerSymbolConfigRepository.save(brokerSymbolConfig);
    }

    /**
     * Get all the brokerSymbolConfigs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BrokerSymbolConfig> findAll(Pageable pageable) {
        log.debug("Request to get all BrokerSymbolConfigs");
        return brokerSymbolConfigRepository.findAll(pageable);
    }


    /**
     * Get one brokerSymbolConfig by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BrokerSymbolConfig> findOne(Long id) {
        log.debug("Request to get BrokerSymbolConfig : {}", id);
        return brokerSymbolConfigRepository.findById(id);
    }

    /**
     * Delete the brokerSymbolConfig by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BrokerSymbolConfig : {}", id);
        brokerSymbolConfigRepository.deleteById(id);
    }
}
