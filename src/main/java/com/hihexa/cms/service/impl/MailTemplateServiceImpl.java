package com.hihexa.cms.service.impl;

import com.hihexa.cms.service.MailTemplateService;
import com.hihexa.cms.domain.MailTemplate;
import com.hihexa.cms.repository.MailTemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MailTemplate}.
 */
@Service
@Transactional
public class MailTemplateServiceImpl implements MailTemplateService {

    private final Logger log = LoggerFactory.getLogger(MailTemplateServiceImpl.class);

    private final MailTemplateRepository mailTemplateRepository;

    public MailTemplateServiceImpl(MailTemplateRepository mailTemplateRepository) {
        this.mailTemplateRepository = mailTemplateRepository;
    }

    /**
     * Save a mailTemplate.
     *
     * @param mailTemplate the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MailTemplate save(MailTemplate mailTemplate) {
        log.debug("Request to save MailTemplate : {}", mailTemplate);
        return mailTemplateRepository.save(mailTemplate);
    }

    /**
     * Get all the mailTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MailTemplate> findAll(Pageable pageable) {
        log.debug("Request to get all MailTemplates");
        return mailTemplateRepository.findAll(pageable);
    }


    /**
     * Get one mailTemplate by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MailTemplate> findOne(Long id) {
        log.debug("Request to get MailTemplate : {}", id);
        return mailTemplateRepository.findById(id);
    }

    /**
     * Delete the mailTemplate by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MailTemplate : {}", id);
        mailTemplateRepository.deleteById(id);
    }
}
