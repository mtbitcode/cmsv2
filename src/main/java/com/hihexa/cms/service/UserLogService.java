package com.hihexa.cms.service;

import com.hihexa.cms.domain.UserLog;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link UserLog}.
 */
public interface UserLogService {

    /**
     * Save a userLog.
     *
     * @param userLog the entity to save.
     * @return the persisted entity.
     */
    UserLog save(UserLog userLog);

    /**
     * Get all the userLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserLog> findAll(Pageable pageable);


    /**
     * Get the "id" userLog.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserLog> findOne(Long id);

    /**
     * Delete the "id" userLog.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
