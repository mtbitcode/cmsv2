package com.hihexa.cms.security;

/**
 * @author trungnv
 *
 */
public final class AuthoritiesConstants {
	private AuthoritiesConstants() {
	}

	public static final String ADMIN = "ROLE_ADMIN";

	public static final String USER = "ROLE_USER";

	public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	public static final String USER_LOGIN_ADD = "USER_LOGIN_ADD";
	public static final String USER_LOGIN_EDIT = "USER_LOGIN_EDIT";
	public static final String USER_LOGIN_DELETE = "USER_LOGIN_DELETE";
	public static final String USER_LOGIN_VIEW = "USER_LOGIN_VIEW";
	public static final String[] USER_LOGIN = { USER_LOGIN_ADD, USER_LOGIN_EDIT, USER_LOGIN_DELETE, USER_LOGIN_VIEW };

}
