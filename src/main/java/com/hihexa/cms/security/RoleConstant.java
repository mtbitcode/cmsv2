package com.hihexa.cms.security;

/**
 * @author trungnv
 *
 */
public interface RoleConstant {
	 String ADMIN = "ROLE_ADMIN";

	 String USER = "ROLE_USER";

	 String ANONYMOUS = "ROLE_ANONYMOUS";

	 interface USER_LOGIN {
		 String ADD = "USER_LOGIN_ADD";
		 String EDIT = "USER_LOGIN_EDIT";
		 String DELETE = "USER_LOGIN_DELETE";
		 String VIEW = "USER_LOGIN_VIEW";
	}

	 interface CURRENCY {
		 String ADD = "CURRENCY_ADD";
		 String EDIT = "CURRENCY_EDIT";
		 String DELETE = "CURRENCY_DELETE";
		 String VIEW = "CURRENCY_VIEW";
	}

	 interface SYMBOL {
		 String ADD = "SYMBOL_CONFIG_ADD";
		 String EDIT = "SYMBOL_CONFIG_EDIT";
		 String DELETE = "SYMBOL_CONFIG_DELETE";
		 String VIEW = "SYMBOL_CONFIG_VIEW";
	}

	 interface BROKER_SYMBOL_CONFIG {
		 String ADD = "BROKER_SYMBOL_CONFIG_ADD";
		 String EDIT = "BROKER_SYMBOL_CONFIG_EDIT";
		 String DELETE = "BROKER_SYMBOL_CONFIG_DELETE";
		 String VIEW = "BROKER_SYMBOL_CONFIG_VIEW";
	}

    interface BROKER_RESOURCE {
        String ADD = "BROKER_RESOURCE_ADD";
        String EDIT = "BROKER_RESOURCE_EDIT";
        String DELETE = "BROKER_RESOURCE_DELETE";
        String VIEW = "BROKER_RESOURCE_VIEW";
    }
    interface MAIL_TEMPLATE {
        String ADD = "MAIL_TEMPLATE_ADD";
        String EDIT = "MAIL_TEMPLATE_EDIT";
        String DELETE = "MAIL_TEMPLATE_DELETE";
        String VIEW = "MAIL_TEMPLATE_VIEW";
    }
}
