package com.hihexa.cms.util;

import java.util.HashMap;
import java.util.Map;

public interface EnumConstants {
	enum HttpStatus {
		SUCCESS(0, "Success"),
		ERR_SYSTEM(-100, "System error"),
		;
		
		private int key;
		private String value;
		
		HttpStatus(int key, String value) {
			this.value = value;
			this.key = key;
		}
		
		public String getValue() {
			return value;
		}
		
		public int getKey() {
			return key;
		}
	}
	enum MatchingCreateOrder {
		REQUESTNEW("REQUESTNEW"),
		RELEASE("RELEASE"),
		REQUESTCANCEL("REQUESTCANCEL")
		;
		
		private String value; 
		MatchingCreateOrder(String value) {
			this.value = value;
		}
		
		public String value() {
			return this.value;
		}
	}

	enum OrderSide {
		SELL("SELL"),
		BUY("BUY");
		
		private String value; 
		OrderSide(String value) {
			this.value = value;
		}
		
		public String value() {
			return this.value;
		}
	}

	enum OrderType {
		LIMIT("LIMIT"),
		MARKET("MARKET"),
		STOP_LIMIT("STOP_LIMIT"),
		KILL_OR_FILL("KILL_OR_FILL"),
		OCO("OCO"),
		OTHER("OTHER"),
		;
		
		private String value; 
		OrderType(String value) {
			this.value = value;
		}
		
		public String value() {
			return this.value;
		}
	}
	
	enum ErrorDatabase {
		SUCCESS(0, "Success"),
		ERR_TRANSACTION(-99, "Error transaction"),
		ERR_NOT_ENOUGHT_MONEY(-1, "Not enought money"),
		ERR_PARAMETER_INVALID(-2, "Invalid parameter."),
		ERR_BALANCE_NEGATIVE(-3, "balance negative"),
		ERR_NOT_EXIST_ORDER_ID(-4, "not exist order id"),
		ERR_ORDER_SIDE_SAME(-5, "order same side"),
		ERR_USER_NOT_EXIST(-6, "User not exist"),
		ERR_RECORD_NOT_EXIST(-7, "Record not exist"),
		ERR_WITHDRAW_SAME_ADDRESS(-8, "Withdraw Same address"),
		ERR_MAX_OPEN_ORDER(-9, "Not allow order bigger than 200 order"),
		
		ERR_SYSTEM(-100, "System error"),
		ERR_MIN_AMOUNT(-101, "Lower than min trade"),
		ERR_MAX_AMOUNT(-102, "Higher than max trade"),
		ERR_SYNBOL_NOT_EXIST(-103, "Symbol is not exist"),
		ERR_VALIDATE(-104, "Lower than fee"),
		ERR_MAX_PRICE(-105, "Higher than max price")
		
		;
		
		private int key;
		private String value;

		ErrorDatabase(int key, String value) {
			this.value = value;
			this.key = key;
		}
		  
		private static final Map<Integer, String> lookup = new HashMap<Integer, String>();

	    static {
	        for (ErrorDatabase d : ErrorDatabase.values()) {
	            lookup.put(d.getKey(), d.getValue());
	        }
	    }

	  

	    public static String getValue(int key) {
			return lookup.get(key);
		}

	    public String getValue() {
	        return value;
	    }

	    public int getKey() {
	        return key;
	    }
	}

	
}
