package com.hihexa.cms.util;

public interface ErrorConstant {
//	DECLARE @ERR_RECORD_NOT_EXIST BIGINT = -7
//			DECLARE @ERR_STATUS_INVALID BIGINT = -2
//			DECLARE @ERR_OVER_AMOUNT_WITHDRAW BIGINT = -10
//			
//			DECLARE @WITHDRAW_STATUS_PENDING SMALLINT  = 0
//			DECLARE @WITHDRAW_STATUS_PROCESSING SMALLINT  = 1
//			DECLARE @WITHDRAW_STATUS_SUCCESSED SMALLINT  = 2
//			DECLARE @WITHDRAW_STATUS_CANCELED SMALLINT  = 3
//			DECLARE @WITHDRAW_STATUS_REJECT SMALLINT  = 4
//			DECLARE @WITHDRAW_STATUS_REQUESTING SMALLINT  = 5
//			DECLARE @WITHDRAW_STATUS_WAITING SMALLINT = 7
	public enum Withdraw {
		ERR_INVALID_AMOUNT(-100, "Lỗi amount < 0"),
		ERR_TRANSACTION(-99, "Lỗi transaction"),
		ERR_MIN_AMOUNT(-101, "Thành công"),
		ERR_MIN_FEE(-104, "Transaction Error"),
		ERR_NOT_ENOUGHT_MONEY(-1, "Not enough money"),
		ERR_USER_NOT_EXIST(-6, "Order status is invalid"),
		ERR_WITHDRAW_SAME_ADDRESS(-8, "Balance negative"),
		ERR_WITHDRAW_SAME_UUID(-15, "Unknow error"),
		ERR_ADDRESS_NULL_EMPTY(-111, "Address is null orr empty"),
		ERR_CURRENCY_NULL_EMPTY(-112, "Currency is null orr empty"),
		ERR_UNKNOW(-101, "Error system"),
		
		//confirm wd
		ERR_STATUS_INVALID(-2,"ERR_STATUS_INVALID"),
        ERR_BROKERID_INVALID(-3,"ERR_BROKERID_INVALID"),
        ERR_USERID_INVALID(-4,"ERR_USERID_INVALID"),
		ERR_OVER_AMOUNT_WITHDRAW(-10,"ERR_OVER_AMOUNT_WITHDRAW"),
		ERR_RECORD_NOT_EXIST(-7,"ERR_RECORD_NOT_EXIST")
		;

		private int code;
		private String message;
		
		Withdraw(int code, String message) {
			this.code = code;
			this.message = message;
		}
		public int getCode() {
			return code;
		}
		public String getMessage() {
			return message;
		}
	}

}
