package com.hihexa.cms.util;

import java.math.BigDecimal;

public class MathUtil {
	public static BigDecimal round(String s) {
		return round(s, DConstants.MAX_ROUND_NUMBER);
	}
	public static BigDecimal round(String s, int decimalPlace) {
		try {
			if (s == null || "".equals(s)) {
				return BigDecimal.ZERO;
			}
			if ("0".equals(s)) {
				return BigDecimal.ZERO;
			}
			BigDecimal b = new BigDecimal(s);
			b = b.setScale(decimalPlace, BigDecimal.ROUND_FLOOR);
			return b;
		} catch (Exception e) {
			return BigDecimal.ZERO; 
		}
	}
	public static BigDecimal round(BigDecimal s) {
		s = s.setScale(8, BigDecimal.ROUND_FLOOR);
		return s;
	}
	public static Integer parseInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return null;
		}
	}
	public static int parseInt(String s, int val) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return val;
		}
	}
	public static Long parseLong(String s) {
		try {
			return Long.parseLong(s);
		} catch (Exception e) {
			return null;
		}
	}
	public static long parseLong(String s, long val) {
		try {
			return Long.parseLong(s);
		} catch (Exception e) {
			return val;
		}
	}

}
