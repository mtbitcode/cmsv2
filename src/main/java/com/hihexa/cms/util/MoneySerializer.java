package com.hihexa.cms.util;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class MoneySerializer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
    	if (value == null || BigDecimal.ZERO.equals(value)) {
    		jgen.writeString("0");
    	} else {
//    		jgen.writeString(value.setScale(DConstants.MAX_ROUND_NUMBER, BigDecimal.ROUND_FLOOR).toPlainString());
    		jgen.writeString(String.valueOf(value.doubleValue()));
    	}
    }
}