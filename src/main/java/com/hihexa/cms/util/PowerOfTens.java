package com.hihexa.cms.util;

public class PowerOfTens {
    public static final long[] pregeneratedPowers = { 1L, 10L, 100L, 1000L, 10000L, 100000L, 1000000L, 10000000L, 100000000L, 1000000000000L };
    public static final double[] pregeneratedPowerDoubles = { 1., 10., 100., 1000., 10000., 100000., 1000000., 10000000., 100000000., 1000000000000., 10000000000000.,
        100000000000000.,1000000000000000.,1000000000000000.,10000000000000000. };

    public static long getPower(int n) {
        return pregeneratedPowers[n];
    }

    public static double getPowerDouble(int n) {
        return pregeneratedPowerDoubles[n];
    }
}
