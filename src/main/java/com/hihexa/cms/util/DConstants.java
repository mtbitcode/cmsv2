package com.hihexa.cms.util;

import java.math.BigDecimal;

public interface DConstants {
	public BigDecimal MIN_STEP = new BigDecimal("0.00000001");
	public BigDecimal MAX_STEP = new BigDecimal("1000000.0");
	public int MAX_ROUND_NUMBER = 8;

	public interface BASE_RESPONSE {
		public static final int STATUS_ERROR = 0;
		public static final int STATUS_SUCCESS = 1;
		public static final String MESSAGE_SUCCESS = "SUCCESS";
		public static final String MESSAGE_ERROR = "ERROR";
	}

	public interface DEPOSIT_ERROR_CODES {
		public int PENDING = 0;
		public int SUCCESS = 1;
		public int DUPLICATED = 2;
		public int INVALID = 3;
		public int ADDRESS_NOTEXIST = 4;
		public int UNKNOW = 5;
	}

	public interface DEPOSIT_STATUS {
		public int PENDING = 0;
		public int CONFIRMED = 1;
		public int CANCELLED = 2;
		public int ERROR = 3;
		public int ERROR_VALIDATION = -5;
		public int ERROR_OVER = -2;
	}

	public interface WITHDRAW_STATUS {

		//step 1 : request
		public int REQUEST_CREATE = 9;
        // step 2 : create
        public int PENDING = 0;

        public int CONFIRM = 10;
		public int PROCESSING = 1;

		public int REQUESTING = 5;


		public int SUCCESS = 2;



		public int CANCELLED = 3;
		public int REQUEST_CANCELLED = 8;

		public int WAITING = 7;
		public int REJECT = 4;
	}

	public interface BALANCE_EVENT {
	    public int DEPOSIT = 1;
        int REQUEST_WITHDRAW = -1;
        int CONFIRM_WITHDRAW = -2;
        int CANCEL_WITHDRAW = -3;
        int CHANGE_BALANCE = 0;
    }

	public interface ERR_WITHDRAW_REST {
		public int OK = 0;
		public int INVALID_ADDRESS = 1;
		public int INVALID_AMOUNT = 2;
		public int DB_ERROR = 3;
	}

	public interface WALLET_STATUS {
		public int FAILED = 0;
		public int PENDING = 1;
	}

//	public interface ACCOUNT_BASE {
//		public static final String ETH = "eth";
//		public static final String USDT = "usdt";
//		public static final String ETC = "etc";
//		public static final String USDC = "usdc";
//		public static final String XTZ = "xtz";
//		public static final String XRP = "xrp";
//		public static final String EOS = "eos";
//	}

	public interface ACCOUNT_BASE {
		public static final String ETH = "ETH";
		public static final String USDT = "USDT";
		public static final String ETC = "ETC";
		public static final String USDC = "USDC";
		public static final String XTZ = "XTZ";
		public static final String XRP = "XRP";
		public static final String EOS = "EOS";
	}

	public String[] BASE_ETH = { ACCOUNT_BASE.ETH, ACCOUNT_BASE.USDC, "HOT", "ZRX", "OMG", "MANA", "BAT",
			"WAX", "GNT", "PAX", "ENJ", "TUSD", "LINK" };

	public String[] BASE_EXTRA = { ACCOUNT_BASE.XRP, ACCOUNT_BASE.EOS };

	public interface ACCOUNT_BAT_UPPER {
		public static final String BTC = "BTC";
		public static final String LTC = "LTC";
	}

	public interface ACCOUNT_BAT {
		public static final String BTC = "btc";
		public static final String LTC = "ltc";
	}

	public interface CURRENCY_UNIT {
		public static final int BTC = 8;
		public static final int LTC = 8;
		public static final int ETH = 8;
		public static final int USDT = 8;
		public static final int ETC = 8;
		public static final int USDC = 6;
		public static final int XTZ = 6;
		public static final int XRP = 6;
		public static final int EOS = 4;
	}

	public interface ADDRESS {
		public static final int ACTIVE = 1;
		public static final int UNACTIVE = 0;
	}

}
