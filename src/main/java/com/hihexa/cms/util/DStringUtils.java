package com.hihexa.cms.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class DStringUtils {
	private static final String NUMBERS = "0123456789";

	/** Pattern for password */
	private static final String PATTERN = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

	/** Length of password */
	private static final int LENGTH_PASS = 6;
	public static final int LENGTH_OTP = 4;
	private static Mac sha256HMAC = null;
	static {
		try {
			sha256HMAC = Mac.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static String convertArrayToString(int[] lines) {
		StringBuffer bf = new StringBuffer();
		
		if (lines == null || lines.length == 0) {
			return "";
		}
		for (int e : lines) {
			bf.append(e).append(",");
		}
		bf.deleteCharAt(bf.length() - 1);
		return bf.toString();
	}

	public static Set<Integer> convertStringToSetInteger(String s) {
		Set<Integer> set = new HashSet<>();
		if (s == null || s.trim().length() == 0) {
			return set;
		}
		String[] arr = s.split(",");
		for (String string : arr) {
			set.add(MathUtil.parseInt(string));
		}
		return set;
	}
	public static String generateCodeRandom(int chars) {
		Random r = new Random();
		int lengNum = NUMBERS.length();
		StringBuffer bf = new StringBuffer();
		for (int i = 0; i < chars; i++) {
			int n = r.nextInt(lengNum - 1);
			bf.append(NUMBERS.charAt(n));
		}
		return bf.toString();
	}

	public static <T>  String listStrToString(Collection<T> list) {
		if (list == null || list.size() == 0)
			return "";
		StringBuffer bf = new StringBuffer();
		for (T string : list) {
			bf.append(String.valueOf(string)).append(",");
		}
		if (bf.length() >= 1)
			bf.deleteCharAt(bf.length() - 1);
		return bf.toString();
	}

	public static <T> String listObjectToString(Collection<T> list) {
		if (list == null || list.size() == 0)
			return "list null";
		StringBuffer bf = new StringBuffer("[");
		for (T object : list) {
			bf.append(object.toString()).append(",");
		}
		if (bf.length() >= 1)
			bf.deleteCharAt(bf.length() - 1);
		bf.append("]");
		return bf.toString();
	}
	public static String listObjectToString(Object[] list) {
		if (list == null || list.length == 0)
			return "list null";
		StringBuffer bf = new StringBuffer("[");
		for (Object object : list) {
			bf.append(object.toString()).append(",");
		}
		if (bf.length() >= 1)
			bf.deleteCharAt(bf.length() - 1);
		bf.append("]");
		return bf.toString();
	}

	/**
	 * Get generate password
	 * 
	 * @param pattern
	 * @param length
	 * @return password
	 */
	public static String generatePassword() {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < LENGTH_PASS; i++) {
			sb.append(PATTERN.charAt(random.nextInt(PATTERN.length())));
		}
		return sb.toString();
	}
	public static String generateOtp(int number) {
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < number; i++) {
			sb.append(random.nextInt(10));
		}
		return sb.toString();
	}

	/**
	 * Get encrypt md5 string
	 * 
	 * @param str
	 * @return encrypt md5 string
	 */
	public static String encryptMD5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());

			byte byteData[] = md.digest();

			// convert the byte to hex format method 1
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

	public static  String encodeSha256(String data, String secretKey) {
		try {
	        
	        SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
	        sha256HMAC.init(secret_key);
	        byte[] bytes = sha256HMAC.doFinal(data.getBytes());
	        String hash = new String(Base64.getEncoder().encode(bytes));
	        return hash;
		} catch (Exception e) {
			return "";
		}
    }
	
	public static String formatNumberMoney(long money) {
		DecimalFormat formatter = new DecimalFormat("###,###,###");
	    return formatter.format(money);
	}
	
	public static String encodeBase64(String str) {
		// Encode data on your side using BASE64
		String bytesEncoded = Base64.getEncoder().encodeToString(str.getBytes());
		System.out.println("encoded value is " + new String(bytesEncoded));
		return bytesEncoded;
	}

	public static String decodeBase64(String str) {
		// Encode data on your side using BASE64
		byte[] bytesEncoded = Base64.getDecoder().decode(str.getBytes());
		String response = new String(bytesEncoded);
		System.out.println("encoded value is " + new String(response));
		return response;
	}
	public static final char[][] UTF_CHAR = { { 'a', 'á', 'à', 'ả', 'ã', 'ạ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ' }, { 'e', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ' },
			{ 'i', 'í', 'ì', 'ỉ', 'ĩ', 'ị' }, { 'o', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ' }, { 'u', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự' },
			{ 'y', 'ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ' }, { 'd', 'đ' }, { 'A', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ' }, { 'E', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ' },
			{ 'I', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị' }, { 'O', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ' }, { 'U', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự' },
			{ 'Y', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ' }, { 'D', 'Đ' } };

	private static char unMark(char c) {
		for (int i = 0; i < UTF_CHAR.length; i++) {
			for (int j = 0; j < UTF_CHAR[i].length; j++) {
				if (c == UTF_CHAR[i][j])
					return UTF_CHAR[i][0];
			}
		}

		return c;
	}

	public static String unMark(String s) {
		char[] c = s.toCharArray();
		int length = c.length;
		char[] unMarkChar = new char[length];

		for (int i = 0; i < length; i++) {
			unMarkChar[i] = unMark(c[i]);
		}

		return String.valueOf(unMarkChar);
	}

	public static boolean containSpecialCharacter(String s) {
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
//		String s = "Iamas.tring";
		Matcher m = p.matcher(s);
		boolean b = m.find();
		return b;
	}
	public static boolean isValidPassword(String s) {
		Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
//		String s = "Iamas.tring";
		Matcher m = p.matcher(s);
		boolean b = m.find();
		if (!b) {
			return b;
		}
		
		return b;
	}
	
	public static boolean isValidPassword(String passwordhere, List<String> errorList) {

	    Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
//	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]", Pattern.CASE_INSENSITIVE);
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	    errorList.clear();

	    boolean flag=true;

	    if (passwordhere.length() < 6) {
	        errorList.add("Mật khẩu phải có ít nhất 6 ký tự!");
	        flag=false;
	    }
	    if (specailCharPatten.matcher(passwordhere).find()) {
	        errorList.add("Mật khẩu không được có ký tự đặc biệt!");
	        flag=false;
	    }
	    if (!lowerCasePatten.matcher(passwordhere).find()) {
	        errorList.add("Mật khẩu phải bao gồm cả chữ và số!");
	        flag=false;
	    }
	    if (!digitCasePatten.matcher(passwordhere).find()) {
	        errorList.add("Mật khẩu phải bao gồm cả chữ và số!");
	        flag=false;
	    }

	    return flag;
	}

	public static String convertMinutesToTitle(String first, int seconds, String late) {
		int minutes = seconds / 60;
		int hour = minutes / 60;
		int remain = minutes % 60;
		StringBuffer bf = new StringBuffer().append(first);
		if (hour == 0) {
			if (minutes != 0) {
				bf.append(" ").append(minutes).append(" phút ").append(late);
			} else {
				bf.append(" ").append(seconds).append(" giây ").append(late);
			}
		} else {
			bf.append(" ").append(hour).append(" tiếng ");
			if (remain != 0) {
				bf.append(" ").append(remain).append(" phút ").append(late);
			} else {
				bf.append(" ").append(late);
			}
		}
		return bf.toString();
	}

	public static <T> String listToString(List<T> list) {
		if (list == null || list.size() == 0) {
			return "null";
		}
		StringBuffer bf = new StringBuffer();
		for (T t : list) {
			bf.append(t.toString()).append("\n");
		}
		return bf.toString();
	}


	public static Integer parseInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return null;
		}
	}

	public static Integer parseInt(String s, int val) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return val;
		}
	}

	public static <T> T findOnList(List<T> list, T obj) {
		for (T t : list) {
			if (t.equals(obj)) {
				return t;
			}
		}
		return null;
	}
	
	public static String MD5(String md5) {
		try {
			if (md5 == null) {
				return null;
			}
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
	
	public static String unicodeEscape(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			// mlog.info("Unicode block" + Character.UnicodeBlock.of(c));
			if (Character.UnicodeBlock.of(c) == Character.UnicodeBlock.BASIC_LATIN || Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_1_SUPPLEMENT
					|| Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_EXTENDED_A || Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_EXTENDED_B
					|| Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_EXTENDED_C || Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_EXTENDED_D
					|| Character.UnicodeBlock.of(c) == Character.UnicodeBlock.LATIN_EXTENDED_ADDITIONAL) {
				sb.append(c);
			} else {
				sb.append(' ');
			}
		}
		return sb.toString();
	}

	public static String normalizePhoneNumber(String mobile) {
		if (mobile == null || mobile.trim().length() == 0) {
			return null;
		}
		if (mobile.length() < 9) {
			return null;
		}
		mobile = mobile.replaceAll("\\+", "").replaceAll("\\.", "").replace(",", "").replace(" ", "");
		String sub = mobile.substring(0, 2);
		if (sub.equals("84")) {
			mobile = mobile.replaceFirst("84", "0");
		}
		return mobile;
	}
	public static int[] convertStringToArrayInt(String lines) {
		String[] arr = lines.split(",");
		if (arr == null || arr.length == 0) {
			return null;
		}
		int[] betLines = new int[arr.length];
		int index = 0;
		for (String s : arr) {
			betLines[index] = Integer.parseInt(s);
			index ++;
		}
		return betLines;
	}
	public static String[] fastSplit(String line, char split){
        String[] temp = new String[line.length()/2];
        int wordCount = 0;
        int i = 0;
        int j = line.indexOf(split);  // First substring
        while( j >= 0){
            temp[wordCount++] = line.substring(i,j);
            i = j + 1;
            j = line.indexOf(split, i);   // Rest of substrings
        }
        temp[wordCount++] = line.substring(i); // Last substring
        String[] result = new String[wordCount];
        System.arraycopy(temp, 0, result, 0, wordCount);
        return result;
    }
	
	public static boolean isNullOrEmpty(String string) {
    	return string == null || string.isEmpty();
    }
    
    public static boolean hasText(String str) {
        if (isNullOrEmpty(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }
    
}
