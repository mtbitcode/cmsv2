package com.hihexa.cms.util;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JacksonUtils {
	private static final ObjectMapper mapper = new ObjectMapper();
	public static <T> String listToString(List<T> list) {
		try {
			String s = mapper.writeValueAsString(list);
			return s;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> String toString1(T object) {
		try {
			String s = mapper.writeValueAsString(object);
			return s;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T toEntity(String s, Class<T> clazz) {
		if (s == null) {
			return null;
		}
		T u = null;
		try {
			u = mapper.readValue(s, clazz);
			return u;
		} catch (Exception e) {
			return null;
		}
	}

	public static  String toJson(Object clazz){
        try {
            return mapper.writeValueAsString(clazz);

        } catch (Exception e) {
            return null;
        }
    }
}
