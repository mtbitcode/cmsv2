package com.hihexa.cms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.ExtraAddressEntity;


@Repository
public interface ExtraAddressRepository extends JpaRepository<ExtraAddressEntity, Long> {
	
	@Query("SELECT o FROM ExtraAddressEntity o WHERE o.currency =:currency and o.status = 1")
	ExtraAddressEntity findByCurrencyAndStatus(@Param("currency") String currency);
}
