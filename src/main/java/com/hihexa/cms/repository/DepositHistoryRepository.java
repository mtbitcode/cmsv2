package com.hihexa.cms.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.DepositHistoryEntity;


@Repository
public interface DepositHistoryRepository extends JpaRepository<DepositHistoryEntity, String>{
	@Query("SELECT mh FROM DepositHistoryEntity mh WHERE mh.userId = :userId order by mh.created desc")
	public Page<DepositHistoryEntity> findByUserId(@Param("userId")String userId, Pageable paeable);


	//SELECT TOP 1 id FROM [deposit_history] WITH(NOLOCK) where tx=@txId and address=@address  and status = @DEPOSIT_STATUS_CONFIRMED and (@extraUuid = '' or extra_uuid = @extraUuid)
	@Query("SELECT h FROM DepositHistoryEntity h WHERE h.tx=:tx and h.address=:address and h.status=:status and (:extraUuid ='' or extraUuid =:extraUuid)")
	Optional<DepositHistoryEntity> findOneByTxAndAddress(@Param("tx") String tx, @Param("address") String address,
			@Param("status") Integer status, @Param("extraUuid") String extraUuid);
}
