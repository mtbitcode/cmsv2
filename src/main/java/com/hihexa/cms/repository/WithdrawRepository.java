package com.hihexa.cms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.model.wallet.ConfirmWithdrawModel;

public interface WithdrawRepository extends JpaRepository<WithdrawEntity, Long> {
	
	@Query("SELECT new com.hihexa.cms.model.wallet.ConfirmWithdrawModel("
			+ "mh.destinationAddress, "
			+ "mh.amount, "
			+ "mh.currency, "
			+ "mh.userId, "
			+ "mh.id, "
			+ "mh.feeValue, "
			+ "mh.extraUuid, "
			+ "mh.destinationExtraUuid,"
			+ "mh.currencyId ,"
			+ "mh.brokerId) FROM WithdrawEntity mh WHERE mh.status = :status ")
	List<ConfirmWithdrawModel> findByStatus(@Param("status") Integer status);


}
