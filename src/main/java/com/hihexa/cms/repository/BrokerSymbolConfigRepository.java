package com.hihexa.cms.repository;

import com.hihexa.cms.domain.BrokerSymbolConfig;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BrokerSymbolConfig entity.
 */
@Repository
public interface BrokerSymbolConfigRepository extends JpaRepository<BrokerSymbolConfig, Long> {

}
