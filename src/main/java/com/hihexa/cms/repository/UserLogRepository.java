package com.hihexa.cms.repository;

import com.hihexa.cms.domain.UserLog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserLog entity.
 */
@Repository
public interface UserLogRepository extends JpaRepository<UserLog, Long> {

}
