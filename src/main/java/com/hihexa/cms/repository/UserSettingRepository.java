package com.hihexa.cms.repository;

import com.hihexa.cms.domain.UserSetting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the UserSetting entity.
 */
@Repository
public interface UserSettingRepository extends JpaRepository<UserSetting, Long> {

}
