package com.hihexa.cms.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.dao.AddressDao;
import com.hihexa.cms.domain.Address;


/**
 * trungnv
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> , AddressDao  {
	List<Address> findAllByUserId(long userId);

	Optional<Address> findByUserIdAndCurrencyAndStatus(long userId, String currency, int status);

	List<Address> findByCurrencyIdAndBrokerId(Integer currencyId, Integer brokerId);

	Optional<Address> findOneByAddressAndStatusAndCurrency(String address, int status ,String currency);

//	@Procedure(procedureName = SQLServerConstant.PROC_ADD_ADDRESS)
//	String addAddress(@Param("p_userId") Long userId,
//			@Param("p_currency") String currency,
//			@Param("p_address") String address,
//			@Param("p_path") String path) throws Exception;

}
