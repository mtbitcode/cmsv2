package com.hihexa.cms.repository;

import com.hihexa.cms.domain.BalanceEntity;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the BalanceEntity entity.
 */
@Repository
public interface BalanceRepository extends JpaRepository<BalanceEntity, Long> {
	public List<BalanceEntity> findAllByUserId(Long userId);

    @Query("SELECT mh from BalanceEntity mh where mh.userId = :userId and mh.currencyId = :currencyId ")
    Page<BalanceEntity> findAllByUserIdAndCurrencyId(@Param("userId") Long userId, @Param("currencyId") Integer currencyId, Pageable pageable);

	public List<BalanceEntity> findAllByUserIdAndCurrencyId(Long userId, Integer currencyId);

}
