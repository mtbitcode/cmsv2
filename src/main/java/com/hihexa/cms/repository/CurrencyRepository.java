package com.hihexa.cms.repository;

import com.hihexa.cms.domain.Currency;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Currency entity.
 */
@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Integer> {
	Optional<Currency> findOneByCode(String code);
}
