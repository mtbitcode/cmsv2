package com.hihexa.cms.repository;

import com.hihexa.cms.domain.Broker;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Broker entity.
 */
@Repository
public interface BrokerRepository extends JpaRepository<Broker, Integer> {

}
