package com.hihexa.cms.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.DepositEntity;

@Repository
public interface DepositRepository extends JpaRepository<DepositEntity, String> {
	@Query("SELECT mh FROM DepositEntity mh WHERE mh.tx = :tx  order by mh.created desc")
	public List<DepositEntity> findByTx(@Param("tx") String tx);

	@Query("SELECT mh FROM DepositEntity mh WHERE mh.currency = :currency and mh.status = :status  order by mh.created desc")
	public List<DepositEntity> findBySymbolAndStatus(@Param("currency") String symbol, @Param("status") Integer status);

	@Query("SELECT mh FROM DepositEntity mh WHERE mh.userId = :userId  order by mh.created desc")
	public Page<DepositEntity> findByUserId(@Param("userId") String userId, Pageable paeable);

	@Query("SELECT d FROM DepositEntity d WHERE d.tx = :tx and d.address =:address and d.status=:status and  (:extraUuid ='' or d.extraUuid =:extraUuid) ")
	DepositEntity checkExist(@Param("tx") String tx, @Param("address") String address, @Param("status") Integer status,
			@Param("extraUuid") String extraUuid);

}
