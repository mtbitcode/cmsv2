package com.hihexa.cms.repository;

import com.hihexa.cms.domain.BrokerCurrency;

import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BrokerCurrency entity.
 */
@Repository
public interface BrokerCurrencyRepository extends JpaRepository<BrokerCurrency, Long> {

	Optional<BrokerCurrency> findOneByBrokerIdAndCurrencyId(Integer brokerId, Integer currencyId);
}
