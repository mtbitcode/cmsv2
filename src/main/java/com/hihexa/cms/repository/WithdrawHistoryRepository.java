package com.hihexa.cms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.WithdrawHistoryEntity;

@Repository
public interface WithdrawHistoryRepository extends JpaRepository<WithdrawHistoryEntity, Long> {
	@Query("SELECT mh FROM WithdrawHistoryEntity mh WHERE mh.userId = :userId order by mh.id desc")
	public Page<WithdrawHistoryEntity> findByUserId(@Param("userId") String userId, Pageable pageable);
}
