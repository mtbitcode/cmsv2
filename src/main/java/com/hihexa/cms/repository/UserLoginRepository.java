package com.hihexa.cms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.UserLogin;


/**
 * Spring Data  repository for the UserLogin entity.
 */
@Repository
public interface UserLoginRepository extends JpaRepository<UserLogin, Long> {
    @Query("SELECT mh From UserLogin mh where mh.email like concat('%',:email,'%')")
    Page<UserLogin> findUserLoginResourceByEmail(@Param("email") String email, Pageable pageable);

}
