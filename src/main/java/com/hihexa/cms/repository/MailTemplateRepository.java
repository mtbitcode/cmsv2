package com.hihexa.cms.repository;

import com.hihexa.cms.domain.MailTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MailTemplate entity.
 */
@Repository
public interface MailTemplateRepository extends JpaRepository<MailTemplate, Long> {

}
