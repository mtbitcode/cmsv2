package com.hihexa.cms.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hihexa.cms.domain.SymbolConfig;


/**
 * Spring Data  repository for the SymbolConfig entity.
 */
@Repository
public interface SymbolConfigRepository extends JpaRepository<SymbolConfig, Long> {
    @Query("select mh from SymbolConfig mh where mh.baseCc = :baseCc Or mh.counterCc = :counterCc")
    Page<SymbolConfig> findSymbolConfigByBaseCcOrCounterCc(@Param("baseCc") String baseCc,@Param("counterCc") String counterCc,Pageable pageable);


}
