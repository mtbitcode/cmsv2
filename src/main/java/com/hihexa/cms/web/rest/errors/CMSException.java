package com.hihexa.cms.web.rest.errors;

public class CMSException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int errorCode;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

	public CMSException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
