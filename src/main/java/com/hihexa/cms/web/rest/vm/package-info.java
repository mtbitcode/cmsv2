/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hihexa.cms.web.rest.vm;
