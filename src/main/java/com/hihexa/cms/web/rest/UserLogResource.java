package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.UserLog;
import com.hihexa.cms.service.UserLogService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.UserLog}.
 */
@RestController
@RequestMapping("/api")
public class UserLogResource {

    private final Logger log = LoggerFactory.getLogger(UserLogResource.class);

    private static final String ENTITY_NAME = "userLog";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserLogService userLogService;

    public UserLogResource(UserLogService userLogService) {
        this.userLogService = userLogService;
    }

    /**
     * {@code POST  /user-logs} : Create a new userLog.
     *
     * @param userLog the userLog to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userLog, or with status {@code 400 (Bad Request)} if the userLog has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-logs")
    public ResponseEntity<UserLog> createUserLog(@RequestBody UserLog userLog) throws URISyntaxException {
        log.debug("REST request to save UserLog : {}", userLog);
        if (userLog.getId() != null) {
            throw new BadRequestAlertException("A new userLog cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserLog result = userLogService.save(userLog);
        return ResponseEntity.created(new URI("/api/user-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-logs} : Updates an existing userLog.
     *
     * @param userLog the userLog to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userLog,
     * or with status {@code 400 (Bad Request)} if the userLog is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userLog couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-logs")
    public ResponseEntity<UserLog> updateUserLog(@RequestBody UserLog userLog) throws URISyntaxException {
        log.debug("REST request to update UserLog : {}", userLog);
        if (userLog.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserLog result = userLogService.save(userLog);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userLog.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-logs} : get all the userLogs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userLogs in body.
     */
    @GetMapping("/user-logs")
    public ResponseEntity<List<UserLog>> getAllUserLogs(Pageable pageable) {
        log.debug("REST request to get a page of UserLogs");
        Page<UserLog> page = userLogService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-logs/:id} : get the "id" userLog.
     *
     * @param id the id of the userLog to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userLog, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-logs/{id}")
    public ResponseEntity<UserLog> getUserLog(@PathVariable Long id) {
        log.debug("REST request to get UserLog : {}", id);
        Optional<UserLog> userLog = userLogService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userLog);
    }

    /**
     * {@code DELETE  /user-logs/:id} : delete the "id" userLog.
     *
     * @param id the id of the userLog to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-logs/{id}")
    public ResponseEntity<Void> deleteUserLog(@PathVariable Long id) {
        log.debug("REST request to delete UserLog : {}", id);
        userLogService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
