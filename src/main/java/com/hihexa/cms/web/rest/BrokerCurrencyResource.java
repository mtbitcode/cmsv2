package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.BrokerCurrency;
import com.hihexa.cms.service.BrokerCurrencyService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.BrokerCurrency}.
 */
@RestController
@RequestMapping("/api")
public class BrokerCurrencyResource {

    private final Logger log = LoggerFactory.getLogger(BrokerCurrencyResource.class);

    private static final String ENTITY_NAME = "brokerCurrency";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BrokerCurrencyService brokerCurrencyService;

    public BrokerCurrencyResource(BrokerCurrencyService brokerCurrencyService) {
        this.brokerCurrencyService = brokerCurrencyService;
    }

    /**
     * {@code POST  /broker-currencies} : Create a new brokerCurrency.
     *
     * @param brokerCurrency the brokerCurrency to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new brokerCurrency, or with status {@code 400 (Bad Request)} if the brokerCurrency has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/broker-currencies")
    public ResponseEntity<BrokerCurrency> createBrokerCurrency(@RequestBody BrokerCurrency brokerCurrency) throws URISyntaxException {
        log.debug("REST request to save BrokerCurrency : {}", brokerCurrency);
        if (brokerCurrency.getId() != null) {
            throw new BadRequestAlertException("A new brokerCurrency cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BrokerCurrency result = null;
		try {
			result = brokerCurrencyService.save(brokerCurrency);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ResponseEntity.created(new URI("/api/broker-currencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /broker-currencies} : Updates an existing brokerCurrency.
     *
     * @param brokerCurrency the brokerCurrency to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated brokerCurrency,
     * or with status {@code 400 (Bad Request)} if the brokerCurrency is not valid,
     * or with status {@code 500 (Internal Server Error)} if the brokerCurrency couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/broker-currencies")
    public ResponseEntity<BrokerCurrency> updateBrokerCurrency(@RequestBody BrokerCurrency brokerCurrency) throws URISyntaxException {
        log.debug("REST request to update BrokerCurrency : {}", brokerCurrency);
        if (brokerCurrency.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BrokerCurrency result = null;
		try {
			result = brokerCurrencyService.save(brokerCurrency);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, brokerCurrency.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /broker-currencies} : get all the brokerCurrencies.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of brokerCurrencies in body.
     */
    @GetMapping("/broker-currencies")
    public ResponseEntity<List<BrokerCurrency>> getAllBrokerCurrencies(Pageable pageable) {
        log.debug("REST request to get a page of BrokerCurrencies");
        Page<BrokerCurrency> page = brokerCurrencyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /broker-currencies/:id} : get the "id" brokerCurrency.
     *
     * @param id the id of the brokerCurrency to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the brokerCurrency, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/broker-currencies/{id}")
    public ResponseEntity<BrokerCurrency> getBrokerCurrency(@PathVariable Long id) {
        log.debug("REST request to get BrokerCurrency : {}", id);
        Optional<BrokerCurrency> brokerCurrency = brokerCurrencyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(brokerCurrency);
    }

    /**
     * {@code DELETE  /broker-currencies/:id} : delete the "id" brokerCurrency.
     *
     * @param id the id of the brokerCurrency to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/broker-currencies/{id}")
    public ResponseEntity<Void> deleteBrokerCurrency(@PathVariable Long id) {
        log.debug("REST request to delete BrokerCurrency : {}", id);
        brokerCurrencyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
