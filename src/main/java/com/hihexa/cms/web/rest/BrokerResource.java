package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.Broker;
import com.hihexa.cms.security.RoleConstant;
import com.hihexa.cms.service.BrokerService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.Broker}.
 */
@RestController
@RequestMapping("/api")
public class BrokerResource {

    private final Logger log = LoggerFactory.getLogger(BrokerResource.class);

    private static final String ENTITY_NAME = "broker";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BrokerService brokerService;

    public BrokerResource(BrokerService brokerService) {
        this.brokerService = brokerService;
    }

    /**
     * {@code POST  /brokers} : Create a new broker.
     *
     * @param broker the broker to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new broker, or with status {@code 400 (Bad Request)} if the broker has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/brokers")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_RESOURCE.ADD})

    public ResponseEntity<Broker> createBroker(@RequestBody Broker broker) throws URISyntaxException {
        log.debug("REST request to save Broker : {}", broker);
        if (broker.getId() != null) {
            throw new BadRequestAlertException("A new broker cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Broker result = brokerService.save(broker);
        return ResponseEntity.created(new URI("/api/brokers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /brokers} : Updates an existing broker.
     *
     * @param broker the broker to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated broker,
     * or with status {@code 400 (Bad Request)} if the broker is not valid,
     * or with status {@code 500 (Internal Server Error)} if the broker couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/brokers")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_RESOURCE.EDIT})

    public ResponseEntity<Broker> updateBroker(@RequestBody Broker broker) throws URISyntaxException {
        log.debug("REST request to update Broker : {}", broker);
        if (broker.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Broker result = brokerService.save(broker);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, broker.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /brokers} : get all the brokers.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of brokers in body.
     */
    @GetMapping("/brokers")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_RESOURCE.VIEW})

    public ResponseEntity<List<Broker>> getAllBrokers(Pageable pageable) {
        log.debug("REST request to get a page of Brokers");
        Page<Broker> page = brokerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /brokers/:id} : get the "id" broker.
     *
     * @param id the id of the broker to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the broker, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/brokers/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_RESOURCE.VIEW})

    public ResponseEntity<Broker> getBroker(@PathVariable Integer id) {
        log.debug("REST request to get Broker : {}", id);
        Optional<Broker> broker = brokerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(broker);
    }

    /**
     * {@code DELETE  /brokers/:id} : delete the "id" broker.
     *
     * @param id the id of the broker to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/brokers/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_RESOURCE.DELETE})
    public ResponseEntity<Void> deleteBroker(@PathVariable Integer id) {
        log.debug("REST request to delete Broker : {}", id);
        brokerService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
