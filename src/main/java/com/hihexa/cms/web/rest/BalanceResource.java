package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.BalanceEntity;
import com.hihexa.cms.service.BalanceService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.BalanceEntity}.
 */
@RestController
@RequestMapping("/api")
public class BalanceResource {

    private final Logger log = LoggerFactory.getLogger(BalanceResource.class);

    private static final String ENTITY_NAME = "balanceEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BalanceService balanceService;

    public BalanceResource(BalanceService balanceService) {
        this.balanceService = balanceService;
    }

    /**
     * {@code POST  /balance-entities} : Create a new balanceEntity.
     *
     * @param balanceEntity the balanceEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new balanceEntity, or with status {@code 400 (Bad Request)} if the balanceEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/balance-entities")
    public ResponseEntity<BalanceEntity> createBalanceEntity(@Valid @RequestBody BalanceEntity balanceEntity) throws URISyntaxException {
        log.debug("REST request to save BalanceEntity : {}", balanceEntity);
        if (balanceEntity.getId() != null) {
            throw new BadRequestAlertException("A new balanceEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BalanceEntity result = balanceService.save(balanceEntity);
        return ResponseEntity.created(new URI("/api/balance-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /balance-entities} : Updates an existing balanceEntity.
     *
     * @param balanceEntity the balanceEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated balanceEntity,
     * or with status {@code 400 (Bad Request)} if the balanceEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the balanceEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/balance-entities")
    public ResponseEntity<BalanceEntity> updateBalanceEntity(@Valid @RequestBody BalanceEntity balanceEntity) throws URISyntaxException {
        log.debug("REST request to update BalanceEntity : {}", balanceEntity);
        if (balanceEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BalanceEntity result = balanceService.save(balanceEntity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, balanceEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /balance-entities} : get all the balanceEntities.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of balanceEntities in body.
     */
    @GetMapping("/balance-entities")
    public ResponseEntity<List<BalanceEntity>> getAllBalanceEntities(Pageable pageable) {
        log.debug("REST request to get a page of BalanceEntities");
        Page<BalanceEntity> page = balanceService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @RequestMapping(value = {"/balance-entities/{userId}/{currencyId}", "/balance-entities/{userId}"} , method = RequestMethod.GET)
    public ResponseEntity<List<BalanceEntity>> getAllBalanceEntities(@PathVariable("userId") Optional<Long> userId,@PathVariable("currencyId") Optional<Integer> currencyId,Pageable pageable) throws Exception {
        log.debug("REST request to get a page of BalanceEntities");
        Integer CurrencyId = currencyId.orElse(0);

        Page<BalanceEntity> page = balanceService.findBalanceByUserIdOrCurrencyId(userId.get(),CurrencyId,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code DELETE  /balance-entities/:id} : delete the "id" balanceEntity.
     *
     * @param id the id of the balanceEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/balance-entities/{id}")
    public ResponseEntity<Void> deleteBalanceEntity(@PathVariable Long id) {
        log.debug("REST request to delete BalanceEntity : {}", id);
        balanceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
