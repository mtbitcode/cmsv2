package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.UserLogin;
import com.hihexa.cms.security.RoleConstant;
import com.hihexa.cms.service.UserLoginService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.UserLogin}.
 */
@RestController
@RequestMapping("/api")
public class UserLoginResource {

    private final Logger log = LoggerFactory.getLogger(UserLoginResource.class);

    private static final String ENTITY_NAME = "userLogin";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserLoginService userLoginService;

    public UserLoginResource(UserLoginService userLoginService) {
        this.userLoginService = userLoginService;
    }

    /**
     * {@code POST  /user-logins} : Create a new userLogin.
     *
     * @param userLogin the userLogin to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userLogin, or with status {@code 400 (Bad Request)} if the userLogin has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-logins")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.USER_LOGIN.ADD})
    public ResponseEntity<UserLogin> createUserLogin(@RequestBody UserLogin userLogin) throws URISyntaxException {
        log.debug("REST request to save UserLogin : {}", userLogin);
        if (userLogin.getId() != null) {
            throw new BadRequestAlertException("A new userLogin cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserLogin result = userLoginService.save(userLogin);
        return ResponseEntity.created(new URI("/api/user-logins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-logins} : Updates an existing userLogin.
     *
     * @param userLogin the userLogin to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userLogin,
     * or with status {@code 400 (Bad Request)} if the userLogin is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userLogin couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-logins")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.USER_LOGIN.EDIT})
    public ResponseEntity<UserLogin> updateUserLogin(@RequestBody UserLogin userLogin) throws URISyntaxException {
        log.debug("REST request to update UserLogin : {}", userLogin);
        if (userLogin.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserLogin result = userLoginService.save(userLogin);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userLogin.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-logins} : get all the userLogins.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userLogins in body.
     */
    @GetMapping("/user-logins")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.USER_LOGIN.VIEW})
    public ResponseEntity<List<UserLogin>> getAllUserLogins(Pageable pageable) {
        log.debug("REST request to get a page of UserLogins");
        Page<UserLogin> page = userLoginService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/user-logins/{email}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.USER_LOGIN.VIEW})
    public ResponseEntity<List<UserLogin>> getAllUserLogins(@PathVariable("email") String email,Pageable pageable) {
        log.debug("REST request to get a page of UserLogins by email");
        Page<UserLogin> page = userLoginService.findUserByEmail(email,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    /**
     * {@code GET  /user-logins/:id} : get the "id" userLogin.
     *
     * @param id the id of the userLogin to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userLogin, or with status {@code 404 (Not Found)}.
     */
    @DeleteMapping("/user-logins/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.USER_LOGIN.DELETE})
    public ResponseEntity<Void> deleteUserLogin(@PathVariable Long id) {
        log.debug("REST request to delete UserLogin : {}", id);
        userLoginService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
