package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.SymbolConfig;
import com.hihexa.cms.security.RoleConstant;
import com.hihexa.cms.service.SymbolConfigService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.SymbolConfig}.
 */
@RestController
@RequestMapping("/api")
public class SymbolConfigResource {

    private final Logger log = LoggerFactory.getLogger(SymbolConfigResource.class);

    private static final String ENTITY_NAME = "symbolConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SymbolConfigService symbolConfigService;

    public SymbolConfigResource(SymbolConfigService symbolConfigService) {
        this.symbolConfigService = symbolConfigService;
    }

    /**
     * {@code POST  /symbol-configs} : Create a new symbolConfig.
     *
     * @param symbolConfig the symbolConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new symbolConfig, or with status {@code 400 (Bad Request)} if the symbolConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.SYMBOL.ADD})

    public ResponseEntity<SymbolConfig> createSymbolConfig(@RequestBody SymbolConfig symbolConfig) throws URISyntaxException {
        log.debug("REST request to save SymbolConfig : {}", symbolConfig);
        if (symbolConfig.getId() != null) {
            throw new BadRequestAlertException("A new symbolConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SymbolConfig result = symbolConfigService.save(symbolConfig);
        return ResponseEntity.created(new URI("/api/symbol-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /symbol-configs} : Updates an existing symbolConfig.
     *
     * @param symbolConfig the symbolConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated symbolConfig,
     * or with status {@code 400 (Bad Request)} if the symbolConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the symbolConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.SYMBOL.EDIT})

    public ResponseEntity<SymbolConfig> updateSymbolConfig(@RequestBody SymbolConfig symbolConfig) throws URISyntaxException {
        log.debug("REST request to update SymbolConfig : {}", symbolConfig);
        if (symbolConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SymbolConfig result = symbolConfigService.save(symbolConfig);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, symbolConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /symbol-configs} : get all the symbolConfigs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of symbolConfigs in body.
     */
    @GetMapping("/symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.SYMBOL.VIEW})
    public ResponseEntity<List<SymbolConfig>> getAllSymbolConfigs(Pageable pageable) {
        log.debug("REST request to get a page of SymbolConfigs");
        Page<SymbolConfig> page = symbolConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/symbol-configs-Cc")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.SYMBOL.VIEW})
    public ResponseEntity<List<SymbolConfig>> getAllSymbolConfigs(@RequestParam Optional<String> baseCc, @RequestParam Optional<String> counterCc, Pageable pageable) {
        log.debug("REST request to get a page of SymbolConfigs");
        String BaseCc = baseCc.orElse("");
        String CounterCc = counterCc.orElse("");

        Page<SymbolConfig> page = symbolConfigService.findSymbolByBaseCcorCounterCc(BaseCc,CounterCc,pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    /**
     * {@code GET  /symbol-configs/:id} : get the "id" symbolConfig.
     *
     * @param id the id of the symbolConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the symbolConfig, or with status {@code 404 (Not Found)}.
     */
//    @GetMapping("/symbol-configs/{id}")
//    public ResponseEntity<SymbolConfig> getSymbolConfig(@PathVariable Long id) {
//        log.debug("REST request to get SymbolConfig : {}", id);
//        Optional<SymbolConfig> symbolConfig = symbolConfigService.findOne(id);
//        return ResponseUtil.wrapOrNotFound(symbolConfig);
//    }

    /**
     * {@code DELETE  /symbol-configs/:id} : delete the "id" symbolConfig.
     *
     * @param id the id of the symbolConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.SYMBOL.DELETE})
    @DeleteMapping("/symbol-configs/{id}")
    public ResponseEntity<Void> deleteSymbolConfig(@PathVariable Long id) {
        log.debug("REST request to delete SymbolConfig : {}", id);
        symbolConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
