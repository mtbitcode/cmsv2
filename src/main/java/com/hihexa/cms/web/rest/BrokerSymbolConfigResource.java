package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.BrokerSymbolConfig;
import com.hihexa.cms.security.RoleConstant;
import com.hihexa.cms.service.BrokerSymbolConfigService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.BrokerSymbolConfig}.
 */
@RestController
@RequestMapping("/api")
public class BrokerSymbolConfigResource {

    private final Logger log = LoggerFactory.getLogger(BrokerSymbolConfigResource.class);

    private static final String ENTITY_NAME = "brokerSymbolConfig";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BrokerSymbolConfigService brokerSymbolConfigService;

    public BrokerSymbolConfigResource(BrokerSymbolConfigService brokerSymbolConfigService) {
        this.brokerSymbolConfigService = brokerSymbolConfigService;
    }

    /**
     * {@code POST  /broker-symbol-configs} : Create a new brokerSymbolConfig.
     *
     * @param brokerSymbolConfig the brokerSymbolConfig to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new brokerSymbolConfig, or with status {@code 400 (Bad Request)} if the brokerSymbolConfig has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/broker-symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_SYMBOL_CONFIG.ADD})

    public ResponseEntity<BrokerSymbolConfig> createBrokerSymbolConfig(@RequestBody BrokerSymbolConfig brokerSymbolConfig) throws URISyntaxException {
        log.debug("REST request to save BrokerSymbolConfig : {}", brokerSymbolConfig);
        if (brokerSymbolConfig.getId() != null) {
            throw new BadRequestAlertException("A new brokerSymbolConfig cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BrokerSymbolConfig result = brokerSymbolConfigService.save(brokerSymbolConfig);
        return ResponseEntity.created(new URI("/api/broker-symbol-configs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /broker-symbol-configs} : Updates an existing brokerSymbolConfig.
     *
     * @param brokerSymbolConfig the brokerSymbolConfig to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated brokerSymbolConfig,
     * or with status {@code 400 (Bad Request)} if the brokerSymbolConfig is not valid,
     * or with status {@code 500 (Internal Server Error)} if the brokerSymbolConfig couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/broker-symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_SYMBOL_CONFIG.EDIT})

    public ResponseEntity<BrokerSymbolConfig> updateBrokerSymbolConfig(@RequestBody BrokerSymbolConfig brokerSymbolConfig) throws URISyntaxException {
        log.debug("REST request to update BrokerSymbolConfig : {}", brokerSymbolConfig);
        if (brokerSymbolConfig.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BrokerSymbolConfig result = brokerSymbolConfigService.save(brokerSymbolConfig);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, brokerSymbolConfig.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /broker-symbol-configs} : get all the brokerSymbolConfigs.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of brokerSymbolConfigs in body.
     */
    @GetMapping("/broker-symbol-configs")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_SYMBOL_CONFIG.VIEW})

    public ResponseEntity<List<BrokerSymbolConfig>> getAllBrokerSymbolConfigs(Pageable pageable) {
        log.debug("REST request to get a page of BrokerSymbolConfigs");
        Page<BrokerSymbolConfig> page = brokerSymbolConfigService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /broker-symbol-configs/:id} : get the "id" brokerSymbolConfig.
     *
     * @param id the id of the brokerSymbolConfig to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the brokerSymbolConfig, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/broker-symbol-configs/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_SYMBOL_CONFIG.VIEW})

    public ResponseEntity<BrokerSymbolConfig> getBrokerSymbolConfig(@PathVariable Long id) {
        log.debug("REST request to get BrokerSymbolConfig : {}", id);
        Optional<BrokerSymbolConfig> brokerSymbolConfig = brokerSymbolConfigService.findOne(id);
        return ResponseUtil.wrapOrNotFound(brokerSymbolConfig);
    }

    /**
     * {@code DELETE  /broker-symbol-configs/:id} : delete the "id" brokerSymbolConfig.
     *
     * @param id the id of the brokerSymbolConfig to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/broker-symbol-configs/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.BROKER_SYMBOL_CONFIG.DELETE})

    public ResponseEntity<Void> deleteBrokerSymbolConfig(@PathVariable Long id) {
        log.debug("REST request to delete BrokerSymbolConfig : {}", id);
        brokerSymbolConfigService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
