package com.hihexa.cms.web.rest;

import com.hihexa.cms.domain.MailTemplate;
import com.hihexa.cms.security.RoleConstant;
import com.hihexa.cms.service.MailTemplateService;
import com.hihexa.cms.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.hihexa.cms.domain.MailTemplate}.
 */
@RestController
@RequestMapping("/api")
public class MailTemplateResource {

    private final Logger log = LoggerFactory.getLogger(MailTemplateResource.class);

    private static final String ENTITY_NAME = "mailTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MailTemplateService mailTemplateService;

    public MailTemplateResource(MailTemplateService mailTemplateService) {
        this.mailTemplateService = mailTemplateService;
    }


    @PostMapping("/mail-templates")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.MAIL_TEMPLATE.ADD})

    public ResponseEntity<MailTemplate> createMailTemplate(@RequestBody MailTemplate mailTemplate) throws URISyntaxException {
        log.debug("REST request to save MailTemplate : {}", mailTemplate);
        if (mailTemplate.getId() != null) {
            throw new BadRequestAlertException("A new mailTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MailTemplate result = mailTemplateService.save(mailTemplate);
        return ResponseEntity.created(new URI("/api/mail-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PutMapping("/mail-templates")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.MAIL_TEMPLATE.EDIT})

    public ResponseEntity<MailTemplate> updateMailTemplate(@RequestBody MailTemplate mailTemplate) throws URISyntaxException {
        log.debug("REST request to update MailTemplate : {}", mailTemplate);
        if (mailTemplate.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MailTemplate result = mailTemplateService.save(mailTemplate);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mailTemplate.getId().toString()))
            .body(result);
    }


    @GetMapping("/mail-templates")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.MAIL_TEMPLATE.VIEW})

    public ResponseEntity<List<MailTemplate>> getAllMailTemplates(Pageable pageable) {
        log.debug("REST request to get a page of MailTemplates");
        Page<MailTemplate> page = mailTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    @GetMapping("/mail-templates/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.MAIL_TEMPLATE.VIEW})

    public ResponseEntity<MailTemplate> getMailTemplate(@PathVariable Long id) {
        log.debug("REST request to get MailTemplate : {}", id);
        Optional<MailTemplate> mailTemplate = mailTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mailTemplate);
    }


    @DeleteMapping("/mail-templates/{id}")
    @RolesAllowed({RoleConstant.ADMIN,RoleConstant.MAIL_TEMPLATE.EDIT})

    public ResponseEntity<Void> deleteMailTemplate(@PathVariable Long id) {
        log.debug("REST request to delete MailTemplate : {}", id);
        mailTemplateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
