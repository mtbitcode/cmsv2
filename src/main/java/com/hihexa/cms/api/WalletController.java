package com.hihexa.cms.api;

import com.hihexa.cms.domain.WithdrawEntity;
import com.hihexa.cms.domain.WithdrawHistoryEntity;
import com.hihexa.cms.model.wallet.*;
import com.hihexa.cms.service.WalletService;
import com.hihexa.cms.util.DConstants;
import com.hihexa.cms.util.ErrorConstant;
import com.hihexa.cms.web.rest.errors.CMSException;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/wallet")
public class WalletController {

	private static final Logger log = LoggerFactory.getLogger(WalletController.class);

	private final WalletService walletService;

	public WalletController(final WalletService walletService) {
		this.walletService = walletService;
	}

	/**
	 * /api/wallet/v2/createAddress/{userId}/{currency}
	 * @param userId
	 * @param currency
	 * @return
	 */
	@PostMapping("/v2/createAddress/{userId}/{currencyId}/{brokerId}")
	@ApiOperation(value = "API gen một địa chỉ ví mới cho mỗi người dùng",response = WalletController.class)
	public ResponseEntity<?> generationAddress(@PathVariable("userId") Long userId,
			@PathVariable("currencyId") Integer currencyId, @PathVariable("brokerId") Integer brokerId) {
		ApiResponse<AddressModel> baseResponse =null;
		AddressModel address = null;
		try {
			long t1 = System.currentTimeMillis();
			address = walletService.generationAddress(userId, currencyId, brokerId);
			long t2 = System.currentTimeMillis();
			log.info("generateNewAddress {} TIMEXXX '{}'", (t2 - t1));
			baseResponse = new ApiResponse<AddressModel>(address);
			return new ResponseEntity<ApiResponse<AddressModel>>(baseResponse, HttpStatus.OK);
		} catch (Exception ex) {
			log.error("Cannot get new address", ex);
			baseResponse =new ApiResponse<AddressModel>(ex.getMessage());
			return new ResponseEntity<>(baseResponse, HttpStatus.OK);
		}
	}

	/**
	 * /api/wallet/v2/confirmWithdraw
	 * @param withdrawRequest
	 * @return
	 */
	@PostMapping("/v2/confirmWithdraw")
	@ApiOperation(value = "[WALLET] phát sinh khi người dùng confirmed email , status in [withdraw] = processing",response = WalletController.class)
	public @ResponseBody ResponseEntity<?> withdrawConfirmed(
			@Valid @RequestBody RestConfirmWithdrawRequest withdrawRequest) {
		ApiResponse<WithdrawEntity> baseResponse =null;
		WithdrawEntity wd = null;
		try {
			long t1 = System.currentTimeMillis();
			wd = walletService.withdrawConfirmed(withdrawRequest);
			long t2 = System.currentTimeMillis();
			log.info("confirmWithdraw success {} TIMEXXX '{}'", (t2 - t1));
			baseResponse = new ApiResponse<WithdrawEntity>(wd);
		} catch (CMSException ex) {
			log.error("Invalid request confirmWithdraw", ex.getMessage());
			baseResponse =new ApiResponse<WithdrawEntity>(ex.getErrorCode(), ex.getMessage());
		} catch (Exception e) {
			log.error("Invalid request confirmWithdraw", e.getMessage());
			baseResponse =new ApiResponse<WithdrawEntity>(DConstants.BASE_RESPONSE.STATUS_ERROR, DConstants.BASE_RESPONSE.MESSAGE_ERROR);
		}
		return new ResponseEntity<>(baseResponse, HttpStatus.OK);
	}


	/**
	 * /api/wallet/v2/createWithdraw
	 * @param withdrawRequest
	 * @return
	 */
	@PostMapping("/v2/createWithdraw")
	@ApiOperation(value = "API xử lý khi có phát sinh withdraw , status in [withdraw] = PENDING", response = WalletController.class)
	public ResponseEntity<ApiResponse<?>> createWithdraw(@Valid @RequestBody WithdrawCreationRequest withdrawRequest) {
		WithdrawEntity response = null;
		try {
			response = walletService.createWithdraw(withdrawRequest);
		} catch (CMSException ex) {
			log.error("Invalid request createWithdraw", ex.getMessage());
			return new ResponseEntity<ApiResponse<?>>(new ApiResponse<>(ex.getErrorCode(), ex.getMessage()), HttpStatus.OK);
		} catch (Exception e) {
			log.error("Invalid request createWithdraw", e.getMessage());
			//e.printStackTrace();
			return new ResponseEntity<ApiResponse<?>>(new ApiResponse<>(e.getMessage()), HttpStatus.OK);
		}
		return new ResponseEntity<ApiResponse<?>>(new ApiResponse<WithdrawEntity>(response), HttpStatus.OK);
	}


	/**
	 * /api/wallet/v2/newDeposit
	 * @param depositRequest
	 * @return
	 */
	@PostMapping("/v2/newDeposit")
	@ApiOperation(value = "API cho wallet gọi , Khi 1 giao dịch trong những địa chỉ thuộc wallet_id có đủ num_confirmations và address có giao dịch gửi vào đã đc confirm trên blockchain",response = WalletController.class)
	public ResponseEntity<ApiResponse<Integer>> createDeposit(@RequestBody @Valid DepositRequestModel depositRequest) {
		ApiResponse<Integer> baseResponse = null;
		Integer responseStatus = null;
		try {
			long t1 = System.currentTimeMillis();
			responseStatus = walletService.createNewDeposit(depositRequest);
			if (responseStatus == null || responseStatus != 1) {
				baseResponse = new ApiResponse<Integer>(DConstants.BASE_RESPONSE.STATUS_ERROR, DConstants.BASE_RESPONSE.MESSAGE_ERROR, responseStatus);
				return new ResponseEntity<ApiResponse<Integer>>(baseResponse, HttpStatus.BAD_REQUEST);
			}
			long t2 = System.currentTimeMillis();
			log.info("createDeposit request '{}' TIMEXXX {}", depositRequest, t2 - t1);
			return new ResponseEntity<ApiResponse<Integer>>(new ApiResponse<Integer>(responseStatus), HttpStatus.OK);
		} catch (Exception ex) {
			log.error("ERRORXXXX Invalid request '{}' '{}'", depositRequest, ex);
			baseResponse = new ApiResponse<Integer>(DConstants.BASE_RESPONSE.STATUS_ERROR, ex.getMessage(), null);
			return new ResponseEntity<ApiResponse<Integer>>(baseResponse, HttpStatus.BAD_GATEWAY);
		}
	}

	/**
	 * /api/wallet/v2/webhook
	 * @param webhook
	 * @return
	 */
	@PostMapping("/v2/webhook")
	@ApiOperation(value = "API chỉ gọi một lần để confirm số num_confirmations",response = WalletController.class)
	public ResponseEntity<?> webhookDeposit(@RequestBody @Valid WebhookRequestModel webhook) {
		WebhookResponseModel reponse = new WebhookResponseModel();
		try {
			long t1 = System.currentTimeMillis();
			reponse = walletService.webhook(webhook);
			long t2 = System.currentTimeMillis();
			log.info("webhookDeposit request '{}' TIMEXXX {}", webhook, t2 - t1);
		} catch (Exception ex) {
			log.error("ERRORXXXX Invalid request '{}'", ex);
		}

		return new ResponseEntity<WebhookResponseModel>(reponse, HttpStatus.OK);
	}

	/**
	 * /api/wallet/v2/findWalletId/{currency_id}
	 * @param currency
	 * @return
	 */
	@PostMapping("/v2/findWalletId/{currency_id}")
	@ApiOperation(value = "API chỉ gọi một lần để lấy ra walletID của mỗi loại currency ,  walletID k đổi",response = WalletController.class)
	public ResponseEntity<?> findWalletId(@PathVariable("currency_id") String currency ,@RequestParam("account_name") String accountName) {
		try {
			WalletInfoResponse reponse = walletService.findWallet(currency ,accountName);
			log.info("webhookDeposit request '{}' ", reponse);
			return new ResponseEntity<WalletInfoResponse>(reponse, HttpStatus.OK);
		} catch (Exception ex) {
			log.error("ERRORXXXX Invalid request '{}'", ex);
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
	}

	/**
	 * /api/wallet/v2/newBlockRequest
	 * @param block
	 * @return
	 */
	@PostMapping("/v2/newBlockRequest")
	@ApiOperation(value = "API chỉ gọi một lần để confirm số num_confirmations",response = WalletController.class)
	public ResponseEntity<?> newBlockRequest(@RequestBody @Valid WalletNewBlock block) {
		try {
			long t1 = System.currentTimeMillis();
			Map<String, String> response = walletService.newBlockRequest(block);
			long t2 = System.currentTimeMillis();
			log.info("webhookDeposit request '{}' TIMEXXX {}", block, t2 - t1);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			log.error("ERRORXXXX Invalid request '{}'", ex);
		}

		return new ResponseEntity<>("FAIL", HttpStatus.OK);
	}



	/**
	 * /api/wallet/v2/getAddress/{userId}/{currency}
	 *
	 * @return
	 */
	@GetMapping("/v2/getAddress/{userId}/{currency}")
	@ApiOperation(value = "API tìm kiếm địa chỉ theo user_id và currency",response = WalletController.class)
	public ResponseEntity<?> getAddress(@PathVariable("userId") Long userId, @PathVariable("currency") String currency) {
		try {
			long t1 = System.currentTimeMillis();
			AddressModel address = walletService.getAddressByUserIdAndCurrency(userId, currency);
			long t2 = System.currentTimeMillis();
			log.info("getAddress new user {} TIMEXXX '{}'", userId, t2 - t1);
			return new ResponseEntity<>(address, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}
	}

	/**
	 * /api/wallet/v2/findAddress/{userId}/{currency}
	 *
	 * @return
	 */
	@GetMapping("/v2/findAddress/{userId}/{currency}")
	@ApiOperation(value = "API tìm kiếm địa chỉ theo user_id và currency",response = WalletController.class)
	public ResponseEntity<?> findAddress(@PathVariable("userId") Long userId, @PathVariable("currency") String currency) {
		try {
			long t1 = System.currentTimeMillis();
			AddressModel addressModel = walletService.getAddressByUserIdAndCurrency(userId, currency);
			String address = addressModel.getAddress();
			long t2 = System.currentTimeMillis();
			log.info("getAddress new user {} TIMEXXX '{}'", userId, t2 - t1);
			return new ResponseEntity<>(address, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("", HttpStatus.OK);
		}
	}

	@PutMapping("/v2/cancelWithdraw")
    public ResponseEntity<?> cancelWithdraw(@RequestBody UpdateWithdrawRequest request) {
        try {
            long t1 = System.currentTimeMillis();
            WithdrawEntity wd = walletService.userCancelWithdraw(request);
            long t2 = System.currentTimeMillis();
            log.info("cancel wd request userId {} - wd Id {} TIMEXXX '{}'", request.getUserId(), request.getWithdrawId() , t2 - t1);
            return new ResponseEntity<>(wd, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("", HttpStatus.OK);
        }
    }

    @PutMapping("/v2/adminUpdate")
    public ResponseEntity<?> adminUpdateWD(@RequestBody UpdateWithdrawRequest request) {
        ApiResponse<WithdrawHistoryEntity> response = new ApiResponse<>();
	    try {
            long t1 = System.currentTimeMillis();
            WithdrawHistoryEntity wd = walletService.adminUpdateWithdraw(request);
            long t2 = System.currentTimeMillis();
            log.info("admin update wd request userId {} - wd Id {} TIMEXXX '{}'", request.getUserId(), request.getWithdrawId() , t2 - t1);
            response.setData(wd);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (CMSException ex){
            response.setErrorCode(ex.getErrorCode());
            response.setErrorMessage(ex.getMessage());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            response.setErrorCode(ErrorConstant.Withdraw.ERR_UNKNOW.getCode());
            response.setErrorMessage(ErrorConstant.Withdraw.ERR_UNKNOW.getMessage());
            return new ResponseEntity<>("", HttpStatus.OK);
        }
    }

}
