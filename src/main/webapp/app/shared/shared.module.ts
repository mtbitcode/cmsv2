import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Cmsv2SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [Cmsv2SharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [Cmsv2SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2SharedModule {
  static forRoot() {
    return {
      ngModule: Cmsv2SharedModule
    };
  }
}
