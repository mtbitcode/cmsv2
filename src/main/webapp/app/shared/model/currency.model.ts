import { Moment } from 'moment';

export interface ICurrency {
  id?: number;
  name?: string;
  code?: string;
  targetConfirms?: number;
  activeFlg?: number;
  created?: Moment;
  updated?: Moment;
  isDemo?: number;
  maxSize?: number;
  minSize?: number;
  maxTime?: number;
  initAmountDemo?: number;
  decimalAmount?: number;
  baseUnit?: number;
}

export class Currency implements ICurrency {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public targetConfirms?: number,
    public activeFlg?: number,
    public created?: Moment,
    public updated?: Moment,
    public isDemo?: number,
    public maxSize?: number,
    public minSize?: number,
    public maxTime?: number,
    public initAmountDemo?: number,
    public decimalAmount?: number,
    public baseUnit?: number
  ) {}
}
