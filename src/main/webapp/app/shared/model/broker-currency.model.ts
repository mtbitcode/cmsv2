import { Moment } from 'moment';

export interface IBrokerCurrency {
  id?: number;
  brokerId?: number;
  currencyId?: number;
  enableWithdraw?: number;
  enableDeposit?: number;
  minDeposit?: number;
  minWithdraw?: number;
  feeWithdraw?: number;
  activeFlg?: number;
  created?: Moment;
  updated?: Moment;
  maxAllowWithdraw?: number;
  walletId?: string;
}

export class BrokerCurrency implements IBrokerCurrency {
  constructor(
    public id?: number,
    public brokerId?: number,
    public currencyId?: number,
    public enableWithdraw?: number,
    public enableDeposit?: number,
    public minDeposit?: number,
    public minWithdraw?: number,
    public feeWithdraw?: number,
    public activeFlg?: number,
    public created?: Moment,
    public updated?: Moment,
    public maxAllowWithdraw?: number,
    public walletId?: string
  ) {}
}
