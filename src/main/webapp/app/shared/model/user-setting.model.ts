import { Moment } from 'moment';

export interface IUserSetting {
  id?: number;
  loginAlert?: number;
  keepAlive?: number;
  detectIpChange?: number;
  activeFlg?: number;
  created?: Moment;
  updated?: Moment;
  dob?: Moment;
  avator?: string;
  username?: string;
  firstName?: string;
  middleName?: string;
  lastName?: string;
  sfa?: number;
  faSeed?: string;
  refId?: string;
  wdWhitelistStatus?: number;
  verifyLevel?: number;
  wdDailyLimit?: number;
  dailyWdAmount?: number;
  address?: string;
  postCode?: string;
  city?: string;
  country?: string;
  takerFee?: number;
  makerFee?: number;
  feeType?: number;
  identityId?: string;
  withdrawBlocktime?: number;
}

export class UserSetting implements IUserSetting {
  constructor(
    public id?: number,
    public loginAlert?: number,
    public keepAlive?: number,
    public detectIpChange?: number,
    public activeFlg?: number,
    public created?: Moment,
    public updated?: Moment,
    public dob?: Moment,
    public avator?: string,
    public username?: string,
    public firstName?: string,
    public middleName?: string,
    public lastName?: string,
    public sfa?: number,
    public faSeed?: string,
    public refId?: string,
    public wdWhitelistStatus?: number,
    public verifyLevel?: number,
    public wdDailyLimit?: number,
    public dailyWdAmount?: number,
    public address?: string,
    public postCode?: string,
    public city?: string,
    public country?: string,
    public takerFee?: number,
    public makerFee?: number,
    public feeType?: number,
    public identityId?: string,
    public withdrawBlocktime?: number
  ) {}
}
