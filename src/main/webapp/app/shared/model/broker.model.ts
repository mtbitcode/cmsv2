export interface IBroker {
  id?: number;
  secretId?: string;
  name?: string;
  activeFlg?: string;
  uri?: string;
}

export class Broker implements IBroker {
  constructor(public id?: number, public secretId?: string, public name?: string, public activeFlg?: string, public uri?: string) {}
}
