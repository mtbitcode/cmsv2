import { Moment } from 'moment';

export interface IAddress {
  id?: number;
  balanceId?: number;
  status?: number;
  address?: string;
  userId?: number;
  currency?: string;
  extraUuid?: string;
  currencyId?: number;
  path?: string;
  created?: Moment;
  brokerId?: number;
}

export class Address implements IAddress {
  constructor(
    public id?: number,
    public balanceId?: number,
    public status?: number,
    public address?: string,
    public userId?: number,
    public currency?: string,
    public extraUuid?: string,
    public currencyId?: number,
    public path?: string,
    public created?: Moment,
    public brokerId?: number
  ) {}
}
