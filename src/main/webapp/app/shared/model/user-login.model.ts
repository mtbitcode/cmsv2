import { Moment } from 'moment';

export interface IUserLogin {
  id?: number;
  brokerId?: number;
  email?: string;
  password?: string;
  status?: number;
  lastLogin?: Moment;
  agents?: string;
  ips?: string;
  activeFlg?: number;
  created?: Moment;
}

export class UserLogin implements IUserLogin {
  constructor(
    public id?: number,
    public brokerId?: number,
    public email?: string,
    public password?: string,
    public status?: number,
    public lastLogin?: Moment,
    public agents?: string,
    public ips?: string,
    public activeFlg?: number,
    public created?: Moment
  ) {}
}
