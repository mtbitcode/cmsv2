import { Moment } from 'moment';

export interface IUserLog {
  id?: number;
  ip?: string;
  userAgent?: string;
  action?: string;
  detail?: string;
  created?: Moment;
}

export class UserLog implements IUserLog {
  constructor(
    public id?: number,
    public ip?: string,
    public userAgent?: string,
    public action?: string,
    public detail?: string,
    public created?: Moment
  ) {}
}
