export interface ISymbolConfig {
  id?: number;
  symbol?: string;
  baseCcId?: number;
  baseCc?: string;
  counterCcId?: number;
  counterCc?: string;
  type?: number;
  groupName?: string;
  description?: string;
  decimalPlaceAmount?: number;
  decimalPlacePrice?: number;
  isDemo?: number;
}

export class SymbolConfig implements ISymbolConfig {
  constructor(
    public id?: number,
    public symbol?: string,
    public baseCcId?: number,
    public baseCc?: string,
    public counterCcId?: number,
    public counterCc?: string,
    public type?: number,
    public groupName?: string,
    public description?: string,
    public decimalPlaceAmount?: number,
    public decimalPlacePrice?: number,
    public isDemo?: number
  ) {}
}
