export interface IBalanceEntity {
  id?: number;
  userId?: number;
  currencyId?: number;
  type?: number;
  amount?: number;
  reserveAmount?: number;
}

export class BalanceEntity implements IBalanceEntity {
  constructor(
    public id?: number,
    public userId?: number,
    public currencyId?: number,
    public type?: number,
    public amount?: number,
    public reserveAmount?: number
  ) {}
}
