import { Moment } from 'moment';

export interface IMailTemplate {
  id?: number;
  mailCode?: string;
  mailContent?: any;
  mailType?: string;
  cc?: string;
  bcc?: string;
  created?: Moment;
  updated?: Moment;
  activeFlg?: number;
  mailSubject?: any;
}

export class MailTemplate implements IMailTemplate {
  constructor(
    public id?: number,
    public mailCode?: string,
    public mailContent?: any,
    public mailType?: string,
    public cc?: string,
    public bcc?: string,
    public created?: Moment,
    public updated?: Moment,
    public activeFlg?: number,
    public mailSubject?: any
  ) {}
}
