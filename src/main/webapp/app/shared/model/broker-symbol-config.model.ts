import { Moment } from 'moment';

export interface IBrokerSymbolConfig {
  id?: number;
  brokerId?: number;
  symbolId?: number;
  tradable?: number;
  minTrade?: number;
  marginEnable?: number;
  maxTrade?: number;
  maxPrice?: number;
  created?: Moment;
  updated?: Moment;
  minPrice?: number;
  activeFlg?: number;
}

export class BrokerSymbolConfig implements IBrokerSymbolConfig {
  constructor(
    public id?: number,
    public brokerId?: number,
    public symbolId?: number,
    public tradable?: number,
    public minTrade?: number,
    public marginEnable?: number,
    public maxTrade?: number,
    public maxPrice?: number,
    public created?: Moment,
    public updated?: Moment,
    public minPrice?: number,
    public activeFlg?: number
  ) {}
}
