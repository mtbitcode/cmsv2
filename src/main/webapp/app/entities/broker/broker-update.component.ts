import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IBroker, Broker } from 'app/shared/model/broker.model';
import { BrokerService } from './broker.service';

@Component({
  selector: 'jhi-broker-update',
  templateUrl: './broker-update.component.html'
})
export class BrokerUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    secretId: [],
    name: [],
    activeFlg: [],
    uri: []
  });

  constructor(protected brokerService: BrokerService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ broker }) => {
      this.updateForm(broker);
    });
  }

  updateForm(broker: IBroker) {
    this.editForm.patchValue({
      id: broker.id,
      secretId: broker.secretId,
      name: broker.name,
      activeFlg: broker.activeFlg,
      uri: broker.uri
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const broker = this.createFromForm();
    if (broker.id !== undefined) {
      this.subscribeToSaveResponse(this.brokerService.update(broker));
    } else {
      this.subscribeToSaveResponse(this.brokerService.create(broker));
    }
  }

  private createFromForm(): IBroker {
    return {
      ...new Broker(),
      id: this.editForm.get(['id']).value,
      secretId: this.editForm.get(['secretId']).value,
      name: this.editForm.get(['name']).value,
      activeFlg: this.editForm.get(['activeFlg']).value,
      uri: this.editForm.get(['uri']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBroker>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
