import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Broker } from 'app/shared/model/broker.model';
import { BrokerService } from './broker.service';
import { BrokerComponent } from './broker.component';
import { BrokerDetailComponent } from './broker-detail.component';
import { BrokerUpdateComponent } from './broker-update.component';
import { BrokerDeletePopupComponent } from './broker-delete-dialog.component';
import { IBroker } from 'app/shared/model/broker.model';

@Injectable({ providedIn: 'root' })
export class BrokerResolve implements Resolve<IBroker> {
  constructor(private service: BrokerService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBroker> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Broker>) => response.ok),
        map((broker: HttpResponse<Broker>) => broker.body)
      );
    }
    return of(new Broker());
  }
}

export const brokerRoute: Routes = [
  {
    path: '',
    component: BrokerComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.broker.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BrokerDetailComponent,
    resolve: {
      broker: BrokerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.broker.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BrokerUpdateComponent,
    resolve: {
      broker: BrokerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.broker.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BrokerUpdateComponent,
    resolve: {
      broker: BrokerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.broker.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const brokerPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BrokerDeletePopupComponent,
    resolve: {
      broker: BrokerResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.broker.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
