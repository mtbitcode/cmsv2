import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBrokerCurrency } from 'app/shared/model/broker-currency.model';
import { BrokerCurrencyService } from './broker-currency.service';

@Component({
  selector: 'jhi-broker-currency-delete-dialog',
  templateUrl: './broker-currency-delete-dialog.component.html'
})
export class BrokerCurrencyDeleteDialogComponent {
  brokerCurrency: IBrokerCurrency;

  constructor(
    protected brokerCurrencyService: BrokerCurrencyService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.brokerCurrencyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'brokerCurrencyListModification',
        content: 'Deleted an brokerCurrency'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-broker-currency-delete-popup',
  template: ''
})
export class BrokerCurrencyDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brokerCurrency }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BrokerCurrencyDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.brokerCurrency = brokerCurrency;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/broker-currency', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/broker-currency', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
