import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBrokerCurrency } from 'app/shared/model/broker-currency.model';

@Component({
  selector: 'jhi-broker-currency-detail',
  templateUrl: './broker-currency-detail.component.html'
})
export class BrokerCurrencyDetailComponent implements OnInit {
  brokerCurrency: IBrokerCurrency;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brokerCurrency }) => {
      this.brokerCurrency = brokerCurrency;
    });
  }

  previousState() {
    window.history.back();
  }
}
