import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IBrokerCurrency, BrokerCurrency } from 'app/shared/model/broker-currency.model';
import { BrokerCurrencyService } from './broker-currency.service';

@Component({
  selector: 'jhi-broker-currency-update',
  templateUrl: './broker-currency-update.component.html'
})
export class BrokerCurrencyUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    brokerId: [],
    currencyId: [],
    enableWithdraw: [],
    enableDeposit: [],
    minDeposit: [],
    minWithdraw: [],
    feeWithdraw: [],
    activeFlg: [],
    created: [],
    updated: [],
    maxAllowWithdraw: [],
    walletId: []
  });

  constructor(protected brokerCurrencyService: BrokerCurrencyService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ brokerCurrency }) => {
      this.updateForm(brokerCurrency);
    });
  }

  updateForm(brokerCurrency: IBrokerCurrency) {
    this.editForm.patchValue({
      id: brokerCurrency.id,
      brokerId: brokerCurrency.brokerId,
      currencyId: brokerCurrency.currencyId,
      enableWithdraw: brokerCurrency.enableWithdraw,
      enableDeposit: brokerCurrency.enableDeposit,
      minDeposit: brokerCurrency.minDeposit,
      minWithdraw: brokerCurrency.minWithdraw,
      feeWithdraw: brokerCurrency.feeWithdraw,
      activeFlg: brokerCurrency.activeFlg,
      created: brokerCurrency.created != null ? brokerCurrency.created.format(DATE_TIME_FORMAT) : null,
      updated: brokerCurrency.updated != null ? brokerCurrency.updated.format(DATE_TIME_FORMAT) : null,
      maxAllowWithdraw: brokerCurrency.maxAllowWithdraw,
      walletId: brokerCurrency.walletId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const brokerCurrency = this.createFromForm();
    if (brokerCurrency.id !== undefined) {
      this.subscribeToSaveResponse(this.brokerCurrencyService.update(brokerCurrency));
    } else {
      this.subscribeToSaveResponse(this.brokerCurrencyService.create(brokerCurrency));
    }
  }

  private createFromForm(): IBrokerCurrency {
    return {
      ...new BrokerCurrency(),
      id: this.editForm.get(['id']).value,
      brokerId: this.editForm.get(['brokerId']).value,
      currencyId: this.editForm.get(['currencyId']).value,
      enableWithdraw: this.editForm.get(['enableWithdraw']).value,
      enableDeposit: this.editForm.get(['enableDeposit']).value,
      minDeposit: this.editForm.get(['minDeposit']).value,
      minWithdraw: this.editForm.get(['minWithdraw']).value,
      feeWithdraw: this.editForm.get(['feeWithdraw']).value,
      activeFlg: this.editForm.get(['activeFlg']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      updated: this.editForm.get(['updated']).value != null ? moment(this.editForm.get(['updated']).value, DATE_TIME_FORMAT) : undefined,
      maxAllowWithdraw: this.editForm.get(['maxAllowWithdraw']).value,
      walletId: this.editForm.get(['walletId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBrokerCurrency>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
