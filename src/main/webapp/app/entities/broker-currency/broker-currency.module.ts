import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  BrokerCurrencyComponent,
  BrokerCurrencyDetailComponent,
  BrokerCurrencyUpdateComponent,
  BrokerCurrencyDeletePopupComponent,
  BrokerCurrencyDeleteDialogComponent,
  brokerCurrencyRoute,
  brokerCurrencyPopupRoute
} from './';

const ENTITY_STATES = [...brokerCurrencyRoute, ...brokerCurrencyPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BrokerCurrencyComponent,
    BrokerCurrencyDetailComponent,
    BrokerCurrencyUpdateComponent,
    BrokerCurrencyDeleteDialogComponent,
    BrokerCurrencyDeletePopupComponent
  ],
  entryComponents: [
    BrokerCurrencyComponent,
    BrokerCurrencyUpdateComponent,
    BrokerCurrencyDeleteDialogComponent,
    BrokerCurrencyDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2BrokerCurrencyModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
