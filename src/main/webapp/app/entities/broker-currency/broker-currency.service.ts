import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBrokerCurrency } from 'app/shared/model/broker-currency.model';

type EntityResponseType = HttpResponse<IBrokerCurrency>;
type EntityArrayResponseType = HttpResponse<IBrokerCurrency[]>;

@Injectable({ providedIn: 'root' })
export class BrokerCurrencyService {
  public resourceUrl = SERVER_API_URL + 'api/broker-currencies';

  constructor(protected http: HttpClient) {}

  create(brokerCurrency: IBrokerCurrency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(brokerCurrency);
    return this.http
      .post<IBrokerCurrency>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(brokerCurrency: IBrokerCurrency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(brokerCurrency);
    return this.http
      .put<IBrokerCurrency>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBrokerCurrency>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBrokerCurrency[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(brokerCurrency: IBrokerCurrency): IBrokerCurrency {
    const copy: IBrokerCurrency = Object.assign({}, brokerCurrency, {
      created: brokerCurrency.created != null && brokerCurrency.created.isValid() ? brokerCurrency.created.toJSON() : null,
      updated: brokerCurrency.updated != null && brokerCurrency.updated.isValid() ? brokerCurrency.updated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
      res.body.updated = res.body.updated != null ? moment(res.body.updated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((brokerCurrency: IBrokerCurrency) => {
        brokerCurrency.created = brokerCurrency.created != null ? moment(brokerCurrency.created) : null;
        brokerCurrency.updated = brokerCurrency.updated != null ? moment(brokerCurrency.updated) : null;
      });
    }
    return res;
  }
}
