import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BrokerCurrency } from 'app/shared/model/broker-currency.model';
import { BrokerCurrencyService } from './broker-currency.service';
import { BrokerCurrencyComponent } from './broker-currency.component';
import { BrokerCurrencyDetailComponent } from './broker-currency-detail.component';
import { BrokerCurrencyUpdateComponent } from './broker-currency-update.component';
import { BrokerCurrencyDeletePopupComponent } from './broker-currency-delete-dialog.component';
import { IBrokerCurrency } from 'app/shared/model/broker-currency.model';

@Injectable({ providedIn: 'root' })
export class BrokerCurrencyResolve implements Resolve<IBrokerCurrency> {
  constructor(private service: BrokerCurrencyService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBrokerCurrency> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BrokerCurrency>) => response.ok),
        map((brokerCurrency: HttpResponse<BrokerCurrency>) => brokerCurrency.body)
      );
    }
    return of(new BrokerCurrency());
  }
}

export const brokerCurrencyRoute: Routes = [
  {
    path: '',
    component: BrokerCurrencyComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.brokerCurrency.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BrokerCurrencyDetailComponent,
    resolve: {
      brokerCurrency: BrokerCurrencyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerCurrency.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BrokerCurrencyUpdateComponent,
    resolve: {
      brokerCurrency: BrokerCurrencyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerCurrency.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BrokerCurrencyUpdateComponent,
    resolve: {
      brokerCurrency: BrokerCurrencyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerCurrency.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const brokerCurrencyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BrokerCurrencyDeletePopupComponent,
    resolve: {
      brokerCurrency: BrokerCurrencyResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerCurrency.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
