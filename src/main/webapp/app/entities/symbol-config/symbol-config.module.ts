import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  SymbolConfigComponent,
  SymbolConfigDetailComponent,
  SymbolConfigUpdateComponent,
  SymbolConfigDeletePopupComponent,
  SymbolConfigDeleteDialogComponent,
  symbolConfigRoute,
  symbolConfigPopupRoute
} from './';

const ENTITY_STATES = [...symbolConfigRoute, ...symbolConfigPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    SymbolConfigComponent,
    SymbolConfigDetailComponent,
    SymbolConfigUpdateComponent,
    SymbolConfigDeleteDialogComponent,
    SymbolConfigDeletePopupComponent
  ],
  entryComponents: [
    SymbolConfigComponent,
    SymbolConfigUpdateComponent,
    SymbolConfigDeleteDialogComponent,
    SymbolConfigDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2SymbolConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
