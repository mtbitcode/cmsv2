import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ISymbolConfig } from 'app/shared/model/symbol-config.model';

type EntityResponseType = HttpResponse<ISymbolConfig>;
type EntityArrayResponseType = HttpResponse<ISymbolConfig[]>;

@Injectable({ providedIn: 'root' })
export class SymbolConfigService {
  public resourceUrl = SERVER_API_URL + 'api/symbol-configs';

  constructor(protected http: HttpClient) {}

  create(symbolConfig: ISymbolConfig): Observable<EntityResponseType> {
    return this.http.post<ISymbolConfig>(this.resourceUrl, symbolConfig, { observe: 'response' });
  }

  update(symbolConfig: ISymbolConfig): Observable<EntityResponseType> {
    return this.http.put<ISymbolConfig>(this.resourceUrl, symbolConfig, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ISymbolConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ISymbolConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
