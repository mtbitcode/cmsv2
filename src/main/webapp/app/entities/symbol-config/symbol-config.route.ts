import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { SymbolConfig } from 'app/shared/model/symbol-config.model';
import { SymbolConfigService } from './symbol-config.service';
import { SymbolConfigComponent } from './symbol-config.component';
import { SymbolConfigDetailComponent } from './symbol-config-detail.component';
import { SymbolConfigUpdateComponent } from './symbol-config-update.component';
import { SymbolConfigDeletePopupComponent } from './symbol-config-delete-dialog.component';
import { ISymbolConfig } from 'app/shared/model/symbol-config.model';

@Injectable({ providedIn: 'root' })
export class SymbolConfigResolve implements Resolve<ISymbolConfig> {
  constructor(private service: SymbolConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISymbolConfig> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SymbolConfig>) => response.ok),
        map((symbolConfig: HttpResponse<SymbolConfig>) => symbolConfig.body)
      );
    }
    return of(new SymbolConfig());
  }
}

export const symbolConfigRoute: Routes = [
  {
    path: '',
    component: SymbolConfigComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.symbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: SymbolConfigDetailComponent,
    resolve: {
      symbolConfig: SymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.symbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: SymbolConfigUpdateComponent,
    resolve: {
      symbolConfig: SymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.symbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: SymbolConfigUpdateComponent,
    resolve: {
      symbolConfig: SymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.symbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const symbolConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: SymbolConfigDeletePopupComponent,
    resolve: {
      symbolConfig: SymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.symbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
