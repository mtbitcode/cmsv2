import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ISymbolConfig } from 'app/shared/model/symbol-config.model';
import { SymbolConfigService } from './symbol-config.service';

@Component({
  selector: 'jhi-symbol-config-delete-dialog',
  templateUrl: './symbol-config-delete-dialog.component.html'
})
export class SymbolConfigDeleteDialogComponent {
  symbolConfig: ISymbolConfig;

  constructor(
    protected symbolConfigService: SymbolConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.symbolConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'symbolConfigListModification',
        content: 'Deleted an symbolConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-symbol-config-delete-popup',
  template: ''
})
export class SymbolConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ symbolConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(SymbolConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.symbolConfig = symbolConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/symbol-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/symbol-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
