import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ISymbolConfig, SymbolConfig } from 'app/shared/model/symbol-config.model';
import { SymbolConfigService } from './symbol-config.service';

@Component({
  selector: 'jhi-symbol-config-update',
  templateUrl: './symbol-config-update.component.html'
})
export class SymbolConfigUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    symbol: [],
    baseCcId: [],
    baseCc: [],
    counterCcId: [],
    counterCc: [],
    type: [],
    groupName: [],
    description: [],
    decimalPlaceAmount: [],
    decimalPlacePrice: [],
    isDemo: []
  });

  constructor(protected symbolConfigService: SymbolConfigService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ symbolConfig }) => {
      this.updateForm(symbolConfig);
    });
  }

  updateForm(symbolConfig: ISymbolConfig) {
    this.editForm.patchValue({
      id: symbolConfig.id,
      symbol: symbolConfig.symbol,
      baseCcId: symbolConfig.baseCcId,
      baseCc: symbolConfig.baseCc,
      counterCcId: symbolConfig.counterCcId,
      counterCc: symbolConfig.counterCc,
      type: symbolConfig.type,
      groupName: symbolConfig.groupName,
      description: symbolConfig.description,
      decimalPlaceAmount: symbolConfig.decimalPlaceAmount,
      decimalPlacePrice: symbolConfig.decimalPlacePrice,
      isDemo: symbolConfig.isDemo
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const symbolConfig = this.createFromForm();
    if (symbolConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.symbolConfigService.update(symbolConfig));
    } else {
      this.subscribeToSaveResponse(this.symbolConfigService.create(symbolConfig));
    }
  }

  private createFromForm(): ISymbolConfig {
    return {
      ...new SymbolConfig(),
      id: this.editForm.get(['id']).value,
      symbol: this.editForm.get(['symbol']).value,
      baseCcId: this.editForm.get(['baseCcId']).value,
      baseCc: this.editForm.get(['baseCc']).value,
      counterCcId: this.editForm.get(['counterCcId']).value,
      counterCc: this.editForm.get(['counterCc']).value,
      type: this.editForm.get(['type']).value,
      groupName: this.editForm.get(['groupName']).value,
      description: this.editForm.get(['description']).value,
      decimalPlaceAmount: this.editForm.get(['decimalPlaceAmount']).value,
      decimalPlacePrice: this.editForm.get(['decimalPlacePrice']).value,
      isDemo: this.editForm.get(['isDemo']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISymbolConfig>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
