import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ISymbolConfig } from 'app/shared/model/symbol-config.model';

@Component({
  selector: 'jhi-symbol-config-detail',
  templateUrl: './symbol-config-detail.component.html'
})
export class SymbolConfigDetailComponent implements OnInit {
  symbolConfig: ISymbolConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ symbolConfig }) => {
      this.symbolConfig = symbolConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
