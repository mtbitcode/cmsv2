import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserSetting } from 'app/shared/model/user-setting.model';

@Component({
  selector: 'jhi-user-setting-detail',
  templateUrl: './user-setting-detail.component.html'
})
export class UserSettingDetailComponent implements OnInit {
  userSetting: IUserSetting;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userSetting }) => {
      this.userSetting = userSetting;
    });
  }

  previousState() {
    window.history.back();
  }
}
