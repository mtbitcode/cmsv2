import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserSetting } from 'app/shared/model/user-setting.model';

type EntityResponseType = HttpResponse<IUserSetting>;
type EntityArrayResponseType = HttpResponse<IUserSetting[]>;

@Injectable({ providedIn: 'root' })
export class UserSettingService {
  public resourceUrl = SERVER_API_URL + 'api/user-settings';

  constructor(protected http: HttpClient) {}

  create(userSetting: IUserSetting): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userSetting);
    return this.http
      .post<IUserSetting>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userSetting: IUserSetting): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userSetting);
    return this.http
      .put<IUserSetting>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserSetting>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserSetting[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(userSetting: IUserSetting): IUserSetting {
    const copy: IUserSetting = Object.assign({}, userSetting, {
      created: userSetting.created != null && userSetting.created.isValid() ? userSetting.created.toJSON() : null,
      updated: userSetting.updated != null && userSetting.updated.isValid() ? userSetting.updated.toJSON() : null,
      dob: userSetting.dob != null && userSetting.dob.isValid() ? userSetting.dob.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
      res.body.updated = res.body.updated != null ? moment(res.body.updated) : null;
      res.body.dob = res.body.dob != null ? moment(res.body.dob) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userSetting: IUserSetting) => {
        userSetting.created = userSetting.created != null ? moment(userSetting.created) : null;
        userSetting.updated = userSetting.updated != null ? moment(userSetting.updated) : null;
        userSetting.dob = userSetting.dob != null ? moment(userSetting.dob) : null;
      });
    }
    return res;
  }
}
