import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IUserSetting, UserSetting } from 'app/shared/model/user-setting.model';
import { UserSettingService } from './user-setting.service';

@Component({
  selector: 'jhi-user-setting-update',
  templateUrl: './user-setting-update.component.html'
})
export class UserSettingUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    loginAlert: [],
    keepAlive: [],
    detectIpChange: [],
    activeFlg: [],
    created: [],
    updated: [],
    dob: [],
    avator: [],
    username: [],
    firstName: [],
    middleName: [],
    lastName: [],
    sfa: [],
    faSeed: [],
    refId: [],
    wdWhitelistStatus: [],
    verifyLevel: [],
    wdDailyLimit: [],
    dailyWdAmount: [],
    address: [],
    postCode: [],
    city: [],
    country: [],
    takerFee: [],
    makerFee: [],
    feeType: [],
    identityId: [],
    withdrawBlocktime: []
  });

  constructor(protected userSettingService: UserSettingService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userSetting }) => {
      this.updateForm(userSetting);
    });
  }

  updateForm(userSetting: IUserSetting) {
    this.editForm.patchValue({
      id: userSetting.id,
      loginAlert: userSetting.loginAlert,
      keepAlive: userSetting.keepAlive,
      detectIpChange: userSetting.detectIpChange,
      activeFlg: userSetting.activeFlg,
      created: userSetting.created != null ? userSetting.created.format(DATE_TIME_FORMAT) : null,
      updated: userSetting.updated != null ? userSetting.updated.format(DATE_TIME_FORMAT) : null,
      dob: userSetting.dob != null ? userSetting.dob.format(DATE_TIME_FORMAT) : null,
      avator: userSetting.avator,
      username: userSetting.username,
      firstName: userSetting.firstName,
      middleName: userSetting.middleName,
      lastName: userSetting.lastName,
      sfa: userSetting.sfa,
      faSeed: userSetting.faSeed,
      refId: userSetting.refId,
      wdWhitelistStatus: userSetting.wdWhitelistStatus,
      verifyLevel: userSetting.verifyLevel,
      wdDailyLimit: userSetting.wdDailyLimit,
      dailyWdAmount: userSetting.dailyWdAmount,
      address: userSetting.address,
      postCode: userSetting.postCode,
      city: userSetting.city,
      country: userSetting.country,
      takerFee: userSetting.takerFee,
      makerFee: userSetting.makerFee,
      feeType: userSetting.feeType,
      identityId: userSetting.identityId,
      withdrawBlocktime: userSetting.withdrawBlocktime
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userSetting = this.createFromForm();
    if (userSetting.id !== undefined) {
      this.subscribeToSaveResponse(this.userSettingService.update(userSetting));
    } else {
      this.subscribeToSaveResponse(this.userSettingService.create(userSetting));
    }
  }

  private createFromForm(): IUserSetting {
    return {
      ...new UserSetting(),
      id: this.editForm.get(['id']).value,
      loginAlert: this.editForm.get(['loginAlert']).value,
      keepAlive: this.editForm.get(['keepAlive']).value,
      detectIpChange: this.editForm.get(['detectIpChange']).value,
      activeFlg: this.editForm.get(['activeFlg']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      updated: this.editForm.get(['updated']).value != null ? moment(this.editForm.get(['updated']).value, DATE_TIME_FORMAT) : undefined,
      dob: this.editForm.get(['dob']).value != null ? moment(this.editForm.get(['dob']).value, DATE_TIME_FORMAT) : undefined,
      avator: this.editForm.get(['avator']).value,
      username: this.editForm.get(['username']).value,
      firstName: this.editForm.get(['firstName']).value,
      middleName: this.editForm.get(['middleName']).value,
      lastName: this.editForm.get(['lastName']).value,
      sfa: this.editForm.get(['sfa']).value,
      faSeed: this.editForm.get(['faSeed']).value,
      refId: this.editForm.get(['refId']).value,
      wdWhitelistStatus: this.editForm.get(['wdWhitelistStatus']).value,
      verifyLevel: this.editForm.get(['verifyLevel']).value,
      wdDailyLimit: this.editForm.get(['wdDailyLimit']).value,
      dailyWdAmount: this.editForm.get(['dailyWdAmount']).value,
      address: this.editForm.get(['address']).value,
      postCode: this.editForm.get(['postCode']).value,
      city: this.editForm.get(['city']).value,
      country: this.editForm.get(['country']).value,
      takerFee: this.editForm.get(['takerFee']).value,
      makerFee: this.editForm.get(['makerFee']).value,
      feeType: this.editForm.get(['feeType']).value,
      identityId: this.editForm.get(['identityId']).value,
      withdrawBlocktime: this.editForm.get(['withdrawBlocktime']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserSetting>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
