import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  UserSettingComponent,
  UserSettingDetailComponent,
  UserSettingUpdateComponent,
  UserSettingDeletePopupComponent,
  UserSettingDeleteDialogComponent,
  userSettingRoute,
  userSettingPopupRoute
} from './';

const ENTITY_STATES = [...userSettingRoute, ...userSettingPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserSettingComponent,
    UserSettingDetailComponent,
    UserSettingUpdateComponent,
    UserSettingDeleteDialogComponent,
    UserSettingDeletePopupComponent
  ],
  entryComponents: [UserSettingComponent, UserSettingUpdateComponent, UserSettingDeleteDialogComponent, UserSettingDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2UserSettingModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
