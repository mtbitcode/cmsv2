import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserSetting } from 'app/shared/model/user-setting.model';
import { UserSettingService } from './user-setting.service';

@Component({
  selector: 'jhi-user-setting-delete-dialog',
  templateUrl: './user-setting-delete-dialog.component.html'
})
export class UserSettingDeleteDialogComponent {
  userSetting: IUserSetting;

  constructor(
    protected userSettingService: UserSettingService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userSettingService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'userSettingListModification',
        content: 'Deleted an userSetting'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-setting-delete-popup',
  template: ''
})
export class UserSettingDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userSetting }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserSettingDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userSetting = userSetting;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/user-setting', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/user-setting', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
