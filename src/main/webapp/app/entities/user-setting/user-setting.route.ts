import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserSetting } from 'app/shared/model/user-setting.model';
import { UserSettingService } from './user-setting.service';
import { UserSettingComponent } from './user-setting.component';
import { UserSettingDetailComponent } from './user-setting-detail.component';
import { UserSettingUpdateComponent } from './user-setting-update.component';
import { UserSettingDeletePopupComponent } from './user-setting-delete-dialog.component';
import { IUserSetting } from 'app/shared/model/user-setting.model';

@Injectable({ providedIn: 'root' })
export class UserSettingResolve implements Resolve<IUserSetting> {
  constructor(private service: UserSettingService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserSetting> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserSetting>) => response.ok),
        map((userSetting: HttpResponse<UserSetting>) => userSetting.body)
      );
    }
    return of(new UserSetting());
  }
}

export const userSettingRoute: Routes = [
  {
    path: '',
    component: UserSettingComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.userSetting.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserSettingDetailComponent,
    resolve: {
      userSetting: UserSettingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userSetting.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserSettingUpdateComponent,
    resolve: {
      userSetting: UserSettingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userSetting.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserSettingUpdateComponent,
    resolve: {
      userSetting: UserSettingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userSetting.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userSettingPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserSettingDeletePopupComponent,
    resolve: {
      userSetting: UserSettingResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userSetting.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
