import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'user-login',
        loadChildren: () => import('./user-login/user-login.module').then(m => m.Cmsv2UserLoginModule)
      },
      {
        path: 'currency',
        loadChildren: () => import('./currency/currency.module').then(m => m.Cmsv2CurrencyModule)
      },
      {
        path: 'balance-entity',
        loadChildren: () => import('./balance-entity/balance-entity.module').then(m => m.Cmsv2BalanceEntityModule)
      },
      {
        path: 'address',
        loadChildren: () => import('./address/address.module').then(m => m.Cmsv2AddressModule)
      },
      {
        path: 'user-setting',
        loadChildren: () => import('./user-setting/user-setting.module').then(m => m.Cmsv2UserSettingModule)
      },
      {
        path: 'symbol-config',
        loadChildren: () => import('./symbol-config/symbol-config.module').then(m => m.Cmsv2SymbolConfigModule)
      },
      {
        path: 'broker-symbol-config',
        loadChildren: () => import('./broker-symbol-config/broker-symbol-config.module').then(m => m.Cmsv2BrokerSymbolConfigModule)
      },
      {
        path: 'broker-currency',
        loadChildren: () => import('./broker-currency/broker-currency.module').then(m => m.Cmsv2BrokerCurrencyModule)
      },
      {
        path: 'user-log',
        loadChildren: () => import('./user-log/user-log.module').then(m => m.Cmsv2UserLogModule)
      },
      {
        path: 'mail-template',
        loadChildren: () => import('./mail-template/mail-template.module').then(m => m.Cmsv2MailTemplateModule)
      },
      {
        path: 'broker',
        loadChildren: () => import('./broker/broker.module').then(m => m.Cmsv2BrokerModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2EntityModule {}
