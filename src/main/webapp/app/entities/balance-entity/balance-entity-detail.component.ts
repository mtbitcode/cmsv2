import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBalanceEntity } from 'app/shared/model/balance-entity.model';

@Component({
  selector: 'jhi-balance-entity-detail',
  templateUrl: './balance-entity-detail.component.html'
})
export class BalanceEntityDetailComponent implements OnInit {
  balanceEntity: IBalanceEntity;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ balanceEntity }) => {
      this.balanceEntity = balanceEntity;
    });
  }

  previousState() {
    window.history.back();
  }
}
