import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IBalanceEntity, BalanceEntity } from 'app/shared/model/balance-entity.model';
import { BalanceEntityService } from './balance-entity.service';

@Component({
  selector: 'jhi-balance-entity-update',
  templateUrl: './balance-entity-update.component.html'
})
export class BalanceEntityUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    userId: [null, [Validators.required]],
    currencyId: [null, [Validators.required]],
    type: [null, [Validators.required]],
    amount: [],
    reserveAmount: []
  });

  constructor(protected balanceEntityService: BalanceEntityService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ balanceEntity }) => {
      this.updateForm(balanceEntity);
    });
  }

  updateForm(balanceEntity: IBalanceEntity) {
    this.editForm.patchValue({
      id: balanceEntity.id,
      userId: balanceEntity.userId,
      currencyId: balanceEntity.currencyId,
      type: balanceEntity.type,
      amount: balanceEntity.amount,
      reserveAmount: balanceEntity.reserveAmount
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const balanceEntity = this.createFromForm();
    if (balanceEntity.id !== undefined) {
      this.subscribeToSaveResponse(this.balanceEntityService.update(balanceEntity));
    } else {
      this.subscribeToSaveResponse(this.balanceEntityService.create(balanceEntity));
    }
  }

  private createFromForm(): IBalanceEntity {
    return {
      ...new BalanceEntity(),
      id: this.editForm.get(['id']).value,
      userId: this.editForm.get(['userId']).value,
      currencyId: this.editForm.get(['currencyId']).value,
      type: this.editForm.get(['type']).value,
      amount: this.editForm.get(['amount']).value,
      reserveAmount: this.editForm.get(['reserveAmount']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBalanceEntity>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
