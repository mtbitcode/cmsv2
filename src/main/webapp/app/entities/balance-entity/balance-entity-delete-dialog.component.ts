import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBalanceEntity } from 'app/shared/model/balance-entity.model';
import { BalanceEntityService } from './balance-entity.service';

@Component({
  selector: 'jhi-balance-entity-delete-dialog',
  templateUrl: './balance-entity-delete-dialog.component.html'
})
export class BalanceEntityDeleteDialogComponent {
  balanceEntity: IBalanceEntity;

  constructor(
    protected balanceEntityService: BalanceEntityService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.balanceEntityService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'balanceEntityListModification',
        content: 'Deleted an balanceEntity'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-balance-entity-delete-popup',
  template: ''
})
export class BalanceEntityDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ balanceEntity }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BalanceEntityDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.balanceEntity = balanceEntity;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/balance-entity', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/balance-entity', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
