import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  BalanceEntityComponent,
  BalanceEntityDetailComponent,
  BalanceEntityUpdateComponent,
  BalanceEntityDeletePopupComponent,
  BalanceEntityDeleteDialogComponent,
  balanceEntityRoute,
  balanceEntityPopupRoute
} from './';

const ENTITY_STATES = [...balanceEntityRoute, ...balanceEntityPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BalanceEntityComponent,
    BalanceEntityDetailComponent,
    BalanceEntityUpdateComponent,
    BalanceEntityDeleteDialogComponent,
    BalanceEntityDeletePopupComponent
  ],
  entryComponents: [
    BalanceEntityComponent,
    BalanceEntityUpdateComponent,
    BalanceEntityDeleteDialogComponent,
    BalanceEntityDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2BalanceEntityModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
