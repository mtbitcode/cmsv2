import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBalanceEntity } from 'app/shared/model/balance-entity.model';

type EntityResponseType = HttpResponse<IBalanceEntity>;
type EntityArrayResponseType = HttpResponse<IBalanceEntity[]>;

@Injectable({ providedIn: 'root' })
export class BalanceEntityService {
  public resourceUrl = SERVER_API_URL + 'api/balance-entities';

  constructor(protected http: HttpClient) {}

  create(balanceEntity: IBalanceEntity): Observable<EntityResponseType> {
    return this.http.post<IBalanceEntity>(this.resourceUrl, balanceEntity, { observe: 'response' });
  }

  update(balanceEntity: IBalanceEntity): Observable<EntityResponseType> {
    return this.http.put<IBalanceEntity>(this.resourceUrl, balanceEntity, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBalanceEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBalanceEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
