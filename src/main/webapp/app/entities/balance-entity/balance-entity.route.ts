import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BalanceEntity } from 'app/shared/model/balance-entity.model';
import { BalanceEntityService } from './balance-entity.service';
import { BalanceEntityComponent } from './balance-entity.component';
import { BalanceEntityDetailComponent } from './balance-entity-detail.component';
import { BalanceEntityUpdateComponent } from './balance-entity-update.component';
import { BalanceEntityDeletePopupComponent } from './balance-entity-delete-dialog.component';
import { IBalanceEntity } from 'app/shared/model/balance-entity.model';

@Injectable({ providedIn: 'root' })
export class BalanceEntityResolve implements Resolve<IBalanceEntity> {
  constructor(private service: BalanceEntityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBalanceEntity> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BalanceEntity>) => response.ok),
        map((balanceEntity: HttpResponse<BalanceEntity>) => balanceEntity.body)
      );
    }
    return of(new BalanceEntity());
  }
}

export const balanceEntityRoute: Routes = [
  {
    path: '',
    component: BalanceEntityComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.balanceEntity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BalanceEntityDetailComponent,
    resolve: {
      balanceEntity: BalanceEntityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.balanceEntity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BalanceEntityUpdateComponent,
    resolve: {
      balanceEntity: BalanceEntityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.balanceEntity.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BalanceEntityUpdateComponent,
    resolve: {
      balanceEntity: BalanceEntityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.balanceEntity.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const balanceEntityPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BalanceEntityDeletePopupComponent,
    resolve: {
      balanceEntity: BalanceEntityResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.balanceEntity.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
