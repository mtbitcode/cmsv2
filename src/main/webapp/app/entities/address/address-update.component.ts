import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IAddress, Address } from 'app/shared/model/address.model';
import { AddressService } from './address.service';

@Component({
  selector: 'jhi-address-update',
  templateUrl: './address-update.component.html'
})
export class AddressUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    balanceId: [],
    status: [],
    address: [],
    userId: [],
    currency: [],
    extraUuid: [],
    currencyId: [],
    path: [],
    created: [],
    brokerId: []
  });

  constructor(protected addressService: AddressService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ address }) => {
      this.updateForm(address);
    });
  }

  updateForm(address: IAddress) {
    this.editForm.patchValue({
      id: address.id,
      balanceId: address.balanceId,
      status: address.status,
      address: address.address,
      userId: address.userId,
      currency: address.currency,
      extraUuid: address.extraUuid,
      currencyId: address.currencyId,
      path: address.path,
      created: address.created != null ? address.created.format(DATE_TIME_FORMAT) : null,
      brokerId: address.brokerId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const address = this.createFromForm();
    if (address.id !== undefined) {
      this.subscribeToSaveResponse(this.addressService.update(address));
    } else {
      this.subscribeToSaveResponse(this.addressService.create(address));
    }
  }

  private createFromForm(): IAddress {
    return {
      ...new Address(),
      id: this.editForm.get(['id']).value,
      balanceId: this.editForm.get(['balanceId']).value,
      status: this.editForm.get(['status']).value,
      address: this.editForm.get(['address']).value,
      userId: this.editForm.get(['userId']).value,
      currency: this.editForm.get(['currency']).value,
      extraUuid: this.editForm.get(['extraUuid']).value,
      currencyId: this.editForm.get(['currencyId']).value,
      path: this.editForm.get(['path']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      brokerId: this.editForm.get(['brokerId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAddress>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
