import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserLog } from 'app/shared/model/user-log.model';
import { UserLogService } from './user-log.service';
import { UserLogComponent } from './user-log.component';
import { UserLogDetailComponent } from './user-log-detail.component';
import { UserLogUpdateComponent } from './user-log-update.component';
import { UserLogDeletePopupComponent } from './user-log-delete-dialog.component';
import { IUserLog } from 'app/shared/model/user-log.model';

@Injectable({ providedIn: 'root' })
export class UserLogResolve implements Resolve<IUserLog> {
  constructor(private service: UserLogService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserLog> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserLog>) => response.ok),
        map((userLog: HttpResponse<UserLog>) => userLog.body)
      );
    }
    return of(new UserLog());
  }
}

export const userLogRoute: Routes = [
  {
    path: '',
    component: UserLogComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.userLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserLogDetailComponent,
    resolve: {
      userLog: UserLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserLogUpdateComponent,
    resolve: {
      userLog: UserLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserLogUpdateComponent,
    resolve: {
      userLog: UserLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLog.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userLogPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserLogDeletePopupComponent,
    resolve: {
      userLog: UserLogResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLog.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
