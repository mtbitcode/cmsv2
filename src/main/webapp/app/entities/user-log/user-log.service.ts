import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserLog } from 'app/shared/model/user-log.model';

type EntityResponseType = HttpResponse<IUserLog>;
type EntityArrayResponseType = HttpResponse<IUserLog[]>;

@Injectable({ providedIn: 'root' })
export class UserLogService {
  public resourceUrl = SERVER_API_URL + 'api/user-logs';

  constructor(protected http: HttpClient) {}

  create(userLog: IUserLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLog);
    return this.http
      .post<IUserLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userLog: IUserLog): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLog);
    return this.http
      .put<IUserLog>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserLog>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserLog[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(userLog: IUserLog): IUserLog {
    const copy: IUserLog = Object.assign({}, userLog, {
      created: userLog.created != null && userLog.created.isValid() ? userLog.created.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userLog: IUserLog) => {
        userLog.created = userLog.created != null ? moment(userLog.created) : null;
      });
    }
    return res;
  }
}
