import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IUserLog } from 'app/shared/model/user-log.model';

@Component({
  selector: 'jhi-user-log-detail',
  templateUrl: './user-log-detail.component.html'
})
export class UserLogDetailComponent implements OnInit {
  userLog: IUserLog;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userLog }) => {
      this.userLog = userLog;
    });
  }

  previousState() {
    window.history.back();
  }
}
