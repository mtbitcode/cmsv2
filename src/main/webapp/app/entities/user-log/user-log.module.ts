import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  UserLogComponent,
  UserLogDetailComponent,
  UserLogUpdateComponent,
  UserLogDeletePopupComponent,
  UserLogDeleteDialogComponent,
  userLogRoute,
  userLogPopupRoute
} from './';

const ENTITY_STATES = [...userLogRoute, ...userLogPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserLogComponent,
    UserLogDetailComponent,
    UserLogUpdateComponent,
    UserLogDeleteDialogComponent,
    UserLogDeletePopupComponent
  ],
  entryComponents: [UserLogComponent, UserLogUpdateComponent, UserLogDeleteDialogComponent, UserLogDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2UserLogModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
