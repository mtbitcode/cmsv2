import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserLog } from 'app/shared/model/user-log.model';
import { UserLogService } from './user-log.service';

@Component({
  selector: 'jhi-user-log-delete-dialog',
  templateUrl: './user-log-delete-dialog.component.html'
})
export class UserLogDeleteDialogComponent {
  userLog: IUserLog;

  constructor(protected userLogService: UserLogService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userLogService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'userLogListModification',
        content: 'Deleted an userLog'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-log-delete-popup',
  template: ''
})
export class UserLogDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userLog }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserLogDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userLog = userLog;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/user-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/user-log', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
