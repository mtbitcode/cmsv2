import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IUserLog, UserLog } from 'app/shared/model/user-log.model';
import { UserLogService } from './user-log.service';

@Component({
  selector: 'jhi-user-log-update',
  templateUrl: './user-log-update.component.html'
})
export class UserLogUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    ip: [],
    userAgent: [],
    action: [],
    detail: [],
    created: []
  });

  constructor(protected userLogService: UserLogService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userLog }) => {
      this.updateForm(userLog);
    });
  }

  updateForm(userLog: IUserLog) {
    this.editForm.patchValue({
      id: userLog.id,
      ip: userLog.ip,
      userAgent: userLog.userAgent,
      action: userLog.action,
      detail: userLog.detail,
      created: userLog.created != null ? userLog.created.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userLog = this.createFromForm();
    if (userLog.id !== undefined) {
      this.subscribeToSaveResponse(this.userLogService.update(userLog));
    } else {
      this.subscribeToSaveResponse(this.userLogService.create(userLog));
    }
  }

  private createFromForm(): IUserLog {
    return {
      ...new UserLog(),
      id: this.editForm.get(['id']).value,
      ip: this.editForm.get(['ip']).value,
      userAgent: this.editForm.get(['userAgent']).value,
      action: this.editForm.get(['action']).value,
      detail: this.editForm.get(['detail']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserLog>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
