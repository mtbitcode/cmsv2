import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

type EntityResponseType = HttpResponse<IBrokerSymbolConfig>;
type EntityArrayResponseType = HttpResponse<IBrokerSymbolConfig[]>;

@Injectable({ providedIn: 'root' })
export class BrokerSymbolConfigService {
  public resourceUrl = SERVER_API_URL + 'api/broker-symbol-configs';

  constructor(protected http: HttpClient) {}

  create(brokerSymbolConfig: IBrokerSymbolConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(brokerSymbolConfig);
    return this.http
      .post<IBrokerSymbolConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(brokerSymbolConfig: IBrokerSymbolConfig): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(brokerSymbolConfig);
    return this.http
      .put<IBrokerSymbolConfig>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBrokerSymbolConfig>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBrokerSymbolConfig[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(brokerSymbolConfig: IBrokerSymbolConfig): IBrokerSymbolConfig {
    const copy: IBrokerSymbolConfig = Object.assign({}, brokerSymbolConfig, {
      created: brokerSymbolConfig.created != null && brokerSymbolConfig.created.isValid() ? brokerSymbolConfig.created.toJSON() : null,
      updated: brokerSymbolConfig.updated != null && brokerSymbolConfig.updated.isValid() ? brokerSymbolConfig.updated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
      res.body.updated = res.body.updated != null ? moment(res.body.updated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((brokerSymbolConfig: IBrokerSymbolConfig) => {
        brokerSymbolConfig.created = brokerSymbolConfig.created != null ? moment(brokerSymbolConfig.created) : null;
        brokerSymbolConfig.updated = brokerSymbolConfig.updated != null ? moment(brokerSymbolConfig.updated) : null;
      });
    }
    return res;
  }
}
