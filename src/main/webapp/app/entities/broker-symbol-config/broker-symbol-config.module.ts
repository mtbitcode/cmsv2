import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  BrokerSymbolConfigComponent,
  BrokerSymbolConfigDetailComponent,
  BrokerSymbolConfigUpdateComponent,
  BrokerSymbolConfigDeletePopupComponent,
  BrokerSymbolConfigDeleteDialogComponent,
  brokerSymbolConfigRoute,
  brokerSymbolConfigPopupRoute
} from './';

const ENTITY_STATES = [...brokerSymbolConfigRoute, ...brokerSymbolConfigPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    BrokerSymbolConfigComponent,
    BrokerSymbolConfigDetailComponent,
    BrokerSymbolConfigUpdateComponent,
    BrokerSymbolConfigDeleteDialogComponent,
    BrokerSymbolConfigDeletePopupComponent
  ],
  entryComponents: [
    BrokerSymbolConfigComponent,
    BrokerSymbolConfigUpdateComponent,
    BrokerSymbolConfigDeleteDialogComponent,
    BrokerSymbolConfigDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2BrokerSymbolConfigModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
