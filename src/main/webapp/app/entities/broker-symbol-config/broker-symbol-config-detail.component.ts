import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

@Component({
  selector: 'jhi-broker-symbol-config-detail',
  templateUrl: './broker-symbol-config-detail.component.html'
})
export class BrokerSymbolConfigDetailComponent implements OnInit {
  brokerSymbolConfig: IBrokerSymbolConfig;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brokerSymbolConfig }) => {
      this.brokerSymbolConfig = brokerSymbolConfig;
    });
  }

  previousState() {
    window.history.back();
  }
}
