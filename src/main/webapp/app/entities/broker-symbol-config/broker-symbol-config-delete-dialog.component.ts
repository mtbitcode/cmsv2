import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';
import { BrokerSymbolConfigService } from './broker-symbol-config.service';

@Component({
  selector: 'jhi-broker-symbol-config-delete-dialog',
  templateUrl: './broker-symbol-config-delete-dialog.component.html'
})
export class BrokerSymbolConfigDeleteDialogComponent {
  brokerSymbolConfig: IBrokerSymbolConfig;

  constructor(
    protected brokerSymbolConfigService: BrokerSymbolConfigService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.brokerSymbolConfigService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'brokerSymbolConfigListModification',
        content: 'Deleted an brokerSymbolConfig'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-broker-symbol-config-delete-popup',
  template: ''
})
export class BrokerSymbolConfigDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ brokerSymbolConfig }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(BrokerSymbolConfigDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.brokerSymbolConfig = brokerSymbolConfig;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/broker-symbol-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/broker-symbol-config', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
