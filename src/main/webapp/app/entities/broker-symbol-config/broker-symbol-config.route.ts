import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';
import { BrokerSymbolConfigService } from './broker-symbol-config.service';
import { BrokerSymbolConfigComponent } from './broker-symbol-config.component';
import { BrokerSymbolConfigDetailComponent } from './broker-symbol-config-detail.component';
import { BrokerSymbolConfigUpdateComponent } from './broker-symbol-config-update.component';
import { BrokerSymbolConfigDeletePopupComponent } from './broker-symbol-config-delete-dialog.component';
import { IBrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

@Injectable({ providedIn: 'root' })
export class BrokerSymbolConfigResolve implements Resolve<IBrokerSymbolConfig> {
  constructor(private service: BrokerSymbolConfigService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBrokerSymbolConfig> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<BrokerSymbolConfig>) => response.ok),
        map((brokerSymbolConfig: HttpResponse<BrokerSymbolConfig>) => brokerSymbolConfig.body)
      );
    }
    return of(new BrokerSymbolConfig());
  }
}

export const brokerSymbolConfigRoute: Routes = [
  {
    path: '',
    component: BrokerSymbolConfigComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.brokerSymbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BrokerSymbolConfigDetailComponent,
    resolve: {
      brokerSymbolConfig: BrokerSymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerSymbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BrokerSymbolConfigUpdateComponent,
    resolve: {
      brokerSymbolConfig: BrokerSymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerSymbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BrokerSymbolConfigUpdateComponent,
    resolve: {
      brokerSymbolConfig: BrokerSymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerSymbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const brokerSymbolConfigPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: BrokerSymbolConfigDeletePopupComponent,
    resolve: {
      brokerSymbolConfig: BrokerSymbolConfigResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.brokerSymbolConfig.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
