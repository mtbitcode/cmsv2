import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IBrokerSymbolConfig, BrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';
import { BrokerSymbolConfigService } from './broker-symbol-config.service';

@Component({
  selector: 'jhi-broker-symbol-config-update',
  templateUrl: './broker-symbol-config-update.component.html'
})
export class BrokerSymbolConfigUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    brokerId: [],
    symbolId: [],
    tradable: [],
    minTrade: [],
    marginEnable: [],
    maxTrade: [],
    maxPrice: [],
    created: [],
    updated: [],
    minPrice: [],
    activeFlg: []
  });

  constructor(
    protected brokerSymbolConfigService: BrokerSymbolConfigService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ brokerSymbolConfig }) => {
      this.updateForm(brokerSymbolConfig);
    });
  }

  updateForm(brokerSymbolConfig: IBrokerSymbolConfig) {
    this.editForm.patchValue({
      id: brokerSymbolConfig.id,
      brokerId: brokerSymbolConfig.brokerId,
      symbolId: brokerSymbolConfig.symbolId,
      tradable: brokerSymbolConfig.tradable,
      minTrade: brokerSymbolConfig.minTrade,
      marginEnable: brokerSymbolConfig.marginEnable,
      maxTrade: brokerSymbolConfig.maxTrade,
      maxPrice: brokerSymbolConfig.maxPrice,
      created: brokerSymbolConfig.created != null ? brokerSymbolConfig.created.format(DATE_TIME_FORMAT) : null,
      updated: brokerSymbolConfig.updated != null ? brokerSymbolConfig.updated.format(DATE_TIME_FORMAT) : null,
      minPrice: brokerSymbolConfig.minPrice,
      activeFlg: brokerSymbolConfig.activeFlg
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const brokerSymbolConfig = this.createFromForm();
    if (brokerSymbolConfig.id !== undefined) {
      this.subscribeToSaveResponse(this.brokerSymbolConfigService.update(brokerSymbolConfig));
    } else {
      this.subscribeToSaveResponse(this.brokerSymbolConfigService.create(brokerSymbolConfig));
    }
  }

  private createFromForm(): IBrokerSymbolConfig {
    return {
      ...new BrokerSymbolConfig(),
      id: this.editForm.get(['id']).value,
      brokerId: this.editForm.get(['brokerId']).value,
      symbolId: this.editForm.get(['symbolId']).value,
      tradable: this.editForm.get(['tradable']).value,
      minTrade: this.editForm.get(['minTrade']).value,
      marginEnable: this.editForm.get(['marginEnable']).value,
      maxTrade: this.editForm.get(['maxTrade']).value,
      maxPrice: this.editForm.get(['maxPrice']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      updated: this.editForm.get(['updated']).value != null ? moment(this.editForm.get(['updated']).value, DATE_TIME_FORMAT) : undefined,
      minPrice: this.editForm.get(['minPrice']).value,
      activeFlg: this.editForm.get(['activeFlg']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBrokerSymbolConfig>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
