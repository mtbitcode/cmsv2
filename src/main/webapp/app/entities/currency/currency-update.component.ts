import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { ICurrency, Currency } from 'app/shared/model/currency.model';
import { CurrencyService } from './currency.service';

@Component({
  selector: 'jhi-currency-update',
  templateUrl: './currency-update.component.html'
})
export class CurrencyUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    code: [],
    targetConfirms: [],
    activeFlg: [],
    created: [],
    updated: [],
    isDemo: [],
    maxSize: [],
    minSize: [],
    maxTime: [],
    initAmountDemo: [],
    decimalAmount: [],
    baseUnit: []
  });

  constructor(protected currencyService: CurrencyService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ currency }) => {
      this.updateForm(currency);
    });
  }

  updateForm(currency: ICurrency) {
    this.editForm.patchValue({
      id: currency.id,
      name: currency.name,
      code: currency.code,
      targetConfirms: currency.targetConfirms,
      activeFlg: currency.activeFlg,
      created: currency.created != null ? currency.created.format(DATE_TIME_FORMAT) : null,
      updated: currency.updated != null ? currency.updated.format(DATE_TIME_FORMAT) : null,
      isDemo: currency.isDemo,
      maxSize: currency.maxSize,
      minSize: currency.minSize,
      maxTime: currency.maxTime,
      initAmountDemo: currency.initAmountDemo,
      decimalAmount: currency.decimalAmount,
      baseUnit: currency.baseUnit
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const currency = this.createFromForm();
    if (currency.id !== undefined) {
      this.subscribeToSaveResponse(this.currencyService.update(currency));
    } else {
      this.subscribeToSaveResponse(this.currencyService.create(currency));
    }
  }

  private createFromForm(): ICurrency {
    return {
      ...new Currency(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      code: this.editForm.get(['code']).value,
      targetConfirms: this.editForm.get(['targetConfirms']).value,
      activeFlg: this.editForm.get(['activeFlg']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      updated: this.editForm.get(['updated']).value != null ? moment(this.editForm.get(['updated']).value, DATE_TIME_FORMAT) : undefined,
      isDemo: this.editForm.get(['isDemo']).value,
      maxSize: this.editForm.get(['maxSize']).value,
      minSize: this.editForm.get(['minSize']).value,
      maxTime: this.editForm.get(['maxTime']).value,
      initAmountDemo: this.editForm.get(['initAmountDemo']).value,
      decimalAmount: this.editForm.get(['decimalAmount']).value,
      baseUnit: this.editForm.get(['baseUnit']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICurrency>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
