import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  UserLoginComponent,
  UserLoginDetailComponent,
  UserLoginUpdateComponent,
  UserLoginDeletePopupComponent,
  UserLoginDeleteDialogComponent,
  userLoginRoute,
  userLoginPopupRoute
} from './';

const ENTITY_STATES = [...userLoginRoute, ...userLoginPopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserLoginComponent,
    UserLoginDetailComponent,
    UserLoginUpdateComponent,
    UserLoginDeleteDialogComponent,
    UserLoginDeletePopupComponent
  ],
  entryComponents: [UserLoginComponent, UserLoginUpdateComponent, UserLoginDeleteDialogComponent, UserLoginDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2UserLoginModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
