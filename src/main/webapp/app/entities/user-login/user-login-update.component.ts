import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { IUserLogin, UserLogin } from 'app/shared/model/user-login.model';
import { UserLoginService } from './user-login.service';

@Component({
  selector: 'jhi-user-login-update',
  templateUrl: './user-login-update.component.html'
})
export class UserLoginUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    brokerId: [],
    email: [],
    password: [],
    status: [],
    lastLogin: [],
    agents: [],
    ips: [],
    activeFlg: [],
    created: []
  });

  constructor(protected userLoginService: UserLoginService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userLogin }) => {
      this.updateForm(userLogin);
    });
  }

  updateForm(userLogin: IUserLogin) {
    this.editForm.patchValue({
      id: userLogin.id,
      brokerId: userLogin.brokerId,
      email: userLogin.email,
      password: userLogin.password,
      status: userLogin.status,
      lastLogin: userLogin.lastLogin != null ? userLogin.lastLogin.format(DATE_TIME_FORMAT) : null,
      agents: userLogin.agents,
      ips: userLogin.ips,
      activeFlg: userLogin.activeFlg,
      created: userLogin.created != null ? userLogin.created.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userLogin = this.createFromForm();
    if (userLogin.id !== undefined) {
      this.subscribeToSaveResponse(this.userLoginService.update(userLogin));
    } else {
      this.subscribeToSaveResponse(this.userLoginService.create(userLogin));
    }
  }

  private createFromForm(): IUserLogin {
    return {
      ...new UserLogin(),
      id: this.editForm.get(['id']).value,
      brokerId: this.editForm.get(['brokerId']).value,
      email: this.editForm.get(['email']).value,
      password: this.editForm.get(['password']).value,
      status: this.editForm.get(['status']).value,
      lastLogin:
        this.editForm.get(['lastLogin']).value != null ? moment(this.editForm.get(['lastLogin']).value, DATE_TIME_FORMAT) : undefined,
      agents: this.editForm.get(['agents']).value,
      ips: this.editForm.get(['ips']).value,
      activeFlg: this.editForm.get(['activeFlg']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserLogin>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
