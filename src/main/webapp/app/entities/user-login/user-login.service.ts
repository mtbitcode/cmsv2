import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserLogin } from 'app/shared/model/user-login.model';

type EntityResponseType = HttpResponse<IUserLogin>;
type EntityArrayResponseType = HttpResponse<IUserLogin[]>;

@Injectable({ providedIn: 'root' })
export class UserLoginService {
  public resourceUrl = SERVER_API_URL + 'api/user-logins';

  constructor(protected http: HttpClient) {}

  create(userLogin: IUserLogin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLogin);
    return this.http
      .post<IUserLogin>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(userLogin: IUserLogin): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(userLogin);
    return this.http
      .put<IUserLogin>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUserLogin>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IUserLogin[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(userLogin: IUserLogin): IUserLogin {
    const copy: IUserLogin = Object.assign({}, userLogin, {
      lastLogin: userLogin.lastLogin != null && userLogin.lastLogin.isValid() ? userLogin.lastLogin.toJSON() : null,
      created: userLogin.created != null && userLogin.created.isValid() ? userLogin.created.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.lastLogin = res.body.lastLogin != null ? moment(res.body.lastLogin) : null;
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((userLogin: IUserLogin) => {
        userLogin.lastLogin = userLogin.lastLogin != null ? moment(userLogin.lastLogin) : null;
        userLogin.created = userLogin.created != null ? moment(userLogin.created) : null;
      });
    }
    return res;
  }
}
