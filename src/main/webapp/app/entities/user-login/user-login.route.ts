import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserLogin } from 'app/shared/model/user-login.model';
import { UserLoginService } from './user-login.service';
import { UserLoginComponent } from './user-login.component';
import { UserLoginDetailComponent } from './user-login-detail.component';
import { UserLoginUpdateComponent } from './user-login-update.component';
import { UserLoginDeletePopupComponent } from './user-login-delete-dialog.component';
import { IUserLogin } from 'app/shared/model/user-login.model';

@Injectable({ providedIn: 'root' })
export class UserLoginResolve implements Resolve<IUserLogin> {
  constructor(private service: UserLoginService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserLogin> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserLogin>) => response.ok),
        map((userLogin: HttpResponse<UserLogin>) => userLogin.body)
      );
    }
    return of(new UserLogin());
  }
}

export const userLoginRoute: Routes = [
  {
    path: '',
    component: UserLoginComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.userLogin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserLoginDetailComponent,
    resolve: {
      userLogin: UserLoginResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLogin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserLoginUpdateComponent,
    resolve: {
      userLogin: UserLoginResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLogin.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserLoginUpdateComponent,
    resolve: {
      userLogin: UserLoginResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLogin.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userLoginPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserLoginDeletePopupComponent,
    resolve: {
      userLogin: UserLoginResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.userLogin.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
