import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Cmsv2SharedModule } from 'app/shared';
import {
  MailTemplateComponent,
  MailTemplateDetailComponent,
  MailTemplateUpdateComponent,
  MailTemplateDeletePopupComponent,
  MailTemplateDeleteDialogComponent,
  mailTemplateRoute,
  mailTemplatePopupRoute
} from './';

const ENTITY_STATES = [...mailTemplateRoute, ...mailTemplatePopupRoute];

@NgModule({
  imports: [Cmsv2SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MailTemplateComponent,
    MailTemplateDetailComponent,
    MailTemplateUpdateComponent,
    MailTemplateDeleteDialogComponent,
    MailTemplateDeletePopupComponent
  ],
  entryComponents: [
    MailTemplateComponent,
    MailTemplateUpdateComponent,
    MailTemplateDeleteDialogComponent,
    MailTemplateDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Cmsv2MailTemplateModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
