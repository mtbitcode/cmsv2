import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MailTemplate } from 'app/shared/model/mail-template.model';
import { MailTemplateService } from './mail-template.service';
import { MailTemplateComponent } from './mail-template.component';
import { MailTemplateDetailComponent } from './mail-template-detail.component';
import { MailTemplateUpdateComponent } from './mail-template-update.component';
import { MailTemplateDeletePopupComponent } from './mail-template-delete-dialog.component';
import { IMailTemplate } from 'app/shared/model/mail-template.model';

@Injectable({ providedIn: 'root' })
export class MailTemplateResolve implements Resolve<IMailTemplate> {
  constructor(private service: MailTemplateService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMailTemplate> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<MailTemplate>) => response.ok),
        map((mailTemplate: HttpResponse<MailTemplate>) => mailTemplate.body)
      );
    }
    return of(new MailTemplate());
  }
}

export const mailTemplateRoute: Routes = [
  {
    path: '',
    component: MailTemplateComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'cmsv2App.mailTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MailTemplateDetailComponent,
    resolve: {
      mailTemplate: MailTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.mailTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MailTemplateUpdateComponent,
    resolve: {
      mailTemplate: MailTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.mailTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MailTemplateUpdateComponent,
    resolve: {
      mailTemplate: MailTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.mailTemplate.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const mailTemplatePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MailTemplateDeletePopupComponent,
    resolve: {
      mailTemplate: MailTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'cmsv2App.mailTemplate.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
