import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IMailTemplate, MailTemplate } from 'app/shared/model/mail-template.model';
import { MailTemplateService } from './mail-template.service';

@Component({
  selector: 'jhi-mail-template-update',
  templateUrl: './mail-template-update.component.html'
})
export class MailTemplateUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    mailCode: [],
    mailContent: [],
    mailType: [],
    cc: [],
    bcc: [],
    created: [],
    updated: [],
    activeFlg: [],
    mailSubject: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected mailTemplateService: MailTemplateService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ mailTemplate }) => {
      this.updateForm(mailTemplate);
    });
  }

  updateForm(mailTemplate: IMailTemplate) {
    this.editForm.patchValue({
      id: mailTemplate.id,
      mailCode: mailTemplate.mailCode,
      mailContent: mailTemplate.mailContent,
      mailType: mailTemplate.mailType,
      cc: mailTemplate.cc,
      bcc: mailTemplate.bcc,
      created: mailTemplate.created != null ? mailTemplate.created.format(DATE_TIME_FORMAT) : null,
      updated: mailTemplate.updated != null ? mailTemplate.updated.format(DATE_TIME_FORMAT) : null,
      activeFlg: mailTemplate.activeFlg,
      mailSubject: mailTemplate.mailSubject
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        if (isImage && !/^image\//.test(file.type)) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => console.log('blob added'), // sucess
      this.onError
    );
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const mailTemplate = this.createFromForm();
    if (mailTemplate.id !== undefined) {
      this.subscribeToSaveResponse(this.mailTemplateService.update(mailTemplate));
    } else {
      this.subscribeToSaveResponse(this.mailTemplateService.create(mailTemplate));
    }
  }

  private createFromForm(): IMailTemplate {
    return {
      ...new MailTemplate(),
      id: this.editForm.get(['id']).value,
      mailCode: this.editForm.get(['mailCode']).value,
      mailContent: this.editForm.get(['mailContent']).value,
      mailType: this.editForm.get(['mailType']).value,
      cc: this.editForm.get(['cc']).value,
      bcc: this.editForm.get(['bcc']).value,
      created: this.editForm.get(['created']).value != null ? moment(this.editForm.get(['created']).value, DATE_TIME_FORMAT) : undefined,
      updated: this.editForm.get(['updated']).value != null ? moment(this.editForm.get(['updated']).value, DATE_TIME_FORMAT) : undefined,
      activeFlg: this.editForm.get(['activeFlg']).value,
      mailSubject: this.editForm.get(['mailSubject']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMailTemplate>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
