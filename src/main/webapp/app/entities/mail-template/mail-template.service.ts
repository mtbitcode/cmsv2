import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMailTemplate } from 'app/shared/model/mail-template.model';

type EntityResponseType = HttpResponse<IMailTemplate>;
type EntityArrayResponseType = HttpResponse<IMailTemplate[]>;

@Injectable({ providedIn: 'root' })
export class MailTemplateService {
  public resourceUrl = SERVER_API_URL + 'api/mail-templates';

  constructor(protected http: HttpClient) {}

  create(mailTemplate: IMailTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mailTemplate);
    return this.http
      .post<IMailTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(mailTemplate: IMailTemplate): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mailTemplate);
    return this.http
      .put<IMailTemplate>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMailTemplate>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMailTemplate[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(mailTemplate: IMailTemplate): IMailTemplate {
    const copy: IMailTemplate = Object.assign({}, mailTemplate, {
      created: mailTemplate.created != null && mailTemplate.created.isValid() ? mailTemplate.created.toJSON() : null,
      updated: mailTemplate.updated != null && mailTemplate.updated.isValid() ? mailTemplate.updated.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.created = res.body.created != null ? moment(res.body.created) : null;
      res.body.updated = res.body.updated != null ? moment(res.body.updated) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((mailTemplate: IMailTemplate) => {
        mailTemplate.created = mailTemplate.created != null ? moment(mailTemplate.created) : null;
        mailTemplate.updated = mailTemplate.updated != null ? moment(mailTemplate.updated) : null;
      });
    }
    return res;
  }
}
