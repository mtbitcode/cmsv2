export * from './mail-template.service';
export * from './mail-template-update.component';
export * from './mail-template-delete-dialog.component';
export * from './mail-template-detail.component';
export * from './mail-template.component';
export * from './mail-template.route';
