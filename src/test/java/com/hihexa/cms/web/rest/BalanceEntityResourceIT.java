package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.BalanceEntity;
import com.hihexa.cms.repository.BalanceRepository;
import com.hihexa.cms.service.BalanceService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BalanceResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class BalanceEntityResourceIT {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Integer DEFAULT_CURRENCY_ID = 1;
    private static final Integer UPDATED_CURRENCY_ID = 2;

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;

    private static final BigInteger DEFAULT_AMOUNT = new BigInteger("1");
    private static final BigInteger UPDATED_AMOUNT = new BigInteger("2");

    private static final BigInteger DEFAULT_RESERVE_AMOUNT = new BigInteger("1");
    private static final BigInteger UPDATED_RESERVE_AMOUNT = new BigInteger("2");

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private BalanceService balanceEntityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBalanceEntityMockMvc;

    private BalanceEntity balanceEntity;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BalanceResource balanceEntityResource = new BalanceResource(balanceEntityService);
        this.restBalanceEntityMockMvc = MockMvcBuilders.standaloneSetup(balanceEntityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BalanceEntity createEntity(EntityManager em) {
        BalanceEntity balanceEntity = new BalanceEntity()
            .userId(DEFAULT_USER_ID)
            .currencyId(DEFAULT_CURRENCY_ID)
            .type(DEFAULT_TYPE)
            .amount(DEFAULT_AMOUNT)
            .reserveAmount(DEFAULT_RESERVE_AMOUNT);
        return balanceEntity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BalanceEntity createUpdatedEntity(EntityManager em) {
        BalanceEntity balanceEntity = new BalanceEntity()
            .userId(UPDATED_USER_ID)
            .currencyId(UPDATED_CURRENCY_ID)
            .type(UPDATED_TYPE)
            .amount(UPDATED_AMOUNT)
            .reserveAmount(UPDATED_RESERVE_AMOUNT);
        return balanceEntity;
    }

    @BeforeEach
    public void initTest() {
        balanceEntity = createEntity(em);
    }

    @Test
    @Transactional
    public void createBalanceEntity() throws Exception {
        int databaseSizeBeforeCreate = balanceRepository.findAll().size();

        // Create the BalanceEntity
        restBalanceEntityMockMvc.perform(post("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isCreated());

        // Validate the BalanceEntity in the database
        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeCreate + 1);
        BalanceEntity testBalanceEntity = balanceEntityList.get(balanceEntityList.size() - 1);
        assertThat(testBalanceEntity.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testBalanceEntity.getCurrencyId()).isEqualTo(DEFAULT_CURRENCY_ID);
        assertThat(testBalanceEntity.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testBalanceEntity.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testBalanceEntity.getReserveAmount()).isEqualTo(DEFAULT_RESERVE_AMOUNT);
    }

    @Test
    @Transactional
    public void createBalanceEntityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = balanceRepository.findAll().size();

        // Create the BalanceEntity with an existing ID
        balanceEntity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBalanceEntityMockMvc.perform(post("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isBadRequest());

        // Validate the BalanceEntity in the database
        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = balanceRepository.findAll().size();
        // set the field null
        balanceEntity.setUserId(null);

        // Create the BalanceEntity, which fails.

        restBalanceEntityMockMvc.perform(post("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isBadRequest());

        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCurrencyIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = balanceRepository.findAll().size();
        // set the field null
        balanceEntity.setCurrencyId(null);

        // Create the BalanceEntity, which fails.

        restBalanceEntityMockMvc.perform(post("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isBadRequest());

        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = balanceRepository.findAll().size();
        // set the field null
        balanceEntity.setType(null);

        // Create the BalanceEntity, which fails.

        restBalanceEntityMockMvc.perform(post("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isBadRequest());

        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBalanceEntities() throws Exception {
        // Initialize the database
        balanceRepository.saveAndFlush(balanceEntity);

        // Get all the balanceEntityList
        restBalanceEntityMockMvc.perform(get("/api/balance-entities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(balanceEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].currencyId").value(hasItem(DEFAULT_CURRENCY_ID.intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].reserveAmount").value(hasItem(DEFAULT_RESERVE_AMOUNT.intValue())));
    }

    @Test
    @Transactional
    public void getBalanceEntity() throws Exception {
        // Initialize the database
        balanceRepository.saveAndFlush(balanceEntity);

        // Get the balanceEntity
        restBalanceEntityMockMvc.perform(get("/api/balance-entities/{id}", balanceEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(balanceEntity.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.currencyId").value(DEFAULT_CURRENCY_ID.intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.intValue()))
            .andExpect(jsonPath("$.reserveAmount").value(DEFAULT_RESERVE_AMOUNT.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBalanceEntity() throws Exception {
        // Get the balanceEntity
        restBalanceEntityMockMvc.perform(get("/api/balance-entities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBalanceEntity() throws Exception {
        // Initialize the database
        balanceEntityService.save(balanceEntity);

        int databaseSizeBeforeUpdate = balanceRepository.findAll().size();

        // Update the balanceEntity
        BalanceEntity updatedBalanceEntity = balanceRepository.findById(balanceEntity.getId()).get();
        // Disconnect from session so that the updates on updatedBalanceEntity are not directly saved in db
        em.detach(updatedBalanceEntity);
        updatedBalanceEntity
            .userId(UPDATED_USER_ID)
            .currencyId(UPDATED_CURRENCY_ID)
            .type(UPDATED_TYPE)
            .amount(UPDATED_AMOUNT)
            .reserveAmount(UPDATED_RESERVE_AMOUNT);

        restBalanceEntityMockMvc.perform(put("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBalanceEntity)))
            .andExpect(status().isOk());

        // Validate the BalanceEntity in the database
        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeUpdate);
        BalanceEntity testBalanceEntity = balanceEntityList.get(balanceEntityList.size() - 1);
        assertThat(testBalanceEntity.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testBalanceEntity.getCurrencyId()).isEqualTo(UPDATED_CURRENCY_ID);
        assertThat(testBalanceEntity.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testBalanceEntity.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testBalanceEntity.getReserveAmount()).isEqualTo(UPDATED_RESERVE_AMOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingBalanceEntity() throws Exception {
        int databaseSizeBeforeUpdate = balanceRepository.findAll().size();

        // Create the BalanceEntity

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBalanceEntityMockMvc.perform(put("/api/balance-entities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(balanceEntity)))
            .andExpect(status().isBadRequest());

        // Validate the BalanceEntity in the database
        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBalanceEntity() throws Exception {
        // Initialize the database
        balanceEntityService.save(balanceEntity);

        int databaseSizeBeforeDelete = balanceRepository.findAll().size();

        // Delete the balanceEntity
        restBalanceEntityMockMvc.perform(delete("/api/balance-entities/{id}", balanceEntity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BalanceEntity> balanceEntityList = balanceRepository.findAll();
        assertThat(balanceEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BalanceEntity.class);
        BalanceEntity balanceEntity1 = new BalanceEntity();
        balanceEntity1.setId(1L);
        BalanceEntity balanceEntity2 = new BalanceEntity();
        balanceEntity2.setId(balanceEntity1.getId());
        assertThat(balanceEntity1).isEqualTo(balanceEntity2);
        balanceEntity2.setId(2L);
        assertThat(balanceEntity1).isNotEqualTo(balanceEntity2);
        balanceEntity1.setId(null);
        assertThat(balanceEntity1).isNotEqualTo(balanceEntity2);
    }
}
