package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.MailTemplate;
import com.hihexa.cms.repository.MailTemplateRepository;
import com.hihexa.cms.service.MailTemplateService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MailTemplateResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class MailTemplateResourceIT {

    private static final String DEFAULT_MAIL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_CC = "AAAAAAAAAA";
    private static final String UPDATED_CC = "BBBBBBBBBB";

    private static final String DEFAULT_BCC = "AAAAAAAAAA";
    private static final String UPDATED_BCC = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_UPDATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;

    private static final String DEFAULT_MAIL_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_SUBJECT = "BBBBBBBBBB";

    @Autowired
    private MailTemplateRepository mailTemplateRepository;

    @Autowired
    private MailTemplateService mailTemplateService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMailTemplateMockMvc;

    private MailTemplate mailTemplate;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MailTemplateResource mailTemplateResource = new MailTemplateResource(mailTemplateService);
        this.restMailTemplateMockMvc = MockMvcBuilders.standaloneSetup(mailTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MailTemplate createEntity(EntityManager em) {
        MailTemplate mailTemplate = new MailTemplate()
            .mailCode(DEFAULT_MAIL_CODE)
            .mailContent(DEFAULT_MAIL_CONTENT)
            .mailType(DEFAULT_MAIL_TYPE)
            .cc(DEFAULT_CC)
            .bcc(DEFAULT_BCC)
            .created(DEFAULT_CREATED)
            .updated(DEFAULT_UPDATED)
            .activeFlg(DEFAULT_ACTIVE_FLG)
            .mailSubject(DEFAULT_MAIL_SUBJECT);
        return mailTemplate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MailTemplate createUpdatedEntity(EntityManager em) {
        MailTemplate mailTemplate = new MailTemplate()
            .mailCode(UPDATED_MAIL_CODE)
            .mailContent(UPDATED_MAIL_CONTENT)
            .mailType(UPDATED_MAIL_TYPE)
            .cc(UPDATED_CC)
            .bcc(UPDATED_BCC)
            .created(UPDATED_CREATED)
            .updated(UPDATED_UPDATED)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .mailSubject(UPDATED_MAIL_SUBJECT);
        return mailTemplate;
    }

    @BeforeEach
    public void initTest() {
        mailTemplate = createEntity(em);
    }

    @Test
    @Transactional
    public void createMailTemplate() throws Exception {
        int databaseSizeBeforeCreate = mailTemplateRepository.findAll().size();

        // Create the MailTemplate
        restMailTemplateMockMvc.perform(post("/api/mail-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailTemplate)))
            .andExpect(status().isCreated());

        // Validate the MailTemplate in the database
        List<MailTemplate> mailTemplateList = mailTemplateRepository.findAll();
        assertThat(mailTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        MailTemplate testMailTemplate = mailTemplateList.get(mailTemplateList.size() - 1);
        assertThat(testMailTemplate.getMailCode()).isEqualTo(DEFAULT_MAIL_CODE);
        assertThat(testMailTemplate.getMailContent()).isEqualTo(DEFAULT_MAIL_CONTENT);
        assertThat(testMailTemplate.getMailType()).isEqualTo(DEFAULT_MAIL_TYPE);
        assertThat(testMailTemplate.getCc()).isEqualTo(DEFAULT_CC);
        assertThat(testMailTemplate.getBcc()).isEqualTo(DEFAULT_BCC);
        assertThat(testMailTemplate.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testMailTemplate.getUpdated()).isEqualTo(DEFAULT_UPDATED);
        assertThat(testMailTemplate.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
        assertThat(testMailTemplate.getMailSubject()).isEqualTo(DEFAULT_MAIL_SUBJECT);
    }

    @Test
    @Transactional
    public void createMailTemplateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mailTemplateRepository.findAll().size();

        // Create the MailTemplate with an existing ID
        mailTemplate.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMailTemplateMockMvc.perform(post("/api/mail-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the MailTemplate in the database
        List<MailTemplate> mailTemplateList = mailTemplateRepository.findAll();
        assertThat(mailTemplateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMailTemplates() throws Exception {
        // Initialize the database
        mailTemplateRepository.saveAndFlush(mailTemplate);

        // Get all the mailTemplateList
        restMailTemplateMockMvc.perform(get("/api/mail-templates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mailTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].mailCode").value(hasItem(DEFAULT_MAIL_CODE.toString())))
            .andExpect(jsonPath("$.[*].mailContent").value(hasItem(DEFAULT_MAIL_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].mailType").value(hasItem(DEFAULT_MAIL_TYPE.toString())))
            .andExpect(jsonPath("$.[*].cc").value(hasItem(DEFAULT_CC.toString())))
            .andExpect(jsonPath("$.[*].bcc").value(hasItem(DEFAULT_BCC.toString())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED.toString())))
            .andExpect(jsonPath("$.[*].updated").value(hasItem(DEFAULT_UPDATED.toString())))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)))
            .andExpect(jsonPath("$.[*].mailSubject").value(hasItem(DEFAULT_MAIL_SUBJECT.toString())));
    }
    
    @Test
    @Transactional
    public void getMailTemplate() throws Exception {
        // Initialize the database
        mailTemplateRepository.saveAndFlush(mailTemplate);

        // Get the mailTemplate
        restMailTemplateMockMvc.perform(get("/api/mail-templates/{id}", mailTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(mailTemplate.getId().intValue()))
            .andExpect(jsonPath("$.mailCode").value(DEFAULT_MAIL_CODE.toString()))
            .andExpect(jsonPath("$.mailContent").value(DEFAULT_MAIL_CONTENT.toString()))
            .andExpect(jsonPath("$.mailType").value(DEFAULT_MAIL_TYPE.toString()))
            .andExpect(jsonPath("$.cc").value(DEFAULT_CC.toString()))
            .andExpect(jsonPath("$.bcc").value(DEFAULT_BCC.toString()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED.toString()))
            .andExpect(jsonPath("$.updated").value(DEFAULT_UPDATED.toString()))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG))
            .andExpect(jsonPath("$.mailSubject").value(DEFAULT_MAIL_SUBJECT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMailTemplate() throws Exception {
        // Get the mailTemplate
        restMailTemplateMockMvc.perform(get("/api/mail-templates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMailTemplate() throws Exception {
        // Initialize the database
        mailTemplateService.save(mailTemplate);

        int databaseSizeBeforeUpdate = mailTemplateRepository.findAll().size();

        // Update the mailTemplate
        MailTemplate updatedMailTemplate = mailTemplateRepository.findById(mailTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedMailTemplate are not directly saved in db
        em.detach(updatedMailTemplate);
        updatedMailTemplate
            .mailCode(UPDATED_MAIL_CODE)
            .mailContent(UPDATED_MAIL_CONTENT)
            .mailType(UPDATED_MAIL_TYPE)
            .cc(UPDATED_CC)
            .bcc(UPDATED_BCC)
            .created(UPDATED_CREATED)
            .updated(UPDATED_UPDATED)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .mailSubject(UPDATED_MAIL_SUBJECT);

        restMailTemplateMockMvc.perform(put("/api/mail-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMailTemplate)))
            .andExpect(status().isOk());

        // Validate the MailTemplate in the database
        List<MailTemplate> mailTemplateList = mailTemplateRepository.findAll();
        assertThat(mailTemplateList).hasSize(databaseSizeBeforeUpdate);
        MailTemplate testMailTemplate = mailTemplateList.get(mailTemplateList.size() - 1);
        assertThat(testMailTemplate.getMailCode()).isEqualTo(UPDATED_MAIL_CODE);
        assertThat(testMailTemplate.getMailContent()).isEqualTo(UPDATED_MAIL_CONTENT);
        assertThat(testMailTemplate.getMailType()).isEqualTo(UPDATED_MAIL_TYPE);
        assertThat(testMailTemplate.getCc()).isEqualTo(UPDATED_CC);
        assertThat(testMailTemplate.getBcc()).isEqualTo(UPDATED_BCC);
        assertThat(testMailTemplate.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testMailTemplate.getUpdated()).isEqualTo(UPDATED_UPDATED);
        assertThat(testMailTemplate.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
        assertThat(testMailTemplate.getMailSubject()).isEqualTo(UPDATED_MAIL_SUBJECT);
    }

    @Test
    @Transactional
    public void updateNonExistingMailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = mailTemplateRepository.findAll().size();

        // Create the MailTemplate

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMailTemplateMockMvc.perform(put("/api/mail-templates")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(mailTemplate)))
            .andExpect(status().isBadRequest());

        // Validate the MailTemplate in the database
        List<MailTemplate> mailTemplateList = mailTemplateRepository.findAll();
        assertThat(mailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMailTemplate() throws Exception {
        // Initialize the database
        mailTemplateService.save(mailTemplate);

        int databaseSizeBeforeDelete = mailTemplateRepository.findAll().size();

        // Delete the mailTemplate
        restMailTemplateMockMvc.perform(delete("/api/mail-templates/{id}", mailTemplate.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MailTemplate> mailTemplateList = mailTemplateRepository.findAll();
        assertThat(mailTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MailTemplate.class);
        MailTemplate mailTemplate1 = new MailTemplate();
        mailTemplate1.setId(1L);
        MailTemplate mailTemplate2 = new MailTemplate();
        mailTemplate2.setId(mailTemplate1.getId());
        assertThat(mailTemplate1).isEqualTo(mailTemplate2);
        mailTemplate2.setId(2L);
        assertThat(mailTemplate1).isNotEqualTo(mailTemplate2);
        mailTemplate1.setId(null);
        assertThat(mailTemplate1).isNotEqualTo(mailTemplate2);
    }
}
