package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.UserSetting;
import com.hihexa.cms.repository.UserSettingRepository;
import com.hihexa.cms.service.UserSettingService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.sameInstant;
import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserSettingResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class UserSettingResourceIT {

    private static final Integer DEFAULT_LOGIN_ALERT = 1;
    private static final Integer UPDATED_LOGIN_ALERT = 2;

    private static final Integer DEFAULT_KEEP_ALIVE = 1;
    private static final Integer UPDATED_KEEP_ALIVE = 2;

    private static final Integer DEFAULT_DETECT_IP_CHANGE = 1;
    private static final Integer UPDATED_DETECT_IP_CHANGE = 2;

    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DOB = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DOB = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_AVATOR = "AAAAAAAAAA";
    private static final String UPDATED_AVATOR = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_MIDDLE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_MIDDLE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_SFA = 1;
    private static final Integer UPDATED_SFA = 2;

    private static final String DEFAULT_FA_SEED = "AAAAAAAAAA";
    private static final String UPDATED_FA_SEED = "BBBBBBBBBB";

    private static final String DEFAULT_REF_ID = "AAAAAAAAAA";
    private static final String UPDATED_REF_ID = "BBBBBBBBBB";

    private static final Integer UPDATED_WD_WHITELIST_STATUS = 2;

    private static final Integer DEFAULT_VERIFY_LEVEL = 1;
    private static final Integer UPDATED_VERIFY_LEVEL = 2;

    private static final BigDecimal DEFAULT_WD_DAILY_LIMIT = new BigDecimal(1);
    private static final BigDecimal UPDATED_WD_DAILY_LIMIT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_DAILY_WD_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DAILY_WD_AMOUNT = new BigDecimal(2);

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_POST_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POST_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final Long DEFAULT_TAKER_FEE = 1L;
    private static final Long UPDATED_TAKER_FEE = 2L;

    private static final Long DEFAULT_MAKER_FEE = 1L;
    private static final Long UPDATED_MAKER_FEE = 2L;

    private static final Integer DEFAULT_FEE_TYPE = 1;
    private static final Integer UPDATED_FEE_TYPE = 2;

    private static final String DEFAULT_IDENTITY_ID = "AAAAAAAAAA";
    private static final String UPDATED_IDENTITY_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_WITHDRAW_BLOCKTIME = 1L;
    private static final Long UPDATED_WITHDRAW_BLOCKTIME = 2L;

    @Autowired
    private UserSettingRepository userSettingRepository;

    @Autowired
    private UserSettingService userSettingService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserSettingMockMvc;

    private UserSetting userSetting;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserSettingResource userSettingResource = new UserSettingResource(userSettingService);
        this.restUserSettingMockMvc = MockMvcBuilders.standaloneSetup(userSettingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserSetting createEntity(EntityManager em) {
        UserSetting userSetting = new UserSetting()
            .loginAlert(DEFAULT_LOGIN_ALERT)
            .keepAlive(DEFAULT_KEEP_ALIVE)
            .detectIpChange(DEFAULT_DETECT_IP_CHANGE)
            .activeFlg(DEFAULT_ACTIVE_FLG)
            .avator(DEFAULT_AVATOR)
            .username(DEFAULT_USERNAME)
            .firstName(DEFAULT_FIRST_NAME)
            .middleName(DEFAULT_MIDDLE_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .sfa(DEFAULT_SFA)
            .faSeed(DEFAULT_FA_SEED)
            .refId(DEFAULT_REF_ID)
            .verifyLevel(DEFAULT_VERIFY_LEVEL)
            .wdDailyLimit(DEFAULT_WD_DAILY_LIMIT)
            .dailyWdAmount(DEFAULT_DAILY_WD_AMOUNT)
            .address(DEFAULT_ADDRESS)
            .postCode(DEFAULT_POST_CODE)
            .city(DEFAULT_CITY)
            .country(DEFAULT_COUNTRY)
            .takerFee(DEFAULT_TAKER_FEE)
            .makerFee(DEFAULT_MAKER_FEE)
            .feeType(DEFAULT_FEE_TYPE)
            .identityId(DEFAULT_IDENTITY_ID)
            .withdrawBlocktime(DEFAULT_WITHDRAW_BLOCKTIME);
        return userSetting;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserSetting createUpdatedEntity(EntityManager em) {
        UserSetting userSetting = new UserSetting()
            .loginAlert(UPDATED_LOGIN_ALERT)
            .keepAlive(UPDATED_KEEP_ALIVE)
            .detectIpChange(UPDATED_DETECT_IP_CHANGE)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .avator(UPDATED_AVATOR)
            .username(UPDATED_USERNAME)
            .firstName(UPDATED_FIRST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .lastName(UPDATED_LAST_NAME)
            .sfa(UPDATED_SFA)
            .faSeed(UPDATED_FA_SEED)
            .refId(UPDATED_REF_ID)
            .verifyLevel(UPDATED_VERIFY_LEVEL)
            .wdDailyLimit(UPDATED_WD_DAILY_LIMIT)
            .dailyWdAmount(UPDATED_DAILY_WD_AMOUNT)
            .address(UPDATED_ADDRESS)
            .postCode(UPDATED_POST_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .takerFee(UPDATED_TAKER_FEE)
            .makerFee(UPDATED_MAKER_FEE)
            .feeType(UPDATED_FEE_TYPE)
            .identityId(UPDATED_IDENTITY_ID)
            .withdrawBlocktime(UPDATED_WITHDRAW_BLOCKTIME);
        return userSetting;
    }

    @BeforeEach
    public void initTest() {
        userSetting = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserSetting() throws Exception {
        int databaseSizeBeforeCreate = userSettingRepository.findAll().size();

        // Create the UserSetting
        restUserSettingMockMvc.perform(post("/api/user-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSetting)))
            .andExpect(status().isCreated());

        // Validate the UserSetting in the database
        List<UserSetting> userSettingList = userSettingRepository.findAll();
        assertThat(userSettingList).hasSize(databaseSizeBeforeCreate + 1);
        UserSetting testUserSetting = userSettingList.get(userSettingList.size() - 1);
        assertThat(testUserSetting.getLoginAlert()).isEqualTo(DEFAULT_LOGIN_ALERT);
        assertThat(testUserSetting.getKeepAlive()).isEqualTo(DEFAULT_KEEP_ALIVE);
        assertThat(testUserSetting.getDetectIpChange()).isEqualTo(DEFAULT_DETECT_IP_CHANGE);
        assertThat(testUserSetting.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
        assertThat(testUserSetting.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testUserSetting.getUpdated()).isEqualTo(DEFAULT_UPDATED);
        assertThat(testUserSetting.getDob()).isEqualTo(DEFAULT_DOB);
        assertThat(testUserSetting.getAvator()).isEqualTo(DEFAULT_AVATOR);
        assertThat(testUserSetting.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testUserSetting.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testUserSetting.getMiddleName()).isEqualTo(DEFAULT_MIDDLE_NAME);
        assertThat(testUserSetting.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testUserSetting.getSfa()).isEqualTo(DEFAULT_SFA);
        assertThat(testUserSetting.getFaSeed()).isEqualTo(DEFAULT_FA_SEED);
        assertThat(testUserSetting.getRefId()).isEqualTo(DEFAULT_REF_ID);
        assertThat(testUserSetting.getVerifyLevel()).isEqualTo(DEFAULT_VERIFY_LEVEL);
        assertThat(testUserSetting.getWdDailyLimit()).isEqualTo(DEFAULT_WD_DAILY_LIMIT);
        assertThat(testUserSetting.getDailyWdAmount()).isEqualTo(DEFAULT_DAILY_WD_AMOUNT);
        assertThat(testUserSetting.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testUserSetting.getPostCode()).isEqualTo(DEFAULT_POST_CODE);
        assertThat(testUserSetting.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testUserSetting.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testUserSetting.getTakerFee()).isEqualTo(DEFAULT_TAKER_FEE);
        assertThat(testUserSetting.getMakerFee()).isEqualTo(DEFAULT_MAKER_FEE);
        assertThat(testUserSetting.getFeeType()).isEqualTo(DEFAULT_FEE_TYPE);
        assertThat(testUserSetting.getIdentityId()).isEqualTo(DEFAULT_IDENTITY_ID);
        assertThat(testUserSetting.getWithdrawBlocktime()).isEqualTo(DEFAULT_WITHDRAW_BLOCKTIME);
    }

    @Test
    @Transactional
    public void createUserSettingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userSettingRepository.findAll().size();

        // Create the UserSetting with an existing ID
        userSetting.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserSettingMockMvc.perform(post("/api/user-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSetting)))
            .andExpect(status().isBadRequest());

        // Validate the UserSetting in the database
        List<UserSetting> userSettingList = userSettingRepository.findAll();
        assertThat(userSettingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserSettings() throws Exception {
        // Initialize the database
        userSettingRepository.saveAndFlush(userSetting);

        // Get all the userSettingList
        restUserSettingMockMvc.perform(get("/api/user-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].loginAlert").value(hasItem(DEFAULT_LOGIN_ALERT)))
            .andExpect(jsonPath("$.[*].keepAlive").value(hasItem(DEFAULT_KEEP_ALIVE)))
            .andExpect(jsonPath("$.[*].detectIpChange").value(hasItem(DEFAULT_DETECT_IP_CHANGE)))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].updated").value(hasItem(sameInstant(DEFAULT_UPDATED))))
            .andExpect(jsonPath("$.[*].dob").value(hasItem(sameInstant(DEFAULT_DOB))))
            .andExpect(jsonPath("$.[*].avator").value(hasItem(DEFAULT_AVATOR.toString())))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].middleName").value(hasItem(DEFAULT_MIDDLE_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].sfa").value(hasItem(DEFAULT_SFA)))
            .andExpect(jsonPath("$.[*].faSeed").value(hasItem(DEFAULT_FA_SEED.toString())))
            .andExpect(jsonPath("$.[*].refId").value(hasItem(DEFAULT_REF_ID.toString())))
            .andExpect(jsonPath("$.[*].verifyLevel").value(hasItem(DEFAULT_VERIFY_LEVEL)))
            .andExpect(jsonPath("$.[*].wdDailyLimit").value(hasItem(DEFAULT_WD_DAILY_LIMIT.intValue())))
            .andExpect(jsonPath("$.[*].dailyWdAmount").value(hasItem(DEFAULT_DAILY_WD_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].postCode").value(hasItem(DEFAULT_POST_CODE.toString())))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].takerFee").value(hasItem(DEFAULT_TAKER_FEE.intValue())))
            .andExpect(jsonPath("$.[*].makerFee").value(hasItem(DEFAULT_MAKER_FEE.intValue())))
            .andExpect(jsonPath("$.[*].feeType").value(hasItem(DEFAULT_FEE_TYPE)))
            .andExpect(jsonPath("$.[*].identityId").value(hasItem(DEFAULT_IDENTITY_ID.toString())))
            .andExpect(jsonPath("$.[*].withdrawBlocktime").value(hasItem(DEFAULT_WITHDRAW_BLOCKTIME.intValue())));
    }
    
    @Test
    @Transactional
    public void getUserSetting() throws Exception {
        // Initialize the database
        userSettingRepository.saveAndFlush(userSetting);

        // Get the userSetting
        restUserSettingMockMvc.perform(get("/api/user-settings/{id}", userSetting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userSetting.getId().intValue()))
            .andExpect(jsonPath("$.loginAlert").value(DEFAULT_LOGIN_ALERT))
            .andExpect(jsonPath("$.keepAlive").value(DEFAULT_KEEP_ALIVE))
            .andExpect(jsonPath("$.detectIpChange").value(DEFAULT_DETECT_IP_CHANGE))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)))
            .andExpect(jsonPath("$.updated").value(sameInstant(DEFAULT_UPDATED)))
            .andExpect(jsonPath("$.dob").value(sameInstant(DEFAULT_DOB)))
            .andExpect(jsonPath("$.avator").value(DEFAULT_AVATOR.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.middleName").value(DEFAULT_MIDDLE_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.sfa").value(DEFAULT_SFA))
            .andExpect(jsonPath("$.faSeed").value(DEFAULT_FA_SEED.toString()))
            .andExpect(jsonPath("$.refId").value(DEFAULT_REF_ID.toString()))
            .andExpect(jsonPath("$.verifyLevel").value(DEFAULT_VERIFY_LEVEL))
            .andExpect(jsonPath("$.wdDailyLimit").value(DEFAULT_WD_DAILY_LIMIT.intValue()))
            .andExpect(jsonPath("$.dailyWdAmount").value(DEFAULT_DAILY_WD_AMOUNT.intValue()))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS.toString()))
            .andExpect(jsonPath("$.postCode").value(DEFAULT_POST_CODE.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.takerFee").value(DEFAULT_TAKER_FEE.intValue()))
            .andExpect(jsonPath("$.makerFee").value(DEFAULT_MAKER_FEE.intValue()))
            .andExpect(jsonPath("$.feeType").value(DEFAULT_FEE_TYPE))
            .andExpect(jsonPath("$.identityId").value(DEFAULT_IDENTITY_ID.toString()))
            .andExpect(jsonPath("$.withdrawBlocktime").value(DEFAULT_WITHDRAW_BLOCKTIME.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUserSetting() throws Exception {
        // Get the userSetting
        restUserSettingMockMvc.perform(get("/api/user-settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserSetting() throws Exception {
        // Initialize the database
        userSettingService.save(userSetting);

        int databaseSizeBeforeUpdate = userSettingRepository.findAll().size();

        // Update the userSetting
        UserSetting updatedUserSetting = userSettingRepository.findById(userSetting.getId()).get();
        // Disconnect from session so that the updates on updatedUserSetting are not directly saved in db
        em.detach(updatedUserSetting);
        updatedUserSetting
            .loginAlert(UPDATED_LOGIN_ALERT)
            .keepAlive(UPDATED_KEEP_ALIVE)
            .detectIpChange(UPDATED_DETECT_IP_CHANGE)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .avator(UPDATED_AVATOR)
            .username(UPDATED_USERNAME)
            .firstName(UPDATED_FIRST_NAME)
            .middleName(UPDATED_MIDDLE_NAME)
            .lastName(UPDATED_LAST_NAME)
            .sfa(UPDATED_SFA)
            .faSeed(UPDATED_FA_SEED)
            .refId(UPDATED_REF_ID)
            .verifyLevel(UPDATED_VERIFY_LEVEL)
            .wdDailyLimit(UPDATED_WD_DAILY_LIMIT)
            .dailyWdAmount(UPDATED_DAILY_WD_AMOUNT)
            .address(UPDATED_ADDRESS)
            .postCode(UPDATED_POST_CODE)
            .city(UPDATED_CITY)
            .country(UPDATED_COUNTRY)
            .takerFee(UPDATED_TAKER_FEE)
            .makerFee(UPDATED_MAKER_FEE)
            .feeType(UPDATED_FEE_TYPE)
            .identityId(UPDATED_IDENTITY_ID)
            .withdrawBlocktime(UPDATED_WITHDRAW_BLOCKTIME);

        restUserSettingMockMvc.perform(put("/api/user-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserSetting)))
            .andExpect(status().isOk());

        // Validate the UserSetting in the database
        List<UserSetting> userSettingList = userSettingRepository.findAll();
        assertThat(userSettingList).hasSize(databaseSizeBeforeUpdate);
        UserSetting testUserSetting = userSettingList.get(userSettingList.size() - 1);
        assertThat(testUserSetting.getLoginAlert()).isEqualTo(UPDATED_LOGIN_ALERT);
        assertThat(testUserSetting.getKeepAlive()).isEqualTo(UPDATED_KEEP_ALIVE);
        assertThat(testUserSetting.getDetectIpChange()).isEqualTo(UPDATED_DETECT_IP_CHANGE);
        assertThat(testUserSetting.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
        assertThat(testUserSetting.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testUserSetting.getUpdated()).isEqualTo(UPDATED_UPDATED);
        assertThat(testUserSetting.getDob()).isEqualTo(UPDATED_DOB);
        assertThat(testUserSetting.getAvator()).isEqualTo(UPDATED_AVATOR);
        assertThat(testUserSetting.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testUserSetting.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testUserSetting.getMiddleName()).isEqualTo(UPDATED_MIDDLE_NAME);
        assertThat(testUserSetting.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testUserSetting.getSfa()).isEqualTo(UPDATED_SFA);
        assertThat(testUserSetting.getFaSeed()).isEqualTo(UPDATED_FA_SEED);
        assertThat(testUserSetting.getRefId()).isEqualTo(UPDATED_REF_ID);
        assertThat(testUserSetting.getVerifyLevel()).isEqualTo(UPDATED_VERIFY_LEVEL);
        assertThat(testUserSetting.getWdDailyLimit()).isEqualTo(UPDATED_WD_DAILY_LIMIT);
        assertThat(testUserSetting.getDailyWdAmount()).isEqualTo(UPDATED_DAILY_WD_AMOUNT);
        assertThat(testUserSetting.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testUserSetting.getPostCode()).isEqualTo(UPDATED_POST_CODE);
        assertThat(testUserSetting.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testUserSetting.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testUserSetting.getTakerFee()).isEqualTo(UPDATED_TAKER_FEE);
        assertThat(testUserSetting.getMakerFee()).isEqualTo(UPDATED_MAKER_FEE);
        assertThat(testUserSetting.getFeeType()).isEqualTo(UPDATED_FEE_TYPE);
        assertThat(testUserSetting.getIdentityId()).isEqualTo(UPDATED_IDENTITY_ID);
        assertThat(testUserSetting.getWithdrawBlocktime()).isEqualTo(UPDATED_WITHDRAW_BLOCKTIME);
    }

    @Test
    @Transactional
    public void updateNonExistingUserSetting() throws Exception {
        int databaseSizeBeforeUpdate = userSettingRepository.findAll().size();

        // Create the UserSetting

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserSettingMockMvc.perform(put("/api/user-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userSetting)))
            .andExpect(status().isBadRequest());

        // Validate the UserSetting in the database
        List<UserSetting> userSettingList = userSettingRepository.findAll();
        assertThat(userSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserSetting() throws Exception {
        // Initialize the database
        userSettingService.save(userSetting);

        int databaseSizeBeforeDelete = userSettingRepository.findAll().size();

        // Delete the userSetting
        restUserSettingMockMvc.perform(delete("/api/user-settings/{id}", userSetting.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserSetting> userSettingList = userSettingRepository.findAll();
        assertThat(userSettingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserSetting.class);
        UserSetting userSetting1 = new UserSetting();
        userSetting1.setId(1L);
        UserSetting userSetting2 = new UserSetting();
        userSetting2.setId(userSetting1.getId());
        assertThat(userSetting1).isEqualTo(userSetting2);
        userSetting2.setId(2L);
        assertThat(userSetting1).isNotEqualTo(userSetting2);
        userSetting1.setId(null);
        assertThat(userSetting1).isNotEqualTo(userSetting2);
    }
}
