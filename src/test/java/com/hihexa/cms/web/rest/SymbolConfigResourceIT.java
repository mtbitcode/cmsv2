package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.SymbolConfig;
import com.hihexa.cms.repository.SymbolConfigRepository;
import com.hihexa.cms.service.SymbolConfigService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SymbolConfigResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class SymbolConfigResourceIT {

    private static final String DEFAULT_SYMBOL = "AAAAAAAAAA";
    private static final String UPDATED_SYMBOL = "BBBBBBBBBB";

    private static final Integer DEFAULT_BASE_CC_ID = 1;
    private static final Integer UPDATED_BASE_CC_ID = 2;
    private static final String DEFAULT_BASE_CC = "AAAAAAAAAA";
    private static final String UPDATED_BASE_CC = "BBBBBBBBBB";

    private static final Integer DEFAULT_COUNTER_CC_ID = 1;
    private static final Integer UPDATED_COUNTER_CC_ID = 2;
    private static final String DEFAULT_COUNTER_CC = "AAAAAAAAAA";
    private static final String UPDATED_COUNTER_CC = "BBBBBBBBBB";

    private static final Integer DEFAULT_TYPE = 1;
    private static final Integer UPDATED_TYPE = 2;
    private static final String DEFAULT_GROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_GROUP_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_DECIMAL_PLACE_AMOUNT = 1;
    private static final Integer UPDATED_DECIMAL_PLACE_AMOUNT = 2;
    private static final Integer DEFAULT_DECIMAL_PLACE_PRICE = 1;
    private static final Integer UPDATED_DECIMAL_PLACE_PRICE = 2;
    private static final Integer DEFAULT_IS_DEMO = 1;
    private static final Integer UPDATED_IS_DEMO = 2;
    @Autowired
    private SymbolConfigRepository symbolConfigRepository;

    @Autowired
    private SymbolConfigService symbolConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSymbolConfigMockMvc;

    private SymbolConfig symbolConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SymbolConfigResource symbolConfigResource = new SymbolConfigResource(symbolConfigService);
        this.restSymbolConfigMockMvc = MockMvcBuilders.standaloneSetup(symbolConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SymbolConfig createEntity(EntityManager em) {
        SymbolConfig symbolConfig = new SymbolConfig()
            .symbol(DEFAULT_SYMBOL)
            .baseCcId(DEFAULT_BASE_CC_ID)
            .baseCc(DEFAULT_BASE_CC)
            .counterCcId(DEFAULT_COUNTER_CC_ID)
            .counterCc(DEFAULT_COUNTER_CC)
            .type(DEFAULT_TYPE)
            .groupName(DEFAULT_GROUP_NAME)
            .description(DEFAULT_DESCRIPTION)
            .decimalPlaceAmount(DEFAULT_DECIMAL_PLACE_AMOUNT)
            .decimalPlacePrice(DEFAULT_DECIMAL_PLACE_PRICE)
            .isDemo(DEFAULT_IS_DEMO);
        return symbolConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SymbolConfig createUpdatedEntity(EntityManager em) {
        SymbolConfig symbolConfig = new SymbolConfig()
            .symbol(UPDATED_SYMBOL)
            .baseCcId(UPDATED_BASE_CC_ID)
            .baseCc(UPDATED_BASE_CC)
            .counterCcId(UPDATED_COUNTER_CC_ID)
            .counterCc(UPDATED_COUNTER_CC)
            .type(UPDATED_TYPE)
            .groupName(UPDATED_GROUP_NAME)
            .description(UPDATED_DESCRIPTION)
            .decimalPlaceAmount(UPDATED_DECIMAL_PLACE_AMOUNT)
            .decimalPlacePrice(UPDATED_DECIMAL_PLACE_PRICE)
            .isDemo(UPDATED_IS_DEMO);
        return symbolConfig;
    }

    @BeforeEach
    public void initTest() {
        symbolConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createSymbolConfig() throws Exception {
        int databaseSizeBeforeCreate = symbolConfigRepository.findAll().size();

        // Create the SymbolConfig
        restSymbolConfigMockMvc.perform(post("/api/symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(symbolConfig)))
            .andExpect(status().isCreated());

        // Validate the SymbolConfig in the database
        List<SymbolConfig> symbolConfigList = symbolConfigRepository.findAll();
        assertThat(symbolConfigList).hasSize(databaseSizeBeforeCreate + 1);
        SymbolConfig testSymbolConfig = symbolConfigList.get(symbolConfigList.size() - 1);
        assertThat(testSymbolConfig.getSymbol()).isEqualTo(DEFAULT_SYMBOL);
        assertThat(testSymbolConfig.getBaseCcId()).isEqualTo(DEFAULT_BASE_CC_ID);
        assertThat(testSymbolConfig.getBaseCc()).isEqualTo(DEFAULT_BASE_CC);
        assertThat(testSymbolConfig.getCounterCcId()).isEqualTo(DEFAULT_COUNTER_CC_ID);
        assertThat(testSymbolConfig.getCounterCc()).isEqualTo(DEFAULT_COUNTER_CC);
        assertThat(testSymbolConfig.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testSymbolConfig.getGroupName()).isEqualTo(DEFAULT_GROUP_NAME);
        assertThat(testSymbolConfig.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSymbolConfig.getDecimalPlaceAmount()).isEqualTo(DEFAULT_DECIMAL_PLACE_AMOUNT);
        assertThat(testSymbolConfig.getDecimalPlacePrice()).isEqualTo(DEFAULT_DECIMAL_PLACE_PRICE);
        assertThat(testSymbolConfig.getIsDemo()).isEqualTo(DEFAULT_IS_DEMO);
    }

    @Test
    @Transactional
    public void createSymbolConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = symbolConfigRepository.findAll().size();

        // Create the SymbolConfig with an existing ID
        symbolConfig.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSymbolConfigMockMvc.perform(post("/api/symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(symbolConfig)))
            .andExpect(status().isBadRequest());

        // Validate the SymbolConfig in the database
        List<SymbolConfig> symbolConfigList = symbolConfigRepository.findAll();
        assertThat(symbolConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllSymbolConfigs() throws Exception {
        // Initialize the database
        symbolConfigRepository.saveAndFlush(symbolConfig);

        // Get all the symbolConfigList
        restSymbolConfigMockMvc.perform(get("/api/symbol-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(symbolConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].symbol").value(hasItem(DEFAULT_SYMBOL.toString())))
            .andExpect(jsonPath("$.[*].baseCcId").value(hasItem(DEFAULT_BASE_CC_ID)))
            .andExpect(jsonPath("$.[*].baseCc").value(hasItem(DEFAULT_BASE_CC.toString())))
            .andExpect(jsonPath("$.[*].counterCcId").value(hasItem(DEFAULT_COUNTER_CC_ID)))
            .andExpect(jsonPath("$.[*].counterCc").value(hasItem(DEFAULT_COUNTER_CC.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].groupName").value(hasItem(DEFAULT_GROUP_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].decimalPlaceAmount").value(hasItem(DEFAULT_DECIMAL_PLACE_AMOUNT)))
            .andExpect(jsonPath("$.[*].decimalPlacePrice").value(hasItem(DEFAULT_DECIMAL_PLACE_PRICE)))
            .andExpect(jsonPath("$.[*].isDemo").value(hasItem(DEFAULT_IS_DEMO)));
    }
    
    @Test
    @Transactional
    public void getSymbolConfig() throws Exception {
        // Initialize the database
        symbolConfigRepository.saveAndFlush(symbolConfig);

        // Get the symbolConfig
        restSymbolConfigMockMvc.perform(get("/api/symbol-configs/{id}", symbolConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(symbolConfig.getId().intValue()))
            .andExpect(jsonPath("$.symbol").value(DEFAULT_SYMBOL.toString()))
            .andExpect(jsonPath("$.baseCcId").value(DEFAULT_BASE_CC_ID))
            .andExpect(jsonPath("$.baseCc").value(DEFAULT_BASE_CC.toString()))
            .andExpect(jsonPath("$.counterCcId").value(DEFAULT_COUNTER_CC_ID))
            .andExpect(jsonPath("$.counterCc").value(DEFAULT_COUNTER_CC.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.groupName").value(DEFAULT_GROUP_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.decimalPlaceAmount").value(DEFAULT_DECIMAL_PLACE_AMOUNT))
            .andExpect(jsonPath("$.decimalPlacePrice").value(DEFAULT_DECIMAL_PLACE_PRICE))
            .andExpect(jsonPath("$.isDemo").value(DEFAULT_IS_DEMO));
    }

    @Test
    @Transactional
    public void getNonExistingSymbolConfig() throws Exception {
        // Get the symbolConfig
        restSymbolConfigMockMvc.perform(get("/api/symbol-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSymbolConfig() throws Exception {
        // Initialize the database
        symbolConfigService.save(symbolConfig);

        int databaseSizeBeforeUpdate = symbolConfigRepository.findAll().size();

        // Update the symbolConfig
        SymbolConfig updatedSymbolConfig = symbolConfigRepository.findById(symbolConfig.getId()).get();
        // Disconnect from session so that the updates on updatedSymbolConfig are not directly saved in db
        em.detach(updatedSymbolConfig);
        updatedSymbolConfig
            .symbol(UPDATED_SYMBOL)
            .baseCcId(UPDATED_BASE_CC_ID)
            .baseCc(UPDATED_BASE_CC)
            .counterCcId(UPDATED_COUNTER_CC_ID)
            .counterCc(UPDATED_COUNTER_CC)
            .type(UPDATED_TYPE)
            .groupName(UPDATED_GROUP_NAME)
            .description(UPDATED_DESCRIPTION)
            .decimalPlaceAmount(UPDATED_DECIMAL_PLACE_AMOUNT)
            .decimalPlacePrice(UPDATED_DECIMAL_PLACE_PRICE)
            .isDemo(UPDATED_IS_DEMO);

        restSymbolConfigMockMvc.perform(put("/api/symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSymbolConfig)))
            .andExpect(status().isOk());

        // Validate the SymbolConfig in the database
        List<SymbolConfig> symbolConfigList = symbolConfigRepository.findAll();
        assertThat(symbolConfigList).hasSize(databaseSizeBeforeUpdate);
        SymbolConfig testSymbolConfig = symbolConfigList.get(symbolConfigList.size() - 1);
        assertThat(testSymbolConfig.getSymbol()).isEqualTo(UPDATED_SYMBOL);
        assertThat(testSymbolConfig.getBaseCcId()).isEqualTo(UPDATED_BASE_CC_ID);
        assertThat(testSymbolConfig.getBaseCc()).isEqualTo(UPDATED_BASE_CC);
        assertThat(testSymbolConfig.getCounterCcId()).isEqualTo(UPDATED_COUNTER_CC_ID);
        assertThat(testSymbolConfig.getCounterCc()).isEqualTo(UPDATED_COUNTER_CC);
        assertThat(testSymbolConfig.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testSymbolConfig.getGroupName()).isEqualTo(UPDATED_GROUP_NAME);
        assertThat(testSymbolConfig.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSymbolConfig.getDecimalPlaceAmount()).isEqualTo(UPDATED_DECIMAL_PLACE_AMOUNT);
        assertThat(testSymbolConfig.getDecimalPlacePrice()).isEqualTo(UPDATED_DECIMAL_PLACE_PRICE);
        assertThat(testSymbolConfig.getIsDemo()).isEqualTo(UPDATED_IS_DEMO);
    }

    @Test
    @Transactional
    public void updateNonExistingSymbolConfig() throws Exception {
        int databaseSizeBeforeUpdate = symbolConfigRepository.findAll().size();

        // Create the SymbolConfig

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSymbolConfigMockMvc.perform(put("/api/symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(symbolConfig)))
            .andExpect(status().isBadRequest());

        // Validate the SymbolConfig in the database
        List<SymbolConfig> symbolConfigList = symbolConfigRepository.findAll();
        assertThat(symbolConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSymbolConfig() throws Exception {
        // Initialize the database
        symbolConfigService.save(symbolConfig);

        int databaseSizeBeforeDelete = symbolConfigRepository.findAll().size();

        // Delete the symbolConfig
        restSymbolConfigMockMvc.perform(delete("/api/symbol-configs/{id}", symbolConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SymbolConfig> symbolConfigList = symbolConfigRepository.findAll();
        assertThat(symbolConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SymbolConfig.class);
        SymbolConfig symbolConfig1 = new SymbolConfig();
        symbolConfig1.setId(1L);
        SymbolConfig symbolConfig2 = new SymbolConfig();
        symbolConfig2.setId(symbolConfig1.getId());
        assertThat(symbolConfig1).isEqualTo(symbolConfig2);
        symbolConfig2.setId(2L);
        assertThat(symbolConfig1).isNotEqualTo(symbolConfig2);
        symbolConfig1.setId(null);
        assertThat(symbolConfig1).isNotEqualTo(symbolConfig2);
    }
}
