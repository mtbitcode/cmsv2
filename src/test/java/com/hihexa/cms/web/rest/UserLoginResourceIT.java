package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.UserLogin;
import com.hihexa.cms.repository.UserLoginRepository;
import com.hihexa.cms.service.UserLoginService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.sameInstant;
import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UserLoginResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class UserLoginResourceIT {

    private static final Integer DEFAULT_BROKER_ID = 1;
    private static final Integer UPDATED_BROKER_ID = 2;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final Integer DEFAULT_STATUS = 1;
    private static final Integer UPDATED_STATUS = 2;

    private static final ZonedDateTime DEFAULT_LAST_LOGIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_LOGIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_AGENTS = "AAAAAAAAAA";
    private static final String UPDATED_AGENTS = "BBBBBBBBBB";

    private static final String DEFAULT_IPS = "AAAAAAAAAA";
    private static final String UPDATED_IPS = "BBBBBBBBBB";

    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Autowired
    private UserLoginService userLoginService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserLoginMockMvc;

    private UserLogin userLogin;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserLoginResource userLoginResource = new UserLoginResource(userLoginService);
        this.restUserLoginMockMvc = MockMvcBuilders.standaloneSetup(userLoginResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLogin createEntity(EntityManager em) {
        UserLogin userLogin = new UserLogin()
            .brokerId(DEFAULT_BROKER_ID)
            .email(DEFAULT_EMAIL)
            .password(DEFAULT_PASSWORD)
            .status(DEFAULT_STATUS)
            .agents(DEFAULT_AGENTS)
            .ips(DEFAULT_IPS)
            .activeFlg(DEFAULT_ACTIVE_FLG);
        return userLogin;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLogin createUpdatedEntity(EntityManager em) {
        UserLogin userLogin = new UserLogin()
            .brokerId(UPDATED_BROKER_ID)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD)
            .status(UPDATED_STATUS)
            .agents(UPDATED_AGENTS)
            .ips(UPDATED_IPS)
            .activeFlg(UPDATED_ACTIVE_FLG);
        return userLogin;
    }

    @BeforeEach
    public void initTest() {
        userLogin = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserLogin() throws Exception {
        int databaseSizeBeforeCreate = userLoginRepository.findAll().size();

        // Create the UserLogin
        restUserLoginMockMvc.perform(post("/api/user-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogin)))
            .andExpect(status().isCreated());

        // Validate the UserLogin in the database
        List<UserLogin> userLoginList = userLoginRepository.findAll();
        assertThat(userLoginList).hasSize(databaseSizeBeforeCreate + 1);
        UserLogin testUserLogin = userLoginList.get(userLoginList.size() - 1);
        assertThat(testUserLogin.getBrokerId()).isEqualTo(DEFAULT_BROKER_ID);
        assertThat(testUserLogin.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserLogin.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testUserLogin.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserLogin.getLastLogin()).isEqualTo(DEFAULT_LAST_LOGIN);
        assertThat(testUserLogin.getAgents()).isEqualTo(DEFAULT_AGENTS);
        assertThat(testUserLogin.getIps()).isEqualTo(DEFAULT_IPS);
        assertThat(testUserLogin.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
        assertThat(testUserLogin.getCreated()).isEqualTo(DEFAULT_CREATED);
    }

    @Test
    @Transactional
    public void createUserLoginWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userLoginRepository.findAll().size();

        // Create the UserLogin with an existing ID
        userLogin.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserLoginMockMvc.perform(post("/api/user-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogin)))
            .andExpect(status().isBadRequest());

        // Validate the UserLogin in the database
        List<UserLogin> userLoginList = userLoginRepository.findAll();
        assertThat(userLoginList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserLogins() throws Exception {
        // Initialize the database
        userLoginRepository.saveAndFlush(userLogin);

        // Get all the userLoginList
        restUserLoginMockMvc.perform(get("/api/user-logins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLogin.getId().intValue())))
            .andExpect(jsonPath("$.[*].brokerId").value(hasItem(DEFAULT_BROKER_ID)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].lastLogin").value(hasItem(sameInstant(DEFAULT_LAST_LOGIN))))
            .andExpect(jsonPath("$.[*].agents").value(hasItem(DEFAULT_AGENTS.toString())))
            .andExpect(jsonPath("$.[*].ips").value(hasItem(DEFAULT_IPS.toString())))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))));
    }
    
    @Test
    @Transactional
    public void getUserLogin() throws Exception {
        // Initialize the database
        userLoginRepository.saveAndFlush(userLogin);

        // Get the userLogin
        restUserLoginMockMvc.perform(get("/api/user-logins/{id}", userLogin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userLogin.getId().intValue()))
            .andExpect(jsonPath("$.brokerId").value(DEFAULT_BROKER_ID))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.lastLogin").value(sameInstant(DEFAULT_LAST_LOGIN)))
            .andExpect(jsonPath("$.agents").value(DEFAULT_AGENTS.toString()))
            .andExpect(jsonPath("$.ips").value(DEFAULT_IPS.toString()))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)));
    }

    @Test
    @Transactional
    public void getNonExistingUserLogin() throws Exception {
        // Get the userLogin
        restUserLoginMockMvc.perform(get("/api/user-logins/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserLogin() throws Exception {
        // Initialize the database
        userLoginService.save(userLogin);

        int databaseSizeBeforeUpdate = userLoginRepository.findAll().size();

        // Update the userLogin
        UserLogin updatedUserLogin = userLoginRepository.findById(userLogin.getId()).get();
        // Disconnect from session so that the updates on updatedUserLogin are not directly saved in db
        em.detach(updatedUserLogin);
        updatedUserLogin
            .brokerId(UPDATED_BROKER_ID)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD)
            .status(UPDATED_STATUS)
            .agents(UPDATED_AGENTS)
            .ips(UPDATED_IPS)
            .activeFlg(UPDATED_ACTIVE_FLG)
            ;

        restUserLoginMockMvc.perform(put("/api/user-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserLogin)))
            .andExpect(status().isOk());

        // Validate the UserLogin in the database
        List<UserLogin> userLoginList = userLoginRepository.findAll();
        assertThat(userLoginList).hasSize(databaseSizeBeforeUpdate);
        UserLogin testUserLogin = userLoginList.get(userLoginList.size() - 1);
        assertThat(testUserLogin.getBrokerId()).isEqualTo(UPDATED_BROKER_ID);
        assertThat(testUserLogin.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserLogin.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testUserLogin.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserLogin.getLastLogin()).isEqualTo(UPDATED_LAST_LOGIN);
        assertThat(testUserLogin.getAgents()).isEqualTo(UPDATED_AGENTS);
        assertThat(testUserLogin.getIps()).isEqualTo(UPDATED_IPS);
        assertThat(testUserLogin.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
        assertThat(testUserLogin.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingUserLogin() throws Exception {
        int databaseSizeBeforeUpdate = userLoginRepository.findAll().size();

        // Create the UserLogin

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserLoginMockMvc.perform(put("/api/user-logins")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLogin)))
            .andExpect(status().isBadRequest());

        // Validate the UserLogin in the database
        List<UserLogin> userLoginList = userLoginRepository.findAll();
        assertThat(userLoginList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserLogin() throws Exception {
        // Initialize the database
        userLoginService.save(userLogin);

        int databaseSizeBeforeDelete = userLoginRepository.findAll().size();

        // Delete the userLogin
        restUserLoginMockMvc.perform(delete("/api/user-logins/{id}", userLogin.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserLogin> userLoginList = userLoginRepository.findAll();
        assertThat(userLoginList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserLogin.class);
        UserLogin userLogin1 = new UserLogin();
        userLogin1.setId(1L);
        UserLogin userLogin2 = new UserLogin();
        userLogin2.setId(userLogin1.getId());
        assertThat(userLogin1).isEqualTo(userLogin2);
        userLogin2.setId(2L);
        assertThat(userLogin1).isNotEqualTo(userLogin2);
        userLogin1.setId(null);
        assertThat(userLogin1).isNotEqualTo(userLogin2);
    }
}
