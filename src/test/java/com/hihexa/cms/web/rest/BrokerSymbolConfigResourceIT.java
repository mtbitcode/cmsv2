package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.BrokerSymbolConfig;
import com.hihexa.cms.repository.BrokerSymbolConfigRepository;
import com.hihexa.cms.service.BrokerSymbolConfigService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.sameInstant;
import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BrokerSymbolConfigResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class BrokerSymbolConfigResourceIT {

    private static final Long DEFAULT_BROKER_ID = 1L;
    private static final Long UPDATED_BROKER_ID = 2L;
    private static final Long DEFAULT_SYMBOL_ID = 1L;
    private static final Long UPDATED_SYMBOL_ID = 2L;
    private static final Integer DEFAULT_TRADABLE = 1;
    private static final Integer UPDATED_TRADABLE = 2;
    private static final Long DEFAULT_MIN_TRADE = 1L;
    private static final Long UPDATED_MIN_TRADE = 2L;
    private static final Integer DEFAULT_MARGIN_ENABLE = 1;
    private static final Integer UPDATED_MARGIN_ENABLE = 2;
    private static final Long DEFAULT_MAX_TRADE = 1L;
    private static final Long UPDATED_MAX_TRADE = 2L;
    private static final Long DEFAULT_MAX_PRICE = 1L;
    private static final Long UPDATED_MAX_PRICE = 2L;
    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_MIN_PRICE = 1L;
    private static final Long UPDATED_MIN_PRICE = 2L;
    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;
    @Autowired
    private BrokerSymbolConfigRepository brokerSymbolConfigRepository;

    @Autowired
    private BrokerSymbolConfigService brokerSymbolConfigService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBrokerSymbolConfigMockMvc;

    private BrokerSymbolConfig brokerSymbolConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BrokerSymbolConfigResource brokerSymbolConfigResource = new BrokerSymbolConfigResource(brokerSymbolConfigService);
        this.restBrokerSymbolConfigMockMvc = MockMvcBuilders.standaloneSetup(brokerSymbolConfigResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BrokerSymbolConfig createEntity(EntityManager em) {
        BrokerSymbolConfig brokerSymbolConfig = new BrokerSymbolConfig()
            .brokerId(DEFAULT_BROKER_ID)
            .symbolId(DEFAULT_SYMBOL_ID)
            .tradable(DEFAULT_TRADABLE)
            .minTrade(DEFAULT_MIN_TRADE)
            .marginEnable(DEFAULT_MARGIN_ENABLE)
            .maxTrade(DEFAULT_MAX_TRADE)
            .maxPrice(DEFAULT_MAX_PRICE)
            .minPrice(DEFAULT_MIN_PRICE)
            .activeFlg(DEFAULT_ACTIVE_FLG);
        return brokerSymbolConfig;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BrokerSymbolConfig createUpdatedEntity(EntityManager em) {
        BrokerSymbolConfig brokerSymbolConfig = new BrokerSymbolConfig()
            .brokerId(UPDATED_BROKER_ID)
            .symbolId(UPDATED_SYMBOL_ID)
            .tradable(UPDATED_TRADABLE)
            .minTrade(UPDATED_MIN_TRADE)
            .marginEnable(UPDATED_MARGIN_ENABLE)
            .maxTrade(UPDATED_MAX_TRADE)
            .maxPrice(UPDATED_MAX_PRICE)
            .minPrice(UPDATED_MIN_PRICE)
            .activeFlg(UPDATED_ACTIVE_FLG);
        return brokerSymbolConfig;
    }

    @BeforeEach
    public void initTest() {
        brokerSymbolConfig = createEntity(em);
    }

    @Test
    @Transactional
    public void createBrokerSymbolConfig() throws Exception {
        int databaseSizeBeforeCreate = brokerSymbolConfigRepository.findAll().size();

        // Create the BrokerSymbolConfig
        restBrokerSymbolConfigMockMvc.perform(post("/api/broker-symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerSymbolConfig)))
            .andExpect(status().isCreated());

        // Validate the BrokerSymbolConfig in the database
        List<BrokerSymbolConfig> brokerSymbolConfigList = brokerSymbolConfigRepository.findAll();
        assertThat(brokerSymbolConfigList).hasSize(databaseSizeBeforeCreate + 1);
        BrokerSymbolConfig testBrokerSymbolConfig = brokerSymbolConfigList.get(brokerSymbolConfigList.size() - 1);
        assertThat(testBrokerSymbolConfig.getBrokerId()).isEqualTo(DEFAULT_BROKER_ID);
        assertThat(testBrokerSymbolConfig.getSymbolId()).isEqualTo(DEFAULT_SYMBOL_ID);
        assertThat(testBrokerSymbolConfig.getTradable()).isEqualTo(DEFAULT_TRADABLE);
        assertThat(testBrokerSymbolConfig.getMinTrade()).isEqualTo(DEFAULT_MIN_TRADE);
        assertThat(testBrokerSymbolConfig.getMarginEnable()).isEqualTo(DEFAULT_MARGIN_ENABLE);
        assertThat(testBrokerSymbolConfig.getMaxTrade()).isEqualTo(DEFAULT_MAX_TRADE);
        assertThat(testBrokerSymbolConfig.getMaxPrice()).isEqualTo(DEFAULT_MAX_PRICE);
        assertThat(testBrokerSymbolConfig.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testBrokerSymbolConfig.getUpdated()).isEqualTo(DEFAULT_UPDATED);
        assertThat(testBrokerSymbolConfig.getMinPrice()).isEqualTo(DEFAULT_MIN_PRICE);
        assertThat(testBrokerSymbolConfig.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
    }

    @Test
    @Transactional
    public void createBrokerSymbolConfigWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = brokerSymbolConfigRepository.findAll().size();

        // Create the BrokerSymbolConfig with an existing ID
        brokerSymbolConfig.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBrokerSymbolConfigMockMvc.perform(post("/api/broker-symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerSymbolConfig)))
            .andExpect(status().isBadRequest());

        // Validate the BrokerSymbolConfig in the database
        List<BrokerSymbolConfig> brokerSymbolConfigList = brokerSymbolConfigRepository.findAll();
        assertThat(brokerSymbolConfigList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBrokerSymbolConfigs() throws Exception {
        // Initialize the database
        brokerSymbolConfigRepository.saveAndFlush(brokerSymbolConfig);

        // Get all the brokerSymbolConfigList
        restBrokerSymbolConfigMockMvc.perform(get("/api/broker-symbol-configs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(brokerSymbolConfig.getId().intValue())))
            .andExpect(jsonPath("$.[*].brokerId").value(hasItem(DEFAULT_BROKER_ID.intValue())))
            .andExpect(jsonPath("$.[*].symbolId").value(hasItem(DEFAULT_SYMBOL_ID.intValue())))
            .andExpect(jsonPath("$.[*].tradable").value(hasItem(DEFAULT_TRADABLE)))
            .andExpect(jsonPath("$.[*].minTrade").value(hasItem(DEFAULT_MIN_TRADE.intValue())))
            .andExpect(jsonPath("$.[*].marginEnable").value(hasItem(DEFAULT_MARGIN_ENABLE)))
            .andExpect(jsonPath("$.[*].maxTrade").value(hasItem(DEFAULT_MAX_TRADE.intValue())))
            .andExpect(jsonPath("$.[*].maxPrice").value(hasItem(DEFAULT_MAX_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].updated").value(hasItem(sameInstant(DEFAULT_UPDATED))))
            .andExpect(jsonPath("$.[*].minPrice").value(hasItem(DEFAULT_MIN_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)));
    }
    
    @Test
    @Transactional
    public void getBrokerSymbolConfig() throws Exception {
        // Initialize the database
        brokerSymbolConfigRepository.saveAndFlush(brokerSymbolConfig);

        // Get the brokerSymbolConfig
        restBrokerSymbolConfigMockMvc.perform(get("/api/broker-symbol-configs/{id}", brokerSymbolConfig.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(brokerSymbolConfig.getId().intValue()))
            .andExpect(jsonPath("$.brokerId").value(DEFAULT_BROKER_ID.intValue()))
            .andExpect(jsonPath("$.symbolId").value(DEFAULT_SYMBOL_ID.intValue()))
            .andExpect(jsonPath("$.tradable").value(DEFAULT_TRADABLE))
            .andExpect(jsonPath("$.minTrade").value(DEFAULT_MIN_TRADE.intValue()))
            .andExpect(jsonPath("$.marginEnable").value(DEFAULT_MARGIN_ENABLE))
            .andExpect(jsonPath("$.maxTrade").value(DEFAULT_MAX_TRADE.intValue()))
            .andExpect(jsonPath("$.maxPrice").value(DEFAULT_MAX_PRICE.intValue()))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)))
            .andExpect(jsonPath("$.updated").value(sameInstant(DEFAULT_UPDATED)))
            .andExpect(jsonPath("$.minPrice").value(DEFAULT_MIN_PRICE.intValue()))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG));
    }

    @Test
    @Transactional
    public void getNonExistingBrokerSymbolConfig() throws Exception {
        // Get the brokerSymbolConfig
        restBrokerSymbolConfigMockMvc.perform(get("/api/broker-symbol-configs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBrokerSymbolConfig() throws Exception {
        // Initialize the database
        brokerSymbolConfigService.save(brokerSymbolConfig);

        int databaseSizeBeforeUpdate = brokerSymbolConfigRepository.findAll().size();

        // Update the brokerSymbolConfig
        BrokerSymbolConfig updatedBrokerSymbolConfig = brokerSymbolConfigRepository.findById(brokerSymbolConfig.getId()).get();
        // Disconnect from session so that the updates on updatedBrokerSymbolConfig are not directly saved in db
        em.detach(updatedBrokerSymbolConfig);
        updatedBrokerSymbolConfig
            .brokerId(UPDATED_BROKER_ID)
            .symbolId(UPDATED_SYMBOL_ID)
            .tradable(UPDATED_TRADABLE)
            .minTrade(UPDATED_MIN_TRADE)
            .marginEnable(UPDATED_MARGIN_ENABLE)
            .maxTrade(UPDATED_MAX_TRADE)
            .maxPrice(UPDATED_MAX_PRICE)
            .minPrice(UPDATED_MIN_PRICE)
            .activeFlg(UPDATED_ACTIVE_FLG);

        restBrokerSymbolConfigMockMvc.perform(put("/api/broker-symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBrokerSymbolConfig)))
            .andExpect(status().isOk());

        // Validate the BrokerSymbolConfig in the database
        List<BrokerSymbolConfig> brokerSymbolConfigList = brokerSymbolConfigRepository.findAll();
        assertThat(brokerSymbolConfigList).hasSize(databaseSizeBeforeUpdate);
        BrokerSymbolConfig testBrokerSymbolConfig = brokerSymbolConfigList.get(brokerSymbolConfigList.size() - 1);
        assertThat(testBrokerSymbolConfig.getBrokerId()).isEqualTo(UPDATED_BROKER_ID);
        assertThat(testBrokerSymbolConfig.getSymbolId()).isEqualTo(UPDATED_SYMBOL_ID);
        assertThat(testBrokerSymbolConfig.getTradable()).isEqualTo(UPDATED_TRADABLE);
        assertThat(testBrokerSymbolConfig.getMinTrade()).isEqualTo(UPDATED_MIN_TRADE);
        assertThat(testBrokerSymbolConfig.getMarginEnable()).isEqualTo(UPDATED_MARGIN_ENABLE);
        assertThat(testBrokerSymbolConfig.getMaxTrade()).isEqualTo(UPDATED_MAX_TRADE);
        assertThat(testBrokerSymbolConfig.getMaxPrice()).isEqualTo(UPDATED_MAX_PRICE);
        assertThat(testBrokerSymbolConfig.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testBrokerSymbolConfig.getUpdated()).isEqualTo(UPDATED_UPDATED);
        assertThat(testBrokerSymbolConfig.getMinPrice()).isEqualTo(UPDATED_MIN_PRICE);
        assertThat(testBrokerSymbolConfig.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
    }

    @Test
    @Transactional
    public void updateNonExistingBrokerSymbolConfig() throws Exception {
        int databaseSizeBeforeUpdate = brokerSymbolConfigRepository.findAll().size();

        // Create the BrokerSymbolConfig

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBrokerSymbolConfigMockMvc.perform(put("/api/broker-symbol-configs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerSymbolConfig)))
            .andExpect(status().isBadRequest());

        // Validate the BrokerSymbolConfig in the database
        List<BrokerSymbolConfig> brokerSymbolConfigList = brokerSymbolConfigRepository.findAll();
        assertThat(brokerSymbolConfigList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBrokerSymbolConfig() throws Exception {
        // Initialize the database
        brokerSymbolConfigService.save(brokerSymbolConfig);

        int databaseSizeBeforeDelete = brokerSymbolConfigRepository.findAll().size();

        // Delete the brokerSymbolConfig
        restBrokerSymbolConfigMockMvc.perform(delete("/api/broker-symbol-configs/{id}", brokerSymbolConfig.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BrokerSymbolConfig> brokerSymbolConfigList = brokerSymbolConfigRepository.findAll();
        assertThat(brokerSymbolConfigList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BrokerSymbolConfig.class);
        BrokerSymbolConfig brokerSymbolConfig1 = new BrokerSymbolConfig();
        brokerSymbolConfig1.setId(1L);
        BrokerSymbolConfig brokerSymbolConfig2 = new BrokerSymbolConfig();
        brokerSymbolConfig2.setId(brokerSymbolConfig1.getId());
        assertThat(brokerSymbolConfig1).isEqualTo(brokerSymbolConfig2);
        brokerSymbolConfig2.setId(2L);
        assertThat(brokerSymbolConfig1).isNotEqualTo(brokerSymbolConfig2);
        brokerSymbolConfig1.setId(null);
        assertThat(brokerSymbolConfig1).isNotEqualTo(brokerSymbolConfig2);
    }
}
