package com.hihexa.cms.web.rest;

import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.UserLog;
import com.hihexa.cms.repository.UserLogRepository;
import com.hihexa.cms.service.UserLogService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

/**
 * Integration tests for the {@link UserLogResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class UserLogResourceIT {

    private static final String DEFAULT_IP = "AAAAAAAAAA";
    private static final String UPDATED_IP = "BBBBBBBBBB";

    private static final String DEFAULT_USER_AGENT = "AAAAAAAAAA";
    private static final String UPDATED_USER_AGENT = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final String DEFAULT_DETAIL = "AAAAAAAAAA";
    private static final String UPDATED_DETAIL = "BBBBBBBBBB";
    static long time =System.currentTimeMillis();
    private static final Timestamp DEFAULT_CREATED = new Timestamp(time);
    private static final Timestamp UPDATED_CREATED = new Timestamp(time);

    @Autowired
    private UserLogRepository userLogRepository;

    @Autowired
    private UserLogService userLogService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserLogMockMvc;

    private UserLog userLog;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserLogResource userLogResource = new UserLogResource(userLogService);
        this.restUserLogMockMvc = MockMvcBuilders.standaloneSetup(userLogResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLog createEntity(EntityManager em) {
        UserLog userLog = new UserLog()
            .ip(DEFAULT_IP)
            .userAgent(DEFAULT_USER_AGENT)
            .action(DEFAULT_ACTION)
            .detail(DEFAULT_DETAIL)
            .created(DEFAULT_CREATED);
        return userLog;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserLog createUpdatedEntity(EntityManager em) {
        UserLog userLog = new UserLog()
            .ip(UPDATED_IP)
            .userAgent(UPDATED_USER_AGENT)
            .action(UPDATED_ACTION)
            .detail(UPDATED_DETAIL)
            .created(UPDATED_CREATED);
        return userLog;
    }

    @BeforeEach
    public void initTest() {
        userLog = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserLog() throws Exception {
        int databaseSizeBeforeCreate = userLogRepository.findAll().size();

        // Create the UserLog
        restUserLogMockMvc.perform(post("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLog)))
            .andExpect(status().isCreated());

        // Validate the UserLog in the database
        List<UserLog> userLogList = userLogRepository.findAll();
        assertThat(userLogList).hasSize(databaseSizeBeforeCreate + 1);
        UserLog testUserLog = userLogList.get(userLogList.size() - 1);
        assertThat(testUserLog.getIp()).isEqualTo(DEFAULT_IP);
        assertThat(testUserLog.getUserAgent()).isEqualTo(DEFAULT_USER_AGENT);
        assertThat(testUserLog.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testUserLog.getDetail()).isEqualTo(DEFAULT_DETAIL);
        assertThat(testUserLog.getCreated()).isEqualTo(DEFAULT_CREATED);
    }

    @Test
    @Transactional
    public void createUserLogWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userLogRepository.findAll().size();

        // Create the UserLog with an existing ID
        userLog.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserLogMockMvc.perform(post("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLog)))
            .andExpect(status().isBadRequest());

        // Validate the UserLog in the database
        List<UserLog> userLogList = userLogRepository.findAll();
        assertThat(userLogList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllUserLogs() throws Exception {
        // Initialize the database
        userLogRepository.saveAndFlush(userLog);

        // Get all the userLogList
        restUserLogMockMvc.perform(get("/api/user-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userLog.getId().intValue())))
            .andExpect(jsonPath("$.[*].ip").value(hasItem(DEFAULT_IP.toString())))
            .andExpect(jsonPath("$.[*].userAgent").value(hasItem(DEFAULT_USER_AGENT.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL.toString())));
    }
    
    @Test
    @Transactional
    public void getUserLog() throws Exception {
        // Initialize the database
        userLogRepository.saveAndFlush(userLog);

        // Get the userLog
        restUserLogMockMvc.perform(get("/api/user-logs/{id}", userLog.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userLog.getId().intValue()))
            .andExpect(jsonPath("$.ip").value(DEFAULT_IP.toString()))
            .andExpect(jsonPath("$.userAgent").value(DEFAULT_USER_AGENT.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.detail").value(DEFAULT_DETAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingUserLog() throws Exception {
        // Get the userLog
        restUserLogMockMvc.perform(get("/api/user-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserLog() throws Exception {
        // Initialize the database
        userLogService.save(userLog);

        int databaseSizeBeforeUpdate = userLogRepository.findAll().size();

        // Update the userLog
        UserLog updatedUserLog = userLogRepository.findById(userLog.getId()).get();
        // Disconnect from session so that the updates on updatedUserLog are not directly saved in db
        em.detach(updatedUserLog);
        updatedUserLog
            .ip(UPDATED_IP)
            .userAgent(UPDATED_USER_AGENT)
            .action(UPDATED_ACTION)
            .detail(UPDATED_DETAIL)
            .created(UPDATED_CREATED);

        restUserLogMockMvc.perform(put("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserLog)))
            .andExpect(status().isOk());

        // Validate the UserLog in the database
        List<UserLog> userLogList = userLogRepository.findAll();
        assertThat(userLogList).hasSize(databaseSizeBeforeUpdate);
        UserLog testUserLog = userLogList.get(userLogList.size() - 1);
        assertThat(testUserLog.getIp()).isEqualTo(UPDATED_IP);
        assertThat(testUserLog.getUserAgent()).isEqualTo(UPDATED_USER_AGENT);
        assertThat(testUserLog.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testUserLog.getDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testUserLog.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingUserLog() throws Exception {
        int databaseSizeBeforeUpdate = userLogRepository.findAll().size();

        // Create the UserLog

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserLogMockMvc.perform(put("/api/user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userLog)))
            .andExpect(status().isBadRequest());

        // Validate the UserLog in the database
        List<UserLog> userLogList = userLogRepository.findAll();
        assertThat(userLogList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserLog() throws Exception {
        // Initialize the database
        userLogService.save(userLog);

        int databaseSizeBeforeDelete = userLogRepository.findAll().size();

        // Delete the userLog
        restUserLogMockMvc.perform(delete("/api/user-logs/{id}", userLog.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserLog> userLogList = userLogRepository.findAll();
        assertThat(userLogList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserLog.class);
        UserLog userLog1 = new UserLog();
        userLog1.setId(1L);
        UserLog userLog2 = new UserLog();
        userLog2.setId(userLog1.getId());
        assertThat(userLog1).isEqualTo(userLog2);
        userLog2.setId(2L);
        assertThat(userLog1).isNotEqualTo(userLog2);
        userLog1.setId(null);
        assertThat(userLog1).isNotEqualTo(userLog2);
    }
}
