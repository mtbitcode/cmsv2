package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.Currency;
import com.hihexa.cms.repository.CurrencyRepository;
import com.hihexa.cms.service.CurrencyService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigInteger;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.sameInstant;
import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CurrencyResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class CurrencyResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Long DEFAULT_TARGET_CONFIRMS = 1L;
    private static final Long UPDATED_TARGET_CONFIRMS = 2L;

    private static final Integer DEFAULT_ENABLE_WITHDRAW = 1;

    private static final Integer DEFAULT_ENABLE_DEPOSIT = 1;

    private static final BigInteger DEFAULT_MIN_DEPOSIT = new BigInteger("1");

    private static final BigInteger DEFAULT_MIN_WITHDRAW = new BigInteger("1");

    private static final BigInteger DEFAULT_FEE_WITHDRAW = new BigInteger("1");

    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_IS_DEMO = 1;
    private static final Integer UPDATED_IS_DEMO = 2;

    private static final Integer DEFAULT_MAX_SIZE = 1;
    private static final Integer UPDATED_MAX_SIZE = 2;

    private static final Integer DEFAULT_MIN_SIZE = 1;
    private static final Integer UPDATED_MIN_SIZE = 2;

    private static final Integer DEFAULT_MAX_TIME = 1;
    private static final Integer UPDATED_MAX_TIME = 2;

    private static final String DEFAULT_WALLET_ID = "AAAAAAAAAA";

    private static final BigInteger DEFAULT_INIT_AMOUNT_DEMO = new BigInteger("1");
    private static final BigInteger UPDATED_INIT_AMOUNT_DEMO = new BigInteger("2");

    private static final Integer DEFAULT_DECIMAL_AMOUNT = 1;
    private static final Integer UPDATED_DECIMAL_AMOUNT = 2;

    private static final Integer DEFAULT_BASE_UNIT = 1;
    private static final Integer UPDATED_BASE_UNIT = 2;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCurrencyMockMvc;

    private Currency currency;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CurrencyResource currencyResource = new CurrencyResource(currencyService);
        this.restCurrencyMockMvc = MockMvcBuilders.standaloneSetup(currencyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Currency createEntity(EntityManager em) {
        Currency currency = new Currency()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .targetConfirms(DEFAULT_TARGET_CONFIRMS)
            .activeFlg(DEFAULT_ACTIVE_FLG)
            .isDemo(DEFAULT_IS_DEMO)
            .maxSize(DEFAULT_MAX_SIZE)
            .minSize(DEFAULT_MIN_SIZE)
            .maxTime(DEFAULT_MAX_TIME)
            .decimalAmount(DEFAULT_DECIMAL_AMOUNT)
            .baseUnit(DEFAULT_BASE_UNIT);
        return currency;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Currency createUpdatedEntity(EntityManager em) {
        Currency currency = new Currency()
        		.name(DEFAULT_NAME)
                .code(DEFAULT_CODE)
                .targetConfirms(DEFAULT_TARGET_CONFIRMS)
                .activeFlg(DEFAULT_ACTIVE_FLG)
                .isDemo(DEFAULT_IS_DEMO)
                .maxSize(DEFAULT_MAX_SIZE)
                .minSize(DEFAULT_MIN_SIZE)
                .maxTime(DEFAULT_MAX_TIME)
                .decimalAmount(DEFAULT_DECIMAL_AMOUNT)
                .baseUnit(DEFAULT_BASE_UNIT);
        return currency;
    }

    @BeforeEach
    public void initTest() {
        currency = createEntity(em);
    }

    @Test
    @Transactional
    public void createCurrency() throws Exception {
        int databaseSizeBeforeCreate = currencyRepository.findAll().size();

        // Create the Currency
        restCurrencyMockMvc.perform(post("/api/currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(currency)))
            .andExpect(status().isCreated());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeCreate + 1);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCurrency.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCurrency.getTargetConfirms()).isEqualTo(DEFAULT_TARGET_CONFIRMS);
        assertThat(testCurrency.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
        assertThat(testCurrency.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testCurrency.getUpdated()).isEqualTo(DEFAULT_UPDATED);
        assertThat(testCurrency.getIsDemo()).isEqualTo(DEFAULT_IS_DEMO);
        assertThat(testCurrency.getMaxSize()).isEqualTo(DEFAULT_MAX_SIZE);
        assertThat(testCurrency.getMinSize()).isEqualTo(DEFAULT_MIN_SIZE);
        assertThat(testCurrency.getMaxTime()).isEqualTo(DEFAULT_MAX_TIME);
        assertThat(testCurrency.getInitAmountDemo()).isEqualTo(DEFAULT_INIT_AMOUNT_DEMO);
        assertThat(testCurrency.getDecimalAmount()).isEqualTo(DEFAULT_DECIMAL_AMOUNT);
        assertThat(testCurrency.getBaseUnit()).isEqualTo(DEFAULT_BASE_UNIT);
    }

    @Test
    @Transactional
    public void createCurrencyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = currencyRepository.findAll().size();

        // Create the Currency with an existing ID
        currency.setId(1);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCurrencyMockMvc.perform(post("/api/currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(currency)))
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCurrencies() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get all the currencyList
        restCurrencyMockMvc.perform(get("/api/currencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(currency.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].targetConfirms").value(hasItem(DEFAULT_TARGET_CONFIRMS.intValue())))
            .andExpect(jsonPath("$.[*].enableWithdraw").value(hasItem(DEFAULT_ENABLE_WITHDRAW)))
            .andExpect(jsonPath("$.[*].enableDeposit").value(hasItem(DEFAULT_ENABLE_DEPOSIT)))
            .andExpect(jsonPath("$.[*].minDeposit").value(hasItem(DEFAULT_MIN_DEPOSIT.intValue())))
            .andExpect(jsonPath("$.[*].minWithdraw").value(hasItem(DEFAULT_MIN_WITHDRAW.intValue())))
            .andExpect(jsonPath("$.[*].feeWithdraw").value(hasItem(DEFAULT_FEE_WITHDRAW.intValue())))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].updated").value(hasItem(sameInstant(DEFAULT_UPDATED))))
            .andExpect(jsonPath("$.[*].isDemo").value(hasItem(DEFAULT_IS_DEMO)))
            .andExpect(jsonPath("$.[*].maxSize").value(hasItem(DEFAULT_MAX_SIZE)))
            .andExpect(jsonPath("$.[*].minSize").value(hasItem(DEFAULT_MIN_SIZE)))
            .andExpect(jsonPath("$.[*].maxTime").value(hasItem(DEFAULT_MAX_TIME)))
            .andExpect(jsonPath("$.[*].walletId").value(hasItem(DEFAULT_WALLET_ID.toString())))
            .andExpect(jsonPath("$.[*].initAmountDemo").value(hasItem(DEFAULT_INIT_AMOUNT_DEMO.intValue())))
            .andExpect(jsonPath("$.[*].decimalAmount").value(hasItem(DEFAULT_DECIMAL_AMOUNT)))
            .andExpect(jsonPath("$.[*].baseUnit").value(hasItem(DEFAULT_BASE_UNIT)));
    }

    @Test
    @Transactional
    public void getCurrency() throws Exception {
        // Initialize the database
        currencyRepository.saveAndFlush(currency);

        // Get the currency
        restCurrencyMockMvc.perform(get("/api/currencies/{id}", currency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(currency.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.targetConfirms").value(DEFAULT_TARGET_CONFIRMS.intValue()))
            .andExpect(jsonPath("$.enableWithdraw").value(DEFAULT_ENABLE_WITHDRAW))
            .andExpect(jsonPath("$.enableDeposit").value(DEFAULT_ENABLE_DEPOSIT))
            .andExpect(jsonPath("$.minDeposit").value(DEFAULT_MIN_DEPOSIT.intValue()))
            .andExpect(jsonPath("$.minWithdraw").value(DEFAULT_MIN_WITHDRAW.intValue()))
            .andExpect(jsonPath("$.feeWithdraw").value(DEFAULT_FEE_WITHDRAW.intValue()))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)))
            .andExpect(jsonPath("$.updated").value(sameInstant(DEFAULT_UPDATED)))
            .andExpect(jsonPath("$.isDemo").value(DEFAULT_IS_DEMO))
            .andExpect(jsonPath("$.maxSize").value(DEFAULT_MAX_SIZE))
            .andExpect(jsonPath("$.minSize").value(DEFAULT_MIN_SIZE))
            .andExpect(jsonPath("$.maxTime").value(DEFAULT_MAX_TIME))
            .andExpect(jsonPath("$.walletId").value(DEFAULT_WALLET_ID.toString()))
            .andExpect(jsonPath("$.initAmountDemo").value(DEFAULT_INIT_AMOUNT_DEMO.intValue()))
            .andExpect(jsonPath("$.decimalAmount").value(DEFAULT_DECIMAL_AMOUNT))
            .andExpect(jsonPath("$.baseUnit").value(DEFAULT_BASE_UNIT));
    }

    @Test
    @Transactional
    public void getNonExistingCurrency() throws Exception {
        // Get the currency
        restCurrencyMockMvc.perform(get("/api/currencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCurrency() throws Exception {
        // Initialize the database
        currencyService.save(currency);

        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();

        // Update the currency
        Currency updatedCurrency = currencyRepository.findById(currency.getId()).get();
        // Disconnect from session so that the updates on updatedCurrency are not directly saved in db
        em.detach(updatedCurrency);
        updatedCurrency
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .targetConfirms(UPDATED_TARGET_CONFIRMS)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .isDemo(UPDATED_IS_DEMO)
            .maxSize(UPDATED_MAX_SIZE)
            .minSize(UPDATED_MIN_SIZE)
            .maxTime(UPDATED_MAX_TIME)
            .decimalAmount(UPDATED_DECIMAL_AMOUNT)
            .baseUnit(UPDATED_BASE_UNIT);

        restCurrencyMockMvc.perform(put("/api/currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCurrency)))
            .andExpect(status().isOk());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
        Currency testCurrency = currencyList.get(currencyList.size() - 1);
        assertThat(testCurrency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCurrency.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCurrency.getTargetConfirms()).isEqualTo(UPDATED_TARGET_CONFIRMS);
        assertThat(testCurrency.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
        assertThat(testCurrency.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testCurrency.getUpdated()).isEqualTo(UPDATED_UPDATED);
        assertThat(testCurrency.getIsDemo()).isEqualTo(UPDATED_IS_DEMO);
        assertThat(testCurrency.getMaxSize()).isEqualTo(UPDATED_MAX_SIZE);
        assertThat(testCurrency.getMinSize()).isEqualTo(UPDATED_MIN_SIZE);
        assertThat(testCurrency.getMaxTime()).isEqualTo(UPDATED_MAX_TIME);
        assertThat(testCurrency.getInitAmountDemo()).isEqualTo(UPDATED_INIT_AMOUNT_DEMO);
        assertThat(testCurrency.getDecimalAmount()).isEqualTo(UPDATED_DECIMAL_AMOUNT);
        assertThat(testCurrency.getBaseUnit()).isEqualTo(UPDATED_BASE_UNIT);
    }

    @Test
    @Transactional
    public void updateNonExistingCurrency() throws Exception {
        int databaseSizeBeforeUpdate = currencyRepository.findAll().size();

        // Create the Currency

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCurrencyMockMvc.perform(put("/api/currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(currency)))
            .andExpect(status().isBadRequest());

        // Validate the Currency in the database
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCurrency() throws Exception {
        // Initialize the database
        currencyService.save(currency);

        int databaseSizeBeforeDelete = currencyRepository.findAll().size();

        // Delete the currency
        restCurrencyMockMvc.perform(delete("/api/currencies/{id}", currency.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Currency> currencyList = currencyRepository.findAll();
        assertThat(currencyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Currency.class);
        Currency currency1 = new Currency();
        currency1.setId(1);
        Currency currency2 = new Currency();
        currency2.setId(currency1.getId());
        assertThat(currency1).isEqualTo(currency2);
        currency2.setId(2);
        assertThat(currency1).isNotEqualTo(currency2);
        currency1.setId(null);
        assertThat(currency1).isNotEqualTo(currency2);
    }
}
