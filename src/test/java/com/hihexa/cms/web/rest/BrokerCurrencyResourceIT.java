package com.hihexa.cms.web.rest;

import com.hihexa.cms.Cmsv2App;
import com.hihexa.cms.domain.BrokerCurrency;
import com.hihexa.cms.repository.BrokerCurrencyRepository;
import com.hihexa.cms.service.BrokerCurrencyService;
import com.hihexa.cms.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.hihexa.cms.web.rest.TestUtil.sameInstant;
import static com.hihexa.cms.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BrokerCurrencyResource} REST controller.
 */
@SpringBootTest(classes = Cmsv2App.class)
public class BrokerCurrencyResourceIT {

    private static final Integer DEFAULT_BROKER_ID = 1;
    private static final Integer UPDATED_BROKER_ID = 2;
    private static final Integer DEFAULT_CURRENCY_ID = 1;
    private static final Integer UPDATED_CURRENCY_ID = 2;
    private static final Integer DEFAULT_ENABLE_WITHDRAW = 1;
    private static final Integer UPDATED_ENABLE_WITHDRAW = 2;
    private static final Integer DEFAULT_ENABLE_DEPOSIT = 1;
    private static final Integer UPDATED_ENABLE_DEPOSIT = 2;
    private static final Long DEFAULT_MIN_DEPOSIT = 1L;
    private static final Long UPDATED_MIN_DEPOSIT = 2L;
    private static final Long DEFAULT_MIN_WITHDRAW = 1L;
    private static final Long UPDATED_MIN_WITHDRAW = 2L;
    private static final Long DEFAULT_FEE_WITHDRAW = 1L;
    private static final Long UPDATED_FEE_WITHDRAW = 2L;
    private static final Integer DEFAULT_ACTIVE_FLG = 1;
    private static final Integer UPDATED_ACTIVE_FLG = 2;
    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final BigDecimal DEFAULT_MAX_ALLOW_WITHDRAW = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_ALLOW_WITHDRAW = new BigDecimal(2);

    private static final String DEFAULT_WALLET_ID = "AAAAAAAAAA";
    private static final String UPDATED_WALLET_ID = "BBBBBBBBBB";

    @Autowired
    private BrokerCurrencyRepository brokerCurrencyRepository;

    @Autowired
    private BrokerCurrencyService brokerCurrencyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBrokerCurrencyMockMvc;

    private BrokerCurrency brokerCurrency;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final BrokerCurrencyResource brokerCurrencyResource = new BrokerCurrencyResource(brokerCurrencyService);
        this.restBrokerCurrencyMockMvc = MockMvcBuilders.standaloneSetup(brokerCurrencyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BrokerCurrency createEntity(EntityManager em) {
        BrokerCurrency brokerCurrency = new BrokerCurrency()
            .brokerId(DEFAULT_BROKER_ID)
            .currencyId(DEFAULT_CURRENCY_ID)
            .enableWithdraw(DEFAULT_ENABLE_WITHDRAW)
            .enableDeposit(DEFAULT_ENABLE_DEPOSIT)
            .minDeposit(DEFAULT_MIN_DEPOSIT)
            .minWithdraw(DEFAULT_MIN_WITHDRAW)
            .feeWithdraw(DEFAULT_FEE_WITHDRAW)
            .activeFlg(DEFAULT_ACTIVE_FLG)
            .maxAllowWithdraw(DEFAULT_MAX_ALLOW_WITHDRAW)
            .walletId(DEFAULT_WALLET_ID);
        return brokerCurrency;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BrokerCurrency createUpdatedEntity(EntityManager em) {
        BrokerCurrency brokerCurrency = new BrokerCurrency()
            .brokerId(UPDATED_BROKER_ID)
            .currencyId(UPDATED_CURRENCY_ID)
            .enableWithdraw(UPDATED_ENABLE_WITHDRAW)
            .enableDeposit(UPDATED_ENABLE_DEPOSIT)
            .minDeposit(UPDATED_MIN_DEPOSIT)
            .minWithdraw(UPDATED_MIN_WITHDRAW)
            .feeWithdraw(UPDATED_FEE_WITHDRAW)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .maxAllowWithdraw(UPDATED_MAX_ALLOW_WITHDRAW)
            .walletId(UPDATED_WALLET_ID);
        return brokerCurrency;
    }

    @BeforeEach
    public void initTest() {
        brokerCurrency = createEntity(em);
    }

    @Test
    @Transactional
    public void createBrokerCurrency() throws Exception {
        int databaseSizeBeforeCreate = brokerCurrencyRepository.findAll().size();

        // Create the BrokerCurrency
        restBrokerCurrencyMockMvc.perform(post("/api/broker-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerCurrency)))
            .andExpect(status().isCreated());

        // Validate the BrokerCurrency in the database
        List<BrokerCurrency> brokerCurrencyList = brokerCurrencyRepository.findAll();
        assertThat(brokerCurrencyList).hasSize(databaseSizeBeforeCreate + 1);
        BrokerCurrency testBrokerCurrency = brokerCurrencyList.get(brokerCurrencyList.size() - 1);
        assertThat(testBrokerCurrency.getBrokerId()).isEqualTo(DEFAULT_BROKER_ID);
        assertThat(testBrokerCurrency.getCurrencyId()).isEqualTo(DEFAULT_CURRENCY_ID);
        assertThat(testBrokerCurrency.getEnableWithdraw()).isEqualTo(DEFAULT_ENABLE_WITHDRAW);
        assertThat(testBrokerCurrency.getEnableDeposit()).isEqualTo(DEFAULT_ENABLE_DEPOSIT);
        assertThat(testBrokerCurrency.getMinDeposit()).isEqualTo(DEFAULT_MIN_DEPOSIT);
        assertThat(testBrokerCurrency.getMinWithdraw()).isEqualTo(DEFAULT_MIN_WITHDRAW);
        assertThat(testBrokerCurrency.getFeeWithdraw()).isEqualTo(DEFAULT_FEE_WITHDRAW);
        assertThat(testBrokerCurrency.getActiveFlg()).isEqualTo(DEFAULT_ACTIVE_FLG);
        assertThat(testBrokerCurrency.getCreated()).isEqualTo(DEFAULT_CREATED);
        assertThat(testBrokerCurrency.getUpdated()).isEqualTo(DEFAULT_UPDATED);
        assertThat(testBrokerCurrency.getMaxAllowWithdraw()).isEqualTo(DEFAULT_MAX_ALLOW_WITHDRAW);
        assertThat(testBrokerCurrency.getWalletId()).isEqualTo(DEFAULT_WALLET_ID);
    }

    @Test
    @Transactional
    public void createBrokerCurrencyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = brokerCurrencyRepository.findAll().size();

        // Create the BrokerCurrency with an existing ID
        brokerCurrency.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBrokerCurrencyMockMvc.perform(post("/api/broker-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerCurrency)))
            .andExpect(status().isBadRequest());

        // Validate the BrokerCurrency in the database
        List<BrokerCurrency> brokerCurrencyList = brokerCurrencyRepository.findAll();
        assertThat(brokerCurrencyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBrokerCurrencies() throws Exception {
        // Initialize the database
        brokerCurrencyRepository.saveAndFlush(brokerCurrency);

        // Get all the brokerCurrencyList
        restBrokerCurrencyMockMvc.perform(get("/api/broker-currencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(brokerCurrency.getId().intValue())))
            .andExpect(jsonPath("$.[*].brokerId").value(hasItem(DEFAULT_BROKER_ID.intValue())))
            .andExpect(jsonPath("$.[*].currencyId").value(hasItem(DEFAULT_CURRENCY_ID.intValue())))
            .andExpect(jsonPath("$.[*].enableWithdraw").value(hasItem(DEFAULT_ENABLE_WITHDRAW)))
            .andExpect(jsonPath("$.[*].enableDeposit").value(hasItem(DEFAULT_ENABLE_DEPOSIT)))
            .andExpect(jsonPath("$.[*].minDeposit").value(hasItem(DEFAULT_MIN_DEPOSIT.intValue())))
            .andExpect(jsonPath("$.[*].minWithdraw").value(hasItem(DEFAULT_MIN_WITHDRAW.intValue())))
            .andExpect(jsonPath("$.[*].feeWithdraw").value(hasItem(DEFAULT_FEE_WITHDRAW.intValue())))
            .andExpect(jsonPath("$.[*].activeFlg").value(hasItem(DEFAULT_ACTIVE_FLG)))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))))
            .andExpect(jsonPath("$.[*].updated").value(hasItem(sameInstant(DEFAULT_UPDATED))))
            .andExpect(jsonPath("$.[*].maxAllowWithdraw").value(hasItem(DEFAULT_MAX_ALLOW_WITHDRAW.intValue())))
            .andExpect(jsonPath("$.[*].walletId").value(hasItem(DEFAULT_WALLET_ID.toString())));
    }

    @Test
    @Transactional
    public void getBrokerCurrency() throws Exception {
        // Initialize the database
        brokerCurrencyRepository.saveAndFlush(brokerCurrency);

        // Get the brokerCurrency
        restBrokerCurrencyMockMvc.perform(get("/api/broker-currencies/{id}", brokerCurrency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(brokerCurrency.getId().intValue()))
            .andExpect(jsonPath("$.brokerId").value(DEFAULT_BROKER_ID.intValue()))
            .andExpect(jsonPath("$.currencyId").value(DEFAULT_CURRENCY_ID.intValue()))
            .andExpect(jsonPath("$.enableWithdraw").value(DEFAULT_ENABLE_WITHDRAW))
            .andExpect(jsonPath("$.enableDeposit").value(DEFAULT_ENABLE_DEPOSIT))
            .andExpect(jsonPath("$.minDeposit").value(DEFAULT_MIN_DEPOSIT.intValue()))
            .andExpect(jsonPath("$.minWithdraw").value(DEFAULT_MIN_WITHDRAW.intValue()))
            .andExpect(jsonPath("$.feeWithdraw").value(DEFAULT_FEE_WITHDRAW.intValue()))
            .andExpect(jsonPath("$.activeFlg").value(DEFAULT_ACTIVE_FLG))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)))
            .andExpect(jsonPath("$.updated").value(sameInstant(DEFAULT_UPDATED)))
            .andExpect(jsonPath("$.maxAllowWithdraw").value(DEFAULT_MAX_ALLOW_WITHDRAW.intValue()))
            .andExpect(jsonPath("$.walletId").value(DEFAULT_WALLET_ID.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingBrokerCurrency() throws Exception {
        // Get the brokerCurrency
        restBrokerCurrencyMockMvc.perform(get("/api/broker-currencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBrokerCurrency() throws Exception {
        // Initialize the database
        brokerCurrencyService.save(brokerCurrency);

        int databaseSizeBeforeUpdate = brokerCurrencyRepository.findAll().size();

        // Update the brokerCurrency
        BrokerCurrency updatedBrokerCurrency = brokerCurrencyRepository.findById(brokerCurrency.getId()).get();
        // Disconnect from session so that the updates on updatedBrokerCurrency are not directly saved in db
        em.detach(updatedBrokerCurrency);
        updatedBrokerCurrency
            .brokerId(UPDATED_BROKER_ID)
            .currencyId(UPDATED_CURRENCY_ID)
            .enableWithdraw(UPDATED_ENABLE_WITHDRAW)
            .enableDeposit(UPDATED_ENABLE_DEPOSIT)
            .minDeposit(UPDATED_MIN_DEPOSIT)
            .minWithdraw(UPDATED_MIN_WITHDRAW)
            .feeWithdraw(UPDATED_FEE_WITHDRAW)
            .activeFlg(UPDATED_ACTIVE_FLG)
            .maxAllowWithdraw(UPDATED_MAX_ALLOW_WITHDRAW)
            .walletId(UPDATED_WALLET_ID);

        restBrokerCurrencyMockMvc.perform(put("/api/broker-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBrokerCurrency)))
            .andExpect(status().isOk());

        // Validate the BrokerCurrency in the database
        List<BrokerCurrency> brokerCurrencyList = brokerCurrencyRepository.findAll();
        assertThat(brokerCurrencyList).hasSize(databaseSizeBeforeUpdate);
        BrokerCurrency testBrokerCurrency = brokerCurrencyList.get(brokerCurrencyList.size() - 1);
        assertThat(testBrokerCurrency.getBrokerId()).isEqualTo(UPDATED_BROKER_ID);
        assertThat(testBrokerCurrency.getCurrencyId()).isEqualTo(UPDATED_CURRENCY_ID);
        assertThat(testBrokerCurrency.getEnableWithdraw()).isEqualTo(UPDATED_ENABLE_WITHDRAW);
        assertThat(testBrokerCurrency.getEnableDeposit()).isEqualTo(UPDATED_ENABLE_DEPOSIT);
        assertThat(testBrokerCurrency.getMinDeposit()).isEqualTo(UPDATED_MIN_DEPOSIT);
        assertThat(testBrokerCurrency.getMinWithdraw()).isEqualTo(UPDATED_MIN_WITHDRAW);
        assertThat(testBrokerCurrency.getFeeWithdraw()).isEqualTo(UPDATED_FEE_WITHDRAW);
        assertThat(testBrokerCurrency.getActiveFlg()).isEqualTo(UPDATED_ACTIVE_FLG);
        assertThat(testBrokerCurrency.getCreated()).isEqualTo(UPDATED_CREATED);
        assertThat(testBrokerCurrency.getUpdated()).isEqualTo(UPDATED_UPDATED);
        assertThat(testBrokerCurrency.getMaxAllowWithdraw()).isEqualTo(UPDATED_MAX_ALLOW_WITHDRAW);
        assertThat(testBrokerCurrency.getWalletId()).isEqualTo(UPDATED_WALLET_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingBrokerCurrency() throws Exception {
        int databaseSizeBeforeUpdate = brokerCurrencyRepository.findAll().size();

        // Create the BrokerCurrency

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBrokerCurrencyMockMvc.perform(put("/api/broker-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(brokerCurrency)))
            .andExpect(status().isBadRequest());

        // Validate the BrokerCurrency in the database
        List<BrokerCurrency> brokerCurrencyList = brokerCurrencyRepository.findAll();
        assertThat(brokerCurrencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBrokerCurrency() throws Exception {
        // Initialize the database
        brokerCurrencyService.save(brokerCurrency);

        int databaseSizeBeforeDelete = brokerCurrencyRepository.findAll().size();

        // Delete the brokerCurrency
        restBrokerCurrencyMockMvc.perform(delete("/api/broker-currencies/{id}", brokerCurrency.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BrokerCurrency> brokerCurrencyList = brokerCurrencyRepository.findAll();
        assertThat(brokerCurrencyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BrokerCurrency.class);
        BrokerCurrency brokerCurrency1 = new BrokerCurrency();
        brokerCurrency1.setId(1L);
        BrokerCurrency brokerCurrency2 = new BrokerCurrency();
        brokerCurrency2.setId(brokerCurrency1.getId());
        assertThat(brokerCurrency1).isEqualTo(brokerCurrency2);
        brokerCurrency2.setId(2L);
        assertThat(brokerCurrency1).isNotEqualTo(brokerCurrency2);
        brokerCurrency1.setId(null);
        assertThat(brokerCurrency1).isNotEqualTo(brokerCurrency2);
    }
}
