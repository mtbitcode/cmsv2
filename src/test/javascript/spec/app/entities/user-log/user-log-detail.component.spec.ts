/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { UserLogDetailComponent } from 'app/entities/user-log/user-log-detail.component';
import { UserLog } from 'app/shared/model/user-log.model';

describe('Component Tests', () => {
  describe('UserLog Management Detail Component', () => {
    let comp: UserLogDetailComponent;
    let fixture: ComponentFixture<UserLogDetailComponent>;
    const route = ({ data: of({ userLog: new UserLog(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [UserLogDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserLogDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserLogDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userLog).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
