/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { UserLogUpdateComponent } from 'app/entities/user-log/user-log-update.component';
import { UserLogService } from 'app/entities/user-log/user-log.service';
import { UserLog } from 'app/shared/model/user-log.model';

describe('Component Tests', () => {
  describe('UserLog Management Update Component', () => {
    let comp: UserLogUpdateComponent;
    let fixture: ComponentFixture<UserLogUpdateComponent>;
    let service: UserLogService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [UserLogUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserLogUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserLogUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserLogService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLog(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLog();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
