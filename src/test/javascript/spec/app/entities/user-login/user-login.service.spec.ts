/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { UserLoginService } from 'app/entities/user-login/user-login.service';
import { IUserLogin, UserLogin } from 'app/shared/model/user-login.model';

describe('Service Tests', () => {
  describe('UserLogin Service', () => {
    let injector: TestBed;
    let service: UserLoginService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserLogin;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(UserLoginService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new UserLogin(0, 0, 'AAAAAAA', 'AAAAAAA', 0, currentDate, 'AAAAAAA', 'AAAAAAA', 0, currentDate);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            lastLogin: currentDate.format(DATE_TIME_FORMAT),
            created: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a UserLogin', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            lastLogin: currentDate.format(DATE_TIME_FORMAT),
            created: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            lastLogin: currentDate,
            created: currentDate
          },
          returnedFromService
        );
        service
          .create(new UserLogin(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a UserLogin', async () => {
        const returnedFromService = Object.assign(
          {
            brokerId: 1,
            email: 'BBBBBB',
            password: 'BBBBBB',
            status: 1,
            lastLogin: currentDate.format(DATE_TIME_FORMAT),
            agents: 'BBBBBB',
            ips: 'BBBBBB',
            activeFlg: 1,
            created: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            lastLogin: currentDate,
            created: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of UserLogin', async () => {
        const returnedFromService = Object.assign(
          {
            brokerId: 1,
            email: 'BBBBBB',
            password: 'BBBBBB',
            status: 1,
            lastLogin: currentDate.format(DATE_TIME_FORMAT),
            agents: 'BBBBBB',
            ips: 'BBBBBB',
            activeFlg: 1,
            created: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            lastLogin: currentDate,
            created: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserLogin', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
