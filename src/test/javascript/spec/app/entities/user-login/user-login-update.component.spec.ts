/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { UserLoginUpdateComponent } from 'app/entities/user-login/user-login-update.component';
import { UserLoginService } from 'app/entities/user-login/user-login.service';
import { UserLogin } from 'app/shared/model/user-login.model';

describe('Component Tests', () => {
  describe('UserLogin Management Update Component', () => {
    let comp: UserLoginUpdateComponent;
    let fixture: ComponentFixture<UserLoginUpdateComponent>;
    let service: UserLoginService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [UserLoginUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserLoginUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserLoginUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserLoginService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLogin(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserLogin();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
