/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { UserSettingUpdateComponent } from 'app/entities/user-setting/user-setting-update.component';
import { UserSettingService } from 'app/entities/user-setting/user-setting.service';
import { UserSetting } from 'app/shared/model/user-setting.model';

describe('Component Tests', () => {
  describe('UserSetting Management Update Component', () => {
    let comp: UserSettingUpdateComponent;
    let fixture: ComponentFixture<UserSettingUpdateComponent>;
    let service: UserSettingService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [UserSettingUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserSettingUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserSettingUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserSettingService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserSetting(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserSetting();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
