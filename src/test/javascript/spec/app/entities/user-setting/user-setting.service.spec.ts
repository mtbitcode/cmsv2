/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { UserSettingService } from 'app/entities/user-setting/user-setting.service';
import { IUserSetting, UserSetting } from 'app/shared/model/user-setting.model';

describe('Service Tests', () => {
  describe('UserSetting Service', () => {
    let injector: TestBed;
    let service: UserSettingService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserSetting;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(UserSettingService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new UserSetting(
        0,
        0,
        0,
        0,
        0,
        currentDate,
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        0,
        0,
        0,
        'AAAAAAA',
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a UserSetting', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate,
            dob: currentDate
          },
          returnedFromService
        );
        service
          .create(new UserSetting(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a UserSetting', async () => {
        const returnedFromService = Object.assign(
          {
            loginAlert: 1,
            keepAlive: 1,
            detectIpChange: 1,
            activeFlg: 1,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
            avator: 'BBBBBB',
            username: 'BBBBBB',
            firstName: 'BBBBBB',
            middleName: 'BBBBBB',
            lastName: 'BBBBBB',
            sfa: 1,
            faSeed: 'BBBBBB',
            refId: 'BBBBBB',
            wdWhitelistStatus: 1,
            verifyLevel: 1,
            wdDailyLimit: 1,
            dailyWdAmount: 1,
            address: 'BBBBBB',
            postCode: 'BBBBBB',
            city: 'BBBBBB',
            country: 'BBBBBB',
            takerFee: 1,
            makerFee: 1,
            feeType: 1,
            identityId: 'BBBBBB',
            withdrawBlocktime: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate,
            dob: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of UserSetting', async () => {
        const returnedFromService = Object.assign(
          {
            loginAlert: 1,
            keepAlive: 1,
            detectIpChange: 1,
            activeFlg: 1,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            dob: currentDate.format(DATE_TIME_FORMAT),
            avator: 'BBBBBB',
            username: 'BBBBBB',
            firstName: 'BBBBBB',
            middleName: 'BBBBBB',
            lastName: 'BBBBBB',
            sfa: 1,
            faSeed: 'BBBBBB',
            refId: 'BBBBBB',
            wdWhitelistStatus: 1,
            verifyLevel: 1,
            wdDailyLimit: 1,
            dailyWdAmount: 1,
            address: 'BBBBBB',
            postCode: 'BBBBBB',
            city: 'BBBBBB',
            country: 'BBBBBB',
            takerFee: 1,
            makerFee: 1,
            feeType: 1,
            identityId: 'BBBBBB',
            withdrawBlocktime: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate,
            dob: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserSetting', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
