/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { UserSettingDetailComponent } from 'app/entities/user-setting/user-setting-detail.component';
import { UserSetting } from 'app/shared/model/user-setting.model';

describe('Component Tests', () => {
  describe('UserSetting Management Detail Component', () => {
    let comp: UserSettingDetailComponent;
    let fixture: ComponentFixture<UserSettingDetailComponent>;
    const route = ({ data: of({ userSetting: new UserSetting(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [UserSettingDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserSettingDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserSettingDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userSetting).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
