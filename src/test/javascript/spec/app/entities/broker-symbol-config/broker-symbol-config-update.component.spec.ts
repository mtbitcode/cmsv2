/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BrokerSymbolConfigUpdateComponent } from 'app/entities/broker-symbol-config/broker-symbol-config-update.component';
import { BrokerSymbolConfigService } from 'app/entities/broker-symbol-config/broker-symbol-config.service';
import { BrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

describe('Component Tests', () => {
  describe('BrokerSymbolConfig Management Update Component', () => {
    let comp: BrokerSymbolConfigUpdateComponent;
    let fixture: ComponentFixture<BrokerSymbolConfigUpdateComponent>;
    let service: BrokerSymbolConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BrokerSymbolConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BrokerSymbolConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BrokerSymbolConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BrokerSymbolConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrokerSymbolConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrokerSymbolConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
