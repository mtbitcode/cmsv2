/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BrokerSymbolConfigService } from 'app/entities/broker-symbol-config/broker-symbol-config.service';
import { IBrokerSymbolConfig, BrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

describe('Service Tests', () => {
  describe('BrokerSymbolConfig Service', () => {
    let injector: TestBed;
    let service: BrokerSymbolConfigService;
    let httpMock: HttpTestingController;
    let elemDefault: IBrokerSymbolConfig;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(BrokerSymbolConfigService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BrokerSymbolConfig(0, 0, 0, 0, 0, 0, 0, 0, currentDate, currentDate, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign(
          {
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a BrokerSymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .create(new BrokerSymbolConfig(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a BrokerSymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            brokerId: 1,
            symbolId: 1,
            tradable: 1,
            minTrade: 1,
            marginEnable: 1,
            maxTrade: 1,
            maxPrice: 1,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            minPrice: 1,
            activeFlg: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of BrokerSymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            brokerId: 1,
            symbolId: 1,
            tradable: 1,
            minTrade: 1,
            marginEnable: 1,
            maxTrade: 1,
            maxPrice: 1,
            created: currentDate.format(DATE_TIME_FORMAT),
            updated: currentDate.format(DATE_TIME_FORMAT),
            minPrice: 1,
            activeFlg: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            created: currentDate,
            updated: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BrokerSymbolConfig', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
