/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BrokerSymbolConfigDetailComponent } from 'app/entities/broker-symbol-config/broker-symbol-config-detail.component';
import { BrokerSymbolConfig } from 'app/shared/model/broker-symbol-config.model';

describe('Component Tests', () => {
  describe('BrokerSymbolConfig Management Detail Component', () => {
    let comp: BrokerSymbolConfigDetailComponent;
    let fixture: ComponentFixture<BrokerSymbolConfigDetailComponent>;
    const route = ({ data: of({ brokerSymbolConfig: new BrokerSymbolConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BrokerSymbolConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BrokerSymbolConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BrokerSymbolConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.brokerSymbolConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
