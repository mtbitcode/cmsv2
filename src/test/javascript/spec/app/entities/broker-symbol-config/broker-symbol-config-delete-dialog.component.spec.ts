/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Cmsv2TestModule } from '../../../test.module';
import { BrokerSymbolConfigDeleteDialogComponent } from 'app/entities/broker-symbol-config/broker-symbol-config-delete-dialog.component';
import { BrokerSymbolConfigService } from 'app/entities/broker-symbol-config/broker-symbol-config.service';

describe('Component Tests', () => {
  describe('BrokerSymbolConfig Management Delete Component', () => {
    let comp: BrokerSymbolConfigDeleteDialogComponent;
    let fixture: ComponentFixture<BrokerSymbolConfigDeleteDialogComponent>;
    let service: BrokerSymbolConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BrokerSymbolConfigDeleteDialogComponent]
      })
        .overrideTemplate(BrokerSymbolConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BrokerSymbolConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BrokerSymbolConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
