/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Cmsv2TestModule } from '../../../test.module';
import { SymbolConfigDeleteDialogComponent } from 'app/entities/symbol-config/symbol-config-delete-dialog.component';
import { SymbolConfigService } from 'app/entities/symbol-config/symbol-config.service';

describe('Component Tests', () => {
  describe('SymbolConfig Management Delete Component', () => {
    let comp: SymbolConfigDeleteDialogComponent;
    let fixture: ComponentFixture<SymbolConfigDeleteDialogComponent>;
    let service: SymbolConfigService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [SymbolConfigDeleteDialogComponent]
      })
        .overrideTemplate(SymbolConfigDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SymbolConfigDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SymbolConfigService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
