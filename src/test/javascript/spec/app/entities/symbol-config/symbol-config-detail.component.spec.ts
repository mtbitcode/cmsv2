/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { SymbolConfigDetailComponent } from 'app/entities/symbol-config/symbol-config-detail.component';
import { SymbolConfig } from 'app/shared/model/symbol-config.model';

describe('Component Tests', () => {
  describe('SymbolConfig Management Detail Component', () => {
    let comp: SymbolConfigDetailComponent;
    let fixture: ComponentFixture<SymbolConfigDetailComponent>;
    const route = ({ data: of({ symbolConfig: new SymbolConfig(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [SymbolConfigDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(SymbolConfigDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(SymbolConfigDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.symbolConfig).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
