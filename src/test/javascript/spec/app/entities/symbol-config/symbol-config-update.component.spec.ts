/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { SymbolConfigUpdateComponent } from 'app/entities/symbol-config/symbol-config-update.component';
import { SymbolConfigService } from 'app/entities/symbol-config/symbol-config.service';
import { SymbolConfig } from 'app/shared/model/symbol-config.model';

describe('Component Tests', () => {
  describe('SymbolConfig Management Update Component', () => {
    let comp: SymbolConfigUpdateComponent;
    let fixture: ComponentFixture<SymbolConfigUpdateComponent>;
    let service: SymbolConfigService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [SymbolConfigUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SymbolConfigUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(SymbolConfigUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(SymbolConfigService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new SymbolConfig(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new SymbolConfig();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
