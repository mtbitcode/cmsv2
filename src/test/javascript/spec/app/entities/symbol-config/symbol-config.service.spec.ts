/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { SymbolConfigService } from 'app/entities/symbol-config/symbol-config.service';
import { ISymbolConfig, SymbolConfig } from 'app/shared/model/symbol-config.model';

describe('Service Tests', () => {
  describe('SymbolConfig Service', () => {
    let injector: TestBed;
    let service: SymbolConfigService;
    let httpMock: HttpTestingController;
    let elemDefault: ISymbolConfig;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(SymbolConfigService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new SymbolConfig(0, 'AAAAAAA', 0, 'AAAAAAA', 0, 'AAAAAAA', 0, 'AAAAAAA', 'AAAAAAA', 0, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a SymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new SymbolConfig(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a SymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            symbol: 'BBBBBB',
            baseCcId: 1,
            baseCc: 'BBBBBB',
            counterCcId: 1,
            counterCc: 'BBBBBB',
            type: 1,
            groupName: 'BBBBBB',
            description: 'BBBBBB',
            decimalPlaceAmount: 1,
            decimalPlacePrice: 1,
            isDemo: 1
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of SymbolConfig', async () => {
        const returnedFromService = Object.assign(
          {
            symbol: 'BBBBBB',
            baseCcId: 1,
            baseCc: 'BBBBBB',
            counterCcId: 1,
            counterCc: 'BBBBBB',
            type: 1,
            groupName: 'BBBBBB',
            description: 'BBBBBB',
            decimalPlaceAmount: 1,
            decimalPlacePrice: 1,
            isDemo: 1
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a SymbolConfig', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
