/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BrokerCurrencyUpdateComponent } from 'app/entities/broker-currency/broker-currency-update.component';
import { BrokerCurrencyService } from 'app/entities/broker-currency/broker-currency.service';
import { BrokerCurrency } from 'app/shared/model/broker-currency.model';

describe('Component Tests', () => {
  describe('BrokerCurrency Management Update Component', () => {
    let comp: BrokerCurrencyUpdateComponent;
    let fixture: ComponentFixture<BrokerCurrencyUpdateComponent>;
    let service: BrokerCurrencyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BrokerCurrencyUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BrokerCurrencyUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BrokerCurrencyUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BrokerCurrencyService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrokerCurrency(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BrokerCurrency();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
