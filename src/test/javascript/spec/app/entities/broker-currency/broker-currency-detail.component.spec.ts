/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BrokerCurrencyDetailComponent } from 'app/entities/broker-currency/broker-currency-detail.component';
import { BrokerCurrency } from 'app/shared/model/broker-currency.model';

describe('Component Tests', () => {
  describe('BrokerCurrency Management Detail Component', () => {
    let comp: BrokerCurrencyDetailComponent;
    let fixture: ComponentFixture<BrokerCurrencyDetailComponent>;
    const route = ({ data: of({ brokerCurrency: new BrokerCurrency(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BrokerCurrencyDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BrokerCurrencyDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BrokerCurrencyDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.brokerCurrency).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
