/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BalanceEntityDetailComponent } from 'app/entities/balance-entity/balance-entity-detail.component';
import { BalanceEntity } from 'app/shared/model/balance-entity.model';

describe('Component Tests', () => {
  describe('BalanceEntity Management Detail Component', () => {
    let comp: BalanceEntityDetailComponent;
    let fixture: ComponentFixture<BalanceEntityDetailComponent>;
    const route = ({ data: of({ balanceEntity: new BalanceEntity(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BalanceEntityDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BalanceEntityDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BalanceEntityDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.balanceEntity).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
