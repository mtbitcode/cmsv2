/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Cmsv2TestModule } from '../../../test.module';
import { BalanceEntityUpdateComponent } from 'app/entities/balance-entity/balance-entity-update.component';
import { BalanceEntityService } from 'app/entities/balance-entity/balance-entity.service';
import { BalanceEntity } from 'app/shared/model/balance-entity.model';

describe('Component Tests', () => {
  describe('BalanceEntity Management Update Component', () => {
    let comp: BalanceEntityUpdateComponent;
    let fixture: ComponentFixture<BalanceEntityUpdateComponent>;
    let service: BalanceEntityService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Cmsv2TestModule],
        declarations: [BalanceEntityUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BalanceEntityUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BalanceEntityUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BalanceEntityService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BalanceEntity(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BalanceEntity();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
